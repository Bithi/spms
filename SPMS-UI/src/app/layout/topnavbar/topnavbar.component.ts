import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common'
declare var $ : any;
@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.scss']
})
export class TopnavbarComponent implements OnInit {
  pushRightClass: string = 'push-right';
  user: User;
  selectedTab = 0;
//   user : any = JSON.parse(localStorage.getItem('loggedinUser'));
//   userInfo : User;
  constructor(public router: Router,private location: Location) {

      this.router.events.subscribe(val => {
          if (
              val instanceof NavigationEnd &&
              window.innerWidth <= 992 &&
              this.isToggled()
          ) {
              this.toggleSidebar();
          }
      });
  }
  back(): void {
    this.location.back();
  }
  ngOnInit() {
    //   console.log(this.user);
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
   
  }

  isToggled(): boolean {
      const dom: Element = document.querySelector('body');
      return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle(this.pushRightClass);
  }

  rltAndLtr() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle('rtl');
  }

  onLoggedout() {
     localStorage.removeItem('isLoggedin');
     localStorage.removeItem('loggedinUser');
     localStorage.removeItem('roleWiseMenu');
     localStorage.removeItem('firstPage');
  }
    // alert("hi");
    onchangePassword() {
        this.router.navigate(['passwordReset']);
   }
   
    openContentOne() {
    this.selectedTab = 0;
}
   
}

