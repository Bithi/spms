import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { User } from 'src/app/models/user';
 import * as $ from 'jquery';
import * as AdminLte from 'admin-lte';
import { Menu } from 'src/app/models/menu';
import { MenuInfoService } from 'src/app/services/menu-info.service';
import { DataService } from 'src/app/data/data.service';
import { MenuItem } from 'primeng';
// declare var $: any;
@Component({
  selector: 'app-asidenavbar',
  templateUrl: './asidenavbar.component.html',
  styleUrls: ['./asidenavbar.component.scss']
})
export class AsidenavbarComponent implements OnInit {
  user: User;
  menu: Menu = null;
   items: MenuItem[];
  menuInfos: Menu[];
  clicked: boolean = false;
   menus =[];
  
  constructor(public router: Router,  private dataService: DataService,private menuInfoService: MenuInfoService) {

  }
  showMenu: string = '#';
  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    //debugger;
    // if(localStorage.getItem('roleWiseMenu')){
      this.menuInfos = JSON.parse(localStorage.getItem('roleWiseMenu'));
      // var r = this.menuInfos==null?this.convert(this.menuInfos):null;
      if(JSON.parse(localStorage.getItem('roleWiseMenu'))){
        var arrayToTree = require('array-to-tree');
        this.menus =arrayToTree(this.menuInfos, {
          parentProperty: 'parentMenuId',
          customID: 'm_id'
      });
      }
     
    //   this.items = [
    //     {
    //         label: 'File',
    //         icon: 'pi pi-pw pi-file',
    //         items: [{
    //                 label: 'New', 
    //                 icon: 'pi pi-fw pi-plus',
    //                 items: [
    //                     {label: 'User', icon: 'pi pi-fw pi-user-plus'},
    //                     {label: 'Filter', icon: 'pi pi-fw pi-filter'}
    //                 ]
    //             },
    //             {label: 'Open', icon: 'pi pi-fw pi-external-link'},
    //             {separator: true},
    //             {label: 'Quit', icon: 'pi pi-fw pi-times'}
    //         ]
    //     },
    //     {
    //         label: 'Edit',
    //         icon: 'pi pi-fw pi-pencil',
    //         items: [
    //             {label: 'Delete', icon: 'pi pi-fw pi-trash'},
    //             {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
    //         ]
    //     },
    //     {
    //         label: 'Help',
    //         icon: 'pi pi-fw pi-question',
    //         items: [
    //             {
    //                 label: 'Contents',
    //                 icon: 'pi pi-pi pi-bars'
    //             },
    //             {
    //                 label: 'Search', 
    //                 icon: 'pi pi-pi pi-search', 
    //                 items: [
    //                     {
    //                         label: 'Text', 
    //                         items: [
    //                             {
    //                                 label: 'Workspace'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         label: 'User',
    //                         icon: 'pi pi-fw pi-file',
    //                     }
    //             ]}
    //         ]
    //     },
    //     {
    //         label: 'Actions',
    //         icon: 'pi pi-fw pi-cog',
    //         items: [
    //             {
    //                 label: 'Edit',
    //                 icon: 'pi pi-fw pi-pencil',
    //                 items: [
    //                     {label: 'Save', icon: 'pi pi-fw pi-save'},
    //                     {label: 'Update', icon: 'pi pi-fw pi-save'},
    //                 ]
    //             },
    //             {
    //                 label: 'Other',
    //                 icon: 'pi pi-fw pi-tags',
    //                 items: [
    //                     {label: 'Delete', icon: 'pi pi-fw pi-minus'}
    //                 ]
    //             }
    //         ]
    //     }
    // ];
    // }
    

  // ngAfterViewInit() {
  //   $('[data-widget="treeview"]').each(function() {
  //       AdminLte.Treeview._jQueryInterface.call($(this), 'init');
  //   });
// }


    }

 convert(list){
  var arrayToTree = require('array-to-tree');
  this.menus =arrayToTree(list, {
    parentProperty: 'parentMenuId',
    customID: 'm_id'
  });
  }

 

}


