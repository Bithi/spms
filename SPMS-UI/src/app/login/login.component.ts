import { Component, OnDestroy, OnInit } from '@angular/core';
import{LoginService} from '../services/login.service';
import { DataService } from '../data/data.service';
import { GlobalVariable } from '../global/global';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user';
import {MessageService} from 'primeng/api';
import{Utility} from '../util/utility';
import { Subscription } from 'rxjs';
declare var $ : any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit  {
  private _subs: Subscription;

  constructor(
    public router: Router,
    private loginService: LoginService,
    private dataService: DataService,
    private http: HttpClient,
    private messageService: MessageService,
    private utility:Utility,
    
    ) 
    { 

    }

    User: User = new User();
    key:string = "toast";
    severity:string;
    detailMsg:string;
    // showProgressSpin:boolean;
    loadingMessage:string = "Loading...";
  ngOnInit() {
    this._subs = this.router.events.subscribe(event => {
      //Event must be handled here...
  });
     document.body.className = 'container';
        $('.js-tilt').tilt({
      scale: 1.1
  })
    
  }
  onLoggedin(){

    if(!this.User.userId||!this.User.password){
      this.severity = 'error';
      this.detailMsg = (this.User.userId?"Password":"Username")+" should not be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
  }
//   this.showProgressSpin = true;

  var dataHeader = this.dataService.createMessageHeader("USER_LOGIN");
  var jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(this.User)
  }
  debugger;
  this.loginService.getUserDetails(JSON.stringify(jsonMessage));

//   

  }

  userNameEmptyCheck(){

    if(!this.User.userId){
        this.detailMsg = "Username should not be empty";
        this.severity = 'error';
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }
    else if(!this.User.userId.trim()){
        this.detailMsg = "Username required";
        this.severity = 'error';
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }

}
passwordEmptyCheck(){
    if(!this.User.password){
        this.detailMsg = "Password should not be empty";
        this.severity = 'error';
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }

}
// ngOnDestroy(): void {
//   this._subs.unsubscribe();
// }

}