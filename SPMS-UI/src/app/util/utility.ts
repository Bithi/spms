import {Injectable} from '@angular/core';
import {MessageService} from 'primeng/api';

@Injectable({
  providedIn: 'root'
})


export class Utility {


  constructor(private messageService: MessageService ) {
  };
  
  ShowToast(key:string, severity:string, detailMsg:string){
        this.messageService.add({key:key, severity:severity, detail: detailMsg});
        // this.messageService.clear();
        
    }
   
  

}