import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { LoginService } from "../services/login.service";

@Injectable({
    providedIn: 'root'
  })
  export class LoginGuard implements CanActivate {
    constructor(public authService: LoginService, public router: Router){
  
    }
    canActivate(): boolean {
      if (this.authService.isAuthenticated()) {
        this.router.navigate(['dashboard']);
        return false;
      }
      return true;
    }
  }