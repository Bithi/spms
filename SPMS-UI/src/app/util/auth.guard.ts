
import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, Route,	Router,
    ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable()
export class AuthGuard implements CanActivate,CanLoad {
    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot) { 
         
        var roleWiseMenu = localStorage.getItem('roleWiseMenu');
      
        if (localStorage.getItem('isLoggedin')==="true") {

            if(this._isContains(JSON.parse(roleWiseMenu), route.data.path)){
                return true;
            }
            
          
        }
       else{

        this.router.navigate(['']);
        return false;
       }
    
}
    _isContains(json, value) {
        if(json!=null){
            for (var i=0;i<json.length;i++){
                if(json[i].url===value){
                    return true;
                }
                
            }
        }
        else{
            return false;
           }
       
      

       
     }
     canLoad(route: Route): boolean {
        let url: string = route.path;
        console.log('Url:'+ url);
        var roleWiseMenu = localStorage.getItem('roleWiseMenu');
      
        if (localStorage.getItem('isLoggedin')==="true") {

            if(this._isContains(JSON.parse(roleWiseMenu), url)){
                return true;
            }
            
          
        }
       else{

        this.router.navigate(['']);
        return false;
       }
      }
}