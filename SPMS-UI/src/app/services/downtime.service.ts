

import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Utility } from '../util/utility';
@Injectable({
  providedIn: 'root'
})
export class DowntimeService {
  str: string = "toast";
  constructor(private http: HttpClient, private dataService: DataService, private router: Router, private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;
  data: any[];

  getLastDowntimeRow(user: string){
    if(user){
      return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getLastDowntimeRow/'+user,
      
      { responseType: 'text'}
         )
      }
   
    
  }

  getDowntimeList(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/downtimeInfo', btoa(jsonMessage), { responseType: 'text' });
  }
  public deleteDowntime(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/downtimeInfo',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                //this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
           this.severity = 'success';
           this.detailMsg = "Deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }
}
