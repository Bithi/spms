import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Role } from '../models/role';
import { Itemtype } from '../models/itemtype';
import { SubItemType } from '../models/SubItemType';
import { Contbillchghead } from '../models/Contbillchghead';
import { Contractbilltax } from '../models/Contractbilltax';
import { Contractbillentry } from '../models/Contractbillentry';
import { Contyearwiserate } from '../models/Contyearwiserate';


@Injectable({
  providedIn: 'root'
})
export class ContyearwiserateService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
    public contRate:Contyearwiserate = new Contyearwiserate();

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}



      public insertContyearwiserate(jsonMessage) {
      
        return this.http.post(GlobalVariable.BASE_API_URL + '/v1/contyearwiserate',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
                
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    localStorage.removeItem('isLoggedin');
                    return;
                }
                this.severity = 'success';
                this.detailMsg = "Inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }


    public showUserInfo(jsonMessage){
      return this.http.post(GlobalVariable.BASE_API_URL+'/v1/contyearwiserate',
         btoa(jsonMessage)
     , { responseType: 'text'}
         )
   }


   public updateContyearwiseRate(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/contyearwiserate',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  return;
              }
              this.severity = 'success';
              this.detailMsg = "Information updated Successfully";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }


  public deleteContyearwiseRate(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/contyearwiserate',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                return;
            }
           this.severity = 'success';
           this.detailMsg = "Information deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }


  passTheValue(contRate){
    var userData = JSON.parse(JSON.stringify(contRate));
    userData.start_date = new Date(userData.start_date.substring(0,10)).toJSON().slice(0, 10);
    userData.end_date =  new Date(userData.end_date.substring(0,10)).toJSON().slice(0, 10);
    this.addToTheList.next(userData);
  }  


  getAllContForRate(){
    return this.http.get(`${this.apiBaseUrl}/v1/contforrate`,
    
 { responseType: 'text'}
    )
}


getContYearWiseRate(){
  return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getContYearWiseRate'
 , { responseType: 'text'}
     )


// var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
// var jsonMessage = {
//   dataHeader : msgHeader,
//   payLoad: new Array()
// };

// return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);

}

  

}