import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { BudgetMaster } from '../models/BudgetMaster';
//import { BudgetbillentryinsertComponent } from '../pages/budgetbillentry/budgetbillentryinsert/budgetbillentryinsert.component';
declare var $: any;


@Injectable({
  providedIn: 'root'
})
export class BudgetMasterEntryService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
  public masterEntry: BudgetMaster = new BudgetMaster();

  key: string = "toast";
  severity: string;
  detailMsg: string;

  
  ones = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine','Ten'];
  teens = ['Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
  tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

  wording: string;

  constructor(
    private http: HttpClient,
    public router: Router,
    private dataService: DataService
    //,private budgetBillEntryInsertComponent : BudgetbillentryinsertComponent
    ,private utility: Utility) { }



  public insertBudgetMasterEntry(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetMasterEntry',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));


        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
          this.severity = 'error';
          this.detailMsg = "There is something wrong";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "Budget Master entry inforamtion inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }

  public updateBudgetMasterInfo(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetMasterEntry',
      btoa(jsonMessage)
      , { responseType: 'text' }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Budget Master information updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);

        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }
  public deleteBudgetMasterInfo(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetMasterEntry',
      btoa(jsonMessage)
      , { responseType: 'text' }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Budget Master information deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }

  passTheValue(budgetMasterInfo) {
    var userData = JSON.parse(JSON.stringify(budgetMasterInfo));
debugger;
    console.log("Data\n"+JSON.stringify(userData));


    userData.amountAllocationDate ?
      userData.amountAllocationDate = new Date(userData.amountAllocationDate.substring(0, 10)).toJSON().slice(0, 10) : userData.amountAllocationDate;

   
     
    if (userData.amount == 0 || !userData.amount) {
      //console.log("\nzero or blank...\n");
      this.wording = "";
    }
    else {
      userData.amount = parseInt(userData.amount, 10);
      this.wording = this.convertGreaterThanThousand(userData.amount) + " Only."
    }
    userData.amount_in_words = this.wording;


    userData.isEdit = true;

    console.log("uuu\n"+JSON.stringify(userData));
   
    this.addToTheList.next(userData);
  }

  getBudgetMasterList() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getBudgetMasterList'
      , { responseType: 'text' }
    )
  }



  convertLessThanThousand(num: number): string {
    if (num === 0) {
      return '';
    } else if (num <= 10) {
      return this.ones[num];
    } else if (num < 20) {
      return this.teens[num - 11];
    } else if (num < 100) {
      return this.tens[Math.floor(num / 10)] + ' ' + this.convertLessThanThousand(num % 10);
    } else {
      return this.ones[Math.floor(num / 100)] + ' Hundred ' + this.convertLessThanThousand(num % 100);
    }
  }

  convertGreaterThanThousand(num: number): string {
    if (num < 1000) {
      return this.convertLessThanThousand(num);
    } else if (num < 100000) {
      return this.convertLessThanThousand(Math.floor(num / 1000)) + ' Thousand ' + this.convertLessThanThousand(num % 1000);
    } else if (num < 10000000) {
      return this.convertLessThanThousand(Math.floor(num / 100000)) + ' Lakh ' + this.convertGreaterThanThousand(num % 100000);
    } else {
      return this.convertLessThanThousand(Math.floor(num / 10000000)) + ' Crore ' + this.convertGreaterThanThousand(num % 10000000);
    }
  }

  convertMillisToWords(milliseconds) {
    const seconds = Math.floor(milliseconds / 1000);
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30.44); // Approximate number of days in a month
    const years = Math.floor(months / 12);

    const result = [];

    if (years > 0) {
      result.push(years + (years === 1 ? ' year' : ' years'));
    }
    if (months > 0) {
      result.push(months % 12 + (months % 12 === 1 ? ' month' : ' months'));
    }
    if (days > 0) {
      result.push(days % 30 + (days % 30 === 1 ? ' day' : ' days'));
    }

    return result.join(', ');
  }

  	
	
  public getTotalBillByContract(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/getBillAmountByContract',
    btoa(jsonMessage)
    , { responseType: 'text' }
  )
  }

  getBillEntry(ref: string) {
       
    if(ref){
    return this.http.get(`${this.apiBaseUrl}/v1/getBudgetBillEntry/${ref}`,
    
    { responseType: 'text'}
       )
    }
  };
}