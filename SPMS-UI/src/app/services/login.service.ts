import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Router } from '@angular/router';
import {MessageService} from 'primeng/api';
import{Utility} from '../util/utility';
import { Menu } from '../models/menu';
import { MenuInfoService } from './menu-info.service';
import { DataService } from '../data/data.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http:HttpClient,
    public router: Router,
    private messageService: MessageService,
    private menuInfoService: MenuInfoService,
    private dataService: DataService,
    private utility:Utility) 
    { }
  
    key:string = "toast";
    severity:string;
    detailMsg:string;
    menu: Menu = null;
    user: User;
    menuInfos: Menu[];
    clicked: boolean = false;
     menus =[];


  getUserDetails(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/login',
      btoa(jsonMessage)
  , { responseType: 'text'}
      )
  .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              debugger;
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                  this.severity = 'error';
                  this.detailMsg = "Invalid Credentials";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  localStorage.removeItem('isLoggedin');
                  localStorage.removeItem('loggedinUser');
                  // this.showProgressSpin = false;
                  return;
              }
              
              localStorage.setItem('isLoggedin', 'true');
              localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
              // this.showProgressSpin = false;
              if(msg.payLoad[0].message==='expired'){
                localStorage.removeItem('roleWiseMenu');
                this.router.navigate(['passwordReset']);
              }
              else if(msg.payLoad[0].message==='locked'){
                this.severity = 'error';
                this.detailMsg = "Account is locked";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                localStorage.removeItem('isLoggedin');
                localStorage.removeItem('loggedinUser');
              }
              else if(msg.payLoad[0].message==='inactive'){
                this.severity = 'error';
                this.detailMsg = "Account is inactive";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                localStorage.removeItem('isLoggedin');
                localStorage.removeItem('loggedinUser');
              }
             else{
              
              this.getMenus();
              
              // if(localStorage.getItem('firstPage')){
              //   this.router.navigate(['/'+localStorage.getItem('firstPage')]);
              // }
              
              // else{
               
                // this.router.navigate(['/default']);
            //  }
             }
              
              
              
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
    }


    getMenus(){
      var dataHeader = this.dataService.createMessageHeader("SHOW_MENU");
      this.user = JSON.parse(localStorage.getItem('loggedinUser'));
        var jsonMessage = {
          dataHeader: dataHeader,
          payLoad: new Array(this.user)
        }
    
        const request = this.menuInfoService.showMenuByUser(JSON.stringify(jsonMessage));
        request.subscribe(
          (res) => {
            const msg = JSON.parse(atob(res));
            // this.menuInfos = msg.payLoad;
            // if (msg.payLoad.length>0)
            localStorage.setItem('roleWiseMenu', JSON.stringify(msg.payLoad));
            localStorage.setItem('firstPage', msg.payLoad[0].url);
            this.router.navigate(['/'+localStorage.getItem('firstPage')])
            .then(() => {
               window.location.reload();
               });
            // var r = this.convert(this.menuInfos)
            if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {
    
    
              
              return;
            }
           
    
    
    
          },
          err => {
            
            return;
          }
        );
    };
    
    public isAuthenticated(): boolean {
      const loggedinUser = localStorage.getItem('loggedinUser');
      if (loggedinUser && loggedinUser.length > 0) {
        return true;
      } else {
        return false;
      }
    }
    //  convert(list){
      
    //   var arrayToTree = require('array-to-tree');
    //   this.menus =arrayToTree(list, {
    //     parentProperty: 'parentMenuId',
    //     customID: 'm_id'
    //   });
    //   console.log(this.menus );
    //   }

  }

