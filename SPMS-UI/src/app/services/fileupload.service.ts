import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { GlobalVariable } from '../global/global';
import { FileUp } from '../models/fileUp';
declare var $ : any;
@Injectable({
  providedIn: 'root'
})
export class FileuploadService { str:string = "toast";
constructor(private http: HttpClient, private dataService: DataService,private router: Router, private utility:Utility) { }
key:string = "toast";
severity:string;
detailMsg:string;
data: any[];
fileup:FileUp = new FileUp;

upload(data,fileUp:FileUp){
  var utilityLocal= this.utility;
  var keyvar = this.key;
  var formFields = document.getElementById('fileUploadForm');
  debugger;
  if(!formFields['files'].files.length){
    this.severity = 'error';
    this.detailMsg = 'please, upload a file';
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
  }
  else{
    data.append('data',JSON.stringify(fileUp));

    $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: GlobalVariable.BASE_API_URL+'/v1/fileUpload',
      data: data,
      processData: false, //prevent jQuery from automatically transforming the data into a query string
      contentType: false,
      cache: false,
      // timeout: 600000,
      success: function (data) {
          this.severity = data[0];
          this.detailMsg = data[1];
          utilityLocal.ShowToast(keyvar,this.severity,this.detailMsg);
          // $("#myForm")[0].reset();
          

      },
      error: function (e) {
          this.severity = 'error';
          this.detailMsg = e;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          // $("#myForm")[0].reset();

      }
  });
  }
}

public fileInfo(jsonMessage){
  return this.http.post(GlobalVariable.BASE_API_URL+'/v1/fileUploadList',
     btoa(jsonMessage)
 , { responseType: 'text'}
     )
}

download(fileLoc){

  var fileName =  fileLoc;//.substring(fileLoc.lastIndexOf("-") + 1);
  debugger;
  var request = new XMLHttpRequest();
  var url = GlobalVariable.BASE_API_URL+"/v1/file/"+fileLoc;
 
  request.open('POST', url, true);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  request.responseType = 'blob';
  // var fileName = "AMC_Tables_NEW.docx";
  request.onload = function(e) {
      if (this.status === 200) {
          var blob = this.response;
          if(window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveBlob(blob, fileName);
          }
          else{
              var downloadLink = window.document.createElement('a');
              var contentTypeHeader = request.getResponseHeader("Content-Type");
              downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
              downloadLink.download = fileName;
              document.body.appendChild(downloadLink);
              downloadLink.click();
              document.body.removeChild(downloadLink);
             }
         }
     };
     request.send();
    }


    public delete(jsonMessage){
      this.http.post(GlobalVariable.BASE_API_URL+'/v1/deleteFile',
      btoa(jsonMessage)
      , { responseType: 'text'}
      )
      .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  //this.showProgressSpin = false;
                  return;
              }
              // this.showProgressSpin = false;
              // debugger;
             this.severity = msg.dataHeader.reponseStatus;
             if(this.severity==='success'){
              this.detailMsg =  'Deleted Successfully';
             }
             else{
              this.detailMsg =  msg.dataHeader.description;
             }
             
             this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
           // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
      );
      }


      getFileUploadList(){
        return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getFileUploadWithContractInfoJoin'
       , { responseType: 'text'}
           )
      
    
      // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
      // var jsonMessage = {
      //   dataHeader : msgHeader,
      //   payLoad: new Array()
      // };
      
      // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);
      
    }

}

    



