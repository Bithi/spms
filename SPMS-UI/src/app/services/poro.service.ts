import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalVariable } from '../global/global';
import { Poro } from '../models/poro';
import {Subject} from 'rxjs';
import { Utility } from '../util/utility';
import { DataService } from '../data/data.service';

@Injectable({
  providedIn: 'root'
})
export class PoroService {
  public addToTheList = new Subject<any>();
  //private baseUrl =GlobalVariable.BASE_API_URL+ '/v1/getPoroInfo';
  
  constructor(private http: HttpClient,
    private dataService: DataService,
    private utility:Utility) { }
  key:string = "toast";
  severity:string;
  detailMsg:string;

  // getPorosList(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}`);
  //   location.reload();
  // }





  getPoros(){

    var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
    var jsonMessage = {
      dataHeader : msgHeader,
      payLoad: new Array()
    };
    
    return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/poroInfo',jsonMessage);
    
  }



  

   passTheValue(poro){
          this.addToTheList.next(poro);
   }
   public updatePoroInfo(jsonMessage){
             debugger;
             this.http.post(GlobalVariable.BASE_API_URL+'/v1/poroInfo',
             btoa(jsonMessage)
         , { responseType: 'text'}
             )
             .subscribe(
               (res ) => {
                       const msg = JSON.parse( atob(res) );
                       console.log(res);
                       
                       if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                            this.severity = 'error';
                            this.detailMsg = "Error occured";
                            this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                            localStorage.removeItem('isLoggedin');
                           //this.showProgressSpin = false;
                           return;
                       }
                       // this.showProgressSpin = false;
                      this.severity = 'success';
                      this.detailMsg = "PORO saved Successfully";
                      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       location.reload();
                       
                       
                   
               },
               err => {
                    this.severity = 'error';
                    this.detailMsg = "Server Error: " + err.message;
                    // this.showProgressSpin = false;
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   return;
               }
          );
           }

  public getSinglePoro(jsonMessage){

    return this.http.post(GlobalVariable.BASE_API_URL+'/v1/updatePoroInfo',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )

  }
  

  public deletePoroInfo(jsonMessage){
    debugger;
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/poroInfo',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  //this.showProgressSpin = false;
                  return;
              }
              // this.showProgressSpin = false;
             // this.severity = 'success';
             // this.detailMsg = "User Info saved Successfully";
             // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              location.reload();
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
           // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }
 

  public insertPoroInfo(jsonMessage){
    
    return this.http.post(GlobalVariable.BASE_API_URL+'/v1/poroInfo',
        btoa(jsonMessage)
    , { responseType: 'text'}
        ) .subscribe(
          (res ) => {
       
                  const msg = JSON.parse( atob(res) );
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                      // this.severity = 'error';
                      // this.detailMsg = "Invalid Cridential";
                      // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                      // localStorage.removeItem('isLoggedin');
                      // // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  localStorage.setItem('isLoggedin', 'true');
                  localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
                  // this.router.navigate(['/dashboard']);
                  
                  location.reload();
              
          },
          err => {
              // this.severity = 'error';
              // this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
              // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
  }

}
