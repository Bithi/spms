import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  GlobalVariable
} from '../global/global';
import {
  Utility
} from '../util/utility';
import {
  Subject
} from 'rxjs';
import {
  Observable
} from 'rxjs';
import {
  AmcNotification
} from '../models/AmcNotification';
declare var $: any;
@Injectable({
  providedIn: 'root'
})

export class AmcNotificationService {
  public amcNotification: AmcNotification = new AmcNotification();

  public addToTheList = new Subject<any>();
  private baseUrl = GlobalVariable.BASE_API_URL + '/v1/getNotifications';

  constructor(private http: HttpClient,
    private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;
  apiBaseUrl = GlobalVariable.BASE_API_URL;
  getNotificationList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  passTheValue(amcNotification) {
    var userData = JSON.parse(JSON.stringify(amcNotification));
    userData.lastNotifDate = new Date(userData.lastNotifDate.substring(0,10)).toJSON().slice(0, 10);
    this.addToTheList.next(userData);
  }
  public updateNotification(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/notification',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "AMC Notification Updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public deleteNotification(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/notification',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "AMC Notification deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public insertNotification(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/notification',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {
          this.severity = 'error';
          this.detailMsg = "Error occured";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "AMC Notification inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }
  getcontAmcJoin(){
    return this.http.get(`${this.apiBaseUrl}/v1/getcontAmcJoin`,
    
  { responseType: 'text'}
    )
  }
}