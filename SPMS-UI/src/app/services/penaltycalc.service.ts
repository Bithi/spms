import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Utility } from '../util/utility';
@Injectable({
  providedIn: 'root'
})
export class PenaltycalcService {
    str: string = "toast";
    constructor(private http: HttpClient, private dataService: DataService, private router: Router, private utility: Utility) { }
    key: string = "toast";
    severity: string;
    detailMsg: string;
    data: any[];


    getPenaltyCalcList(jsonMessage) {
      
      return this.http.post(GlobalVariable.BASE_API_URL + '/v1/penaltycalc', btoa(jsonMessage), { responseType: 'text' });
    }
  }
  