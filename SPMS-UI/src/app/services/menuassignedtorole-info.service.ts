import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Role } from '../models/role';
import { Menu } from '../models/menu';
import { MenuAssigndedToRole } from '../models/menuassignedtorole';


@Injectable({
  providedIn: 'root'
})
export class MenuAssignedToRoleService {

  public addToTheList = new Subject<any>();

  apiBaseUrl = GlobalVariable.BASE_API_URL;

    public menuAssigndedToRole:MenuAssigndedToRole = new MenuAssigndedToRole();

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}


      public insertMenuAssignedToRole(jsonMessage) {
        return this.http.post(GlobalVariable.BASE_API_URL + '/v1/insertMenuAssignedToRole',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong.";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    localStorage.removeItem('isLoggedin');
                    // // this.showProgressSpin = false;
                    return;
                }
                // this.showProgressSpin = false;
                //localStorage.setItem('isLoggedin', 'true');
                //localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
                // this.router.navigate(['/dashboard']);
                this.severity = 'success';
                this.detailMsg = "Insertion Successful";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }

    getMenuList(){
      // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
      //     catchError(this.handleError)
      // );
      return this.http.get(`${this.apiBaseUrl}/v1/menuList`,
      
   { responseType: 'text'}
      )
  };

  getMenus(){
    // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
    //     catchError(this.handleError)
    // );
    return this.http.get(`${this.apiBaseUrl}/v1/menus`,
    
 { responseType: 'text'}
    )
};

  getMyRoleList(){
    // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
    //     catchError(this.handleError)
    // );
    return this.http.get(`${this.apiBaseUrl}/v1/roles`,
    
 { responseType: 'text'}
    )
};

public showUserInfo(jsonMessage){
  return this.http.post(GlobalVariable.BASE_API_URL+'/v1/insertMenuAssignedToRole',
     btoa(jsonMessage)
 , { responseType: 'text'}
     )
}

public menuAssignedToRoleJoin(){
  return this.http.get(GlobalVariable.BASE_API_URL+'/v1/menuAssignedToRoleJoin'
 , { responseType: 'text'}
     )
}



passTheValue(menuAssigendToRole){
  var userData = JSON.parse(JSON.stringify(menuAssigendToRole));
  this.addToTheList.next(userData);
}

public updateMenuAssigendToRoleInfo(jsonMessage){
  this.http.post(GlobalVariable.BASE_API_URL+'/v1/insertMenuAssignedToRole',
  btoa(jsonMessage)
, { responseType: 'text'}
  )
  .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                // this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
            this.severity = 'success';
            this.detailMsg = "Updated Successfully";
            this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            
            
        
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);
}

public deleteMenuAssignedToRoleInfo(jsonMessage){
  this.http.post(GlobalVariable.BASE_API_URL+'/v1/insertMenuAssignedToRole',
  btoa(jsonMessage)
  , { responseType: 'text'}
  )
  .subscribe(
  (res ) => {
          const msg = JSON.parse( atob(res) );
          console.log(res);
          
          if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
               this.severity = 'error';
               this.detailMsg = "Error occured";
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
               localStorage.removeItem('isLoggedin');
              //this.showProgressSpin = false;
              return;
          }
          // this.showProgressSpin = false;
         this.severity = 'success';
         this.detailMsg = "Deleted Successfully";
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
  },
  err => {
       this.severity = 'error';
       this.detailMsg = "Server Error: " + err.message;
       // this.showProgressSpin = false;
       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
  }
  );
  }

}