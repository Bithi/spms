import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Role } from '../models/role';
import { Itemtype } from '../models/itemtype';
import { SubItemType } from '../models/SubItemType';
import { Brand } from '../models/brand';
import { Model } from '../models/model';


@Injectable({
  providedIn: 'root'
})
export class ModelService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
    public modelInfo:Model = new Model();

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}



      public insertModel(jsonMessage) {
      
        return this.http.post(GlobalVariable.BASE_API_URL + '/v1/model',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
                const msg = JSON.parse(atob(res));
                debugger;
                
                if(msg.dataHeader.reponseStatus=="duplicate"){
                  this.severity = 'error';
                  this.detailMsg = "Model Name with Brand must be unique";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  // // this.showProgressSpin = false;
                  return;
                }
                else if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    localStorage.removeItem('isLoggedin');
                    // // this.showProgressSpin = false;
                    return;
                }
                // this.showProgressSpin = false;
                //localStorage.setItem('isLoggedin', 'true');
                //localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
                // this.router.navigate(['/dashboard']);
                this.severity = 'success';
                this.detailMsg = "Model information inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }


    public showModel(jsonMessage){
      return this.http.post(GlobalVariable.BASE_API_URL+'/v1/model',
         btoa(jsonMessage)
     , { responseType: 'text'}
         )
   }


   public updateModel(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/model',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              if(msg.dataHeader.reponseStatus=="duplicate"){
                this.severity = 'error';
                this.detailMsg = "Model Name with Brand must be unique";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                // // this.showProgressSpin = false;
                return;
              }
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  // this.showProgressSpin = false;
                  return;
              }
              // this.showProgressSpin = false;
              this.severity = 'success';
              this.detailMsg = "Model information updated Successfully";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }


  public deleteModel(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/model',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                //this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
           this.severity = 'success';
           this.detailMsg = "Model information deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }


  passTheValue(modelInfo){
    debugger;
    var userData = JSON.parse(JSON.stringify(modelInfo));
    this.addToTheList.next(userData);
  }


  getAllBrand(){
    return this.http.get(`${this.apiBaseUrl}/v1/BrandList`,
    
 { responseType: 'text'}
    )
}
getModelBrandJoin(){
  return this.http.get(`${this.apiBaseUrl}/v1/getmodelBrandJoin`,
  
{ responseType: 'text'}
  )
}

getDuplicateEntryByModelNameWithBrand(jsonMessage){
  debugger;
  return this.http.post(`${this.apiBaseUrl}/v1/getDuplicateEntryByModelNameWithBrand/`,
  jsonMessage
  ,
{ responseType: 'text'}
  )
}

}