import {
  Injectable
} from '@angular/core';
import {
  DataService
} from '../data/data.service';
import {
  Router
} from '@angular/router';
import {
  HttpClient
} from '@angular/common/http';
import {
  GlobalVariable
} from '../global/global';
import {
  Utility
} from '../util/utility';
import {
  Subject
} from 'rxjs';
import {
  Observable
} from 'rxjs';
import {
  ContractInfo
} from 'src/app/models/contractinfo';
declare var $: any;
@Injectable({
  providedIn: 'root'
})

export class ContractInfoService {
  public conInfo: ContractInfo = new ContractInfo();

  public addToTheList = new Subject<any>();
  private baseUrl = GlobalVariable.BASE_API_URL + '/v1/contractInfo';
  apiBaseUrl = GlobalVariable.BASE_API_URL;
  constructor(private http: HttpClient,
    private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;

  getList(jsonMessage): Observable<any> {
    return this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
    );
  }

  getContractInfoList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  public contractInfoJoin() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getcontractInfoJoin'
      , { responseType: 'text' }
    )
  }

  passTheValue(ContractInfo) {
    var userData = JSON.parse(JSON.stringify(ContractInfo));

    userData.contValidStart = new Date(userData.contValidStart.substring(0, 10)).toJSON().slice(0, 10);
    userData.contValidEnd = new Date(userData.contValidEnd.substring(0, 10)).toJSON().slice(0, 10);
    userData.tenderDt = new Date(userData.tenderDt.substring(0, 10)).toJSON().slice(0, 10);
    userData.commDate = new Date(userData.commDate.substring(0, 10)).toJSON().slice(0, 10);
    userData.contDate = new Date(userData.contDate.substring(0, 10)).toJSON().slice(0, 10);
   
    userData.performReportDt ?
      userData.performReportDt = new Date(userData.performReportDt.substring(0, 10)).toJSON().slice(0, 10) : userData.performReportDt;
    userData.inspectReportDt ?
      userData.inspectReportDt = new Date(userData.inspectReportDt.substring(0, 10)).toJSON().slice(0, 10) : userData.inspectReportDt;
    
    if (userData.status == 'Active') {
      userData.status = 'ACT';
    }
    else if (userData.status == 'Inactive') {
      userData.status = 'CAN';
    }
    else if (userData.status == 'Closed') {
      userData.status = 'CLS';
    }
    // userData.status = userData.status=='active'?'ACT':'CAN';

    // userData.vendor = vendorNameSet.get(contractInfo.venId);
    this.addToTheList.next(userData);

  }


  public updateContractInfo(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractInfo',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            //this.showProgressSpin = false;
            return;
          }
          // this.showProgressSpin = false;
          this.severity = 'success';
          this.detailMsg = "Contract Info Updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }

  public closeContractInfo(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractInfo',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            //this.showProgressSpin = false;
            return;
          }
          // this.showProgressSpin = false;
          this.severity = 'success';
          this.detailMsg = "Contract Info Closed Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }

  public renewContractInfo(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractInfo',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            //this.showProgressSpin = false;
            return;
          }
          // this.showProgressSpin = false;
          this.severity = 'success';
          this.detailMsg = "Contract Info Renew Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }

  public deleteContractInfo(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractInfo',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            //this.showProgressSpin = false;
            return;
          }
          // this.showProgressSpin = false;
          this.severity = 'success';
          this.detailMsg = "Contract Info deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public insertContractInfo(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractInfo',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
          this.severity = 'error';
          this.detailMsg = "Error Occured";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          // localStorage.removeItem('isLoggedin');
          // // this.showProgressSpin = false;
          return;
        }
        // this.showProgressSpin = false;
        //localStorage.setItem('isLoggedin', 'true');
        //localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
        // this.router.navigate(['/dashboard']);
        this.severity = 'success';
        this.detailMsg = "Contract Info inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }
  upload(data, con: ContractInfo) {
    var utilityLocal = this.utility;
    var keyvar = this.key;
    var formFields = document.getElementById('fileUploadForm');
    var data2 = data.getAll('contractInformation');

    if (!formFields['files'].files.length) {
      this.severity = 'error';
      this.detailMsg = 'please, upload a file';
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    }
    else {
      data.append('data', JSON.stringify(con));
      $('#contractInfoUploadSpinner').show();
      $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: GlobalVariable.BASE_API_URL + '/v1/uploadContractInfoFile',
        data: data,
        processData: false, //prevent jQuery from automatically transforming the data into a query string
        contentType: false,
        cache: false,
        // timeout: 600000,
        success: function (data) {
          $("#result").text(data);
          $('#contractInfoUploadSpinner').hide();
          this.severity = data[0];
          this.detailMsg = data[1];
          utilityLocal.ShowToast(keyvar, this.severity, this.detailMsg);
          $("#btnSubmit").prop("disabled", false);
          $("#myForm")[0].reset();


        },
        error: function (e) {

          $("#result").text(e.responseText);
          $('#contractInfoUploadSpinner').hide();
          this.severity = 'error';
          this.detailMsg = e;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          $("#btnSubmit").prop("disabled", false);
          $("#myForm")[0].reset();

        }
      });

    }

  }

  getChildBycon() {
    return this.http.get(`${this.apiBaseUrl}/v1/childBycon`,

      { responseType: 'text' }
    )


  }
}






