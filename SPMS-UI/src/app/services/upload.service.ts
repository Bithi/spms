import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Router } from '@angular/router';
import {MessageService} from 'primeng/api';
import{Utility} from '../util/utility';
import { DowntimeService } from './downtime.service';
import { Downtime } from '../models/downtime';
declare var $ : any;
@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    private http:HttpClient,
    public router: Router,
    private messageService: MessageService,
    private utility:Utility) 
    { }
    key:string = "toast";
    severity:string;
    detailMsg:string;
    public downtime:Downtime = new Downtime();
   



  upload(data){

    var utilityLocal= this.utility;
    var keyvar = this.key;
    var formFields = document.getElementById('fileUploadForm');

    var today = new Date();
    var date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();
    this.downtime.entryDate = new Date(date);
    this.downtime.entryUser = JSON.parse(localStorage.getItem('loggedinUser')).userId; 
    this.downtime.updateDt = new Date('01/01/1970');   
    this.downtime.updateUser = "";

    if(!formFields['files'].files.length){
      this.severity = 'error';
      this.detailMsg = 'please, upload a file';
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }
    else{
      data.append('data',JSON.stringify(this.downtime));
      $('#downtimeUploadSpinner').show();
      $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: GlobalVariable.BASE_API_URL+'/v1/uploadFile',
        data: data,
        processData: false, //prevent jQuery from automatically transforming the data into a query string
        contentType: false,
        cache: false,
        // timeout: 600000,
        success: function (data) {
            $("#result").text(data);
            $('#downtimeUploadSpinner').hide();
            console.log("SUCCESS : ", data);
            this.severity = data[0];
            this.detailMsg = data[1];
            utilityLocal.ShowToast(keyvar,this.severity,this.detailMsg);
            $("#btnSubmit").prop("disabled", false);
            $("#fileUploadForm")[0].reset();
            $('.custom-file-label').html('Choose file');
  
        },
        error: function (e) {
  
            $("#result").text(e.responseText);
            $('#downtimeUploadSpinner').hide();
            console.log("ERROR : ", e);
            this.severity = 'error';
            this.detailMsg = e;
            this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            $("#btnSubmit").prop("disabled", false);
            $("#fileUploadForm")[0].reset();
  
        }
    });
    
    
  
      
    }
  }

  }

