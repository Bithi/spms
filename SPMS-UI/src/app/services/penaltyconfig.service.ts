import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Utility } from '../util/utility';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { PenaltyConfig } from 'src/app/models/penaltyconfig';
import { DropdownInfoService } from './dropdown-info.service';
import { SelectItem } from 'primeng/api/selectitem';

@Injectable({
  providedIn: 'root'
})
export class PenaltyConfigService{
  private subject = new Subject<any>();
    public addToTheList = new Subject<any>();
    private baseUrl =GlobalVariable.BASE_API_URL+ '/v1/getPenaltyConfigInfo';
    selectedcontract:SelectItem[];
    constructor(private http: HttpClient,
      private utility:Utility,
      private dataService: DataService,
      private dropdownservice:DropdownInfoService,
      //  private pconInsert: PenaltyConfigInsertComponent
      ) { }
    key:string = "toast";
    severity:string;
    detailMsg:string;

    // getPenaltyConfigList(): Observable<any> {
    //     return this.http.get(`${this.baseUrl}`);
    //   }




      showPenaltyConfig(){
        
          return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getPenaltyConfigJoin'
         , { responseType: 'text'}
             )
        // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
        // var jsonMessage = {
        //   dataHeader : msgHeader,
        //   payLoad: new Array()
        // };
        
        // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/penaltyConfig',jsonMessage);
        
      }




      
   passTheValue(PenaltyConfig){
       // var a = PenaltyConfig;
       //this.addToTheList.next(PenaltyConfig);
      //  debugger;
     var userData = JSON.parse(JSON.stringify(PenaltyConfig));
     if(userData.maxDowntime==-1){
      userData.maxDowntime=null;
     }
     if(userData.penaltyRate==-1){
      userData.penaltyRate=null;
     }
       this.addToTheList.next(userData);
        
}
public updatePenaltyConfigInfo(jsonMessage){
       this.http.post(GlobalVariable.BASE_API_URL+'/v1/penaltyConfig',
       btoa(jsonMessage)
   , { responseType: 'text'}
       )
       .subscribe(
         (res ) => {
                 const msg = JSON.parse( atob(res) );
                 
                 if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                      this.severity = 'error';
                      this.detailMsg = "Error occured";
                      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                      localStorage.removeItem('isLoggedin');
                     //this.showProgressSpin = false;
                     return;
                 }
                 // this.showProgressSpin = false;
                this.severity = 'success';
                this.detailMsg = "Penalty configuration updated Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 
                 
             
         },
         err => {
              this.severity = 'error';
              this.detailMsg = "Server Error: " + err.message;
              // this.showProgressSpin = false;
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
             return;
         }
    );
     }


public deletePenaltyConfigInfo(jsonMessage){


this.http.post(GlobalVariable.BASE_API_URL+'/v1/penaltyConfig',
btoa(jsonMessage)
, { responseType: 'text'}
)
.subscribe(
(res ) => {
        const msg = JSON.parse( atob(res) );
        console.log(res);
        
        if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
             this.severity = 'error';
             this.detailMsg = "Error occured";
             this.utility.ShowToast(this.key,this.severity,this.detailMsg);
             localStorage.removeItem('isLoggedin');
            //this.showProgressSpin = false;
            return;
        }
        // this.showProgressSpin = false;
       this.severity = 'success';
       this.detailMsg = "Penalty configuration deleted Successfully";
       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        
        
    
},
err => {
     this.severity = 'error';
     this.detailMsg = "Server Error: " + err.message;
     // this.showProgressSpin = false;
     this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
}
);
}


public insertPenaltyConfigInfo(jsonMessage){

return this.http.post(GlobalVariable.BASE_API_URL+'/v1/penaltyConfig',
  btoa(jsonMessage)
, { responseType: 'text'}
  ) .subscribe(
    (res ) => {
 
            const msg = JSON.parse( atob(res) );
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
              this.severity = 'error';
              this.detailMsg = "Error Occured";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              localStorage.removeItem('isLoggedin');
             // // this.showProgressSpin = false;
             return;
         }
            // this.showProgressSpin = false;
            //localStorage.setItem('isLoggedin', 'true');
            //localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
            this.severity = 'success';
            this.detailMsg = "Penalty configuration inserted Successfully";
            this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            // this.router.navigate(['/dashboard']);
            
        
    },
    err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);
}


getContractList(penCOnfig: PenaltyConfig){
  //console.log(venId);
  
    // this.dropdownInfoService.getContracts(this.penaltyCalcRep.venId).subscribe(
    //   data => this.contractinfos = data
    // );
    this.selectedcontract = [];
    //this.selectedcontract.push({ label: 'Select Contract Number', value: -1});

    if(penCOnfig.vendorId){
    const request = this.dropdownservice.getContracts(penCOnfig.vendorId);
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              // console.log(msg);
              for(let i = 0; i< msg.length; i++) {
               
                this.selectedcontract.push({label: msg[i].contNo, value: msg[i].contId});
                // console.log( this.vendorList);
           }

           this.passTheValue(penCOnfig);
      }
 );
  }
  else{
    // this.contractinfos=null;
    // this.contractbranches=null;
  }
}

}