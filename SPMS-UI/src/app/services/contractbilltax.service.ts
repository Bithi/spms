import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Role } from '../models/role';
import { Itemtype } from '../models/itemtype';
import { SubItemType } from '../models/SubItemType';
import { Contbillchghead } from '../models/Contbillchghead';
import { Contractbilltax } from '../models/Contractbilltax';


@Injectable({
  providedIn: 'root'
})
export class ContractbilltaxService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
    public contTax:Contractbilltax = new Contractbilltax();

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}



      public insertContractBillTax(jsonMessage) {
      
        return this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractbilltax',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
                
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    return;
                }
                this.severity = 'success';
                this.detailMsg = "Contract bill tax inforamtion inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }


    public showUserInfo(jsonMessage){
      return this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractbilltax',
         btoa(jsonMessage)
     , { responseType: 'text'}
         )
   }


   public updateContractBillTax(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractbilltax',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  return;
              }
              this.severity = 'success';
              this.detailMsg = "Contract bill tax information updated Successfully";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }


  public deleteContractBillTax(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractbilltax',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                return;
            }
           this.severity = 'success';
           this.detailMsg = "Contract bill tax information deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }


  passTheValue(contTax){
    var userData = JSON.parse(JSON.stringify(contTax));
    this.addToTheList.next(userData);
  }  

}