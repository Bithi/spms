import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Role } from '../models/role';
import { Itemtype } from '../models/itemtype';
import { SubItemType } from '../models/SubItemType';
import { Contbillchghead } from '../models/Contbillchghead';
import { Contractbilltax } from '../models/Contractbilltax';
import { Contractbillentry } from '../models/Contractbillentry';
import { Contractbilltran } from '../models/Contractbilltran';
import { async } from 'rxjs/internal/scheduler/async';


@Injectable({
  providedIn: 'root'
})
export class ContractbilltransactionService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
    public contTran:Contractbilltran = new Contractbilltran();

    key: string = "toast";
    severity: string;
    detailMsg: string;
    ret:boolean;
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}



      public insertContractBillTransaction(jsonMessage) {
        return this.http.post(GlobalVariable.BASE_API_URL + '/v1/conbilltran',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
                
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    return;
                }
                this.severity = 'success';
                this.detailMsg = "Inforamtion inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }


    public showUserInfo(jsonMessage){
      return this.http.post(GlobalVariable.BASE_API_URL+'/v1/conbilltran',
         btoa(jsonMessage)
     , { responseType: 'text'}
         )
   }


   public updateContractBillTransaction(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/conbilltran',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  return;
              }
              this.severity = 'success';
              this.detailMsg = "Information updated Successfully";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }


  public deleteContractBillTransaction(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/conbilltran',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                return;
            }
           this.severity = 'success';
           this.detailMsg = "Information deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }


  passTheValue(contTran){
    var userData = JSON.parse(JSON.stringify(contTran));
    userData.reference_date = new Date(userData.reference_date.substring(0,10)).toJSON().slice(0, 10);
    userData.next_bill_date = new Date(userData.next_bill_date.substring(0,10)).toJSON().slice(0, 10);
    this.addToTheList.next(userData);
  }  


  getBillEntry(){
    return this.http.get(`${this.apiBaseUrl}/v1/billEntryByRet`,
    
 { responseType: 'text'}
    )
}

getBillEntryBycontract(contractId: string){
  if(contractId){
    return this.http.get(`${this.apiBaseUrl}/v1/billEntryByRetandcon/${contractId}`,
    
    { responseType: 'text'}
       )
    }
 
  
}

getInitialBillTran(){
  return this.http.get(`${this.apiBaseUrl}/v1/getInitialBillTran`,
  
{ responseType: 'text'}
  )
}

getBillChargeHead(){
  return this.http.get(`${this.apiBaseUrl}/v1/billChgHead`,
  
{ responseType: 'text'}
  )
}

getItem(contractId: string) {
       
  if(contractId){
  return this.http.get(`${this.apiBaseUrl}/v1/ItemByContract/${contractId}`,
  
  { responseType: 'text'}
     )
  }
};

getItemByEntry(entry: string) {
       
  if(entry){
  return this.http.get(`${this.apiBaseUrl}/v1/ItemByEntry/${entry}`,
  
  { responseType: 'text'}
     )
  }
};


getBillTran(entryId: string) {
       
  if(entryId){
  return this.http.get(`${this.apiBaseUrl}/v1/getBillTranByEntry/${entryId}`,
  
  { responseType: 'text'}
     )
  }
};

  public async  exists(entry:string) {
  
    this.ret = false;
    return this.http.get(`${this.apiBaseUrl}/v1/duplicateEntry/${entry}`
    
  , { responseType: 'text'}
    )
    .toPromise();
  //return this.ret;

  }


  getContractBillTransactionList(){
    return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getContractBillTransactionJoin'
   , { responseType: 'text'}
       )
  

  // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
  // var jsonMessage = {
  //   dataHeader : msgHeader,
  //   payLoad: new Array()
  // };
  
  // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);
  
}

public getNetPenaltyForBillPay(jsonMessage){
  return this.http.post(GlobalVariable.BASE_API_URL+'/v1/getNetPenaltyForBillPay',
  btoa(jsonMessage)
  , { responseType: 'text'}
  
  );
  }


}