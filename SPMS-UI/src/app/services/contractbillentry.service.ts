import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Role } from '../models/role';
import { Itemtype } from '../models/itemtype';
import { SubItemType } from '../models/SubItemType';
import { Contbillchghead } from '../models/Contbillchghead';
import { Contractbilltax } from '../models/Contractbilltax';
import { Contractbillentry } from '../models/Contractbillentry';
declare var $: any;


@Injectable({
  providedIn: 'root'
})
export class ContractbillentryService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
    public contEntry:Contractbillentry = new Contractbillentry();

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}



      public insertContractBillEntry(jsonMessage) {
        return this.http.post(GlobalVariable.BASE_API_URL + '/v1/contractbillentry',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
                
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    localStorage.removeItem('isLoggedin');
                    return;
                }
                this.severity = 'success';
                this.detailMsg = "Contract bill entry inforamtion inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }


    public showUserInfo(jsonMessage){
      return this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractbillentry',
         btoa(jsonMessage)
     , { responseType: 'text'}
         )
   }


   public updateContractBillEntry(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractbillentry',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   localStorage.removeItem('isLoggedin');
                  return;
              }
              this.severity = 'success';
              this.detailMsg = "Contract bill entry information updated Successfully";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }


  public deleteContractBillEntry(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractbillentry',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                return;
            }
           this.severity = 'success';
           this.detailMsg = "Contract bill entry information deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }


  passTheValue(contEntry){
    var userData = JSON.parse(JSON.stringify(contEntry));
    userData.bill_date = new Date(userData.bill_date.substring(0,10)).toJSON().slice(0, 10);
    userData.bill_recieve_date =  new Date(userData.bill_recieve_date.substring(0,10)).toJSON().slice(0, 10);
    userData.send_audit_date =  new Date(userData.send_audit_date.substring(0,10)).toJSON().slice(0, 10);
    userData.return_from_audit_date =  new Date(userData.return_from_audit_date.substring(0,10)).toJSON().slice(0, 10);
    this.addToTheList.next(userData);
  }  

  getItem(contractId: string) {
    if(contractId){
    return this.http.get(`${this.apiBaseUrl}/v1/ItemByContract/${contractId}`,
    
    { responseType: 'text'}
       )
    }
  };

  getItemByEntry(entry: string) {
       
    if(entry){
    return this.http.get(`${this.apiBaseUrl}/v1/ItemByEntry/${entry}`,
    
    { responseType: 'text'}
       )
    }
  };

  getAllByEntry(entry: string) {
       
    if(entry){
    return this.http.get(`${this.apiBaseUrl}/v1/AllByEntry/${entry}`,
    
    { responseType: 'text'}
       )
    }
  };

  getContractBillEntryList(){
    return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getContractBillEntryList'
   , { responseType: 'text'}
       )
  

  // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
  // var jsonMessage = {
  //   dataHeader : msgHeader,
  //   payLoad: new Array()
  // };
  
  // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);
  
}
}