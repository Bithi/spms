import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Budgetbillentry } from '../models/Budgetbillentry';
//import { BudgetbillentryinsertComponent } from '../pages/budgetbillentry/budgetbillentryinsert/budgetbillentryinsert.component';
declare var $: any;


@Injectable({
  providedIn: 'root'
})
export class BudgetbillentryService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
  public contEntry: Budgetbillentry = new Budgetbillentry();

  key: string = "toast";
  severity: string;
  detailMsg: string;

  
  ones = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine','Ten'];
  teens = ['Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
  tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

  wording: string;

  constructor(
    private http: HttpClient,
    public router: Router,
    private dataService: DataService
    //,private budgetBillEntryInsertComponent : BudgetbillentryinsertComponent
    ,private utility: Utility) { }



  public insertBudgetBillEntry(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetBillEntry',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));


        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
          this.severity = 'error';
          this.detailMsg = "There is something wrong";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "Budget bill entry inforamtion inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }

  public updateBudgetBillEntry(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetBillEntry',
      btoa(jsonMessage)
      , { responseType: 'text' }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Contract bill entry information updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);

        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }
  public deleteBudgetBillEntry(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetBillEntry',
      btoa(jsonMessage)
      , { responseType: 'text' }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Budget bill entry information deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }

  passTheValue(budgetBillEntry) {
    var userData = JSON.parse(JSON.stringify(budgetBillEntry));
debugger;
    console.log("Data\n"+JSON.stringify(userData));
    userData.invoice_dt ?
      userData.invoice_dt = new Date(userData.invoice_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.invoice_dt;

    userData.invoice_dt ? userData.invoice_dt = new Date(userData.invoice_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.invoice_dt;

    // userData.agreement_sign_dt ? userData.agreement_sign_dt = new Date(userData.agreement_sign_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.agreement_sign_dt;

    userData.delivery_dt ? userData.delivery_dt = new Date(userData.delivery_dt.substring(0, 10)).toJSON().slice(0, 10) :
      userData.delivery_dt;

    userData.bill_recieve_dt ? userData.bill_recieve_dt = new Date(userData.bill_recieve_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.bill_recieve_dt;

    userData.performReportDt ? userData.performReportDt = new Date(userData.performReportDt.substring(0, 10)).toJSON().slice(0, 10) : userData.performReportDt;

    userData.inspectReportDt ? userData.inspectReportDt = new Date(userData.inspectReportDt.substring(0, 10)).toJSON().slice(0, 10) : userData.inspectReportDt;

    userData.bill_to_audit_dt ? userData.bill_to_audit_dt = new Date(userData.bill_to_audit_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.bill_to_audit_dt;

    userData.audit_report_receive_dt ? userData.audit_report_receive_dt = new Date(userData.audit_report_receive_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.audit_report_receive_dt;

    userData.send_to_bdc_dt ? userData.send_to_bdc_dt = new Date(userData.send_to_bdc_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.send_to_bdc_dt;

    userData.payment_note_dt ? userData.payment_note_dt = new Date(userData.payment_note_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.payment_note_dt;

    userData.from_bdc_dt ? userData.from_bdc_dt = new Date(userData.from_bdc_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.from_bdc_dt;

    userData.note_approve_dt ? userData.note_approve_dt = new Date(userData.note_approve_dt.substring(0, 10)).toJSON().slice(0, 10) : userData.note_approve_dt;

     
    if (userData.bill_amount == 0 || !userData.bill_amount) {
      //console.log("\nzero or blank...\n");
      this.wording = "";
    }
    else {
      userData.bill_amount = parseInt(userData.bill_amount, 10);
      this.wording = this.convertGreaterThanThousand(userData.bill_amount) + " Only."
    }
    userData.bill_amount_in_words = this.wording;

   
    var delivaryDt = new Date(userData.delivery_dt);
    //userData.delivery_dt = new Date(userData.delivery_dt );
    
    var currentDate = new Date();
    var todaysDate = new Date(currentDate);
    
    todaysDate.setDate(currentDate.getDate() - 1);

    var delivaryTimeLeft = delivaryDt.getTime() - todaysDate.getTime();

    //console.log("\nrr:" + delivaryTimeLeft);

    delivaryTimeLeft > 0 ? userData.delivary_time_left = this.convertMillisToWords(delivaryTimeLeft) : userData.delivary_time_left = "Expired!"

    if(userData.delivary_time_left === ""){
      userData.delivary_time_left = "Today";
    }

    var expirationDate = new Date(userData.agreement_sign_dt);
    const date = new Date(expirationDate);
    date.setMonth(date.getMonth() + parseInt(userData.warranty));
    date.toISOString().slice(0, 10);
    userData.warranty_valid_upto = JSON.parse(JSON.stringify(date)).substring(0, 10);

    userData.isEdit = true;


    console.log("uuu\n"+userData);
   
    this.addToTheList.next(userData);
  }

  getBudgetBillEntryList() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getBudgetBillEntryList'
      , { responseType: 'text' }
    )


    // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
    // var jsonMessage = {
    //   dataHeader : msgHeader,
    //   payLoad: new Array()
    // };

    // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);

  }

  convertLessThanThousand(num: number): string {
    if (num === 0) {
      return '';
    } else if (num <= 10) {
      return this.ones[num];
    } else if (num < 20) {
      return this.teens[num - 11];
    } else if (num < 100) {
      return this.tens[Math.floor(num / 10)] + ' ' + this.convertLessThanThousand(num % 10);
    } else {
      return this.ones[Math.floor(num / 100)] + ' Hundred ' + this.convertLessThanThousand(num % 100);
    }
  }

  convertGreaterThanThousand(num: number): string {
    if (num < 1000) {
      return this.convertLessThanThousand(num);
    } else if (num < 100000) {
      return this.convertLessThanThousand(Math.floor(num / 1000)) + ' Thousand ' + this.convertLessThanThousand(num % 1000);
    } else if (num < 10000000) {
      return this.convertLessThanThousand(Math.floor(num / 100000)) + ' Lakh ' + this.convertGreaterThanThousand(num % 100000);
    } else {
      return this.convertLessThanThousand(Math.floor(num / 10000000)) + ' Crore ' + this.convertGreaterThanThousand(num % 10000000);
    }
  }

  convertMillisToWords(milliseconds) {
    const seconds = Math.floor(milliseconds / 1000);
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);
    const days = Math.floor(hours / 24);
    const months = Math.floor(days / 30.44); // Approximate number of days in a month
    const years = Math.floor(months / 12);

    const result = [];

    if (years > 0) {
      result.push(years + (years === 1 ? ' year' : ' years'));
    }
    if (months > 0) {
      result.push(months % 12 + (months % 12 === 1 ? ' month' : ' months'));
    }
    if (days > 0) {
      result.push(days % 30 + (days % 30 === 1 ? ' day' : ' days'));
    }

    return result.join(', ');
  }

  	
	
  public getTotalBillByContract(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/getBillAmountByContract',
    btoa(jsonMessage)
    , { responseType: 'text' }
  )
  }

  getBillEntry(ref: string) {
       
    if(ref){
    return this.http.get(`${this.apiBaseUrl}/v1/getBudgetBillEntry/${ref}`,
    
    { responseType: 'text'}
       )
    }
  };
}