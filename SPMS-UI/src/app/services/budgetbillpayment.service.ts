import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';
import { Budgetbillentry } from '../models/Budgetbillentry';
//import { BudgetbillentryinsertComponent } from '../pages/budgetbillentry/budgetbillentryinsert/budgetbillentryinsert.component';
declare var $: any;


@Injectable({
  providedIn: 'root'
})
export class BudgetbillpaymentService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();
  public contEntry: Budgetbillentry = new Budgetbillentry();

  key: string = "toast";
  severity: string;
  detailMsg: string;

  
  ones = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine','Ten'];
  teens = ['Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
  tens = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

  wording: string;

  constructor(
    private http: HttpClient,
    public router: Router,
    private dataService: DataService
    //,private budgetBillEntryInsertComponent : BudgetbillentryinsertComponent
    ,private utility: Utility) { }



  public insertBudgetBillPayment(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/budgetBillPayment',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));


        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0] || msg.dataHeader.reponseStatus == "error") {
          this.severity = 'error';
          this.detailMsg = "There is something wrong";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "Budget bill payment done Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }

  public getBillPaymentAmount(in_bill_amount: number,in_vat:number,in_inclusive:number,in_with_vat:number,in_tds:number,in_with_tds:number,in_penalty_amt:number,in_invoice:string,in_fiscal_year:string) {
   
       
    return this.http.get(`${this.apiBaseUrl}/v1/getBillPaymentAmount/${in_bill_amount}/${in_vat}/${in_inclusive}/${in_with_vat}/${in_tds}/${in_with_tds}/${in_penalty_amt}/${in_invoice}/${in_fiscal_year}`,
    
    { responseType: 'text'}
       )
  
  };

  

}