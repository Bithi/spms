import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { DataService } from '../data/data.service';
import { GlobalVariable } from '../global/global';
import { User } from '../models/user';
import { Subject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ContactInfoService {
  apiBaseUrl = GlobalVariable.BASE_API_URL;

  constructor(
    private http:HttpClient,
    public router: Router,
    private dataService: DataService,
    private utility:Utility) { }
    
  
    public showContractInfo(jsonMessage){
        // const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/getUserInfo', jsonMessage);
        
        // //return this.http.post(GlobalVariable.BASE_API_URL + '/v1/getUserInfo',jsonMessage);
        
        // return request;
         return this.http.post(GlobalVariable.BASE_API_URL+'/v1/getContractInfo',
            btoa(jsonMessage)
        , { responseType: 'text'}
            )
      }
      
      
}
