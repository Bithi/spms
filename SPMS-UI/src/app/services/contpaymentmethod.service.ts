import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GlobalVariable} from '../global/global';
import {Utility} from '../util/utility';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs';
declare var $: any;
@Injectable({
  providedIn: 'root'
})

export class ContPaymentMethodService {
  
  public addToTheList = new Subject<any>();
  private baseUrl = GlobalVariable.BASE_API_URL + '/v1/getContPaymentMethods';

  constructor(private http: HttpClient,
    private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;

  getList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  passTheValue(paymentMethod) {
    var userData = JSON.parse(JSON.stringify(paymentMethod));

    userData.paymentDate = new Date(userData.paymentDate.substring(0,10)).toJSON().slice(0, 10);
    userData.dueDate = new Date(userData.dueDate.substring(0,10)).toJSON().slice(0, 10);

   
    this.addToTheList.next(userData);
  }
  public updateData(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contPaymentMethod',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Contract Payment Method Updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public deleteData(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contPaymentMethod',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Contract Payment Method deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public insertData(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/contPaymentMethod',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
          this.severity = 'error';
          this.detailMsg = "error occured";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "Contract Payment Method inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }


  getPaymentMethodList(){
    return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getPaymentMethodJoin'
   , { responseType: 'text'}
       )
  

  // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
  // var jsonMessage = {
  //   dataHeader : msgHeader,
  //   payLoad: new Array()
  // };
  
  // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);
  
}

}