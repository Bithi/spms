import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GlobalVariable} from '../global/global';
import {Utility} from '../util/utility';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs';
declare var $: any;
@Injectable({
  providedIn: 'root'
})

export class ContSubItemService {
  
  public addToTheList = new Subject<any>();
  private baseUrl = GlobalVariable.BASE_API_URL + '/v1/contSubItem';

  constructor(private http: HttpClient,
    private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;
  apiBaseUrl = GlobalVariable.BASE_API_URL;
  getList(jsonMessage): Observable<any> {
    return this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
  );
  }
  
  public getModelList(jsonMessage){
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/model',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    );

  }
  passTheValue(SubItem) {
    var userData = JSON.parse(JSON.stringify(SubItem));
   
    this.addToTheList.next(userData);
  }
  public updateData(jsonMessage) {
    this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Contract Sub Item Updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public deleteData(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/contSubItem',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Contract Sub Item deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public insertData(jsonMessage) {
    return this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
          this.severity = 'error';
          this.detailMsg = "Invalid Cridential";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "Contract Sub Item inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }
  public getItemList(jsonMessage){
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/subItemType',
      btoa(jsonMessage), {
      responseType: 'text'
    }
    );

  }
  getcontsubitemModelJoin(){
    return this.http.get(`${this.apiBaseUrl}/v1/getcontsubitemModelJoin`,
    
  { responseType: 'text'}
    )
  }

getmodelBrandJoin(){
  return this.http.get(`${this.apiBaseUrl}/v1/getmodelBrandJoin`,
  
{ responseType: 'text'}
  )
}
}
