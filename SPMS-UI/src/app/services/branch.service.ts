import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { GlobalVariable } from '../global/global';

@Injectable({
  providedIn: 'root'
})
export class BranchService { str:string = "toast";
constructor(private http: HttpClient, private dataService: DataService,private router: Router, private utility:Utility) { }
key:string = "toast";
severity:string;
detailMsg:string;
data: any[];

branchList(jsonMessage){
    return this.http.post(GlobalVariable.BASE_API_URL+'/v1/branchList',
    btoa(jsonMessage)
, { responseType: 'text'}
    )

  
}
}



