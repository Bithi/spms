import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GlobalVariable} from '../global/global';
import {Utility} from '../util/utility';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs';
declare var $: any;
@Injectable({
  providedIn: 'root'
})

export class ContSecurityMoneyService {
  
  public addToTheList = new Subject<any>();
  private baseUrl = GlobalVariable.BASE_API_URL + '/v1/contSecurityMoney';

  constructor(private http: HttpClient,
    private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;

  getList(jsonMessage): Observable<any> {
    return this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
  );
  }
  passTheValue(contSecurityMoney) {
    var userData = JSON.parse(JSON.stringify(contSecurityMoney));
    userData.issueDate = new Date(userData.issueDate.substring(0,10)).toJSON().slice(0, 10);
    userData.returnDate = new Date(userData.returnDate.substring(0,10)).toJSON().slice(0, 10);
    userData.securityType=(userData.securityType=='Bank Gaurantee')?'BG':'PO';

    this.addToTheList.next(userData);
  }


  public updateData(jsonMessage) {
    this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Security Money Updated Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }


  public deleteData(jsonMessage) {
    this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
    )
      .subscribe(
        (res) => {
          const msg = JSON.parse(atob(res));
          console.log(res);

          if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
            this.severity = 'error';
            this.detailMsg = "Error occured";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            localStorage.removeItem('isLoggedin');
            return;
          }
          this.severity = 'success';
          this.detailMsg = "Security Money deleted Successfully";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);



        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }





  











  public insertData(jsonMessage) {
    return this.http.post(this.baseUrl,
      btoa(jsonMessage), {
      responseType: 'text'
    }
    ).subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
          this.severity = 'error';
          this.detailMsg = "error occured";
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          localStorage.removeItem('isLoggedin');
          return;
        }
        this.severity = 'success';
        this.detailMsg = "Security Money inserted Successfully";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);


      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }


  getSecurityMoneyList(){
    return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getSecurityMoneyList'
   , { responseType: 'text'}
       )
  

  // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
  // var jsonMessage = {
  //   dataHeader : msgHeader,
  //   payLoad: new Array()
  // };
  
  // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);
  
}
}