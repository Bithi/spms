import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { DataService } from '../data/data.service';
import { GlobalVariable } from '../global/global';
import { Subject, Observable } from 'rxjs';
import { VendorInfo } from '../models/vendorinfo';
declare var $ : any;
@Injectable({
  providedIn: 'root'
})
export class VendorInfoService {
  public venInfo:VendorInfo = new VendorInfo();

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();

  constructor(
    private http:HttpClient,
    public router: Router,
    private dataService: DataService,
    private utility:Utility,
    
  ) { }
  key:string = "toast";
    severity:string;
    detailMsg:string;


  public showUserInfo(jsonMessage){
     return this.http.post(GlobalVariable.BASE_API_URL+'/v1/vendorInfo',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
  }


  getVendorType(){
    return this.http.get(`${this.apiBaseUrl}/v1/vendorType`,
    
 { responseType: 'text'}
    )
}


  getContractNumber(){
    return this.http.get(`${this.apiBaseUrl}/v1/contractNumber`,
    
 { responseType: 'text'}
    )
  }


getVendorServiceType(){
  return this.http.get(`${this.apiBaseUrl}/v1/vendorServiceType`,
  
{ responseType: 'text'}
  )
}



  public insertVendorInfo(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/vendorInfo',
       btoa(jsonMessage)
   , { responseType: 'text'}
       )
       .subscribe(
         (res ) => {
                 const msg = JSON.parse( atob(res) );
                 console.log(msg);
                 if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                      this.severity = 'error';
                      this.detailMsg = "Error occured";
                      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                      localStorage.removeItem('isLoggedin');
                     // this.showProgressSpin = false;
                     return;
                 }
                 // this.showProgressSpin = false;
                 this.severity = 'success';
                 this.detailMsg = "Vendor saved Successfully";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 
                 
             
         },
         err => {
              this.severity = 'error';
              this.detailMsg = "Server Error: " + err.message;
             // // this.showProgressSpin = false;
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
             return;
         }
    );
       }

       passTheValue(data){
        var vendorData = JSON.parse(JSON.stringify(data));
        this.addToTheList.next(vendorData);
      }

      public updateVendorInfo(jsonMessage){
        this.http.post(GlobalVariable.BASE_API_URL+'/v1/vendorInfo',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       localStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Vendor updated Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }


      public deleteVendorInfo(jsonMessage){
        this.http.post(GlobalVariable.BASE_API_URL+'/v1/vendorInfo',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       localStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Vendor deleted Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }

      upload(data){
        var utilityLocal= this.utility;
        var keyvar = this.key;
        var formFields = document.getElementById('fileUploadForm');
      
        var today = new Date();
        var date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();
        this.venInfo.entryDt = new Date(date);
        this.venInfo.userId = JSON.parse(localStorage.getItem('loggedinUser')).userId; 
        this.venInfo.updatedt = new Date('01/01/1970');   
        this.venInfo.updateusr = "";
      
        if(!formFields['files'].files.length){
          this.severity = 'error';
          this.detailMsg = 'please, upload a file';
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        }
        else{
          data.append('data',JSON.stringify(this.venInfo));
          $('#uploadSpinner').show();
          $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: GlobalVariable.BASE_API_URL+'/v1/uploadVendorInfoFile',
            data: data,
            processData: false, //prevent jQuery from automatically transforming the data into a query string
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                $("#result").text(data);
                $('#uploadSpinner').hide();
                
                console.log("SUCCESS : ", data);
                this.severity = data[0];
                this.detailMsg = data[1];
                utilityLocal.ShowToast(keyvar,this.severity,this.detailMsg);
                $("#btnSubmit").prop("disabled", false);
                $("#myForm")[0].reset();
                this.reloadPage();
      
            },
            error: function (e) {
                $('#uploadSpinner').hide();
      
                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                this.severity = 'error';
                this.detailMsg = e;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                $("#btnSubmit").prop("disabled", false);
                $("#myForm")[0].reset();
      
            }
        });
        }

      }
}
