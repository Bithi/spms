import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataService } from '../data/data.service';
import { GlobalVariable } from '../global/global';
import { Utility } from '../util/utility';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  public addToTheList = new Subject<any>();

  key: string = "toast";
  severity: string;
  detailMsg: string;

  constructor(
    private http: HttpClient,
    public router: Router,
    private dataService: DataService,
    private utility: Utility) { }




  getConnectivityChart() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getConnectivityChart'
      , { responseType: 'text' }
    )

  }

  getCountSLAChartValidity() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getCountSLAChartValidity/'
      , { responseType: 'text' }
    )

  }

  getConnectivityChartValidity() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getConnectivityChartValidity/'
      , { responseType: 'text' }
    )

  }

  getCountAMCChartValidity() {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getCountAMCChartValidity/'
      , { responseType: 'text' }
    )

  }

  getCountAMCChartValidityPrevData(fdate:String,tdate:String) {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getCountAMCChartValidityPrevData/'+fdate+' '+tdate
      , { responseType: 'text' }
    )

  }

  getCountSLAChartValidityPrevData(fdate:String,tdate:String) {
    return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getCountSLAChartValidityPrevData/'+fdate+' '+tdate
      , { responseType: 'text' }
    )

  }

  // getSLAChartValidity(duration: number) {
  //   return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getSLAChartValidity/' + duration
  //     , { responseType: 'text' }
  //   )

  // }
  // getAMCChartValidity(duration: number) {
  //   return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getAMCChartValidity/' + duration
  //     , { responseType: 'text' }
  //   )

  // }
  // getAMCChartValidityPrev(duration: number) {
  //   return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getAMCChartValidityPrev/' + duration
  //     , { responseType: 'text' }
  //   )

  // }

  // getSLAChartValidityPrev(duration: number) {
  //   return this.http.get(GlobalVariable.BASE_API_URL + '/v1/getSLAChartValidityPrev/' + duration
  //     , { responseType: 'text' }
  //   )

  // }
  generateAMCExReport(jsonMessage) {

    const request = this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/AmcExReport', jsonMessage);
    request.subscribe(
      (res) => {
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      },
      err => {
        console.log(err);

      }
    );
  }

  generateAMCExPrevReport(jsonMessage) {

    const request = this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/AmcExPrevReport', jsonMessage);
    request.subscribe(
      (res) => {
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      },
      err => {
        console.log(err);

      }
    );
  }
  generateSLAExPrevReport(jsonMessage) {

    const request = this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/SlaExPrevReport', jsonMessage);
    request.subscribe(
      (res) => {
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      },
      err => {
        console.log(err);

      }
    );
  }

  generateSLAExReport(jsonMessage) {

    const request = this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/SlaExReport', jsonMessage);
    request.subscribe(
      (res) => {
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      },
      err => {
        console.log(err);

      }
    );
  }

  generateCONNExReport(jsonMessage) {

    const request = this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/ConnExReport', jsonMessage);
    request.subscribe(
      (res) => {
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      },
      err => {
        console.log(err);

      }
    );
  }
}