import { Injectable } from '@angular/core';
import { GlobalVariable } from '../global/global';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { DataService } from '../data/data.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor( private http:HttpClient,
    public router: Router,
    private dataService: DataService,
    private utility:Utility) { 
    
  }

  generateDownTimeDetailsReportRequest(jsonMessage){
    debugger;
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/DownTimeCalcReport', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }


  generatePenaltyCalcReportRequest(jsonMessage){
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/penaltyReport', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }
}
