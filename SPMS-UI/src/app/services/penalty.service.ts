import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { GlobalVariable } from '../global/global';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PenaltyService {

  str:string = "toast";
  constructor(private http: HttpClient, private dataService: DataService,private router: Router, private utility:Utility) { }
  key:string = "toast";
  severity:string;
  detailMsg:string;
  data: any[];

  process(jsonMessage){
      
      return this.http.post(GlobalVariable.BASE_API_URL+'/v1/penaltyProcess',
      btoa(jsonMessage)
  , { responseType: 'text'}
      )
  
    
  }

  getPenaltyList(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/penaltyProcess', btoa(jsonMessage), { responseType: 'text' });
  }

  
  public deletePenalty(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/v1/penaltyProcess',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                //this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
           this.severity = 'success';
           this.detailMsg = "Deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }
}
