import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Utility } from '../util/utility';
@Injectable({
  providedIn: 'root'
})
export class PenaltyReportService {

  apiBaseUrl = GlobalVariable.BASE_API_URL;

  str: string = "toast";
  constructor(private http: HttpClient, private dataService: DataService, private router: Router, private utility: Utility) { }
  key: string = "toast";
  severity: string;
  detailMsg: string;
  data: any[];
  getDowntimeList(jsonMessage) {
    return this.http.post(GlobalVariable.BASE_API_URL + '/v1/downtimeInfo', btoa(jsonMessage), { responseType: 'text' });
  }


  getCotractId(){
    return this.http.get(`${this.apiBaseUrl}/v1/contractID`,
    
  { responseType: 'text'}
    )
  }


  getVendorId(){
    return this.http.get(`${this.apiBaseUrl}/v1/vendorID`,
    
  { responseType: 'text'}
    )
  }


  generatePenaltyReport(jsonMessage){
    debugger;
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/penaltyReport', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }

  generateAMCReport(jsonMessage){
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/AmcReport', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }

  generateConnectivityReport(jsonMessage){
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/ConnectivityReport', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }

  generateServiceReport(jsonMessage){
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/ServiceReport', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }


  generateValidEnd(jsonMessage){
    const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/findValidEnd', jsonMessage);
    request.subscribe(
      (res) => {
          var file = new Blob([res], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      },
      err => {
          console.log(err);
          
      }
  );

  }
}
