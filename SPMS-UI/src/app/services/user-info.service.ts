import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Router
} from '@angular/router';
import {
  Utility
} from '../util/utility';
import {
  DataService
} from '../data/data.service';
import {
  GlobalVariable
} from '../global/global';
import {
  User
} from '../models/user';
import {
  Subject,
  Observable
} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserInfoService {


  public addToTheList = new Subject < any > ();

  public persons: any = [];

  User: User = new User();
  jsonMessage: {
      dataHeader: any;payLoad: any;
  };
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}
  key: string = "toast";
  severity: string;
  detailMsg: string;

  checkDigit() {

  }

  public showUserInfo(jsonMessage) {
      // const request =this.dataService.createRequestArrayBuffer(GlobalVariable.BASE_API_URL + '/v1/getUserInfo', jsonMessage);

      // //return this.http.post(GlobalVariable.BASE_API_URL + '/v1/getUserInfo',jsonMessage);

      // return request;
      return this.http.post(GlobalVariable.BASE_API_URL + '/v1/userInfo',
          btoa(jsonMessage), {
              responseType: 'text'
          }
      )
  }


  public insertUserInfo(jsonMessage) {
      this.http.post(GlobalVariable.BASE_API_URL + '/v1/userInfo',
              btoa(jsonMessage), {
                  responseType: 'text'
              }
          )
          .subscribe(
              (res) => {
                  const msg = JSON.parse(atob(res));
                  if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                      this.severity = 'error';
                      this.detailMsg = "Error occured";
                      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                      localStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "User Info saved Successfully";
                  this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                  
              },
              err => {
                  this.severity = 'error';
                  this.detailMsg = "Server Error: " + err.message;
                  // // this.showProgressSpin = false;
                  this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                  return;
              }
          );
  }

  


  public updateUserInfo(jsonMessage) {
      this.http.post(GlobalVariable.BASE_API_URL + '/v1/userInfo',
              btoa(jsonMessage), {
                  responseType: 'text'
              }
          )
          .subscribe(
              (res) => {
                  const msg = JSON.parse(atob(res));
                  console.log(res);

                  if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                      this.severity = 'error';
                      this.detailMsg = "Error occured";
                      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                      localStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "User Info updated Successfully";
                  this.utility.ShowToast(this.key, this.severity, this.detailMsg);



              },
              err => {
                  this.severity = 'error';
                  this.detailMsg = "Server Error: " + err.message;
                  // // this.showProgressSpin = false;
                  this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                  return;
              }
          );
  }

  public resetPass(jsonMessage) {
    this.http.post(GlobalVariable.BASE_API_URL + '/v1/userInfo',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        )
        .subscribe(
            (res) => {
                const msg = JSON.parse(atob(res));
                console.log(res);

                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "Error occured";
                    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                    localStorage.removeItem('isLoggedin');
                    // this.showProgressSpin = false;
                    return;
                }
                // this.showProgressSpin = false;
                this.severity = 'success';
                this.detailMsg = "Password reset Successfully";
                this.utility.ShowToast(this.key, this.severity, this.detailMsg);



            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // // this.showProgressSpin = false;
                this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                return;
            }
        );
}


  public deleteUserInfo(jsonMessage) {
      this.http.post(GlobalVariable.BASE_API_URL + '/v1/userInfo',
              btoa(jsonMessage), {
                  responseType: 'text'
              }
          )
          .subscribe(
              (res) => {
                  const msg = JSON.parse(atob(res));
                  console.log(res);

                  if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                      this.severity = 'error';
                      this.detailMsg = "Error occured";
                      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                      localStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "User Info deleted Successfully";
                  this.utility.ShowToast(this.key, this.severity, this.detailMsg);



              },
              err => {
                  this.severity = 'error';
                  this.detailMsg = "Server Error: " + err.message;
                  // // this.showProgressSpin = false;
                  this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                  return;
              }
          );
  }




  passTheValue(data) {
    var userData = JSON.parse(JSON.stringify(data));
      this.addToTheList.next(userData);
  }

}