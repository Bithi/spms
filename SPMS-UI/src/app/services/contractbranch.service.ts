import { Injectable } from '@angular/core';
import { DataService } from '../data/data.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Utility } from '../util/utility';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { ContractBranch } from 'src/app/models/contractbranch';
declare var $ : any;
@Injectable({
  providedIn: 'root'
})
export class ContractBranchService{

    public addToTheList = new Subject<any>();
    private baseUrl =GlobalVariable.BASE_API_URL+ '/v1/getContractBranch';
    public conBranch:ContractBranch = new ContractBranch();

    constructor(private http: HttpClient,
      private utility:Utility,private dataService: DataService,) { }
    key:string = "toast";
    severity:string;
    detailMsg:string;

    // getContractBranchList(): Observable<any> {
    //     return this.http.post(`${this.baseUrl}`);
    //   }

      getContractBranchList(){
          return this.http.get(GlobalVariable.BASE_API_URL+'/v1/getcontractBranchJoin'
         , { responseType: 'text'}
             )
        
      
        // var msgHeader = this.dataService.createMessageHeader("SELECT_ALL");
        // var jsonMessage = {
        //   dataHeader : msgHeader,
        //   payLoad: new Array()
        // };
        
        // return this.dataService.createRequest(GlobalVariable.BASE_API_URL+'/v1/contractBranch',jsonMessage);
        
      }

      
   passTheValue(ContractBranch){
    var userData = JSON.parse(JSON.stringify(ContractBranch));
        this.addToTheList.next(userData);
}
public updateContractBranchInfo(jsonMessage){
       this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractBranch',
       btoa(jsonMessage)
   , { responseType: 'text'}
       )
       .subscribe(
         (res ) => {
                 const msg = JSON.parse( atob(res) );
                 if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                      this.severity = 'error';
                      this.detailMsg = "Error occured";
                      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                      localStorage.removeItem('isLoggedin');
                     //this.showProgressSpin = false;
                     return;
                 }
                 // this.showProgressSpin = false;
                 this.severity = msg.dataHeader.reponseStatus;
                 this.detailMsg = msg.dataHeader.description==""?"Contract Branch updated Successfully":msg.dataHeader.description;
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 
                 
             
         },
         err => {
              this.severity = 'error';
              this.detailMsg = "Server Error: " + err.message;
              // this.showProgressSpin = false;
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
             return;
         }
    );
     }


public deleteContractBranchInfo(jsonMessage){
this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractBranch',
btoa(jsonMessage)
, { responseType: 'text'}
)
.subscribe(
(res ) => {
        const msg = JSON.parse( atob(res) );
        console.log(res);
        
        if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
             this.severity = 'error';
             this.detailMsg = "Error occured";
             this.utility.ShowToast(this.key,this.severity,this.detailMsg);
             localStorage.removeItem('isLoggedin');
            //this.showProgressSpin = false;
            return;
        }
        // this.showProgressSpin = false;
       this.severity = 'success';
       this.detailMsg = "Contract branch deleted Successfully";
       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        
        
    
},
err => {
     this.severity = 'error';
     this.detailMsg = "Server Error: " + err.message;
     // this.showProgressSpin = false;
     this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
}
);
}


public insertContractBranchInfo(jsonMessage){
return this.http.post(GlobalVariable.BASE_API_URL+'/v1/contractBranch',
  btoa(jsonMessage)
, { responseType: 'text'}
  ) .subscribe(
    (res ) => {
 
            const msg = JSON.parse( atob(res) );
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error Occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 localStorage.removeItem('isLoggedin');
                // // this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
            this.severity = 'success';
            this.detailMsg = "Contract branch inserted Successfully";
            this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            
        
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);
}

upload(data){
  var utilityLocal= this.utility;
  var keyvar = this.key;
  var formFields = document.getElementById('fileUploadForm');

  var today = new Date();
  var date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();
  this.conBranch.entryDate = new Date(date);
  this.conBranch.entryUser = JSON.parse(localStorage.getItem('loggedinUser')).userId; 
  this.conBranch.updateDt = new Date('01/01/1970');   
  this.conBranch.updateUser = "";

  if(!formFields['files'].files.length){
    this.severity = 'error';
    this.detailMsg = 'please, upload a file';
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
  }
  else{
    data.append('data',JSON.stringify(this.conBranch));
    $('#contractBranchUploadSpinner').show();
    $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: GlobalVariable.BASE_API_URL+'/v1/uploadContractBranchFile',
      data: data,
      processData: false, //prevent jQuery from automatically transforming the data into a query string
      contentType: false,
      cache: false,
      // timeout: 600000,
      success: function (data) {
          $("#result").text(data);
          $('#contractBranchUploadSpinner').hide();
          console.log("SUCCESS : ", data);
          this.severity = data[0];
          this.detailMsg = data[1];
          utilityLocal.ShowToast(keyvar,this.severity,this.detailMsg);
          $("#btnSubmit").prop("disabled", false);
          $("#myForm")[0].reset();
          

      },
      error: function (e) {

          $("#result").text(e.responseText);
          $('#contractBranchUploadSpinner').hide();
          console.log("ERROR : ", e);
          this.severity = 'error';
          this.detailMsg = e;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          $("#btnSubmit").prop("disabled", false);
          $("#myForm")[0].reset();

      }
  });
  


    
  }
}

}
  

