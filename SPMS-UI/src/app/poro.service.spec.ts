import { TestBed } from '@angular/core/testing';

import { PoroService } from './services/poro.service';

describe('PoroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoroService = TestBed.get(PoroService);
    expect(service).toBeTruthy();
  });
});
