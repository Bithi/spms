export class DataHeader {
  requestTimestamp: string;
  actionType: string;
  userId: string;
  password: string;
}
