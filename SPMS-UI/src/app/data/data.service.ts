import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {DataHeader} from './data.header';

/*const headers = new HttpHeaders({ 'Content-Type': 'application/json'});
*/

//const headers =new HttpHeaders().set("Authorization","Bearer "+localStorage.getItem('loggedinUser'));

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) {

  }

  createMessageHeader(actionType: string) {
    var dataHeader = new DataHeader();
    dataHeader.requestTimestamp = this.getSqlDate(new Date().toString());
    dataHeader.actionType = actionType;
    return dataHeader;

  }

  createRequest(reqUrl, jsonMessage) {
    return this.http.post(reqUrl,
      btoa(JSON.stringify(jsonMessage)),
      {responseType: 'text'}
    );

  }

  createRequestArrayBuffer(reqUrl, jsonMessage) {
    
    var replacer = function(key, value) {

        if (this[key] instanceof Date) {
          var d = this[key];
          var z  = n =>  ('0' + n).slice(-2);
          var zz = n => ('00' + n).slice(-3);
          var off = d.getTimezoneOffset();
          var sign = off < 0? '+' : '-';
          off = Math.abs(off);

          return d.getFullYear() + '-'
                + z(d.getMonth()+1) + '-' +
                z(d.getDate()) + 'T' +
                z(d.getHours()) + ':'  + 
                z(d.getMinutes()) + ':' +
                z(d.getSeconds()) + '.' +
                zz(d.getMilliseconds()) +
                sign + z(off/60|0) + ':' + z(off%60); 
        }
        
        return value;
    };
    return this.http.post(reqUrl,
      btoa(JSON.stringify(jsonMessage, replacer)),
      {responseType: 'arraybuffer'}
    );

  }

  randomString(length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

    if (!length) {
      length = Math.floor(Math.random() * chars.length);
    }

    var str = '';
    for (var i = 0; i < length; i++) {
      str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
  }

  getSqlDate(stringDate) {
    if (!stringDate) {
      return stringDate;
    }
    return new Date(stringDate).toISOString();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
