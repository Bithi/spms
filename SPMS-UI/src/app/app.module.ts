import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LayoutModule } from './layout/layout.module';
import { PanelMenuModule, RadioButtonModule } from 'primeng';
import { QuicklinkModule, QuicklinkStrategy } from 'ngx-quicklink';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ReportModule } from './pages/report/report.module';
import { UserInfoModule } from './pages/user-info/user-info.module';
//import { BudgetMasterComponent } from './budget-master/budget-master.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent, LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, LayoutModule,
    ToastModule,
    DialogModule,
    BrowserAnimationsModule, PanelMenuModule,
    FormsModule, ReportModule, UserInfoModule
    // DashboardModule,
    // DowntimeModule,
    // ReportModule,
    // PenaltyModule,
    // POROModule,
    // UserInfoModule,
    // VendorInfoModule,
    // ContractBranchModule,
    // ContractInfoModule,
    // PenaltyConfigModule,
    // RoleInfoModule,
    // MenuInfoModule

  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
