import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContbillchgheadRoutingModule } from './contbillchghead-routing.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';
import { ContbillchgheadinfoComponent } from './contbillchgheadinfo/contbillchgheadinfo.component';
import { ContbillchgheadinsertComponent } from './contbillchgheadinsert/contbillchgheadinsert.component';
import { LayoutModule } from 'src/app/layout/layout.module';

// NgModule({
//   declarations: [ContbillchgheadinfoComponent, ContbillchgheadinsertComponent],
//   imports: [
//     CommonModule,
//     LayoutModule,
//     FormsModule,TableModule,DropdownModule,ContbillchgheadRoutingModule
//   ]
// })

// export class ContbillchgheadModule { }

@NgModule({
  declarations: [ContbillchgheadinfoComponent,ContbillchgheadinsertComponent],
  imports: [
    ContbillchgheadRoutingModule,
    CommonModule,LayoutModule,
    FormsModule,TableModule,DropdownModule
  ],
  exports: [ContbillchgheadinfoComponent,ContbillchgheadinsertComponent]
})
export class ContbillchgheadModule { }