import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContbillchgheadinsertComponent } from './contbillchgheadinsert/contbillchgheadinsert.component';


const routes: Routes = [
  {path: '', component: ContbillchgheadinsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContbillchgheadRoutingModule { }
