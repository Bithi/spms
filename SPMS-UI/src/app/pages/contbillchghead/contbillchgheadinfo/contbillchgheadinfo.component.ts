import { Component, OnInit, ViewChild } from '@angular/core';
import { Contbillchghead } from 'src/app/models/Contbillchghead';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { ContbillchgheadService } from 'src/app/services/contbillchghead.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-contbillchgheadinfo',
  templateUrl: './contbillchgheadinfo.component.html',
  styleUrls: ['./contbillchgheadinfo.component.scss']
})
export class ContbillchgheadinfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  contBill: Contbillchghead = null;

  contBills: Contbillchghead[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contbillchgheadService: ContbillchgheadService,
    public router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit(): void {
    this.cols = [

      { field: 'head_id', header: 'Head ID' },
      { field: 'chg_head', header: 'Charge Head' },
      { field: 'chg_head_name', header: 'Charge Head Name' },

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contBill)
    }

    const request = this.contbillchgheadService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.contBills = msg.payLoad;

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contbillchghead']);
  });
  }

  editContBillChgHead(contBill): void {
    this.contbillchgheadService.passTheValue(contBill);
  }

}
