import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Contbillchghead } from 'src/app/models/Contbillchghead';
import { DataService } from 'src/app/data/data.service';
import { ContbillchgheadService } from 'src/app/services/contbillchghead.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contbillchgheadinsert',
  templateUrl: './contbillchgheadinsert.component.html',
  styleUrls: ['./contbillchgheadinsert.component.scss']
})
export class ContbillchgheadinsertComponent implements OnInit {
  public user: User;
  selectedTypeId: SelectItem[];

  currentFileUpload: File;
  options: boolean = false;
  contBill:Contbillchghead=new Contbillchghead();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;

  contBillStatus = [
    { value: '', label: 'Select status' },
    { value: 'A', label: 'Active' },
    { value: 'I', label: 'Inactive' }
];

  constructor(
    private dataService: DataService,
    private contbillchgheadService: ContbillchgheadService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.contbillchgheadService.addToTheList.subscribe(
      data =>{
        
       this.contBill = data;
       this.options = false;
      }
    )
  }

  createContBillChgHead(): void {

    if(!this.contBill.chg_head_name){
      this.severity = 'error';
      this.detailMsg = "Charge head name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contBill.entrydt = new Date(date);
    this.contBill.updatedt = new Date('01/01/1970');
    this.contBill.updateusr = "";
    this.contBill.entryusr = this.user.userId;
    
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contBill)
    }

    this.contbillchgheadService.insertContBillChgHead(JSON.stringify(this.jsonMessage));
    this.reset(this.contBill);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contbillchghead']);
  // });
    
  };


  updateContBillChgHead(contBill): void {


    if(!this.contBill.chg_head_name){
      this.severity = 'error';
      this.detailMsg = "Charge head name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    var today = new Date();
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contBill.updatedt = new Date(date);
    this.contBill.updateusr = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contBill)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contbillchgheadService.updateContBillChgHead(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });
    this.reset(this.contBill);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contbillchghead']);
  // });
  };


  deleteContBillChgHead(contBill): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(contBill)
    }
    this.contbillchgheadService.deletecontbillchghead(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contbillchghead']);
  // });
  this.reset(this.contBill);
  };

  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.contBill  = new Contbillchghead();
      form.reset({ status: this.contBill.status });
      form.reset({ options: false });
    }
}



}
