import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { SelectItem } from 'primeng';
import { Contractbilltran } from 'src/app/models/Contractbilltran';
import { DataService } from 'src/app/data/data.service';
import { ContractbilltransactionService } from 'src/app/services/contbilltran.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bill-auth',
  templateUrl: './bill-auth.component.html',
  styleUrls: ['./bill-auth.component.scss']
})
export class BillAuthComponent implements OnInit {
  public user: User;
  ex:any ;
  selectedBillEnt: SelectItem[];
  selecteEntry: SelectItem[];
  selecteBillChargeHeadList: SelectItem[];
  selecteItemIdList: string[] = [];
  entryList: any;
  tranList:any;
  currentFileUpload: File;
  options: boolean = false;
  contTran:Contractbilltran=new Contractbilltran();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;


  constructor(
    private dataService: DataService,
    private contractbilltransactionService: ContractbilltransactionService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {

    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.contractbilltransactionService.addToTheList.subscribe(
      data =>{
       this.contTran = data;
       this.getValuesByEntry(this.contTran.entry_id);
       this.options = false;
      }
    )


    this.selecteEntry = [];
    this.selecteEntry.push({ label: 'Select Entry ID', value: -1 });




    this.getBillEntry();
    this.getBillTran();

    // if(this.contTran.cont_id!=null){
    //   this.getAllItemId(this.contTran.cont_id);
    // }
  }


  getBillTran() {

    const request = this.contractbilltransactionService.getInitialBillTran()
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        this.tranList = msg;
        for (let i = 0; i < msg.length; i++) {
          this.selecteEntry.push({ label: msg[i].entry_id, value: msg[i].entry_id });
       

        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }


  


  getAllItemId(entry: string) {
    this.selecteItemIdList = [];
    // this.selecteItemIdList.push({ label: 'Select Item Id', value: -1 });
    // this.selecteItemIdList.push({ label: 'All', value: 0 });
    const request = this.contractbilltransactionService.getItemByEntry(entry);
    request.subscribe(
      (res) => {
        const msg = JSON.parse(res);
        for (let i = 0; i < msg.length; i++) {
          this.selecteItemIdList.push(msg[i][0]+"("+msg[i][1]+")");
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );

  }


//   async createContBillTran(): Promise<void> {
//   //   debugger;
//   this.ex = null;
//     if(!this.contTran.entry_id){
//       this.severity = 'error';
//       this.detailMsg = "Entry id cannot be empty";
//       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
//       return;
//     }
     
//      await this.contractbilltransactionService.exists(this.contTran.entry_id).then(
//       ( async res  => {
//               if(res=="true"){
//                 this.ex = true;
                
//               }else{
//                 this.ex = false;
//               }
              
              
//   }));

//       if(this.ex){
//         this.severity = 'error';
//         this.detailMsg = "Duplicate entry not allowed";
//         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
//         return;
//       }
//    else{
     
//     // if(!this.contTran.penalty){
//     //   this.severity = 'error';
//     //   this.detailMsg = "Penalty amount cannot be empty";
//     //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
//     //   return;
//     // }

//     if(!this.contTran.chg_head){
//       this.severity = 'error';
//       this.detailMsg = "Charge head cannot be empty";
//       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
//       return;
//     }

   

//     var today = new Date();
//     //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
//     //var date = today.toUTCString();
//     //this.vendor.entryDt = new Date(date);
//     var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
//     this.contTran.entrydt = new Date(date);
//     this.contTran.updatedt = new Date('01/01/1970');
//     this.contTran.updateusr = "";
//     this.contTran.entryusr = this.user.userId;
//     this.contTran.status = "";
      
//     var dataHeader = this.dataService.createMessageHeader("SAVE");
//     this.jsonMessage = {
//         dataHeader : dataHeader,
//         payLoad: new Array(this.contTran)
//     }

//     this.contractbilltransactionService.insertContractBillTransaction(JSON.stringify(this.jsonMessage));

//     this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
//       this.router.navigate(['contBillTran']);
//   });
// }
//   };


billTranAuth(contTran): void {
    if(!this.contTran.entry_id){
      this.severity = 'error';
      this.detailMsg = "Entry id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    
    this.contTran.bill_authorize_id = this.user.userId;
    this.contTran.status="auth";

    var dataHeader = this.dataService.createMessageHeader("AUTH");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contTran)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractbilltransactionService.updateContractBillTransaction(JSON.stringify(this.jsonMessage));


  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['billAuth']);
  // });\
  this.reset(this.contTran);
  };


  

  


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.contTran  = new Contractbilltran();
      form.reset({ status: this.contTran.status });
      form.reset({ options: false });
    }
}


getValuesByEntry(entryId: string) {
  
    // else{
      // this.contTran=null;
    
    for(let i = 0; i<  this.entryList.length; i++) {
      if(entryId==this.entryList[i].entry_id) {

        this.contTran.bill_no =this.entryList[i].bill_no;
            this.contTran.cont_id = this.entryList[i].cont_id;
            this.contTran.bill_part_full = this.entryList[i].part_full;
            this.contTran.bill_date =this.entryList[i].bill_date;
            this.getAllItemId(this.contTran.entry_id);
            // this.getBillTran(this.contTran.entry_id);
            this.contTran.current_bill=this.entryList[i].billamount;
            this.contTran.bill_initiate_id=this.entryList[i].entryusr;
      } 
    }
    for(let i = 0; i<  this.tranList.length; i++) {
      if(entryId==this.tranList[i].entry_id) {

        this.contTran.bill_tran_id= this.tranList[i].bill_tran_id;
        this.contTran.chg_head=this.tranList[i].chg_head;
        this.contTran.bill_period_start=this.tranList[i].bill_period_start;
        this.contTran.bill_period_end=this.tranList[i].bill_period_end;
        this.contTran.vat=this.tranList[i].vat;
        this.contTran.bill_after_vat=this.tranList[i].bill_after_vat;
        this.contTran.accum_bill_after_vat=this.tranList[i].accum_bill_after_vat;
        this.contTran.current_income_tax=this.tranList[i].current_income_tax;
        this.contTran.accum_income_tax=this.tranList[i].accum_income_tax;
        this.contTran.penalty=this.tranList[i].penalty;
        this.contTran.final_bill_vendor=this.tranList[i].final_bill_vendor;
        this.contTran.contract_no=this.tranList[i].contract_no;
        this.contTran.reference_no=this.tranList[i].reference_no;
        this.contTran.reference_date=this.tranList[i].reference_date;
        this.contTran.narration=this.tranList[i].narration;
        this.contTran.next_bill_date=this.tranList[i].next_bill_date;
        this.contTran.status=this.tranList[i].status;
        this.contTran.bill_initiate_id=this.tranList[i].bill_initiate_id;
        this.contTran.bill_authorize_id=this.tranList[i].bill_authorize_id;
        this.contTran.entryusr=this.tranList[i].entryusr;
        this.contTran.entrydt=this.tranList[i].entrydt;
        this.contTran.updateusr=this.tranList[i].updateusr;
        this.contTran.updatedt=this.tranList[i].updatedt;
        this.contTran.entry_id=this.tranList[i].entry_id;

      } 
    }


    }

getBillEntry() {

  const request = this.contractbilltransactionService.getBillEntry()
  request.subscribe(
    (res) => {

      const msg = JSON.parse(res);
      this.entryList = msg;

    },
    err => {
      this.severity = 'error';
      this.detailMsg = "Server Error: " + err.message;
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
  );


}
}
