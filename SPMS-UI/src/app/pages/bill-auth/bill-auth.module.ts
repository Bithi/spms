import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillAuthRoutingModule } from './bill-auth-routing.module';
import { BillAuthComponent } from './bill-auth/bill-auth.component';
import { BillAuthListComponent } from './bill-auth-list/bill-auth-list.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';



@NgModule({
  declarations: [BillAuthComponent, BillAuthListComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,
  ]
})
export class BillAuthModule { }
