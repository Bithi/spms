import { Component, OnInit, ViewChild } from '@angular/core';
import { Contractbilltran } from 'src/app/models/Contractbilltran';
import { ContractbilltransactionService } from 'src/app/services/contbilltran.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-bill-auth-list',
  templateUrl: './bill-auth-list.component.html',
  styleUrls: ['./bill-auth-list.component.scss']
})
export class BillAuthListComponent implements OnInit { 
  @ViewChild(Table) dt: Table;
  contTran: Contractbilltran = null;

  contTrans: Contractbilltran[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contractbilltransactionService: ContractbilltransactionService,
    public router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit(): void {
    this.cols = [

      { field: 'bill_tran_id', header: 'Bill Transaction ID' },
      { field: 'entry_id', header: 'Entry ID' },
      { field: 'chg_head', header: 'Charge head' },
      { field: 'final_bill_vendor', header: 'Final Bill' }
    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contTran)
    }

    const request = this.contractbilltransactionService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.contTrans = msg.payLoad;

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }


  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contBillTran']);
  });
  }

  editContBillTran(contBill): void {
    this.contractbilltransactionService.passTheValue(contBill);
  }

}
