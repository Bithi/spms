import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModelRoutingModule } from './model-routing.module';
import { ModelCreateComponent } from './model-create/model-create.component';
import { ModelListComponent } from './model-list/model-list.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';


@NgModule({
  declarations: [ModelCreateComponent, ModelListComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,
    ModelRoutingModule
  ]
})
export class ModelModule { }
