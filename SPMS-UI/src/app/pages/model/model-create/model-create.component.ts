import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { SelectItem, Table } from 'primeng';
import { Model } from 'src/app/models/model';
import { DataService } from 'src/app/data/data.service';
import { BrandService } from 'src/app/services/brand.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { ModelService } from 'src/app/services/model.service';

@Component({
  selector: 'app-model-create',
  templateUrl: './model-create.component.html',
  styleUrls: ['./model-create.component.scss']
})
export class ModelCreateComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  public user: User;
  selectedBrand: SelectItem[];

  currentFileUpload: File;
  options: boolean = false;
  modelInfo:Model=new Model();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  constructor(    private dataService: DataService,
    private brandService: BrandService,
    private modelService: ModelService,
    private utility:Utility,
    public router:Router,) { }

  ngOnInit(): void {

    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.modelService.addToTheList.subscribe(
      data =>{
        
       this.modelInfo = data;
       this.options = false;
      }
    )


    this.selectedBrand = [];
    this.selectedBrand.push({ label: 'Select Brand', value: -1 });

    this.getBrand();
  }

  getBrand() {
    const request = this.modelService.getAllBrand();
    request.subscribe(
      (res) => {
  
        const msg = JSON.parse(res);
  
        for (let i = 0; i < msg.length; i++) {
          this.selectedBrand.push(({ label: msg[i].brandName, value: msg[i].brandId }));
          // console.log( this.vendorList);
        }
  
  
  
  
  
      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  createModel(): void {

    var str;
    var cleanStr;
    if(this.modelInfo.modelName){
       str=this.modelInfo.modelName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.modelInfo.modelName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "Model name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    if(!this.modelInfo.brandId){
      this.severity = 'error';
      this.detailMsg = "Brand cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
   
    if((!this.modelInfo.warrantyInMonth)||this.modelInfo.warrantyInMonth.toString()==""){
      this.modelInfo.warrantyInMonth=0;
    }

    // var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.modelInfo.entryDt = new Date();
    this.modelInfo.updateDt = new Date('01/01/1970');
    this.modelInfo.updateUser = "";
    this.modelInfo.entryUser = this.user.userId;
    
     
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.modelInfo)
    }

    // if(this.modelService.getDuplicateEntryByModelNameWithBrand(this.modelInfo)){
    //   debugger;
    //   this.severity = 'error';
    //   this.detailMsg = "Brand model duplicate";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    // //   return;
    // };

    this.modelService.insertModel(JSON.stringify(this.jsonMessage));

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['model']);
  // });
  this.reset(this.modelInfo);
  };

  updateModel(modelInfo): void {


    var str;
    var cleanStr;
    if(this.modelInfo.modelName){
       str=this.modelInfo.modelName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.modelInfo.modelName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "Model name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    debugger;
    var noData='-1';
    if(this.modelInfo.brandId==noData){
      this.severity = 'error';
      this.detailMsg = "Brand cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.modelInfo.warrantyInMonth){
      this.modelInfo.warrantyInMonth=0;
   }
    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.modelInfo.updateDt = new Date();
    this.modelInfo.updateUser = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.modelInfo)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.modelService.updateModel(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['model']);
  // });
  this.reset(this.modelInfo);
  };

  deleteModel(modelInfo): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(modelInfo)
    }
    this.modelService.deleteModel(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['model']);
  // });
  this.reset(this.modelInfo);
   
  };


  reset(form) {
  
      this.modelInfo  = new Model();
    }

}
