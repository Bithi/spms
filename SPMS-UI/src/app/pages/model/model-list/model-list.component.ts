import { Component, OnInit, ViewChild } from '@angular/core';
import { Model } from 'src/app/models/model';
import { ModelService } from 'src/app/services/model.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.scss']
})
export class ModelListComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  model: Model = null;

  models: Model[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  constructor( private modelService: ModelService,
    public router: Router,
    private dataService: DataService,) { }

  ngOnInit(): void {
    
    this.cols = [

      { field: 'brandName', header: 'Brand' },
      { field: 'modelName', header: 'Model Name' },

    ];

    

    const request = this.modelService.getModelBrandJoin();
    request.subscribe(
      (res) => {
        const msg = JSON.parse(res) ;
        this.models =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.model = new Model;
          this.model.modelId= msg[i][0];
          this.model.brandId= msg[i][1];
          this.model.modelName= msg[i][2];
          this.model.warrantyInMonth= msg[i][3];
          this.model.entryDt= msg[i][4];
          this.model.entryUser= msg[i][5];
          this.model.updateUser= msg[i][6];
          this.model.updateDt= msg[i][7];
          this.model.brandName= msg[i][8];
       

          this.models[i] = this.model;
       
        }


      },
      err => {
        
        return;
      }
    );
  }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['model']);
  });
  }

  editModel(model): void {
    this.modelService.passTheValue(model);
  }

}
