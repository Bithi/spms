import { Component, OnInit, ViewChild } from '@angular/core';
import { Itemtype } from 'src/app/models/itemtype';
import { ItemTypeService } from 'src/app/services/itemtype.service ';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-item-type',
  templateUrl: './item-type.component.html',
  styleUrls: ['./item-type.component.scss']
})
export class ItemTypeComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  itemType: Itemtype = null;

  itemTypes: Itemtype[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };


  constructor(
    private itemTypeService: ItemTypeService,
    public router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit(): void {

    this.cols = [

      { field: 'shortName', header: 'Short Name' },
      { field: 'category', header: 'Category' }

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.itemType)
    }

    const request = this.itemTypeService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.itemTypes = msg.payLoad;
        
        console.log(this.itemTypes);

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }


  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['itemType']);
  });
  }

  editItemType(itemtype): void {
    this.itemTypeService.passTheValue(itemtype);
  }

}
