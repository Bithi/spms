import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemTypeRoutingModule } from './item-type-routing.module';
import { ItemTypeInsertComponent } from './item-type-insert/item-type-insert.component';
import { ItemTypeComponent } from './item-type/item-type.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';


@NgModule({
  declarations: [ItemTypeInsertComponent, ItemTypeComponent],
  imports: [
    CommonModule,
    ItemTypeRoutingModule,
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule
  ]
})
export class ItemTypeModule { }
