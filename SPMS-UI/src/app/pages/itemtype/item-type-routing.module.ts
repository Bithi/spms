import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemTypeInsertComponent } from './item-type-insert/item-type-insert.component';


const routes: Routes = [
  {path: '', component: ItemTypeInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemTypeRoutingModule { }
