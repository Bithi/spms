import { Component, OnInit } from '@angular/core';
import { Itemtype } from 'src/app/models/itemtype';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { ItemTypeService } from 'src/app/services/itemtype.service ';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-item-type-insert',
  templateUrl: './item-type-insert.component.html',
  styleUrls: ['./item-type-insert.component.scss']
})
export class ItemTypeInsertComponent implements OnInit {

  public user: User;

  currentFileUpload: File;
  options: boolean = false;
  itemtype:Itemtype=new Itemtype();
  jsonMessage:any;
  key:string = "toast";
    severity:string;
    detailMsg:string;

    itemTypeCategory = [
      { value: '', label: 'Select Category' },
      { value: 'B', label: 'Building' },
      { value: 'L', label: 'Land' },
      { value: 'O', label: 'Others' },
      { value: 'C', label: 'Building Construction' },
  ];

  constructor(
    private dataService: DataService,
    private itemTypeService: ItemTypeService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.itemTypeService.addToTheList.subscribe(
      data =>{
        if(data.category=='B') this.itemtype.category = 'Building';
        else if(data.category==='L') this.itemtype.category = 'Land';
        else if(data.category==='O') this.itemtype.category = 'Others';
        else if(data.category==='C') this.itemtype.category = 'Building Construction';
       this.itemtype = data;
       this.options = false;
      }
    )
  }

  createItemType(): void {
    var str;
    var cleanStr;
    if(this.itemtype.shortName){
       str=this.itemtype.shortName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.itemtype.shortName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "short name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.itemtype.category){
      this.severity = 'error';
      this.detailMsg = "Category cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    // if(!this.itemtype.shortName){
    //   this.severity = 'error';
    //   this.detailMsg = "Type short name cannot be empty";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;
    // }


    // var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.itemtype.entryDate = new Date();
    this.itemtype.updateDt = new Date('01/01/1970');
    this.itemtype.updateUser = "";
    this.itemtype.entryUser = this.user.userId;
    // this.itemtype.typeId="hello";
    
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.itemtype)
    }

    this.itemTypeService.insertItemType(JSON.stringify(this.jsonMessage));
    this.reset(this.itemtype);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['itemType']);
  // });
    
  };



  updateItemType(itemtype): void {


    var str;
    var cleanStr;
    if(this.itemtype.shortName){
       str=this.itemtype.shortName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.itemtype.shortName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "short name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.itemtype.category){
      this.severity = 'error';
      this.detailMsg = "Category cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.itemtype.updateDt = new Date();
    this.itemtype.updateUser = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.itemtype)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.itemTypeService.updateRoleInfo(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['itemType']);
  });
  };


  deleteItemType(itemtype): void {
   
    

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(itemtype)
    }
    this.itemTypeService.deleteRoleInfo(JSON.stringify(this.jsonMessage));
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['itemType']);
  });
   
  };



  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.itemtype  = new Itemtype();
      form.reset({ type_shortname: this.itemtype.shortName });
      form.reset({ status: this.itemtype.description });
      form.reset({ options: false });
    }
}

  



}
