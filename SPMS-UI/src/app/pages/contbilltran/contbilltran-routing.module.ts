
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DropdownModule, TableModule } from 'primeng';
import { LayoutModule } from 'src/app/layout/layout.module';
import { ContbilltranInfoComponent } from './contbilltran-info/contbilltran-info.component';
import { ContbilltranInsertComponent } from './contbilltran-insert/contbilltran-insert.component';


const routes: Routes = [
  {path: '', component: ContbilltranInsertComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContbilltranRoutingModule { }
