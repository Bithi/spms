import { Component, OnInit, ViewChild } from '@angular/core';
import { Contractbilltran } from 'src/app/models/Contractbilltran';
import { ContractbilltransactionService } from 'src/app/services/contbilltran.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-contbilltran-info',
  templateUrl: './contbilltran-info.component.html',
  styleUrls: ['./contbilltran-info.component.scss']
})
export class ContbilltranInfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  contTran: Contractbilltran = null;

  contTrans: Contractbilltran[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contractbilltransactionService: ContractbilltransactionService,
    public router: Router,
    private dataService: DataService,
  ) { }


  getContractBillTransaction() {

    var request = this.contractbilltransactionService.getContractBillTransactionList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.contTrans =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.contTran = new Contractbilltran;
         
        debugger;
          this.contTran.bill_tran_id= msg[i][0];
          this.contTran.chg_head= msg[i][1];
          this.contTran.bill_period_start= msg[i][2];
          this.contTran.current_bill= msg[i][3];
          this.contTran.vat= msg[i][4];
          this.contTran.bill_after_vat= msg[i][5];
          this.contTran.accum_bill_after_vat= msg[i][6];
          this.contTran.current_income_tax= msg[i][7]; 
          this.contTran.accum_income_tax= msg[i][8];
          this.contTran.penalty= msg[i][9];
          this.contTran.final_bill_vendor= msg[i][10];
          this.contTran.contract_no= msg[i][11];
          this.contTran.reference_no= msg[i][12]; 
          this.contTran.reference_date= msg[i][13];
          this.contTran.narration= msg[i][14];
          this.contTran.next_bill_date= msg[i][15];
          this.contTran.status= msg[i][16];
          this.contTran.bill_initiate_id= msg[i][17]; 
          this.contTran.bill_authorize_id= msg[i][18];
          this.contTran.entryusr= msg[i][19];
          this.contTran.entrydt= msg[i][20];
          this.contTran.updateusr= msg[i][21];
          this.contTran.updatedt= msg[i][22];
          this.contTran.entry_id= msg[i][23];
          this.contTran.chgHeadName= msg[i][24];
          this.contTran.bill_period_end=msg[i][25];
            
         
          this.contTrans[i] = this.contTran;
          // console.log( this.vendorList);
     }
    });

  }


  ngOnInit(): void {
    this.getContractBillTransaction();
    this.cols = [

      { field: 'bill_tran_id', header: 'Bill Transaction ID' },
      { field: 'entry_id', header: 'Entry ID' },
      { field: 'chgHeadName', header: 'Charge head name' },
      { field: 'final_bill_vendor', header: 'Final Bill' }
    ];

    
  }


  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contBillTran']);
  });
  }

  editContBillTran(contBill): void {
    this.contractbilltransactionService.passTheValue(contBill);
  }

}
