import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContbilltranRoutingModule } from './contbilltran-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';
import { ContbilltranInfoComponent } from './contbilltran-info/contbilltran-info.component';
import { ContbilltranInsertComponent } from './contbilltran-insert/contbilltran-insert.component';


@NgModule({
  declarations: [ContbilltranInfoComponent,ContbilltranInsertComponent],
  imports: [
    CommonModule,
    ContbilltranRoutingModule,
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule
  ],
  exports: [ContbilltranInfoComponent,ContbilltranInsertComponent]
})
export class ContbilltranModule { }
