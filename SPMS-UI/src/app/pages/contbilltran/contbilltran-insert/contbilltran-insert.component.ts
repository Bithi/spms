import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Contractbilltran } from 'src/app/models/Contractbilltran';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { Utility } from 'src/app/util/utility';
import { ContractbilltransactionService } from 'src/app/services/contbilltran.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { Contractbillentry } from 'src/app/models/Contractbillentry';
import { exists } from 'fs';
import { ContractbillentryService } from 'src/app/services/contractbillentry.service';
import { PenaltyCalc } from 'src/app/models/PenaltyCalc';

@Component({
  selector: 'app-contbilltran-insert',
  templateUrl: './contbilltran-insert.component.html',
  styleUrls: ['./contbilltran-insert.component.scss']
})
export class ContbilltranInsertComponent implements OnInit {

  public user: User;
  ex:any ;
  penaltyCalc:PenaltyCalc = new PenaltyCalc();
  selectedCon: SelectItem[];
  selecteEntry: SelectItem[];
  selectedvendor: SelectItem[];
  contractTypeSet = new Map();
  contractNameSet= new Map();
  selecteBillChargeHeadList: SelectItem[];
  selecteItemIdList: string[] = [];
  entryList: any;
  currentFileUpload: File;
  options: boolean = false;
  contTran:Contractbilltran=new Contractbilltran();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  cont_id:string;
  venID:string;


  constructor(
    private dataService: DataService,
    private contractbilltransactionService: ContractbilltransactionService,
    private contractbillentryService: ContractbillentryService,
    private utility:Utility,
    public router:Router,   private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.contractbilltransactionService.addToTheList.subscribe(
      data =>{
       this.contTran = data;
       this.getAnotherFourVlues(this.contTran.entry_id);
       this.options = false;
      }
    )
    this.selectedvendor=[];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.getVendorList();
    // this.selectedCon = [];
    // this.selectedCon.push({ label: 'Select Contract ', value: -1 });
    // this.getContractListInfo();

    // this.getEntryBycon(this.cont_id);
    this.selecteEntry = [];
    this.selecteEntry.push({ label: 'Select Entry ', value: -1 });

    this.selecteBillChargeHeadList = [];
    this.selecteBillChargeHeadList.push({ label: 'Select Bill Charge Head', value: -1 });




    // this.getBillEntry();
    this.getBillChargeHeadInfo();
    // if(this.contTran.cont_id!=null){
    //   this.getAllItemId(this.contTran.cont_id);
    // }
  }
getContractListInfo(){
  this.selectedCon = [];
  this.selectedCon.push({ label: 'Select Contract ', value: -1 });
    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;

              for(let i = 0; i< msg.length; i++) {
                this.selectedCon.push({label: msg[i].contNo, value: msg[i].contId});
           }


              
              
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );


}
  getEntryBycon(cont_id:string){
      this.selecteEntry = [];
       this.selecteEntry.push({ label: 'Select Entry', value: -1 });
      
      const request = this.contractbilltransactionService.getBillEntryBycontract(cont_id);
      request.subscribe(
        (res) => {
          // debugger;
          const msg = JSON.parse(res);
          this.entryList = msg;
          for (let i = 0; i < msg.length; i++) {
          this.selecteEntry.push({ label: msg[i].bill_no+"("+msg[i].entry_id+")", value: msg[i].entry_id });
       

        }
  
        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
  }
  getBillEntry() {

    const request = this.contractbilltransactionService.getBillEntry()
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        this.entryList = msg;
        for (let i = 0; i < msg.length; i++) {
          this.selecteEntry.push({ label: msg[i].bill_no+"("+msg[i].entry_id+")", value: msg[i].entry_id });
       

        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }


  getBillChargeHeadInfo() {

    const request = this.contractbilltransactionService.getBillChargeHead();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteBillChargeHeadList.push({ label: msg[i].chg_head_name, value: msg[i].head_id });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }


  getAllItemId(entry: string) {
    this.selecteItemIdList = [];
    // this.selecteItemIdList.push({ label: 'Select Item Id', value: -1 });
    // this.selecteItemIdList.push({ label: 'All', value: 0 });
    const request = this.contractbilltransactionService.getItemByEntry(entry);
    request.subscribe(
      (res) => {
        debugger;
        const msg = JSON.parse(res);
        for (let i = 0; i < msg.length; i++) {
          this.selecteItemIdList.push(msg[i][0]+"("+msg[i][1]+")");
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );

  }


  async createContBillTran(): Promise<void> {
  //   debugger;
  this.ex = null;
    if(!this.contTran.entry_id){
      this.severity = 'error';
      this.detailMsg = "Entry id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
     
     await this.contractbilltransactionService.exists(this.contTran.entry_id).then(
      ( async res  => {
              if(res=="true"){
                this.ex = true;
                
              }else{
                this.ex = false;
              }
              
              
  }));

      if(this.ex){
        this.severity = 'error';
        this.detailMsg = "Duplicate entry not allowed";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
   else{
     
    // if(!this.contTran.penalty){
    //   this.severity = 'error';
    //   this.detailMsg = "Penalty amount cannot be empty";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;
    // }

    if(!this.contTran.chg_head){
      this.severity = 'error';
      this.detailMsg = "Charge head cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

   

    var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contTran.entrydt = new Date(date);
    this.contTran.updatedt = new Date('01/01/1970');
    this.contTran.updateusr = "";
    this.contTran.entryusr = this.user.userId;
    this.contTran.status = "";
    debugger;
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contTran),
        other: this.cont_id
    }

    this.contractbilltransactionService.insertContractBillTransaction(JSON.stringify(this.jsonMessage));
    this.reset(this.contTran);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contBillTran']);
  // });
}
  };


  updateContBillTran(contTran): void {
    if(!this.contTran.entry_id){
      this.severity = 'error';
      this.detailMsg = "Entry id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    // if(!this.contTran.penalty){
    //   this.severity = 'error';
    //   this.detailMsg = "Penalty amount cannot be empty";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;
    // }

    if(!this.contTran.chg_head){
      this.severity = 'error';
      this.detailMsg = "Charge head cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    var today = new Date();
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contTran.updatedt = new Date(date);
    this.contTran.updateusr = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contTran)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractbilltransactionService.updateContractBillTransaction(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contBillTran']);
  // });
  this.reset(this.contTran);
  };


  deleteContBillTran(contTran): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(contTran)
    }
    this.contractbilltransactionService.deleteContractBillTransaction(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contBillTran']);
  // });
  this.reset(this.contTran);
  };

  getNetPenaltyForBillPay(penaltyCalc): void {

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(penaltyCalc)
    }
    const request = this.contractbilltransactionService.getNetPenaltyForBillPay(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse( atob(res) );
        this.contTran.penalty=msg.payLoad[0].netPenalty;

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
 
   
  };
   

  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.contTran  = new Contractbilltran();
      form.reset({ status: this.contTran.status });
      form.reset({ options: false });
    }
}
getVendorList() {
   
  const request = this.dropdownInfoService.getVendors();
  request.subscribe(
    (res) => {

      const msg = JSON.parse(res);
      
      for (let i = 0; i < msg.length; i++) {
       
        this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
        //this.vendorNameSet.set(msg[i].venId, msg[i].venName);
        // //console.log( this.vendorList);
      }

  


    },
    err => {
      // this.severity = 'error';
      // this.detailMsg = "Server Error: " + err.message;
      // // this.showProgressSpin = false;
      // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
  );


}


getAnotherFourVlues(entryId: string) {
  
    // else{

    
    for(let i = 0; i<  this.entryList.length; i++) {
      if(entryId==this.entryList[i].entry_id) {
        this.contTran.bill_no =this.entryList[i].bill_no;
            this.contTran.cont_id = this.entryList[i].cont_id;
            this.contTran.bill_part_full = this.entryList[i].part_full;
            this.contTran.bill_date =this.entryList[i].bill_date.substring(0,10);
            this.getAllItemId(this.contTran.entry_id);
            this.contTran.current_bill=this.entryList[i].billamount;
            this.contTran.bill_initiate_id=this.entryList[i].entryusr;
            this.penaltyCalc.contId= this.contTran.cont_id ;
            this.penaltyCalc.bill_period_start=this.contTran.bill_period_start;
            this.penaltyCalc.bill_period_end=this.contTran.bill_period_end;
            this.getNetPenaltyForBillPay(this.penaltyCalc);
      } 
    }
    }
    getCon(venID){
      this.selectedCon = [];
    this.selectedCon.push({ label: 'Select Contract ID', value: -1 });
      const request = this.dropdownInfoService.getContracts(venID);
      request.subscribe(
        (res) => {
  
          const msg = JSON.parse(res);
            for(let i = 0; i< msg.length; i++) {
              this.selectedCon.push({label: msg[i].contNo, value: msg[i].contId});
              
              this.contractTypeSet.set(msg[i].contId, msg[i].type);
              this.contractNameSet.set(msg[i].contId, msg[i].contNo);
         }
  
        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
   
    }
   
  // }
  
}

// }
