import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandInfoInsertComponent } from './brand-info-insert/brand-info-insert.component';


const routes: Routes = [
  {path: '', component: BrandInfoInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }
