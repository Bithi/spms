import { Component, OnInit, ViewChild } from '@angular/core';
import { Brand } from 'src/app/models/brand';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { BrandService } from 'src/app/services/brand.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-brand-info',
  templateUrl: './brand-info.component.html',
  styleUrls: ['./brand-info.component.scss']
})
export class BrandInfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  brand: Brand = null;

  brands: Brand[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private brandService: BrandService,
    public router: Router,
    private dataService: DataService,
  ) { }


  getBrand() {

    var request = this.brandService.getBrandList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.brands =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.brand = new Brand;
          this.brand.brandId= msg[i][0];
          this.brand.itemId= msg[i][1];
          this.brand.brandName= msg[i][2];
          this.brand.entryDt= msg[i][3];
          this.brand.entryUser= msg[i][4];
          this.brand.updateUser= msg[i][5];
          this.brand.updateDt= msg[i][6];
          this.brand.itemName= msg[i][7];

          this.brands[i] = this.brand;
          // console.log( this.vendorList);
          debugger;
     }
    });

  }



  ngOnInit(): void {
    this.getBrand();
    this.cols = [

      { field: 'itemName', header: 'Item Name' },
      { field: 'brandName', header: 'Brand Name' },

    ];

    // var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    // var jsonMessage = {
    //   dataHeader: dataHeader,
    //   payLoad: new Array(this.brand)
    // }

    // const request = this.brandService.showBrand(JSON.stringify(jsonMessage));
    // request.subscribe(
    //   (res) => {

    //     const msg = JSON.parse(atob(res));
    //     console.log(msg);
    //     this.brands = msg.payLoad;

    //     if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
    //       return;
    //     }
       



    //   },
    //   err => {
        
    //     return;
    //   }
    // );
  }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['brand']);
  });
  }

  editBrand(brand): void {
    this.brandService.passTheValue(brand);
  }

}
