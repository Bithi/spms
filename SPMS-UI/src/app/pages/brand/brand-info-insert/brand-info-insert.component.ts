import { Component, OnInit } from '@angular/core';
import { Brand } from 'src/app/models/brand';
import { MessageService, SelectItem } from 'primeng/api';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { BrandService } from 'src/app/services/brand.service';
import { User } from 'src/app/models/user';


@Component({
  selector: 'app-brand-info-insert',
  templateUrl: './brand-info-insert.component.html',
  styleUrls: ['./brand-info-insert.component.scss']
})
export class BrandInfoInsertComponent implements OnInit {

  public user: User;
  selectedItemId: SelectItem[];

  currentFileUpload: File;
  options: boolean = false;
  brandInfo:Brand=new Brand();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;

  constructor(
    private dataService: DataService,
    private brandService: BrandService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.brandService.addToTheList.subscribe(
      data =>{
        
       this.brandInfo = data;
       this.options = false;
      }
    )


    this.selectedItemId = [];
    this.selectedItemId.push({ label: 'Select Item', value: -1 });

    this.getItemId();
  }


  getItemId() {
    const request = this.brandService.getAllItemId();
    request.subscribe(
      (res) => {
  
        const msg = JSON.parse(res);
  
        for (let i = 0; i < msg.length; i++) {
          this.selectedItemId.push(({ label: msg[i].itemName, value: msg[i].itemId }));
          // console.log( this.vendorList);
        }
  
  
  
  
  
      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }


  createBrand(): void {
    var str;
    var cleanStr;
    if(this.brandInfo.brandName){
       str=this.brandInfo.brandName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.brandInfo.brandName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "Brand name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.brandInfo.itemId){
      this.severity = 'error';
      this.detailMsg = "Item cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    // var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.brandInfo.entryDt = new Date();
    this.brandInfo.updateDt = new Date('01/01/1970');
    this.brandInfo.updateUser = "";
    this.brandInfo.entryUser = this.user.userId;
    
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.brandInfo)
    }

    this.brandService.insertBrand(JSON.stringify(this.jsonMessage));
    this.reset(this.brandInfo);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['brand']);
  // });
    
  };


  updateBrand(brandInfo): void {


    var str;
    var cleanStr;
    if(this.brandInfo.brandName){
       str=this.brandInfo.brandName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.brandInfo.brandName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "Brand name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.brandInfo.itemId){
      this.severity = 'error';
      this.detailMsg = "Item cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.brandInfo.updateDt = new Date();
    this.brandInfo.updateUser = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.brandInfo)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.brandService.updateBrand(JSON.stringify(this.jsonMessage));

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['brand']);
  // });
  this.reset(this.brandInfo);
  };


  deleteBrand(brandInfo): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(brandInfo)
    }
    this.brandService.deleteBrand(JSON.stringify(this.jsonMessage));
    this.reset(this.brandInfo);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['brand']);
  // });
   
  };

  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.brandInfo  = new Brand();
      form.reset({ brandName: this.brandInfo.brandName });
      form.reset({ itemId: this.brandInfo.itemId });
      form.reset({ options: false });
    }
}



}
