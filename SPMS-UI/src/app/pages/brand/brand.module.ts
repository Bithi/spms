import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandRoutingModule } from './brand-routing.module';
import { BrandInfoComponent } from './brand-info/brand-info.component';
import { BrandInfoInsertComponent } from './brand-info-insert/brand-info-insert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';


@NgModule({
  declarations: [BrandInfoComponent, BrandInfoInsertComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,BrandRoutingModule
  ]
})
export class BrandModule { }
