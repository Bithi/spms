import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BudgetbillpaymentComponent } from './budgetbillpayment/budgetbillpayment.component';
import { BudgetBillPaymentRoutingModule } from './budgetbillpayment/budget-bill-payment-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, MultiSelectModule, RadioButtonModule, TableModule } from 'primeng';



@NgModule({
  declarations: [BudgetbillpaymentComponent],
  imports: [
    CommonModule,
    BudgetBillPaymentRoutingModule,CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,MultiSelectModule,RadioButtonModule
  ]
})
export class BudgetbillpaymentModule { }
