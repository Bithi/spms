import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetbillpaymentComponent } from './budgetbillpayment.component';

describe('BudgetbillpaymentComponent', () => {
  let component: BudgetbillpaymentComponent;
  let fixture: ComponentFixture<BudgetbillpaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetbillpaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetbillpaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
