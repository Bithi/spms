import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetbillpaymentComponent } from './budgetbillpayment.component';


const routes: Routes = [
  {path: '', component: BudgetbillpaymentComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetBillPaymentRoutingModule { }
