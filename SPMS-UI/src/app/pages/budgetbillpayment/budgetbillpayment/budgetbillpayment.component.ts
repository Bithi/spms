import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng';
import { DataService } from 'src/app/data/data.service';
import { Budgetbillentry } from 'src/app/models/Budgetbillentry';
import { Budgetbillpayment } from 'src/app/models/Budgetbillpayment';
import { BudgetAllocationMap } from 'src/app/models/BudgetAllocationMap';
import { User } from 'src/app/models/user';
import { BudgetAllocationEntryService } from 'src/app/services/budgetAllocationEntry.service';
import { BudgetbillentryService } from 'src/app/services/budgetbillentry.service';
import { BudgetbillpaymentService } from 'src/app/services/budgetbillpayment.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-budgetbillpayment',
  templateUrl: './budgetbillpayment.component.html',
  styleUrls: ['./budgetbillpayment.component.scss']
})
export class BudgetbillpaymentComponent implements OnInit {

  budgetBillPayment: Budgetbillpayment = new Budgetbillpayment();
  
  selectedInvoice: SelectItem[];
  selecteContractList:SelectItem[];
  selectedvendor:SelectItem[];
  venId:string;
  wording: string;
  incexc:number=1;
  budgetBillEntry: Budgetbillentry = new Budgetbillentry();
  public user: User;
  jsonMessage: { dataHeader: any; payLoad: any; };
  key:string = "toast";
  severity:string;
  detailMsg:string;
  fiscal_year = [
    { value: null, label: 'Select  Year' },
    { value: '2022', label: '2022' },
    { value: '2023', label: '2023' },
    { value: '2024', label: '2024' },
    { value: '2025', label: '2025' },
    { value: '2026', label: '2026' },
  ];

  sectorList: SelectItem[];

  SectorYearwiseAllocationInfo: BudgetAllocationMap = new BudgetAllocationMap()
  SectorYearwiseAllocationInfos: BudgetAllocationMap[];
  insufficientFund: boolean = false;
  
  constructor(
    private dataService: DataService,
    public router:Router,
    private messageService:MessageService,
    private utility:Utility,
    private dropdownInfoService: DropdownInfoService,
    private budgetBillEntryService: BudgetbillentryService,
    private budgetBillPaymentService: BudgetbillpaymentService
    ,private budgetAllocationService: BudgetAllocationEntryService
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.selectedvendor = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.getVendors();

    this.sectorList = [];
    this.sectorList.push({ label: 'Select Sector', value: -1 });
    this.getSector();

    this.getYearWiseAllocation();


  }

  getYearWiseAllocation() {

    const request = this.budgetAllocationService.getFundRealtedList("getSectorAndYearWiseAllocation");
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res);
        this.SectorYearwiseAllocationInfos = [];

        for (let i = 0; i < msg.length; i++) {
          // tslint:disable-next-line:new-parens
          this.SectorYearwiseAllocationInfo = new BudgetAllocationMap;
          this.SectorYearwiseAllocationInfo.sectorId = msg[i][0];
          this.SectorYearwiseAllocationInfo.year = msg[i][1];
          this.SectorYearwiseAllocationInfo.amount = msg[i][2];
          this.SectorYearwiseAllocationInfo.use = msg[i][3];
          this.SectorYearwiseAllocationInfo.id = msg[i][4];

          this.SectorYearwiseAllocationInfos[i] = this.SectorYearwiseAllocationInfo;
        }
      });

      //console.log("DATA:"+JSON.stringify(this.budgetAllocationInfos));

  }

  getSector(){

    const request = this.dropdownInfoService.getSector();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;

              console.log("Sector\n"+JSON.stringify(msg));

              for(let i=0; i<msg.length; i++){
                this.sectorList.push({ label: msg[i][5]+"-"+msg[i][1], value: msg[i][4] });
               //console.log("\n"+msg[i][8]+"-"+msg[i][3]);
               //console.log("\n"+msg[i][1]);
              }
  
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
  );

  }

  getVendors(){
    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
  
              for(let i = 0; i< msg.length; i++) {
                this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
                // this.selectedvendor.set(msg[i].venId, msg[i].venName);
                // console.log( this.vendorList);
           }
  
  
              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
  );
  
  }

  getContractListInfo(ven_id:string) {
    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract', value: -1 });
    const request = this.dropdownInfoService.getContracts(ven_id);
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);


        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
          //this.contractTypeSet.set(msg[i].contId, msg[i].type);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }


  getInvoiceListInfo(cont_id:string){
    this.selectedInvoice = [];
    this.selectedInvoice.push({ label: 'Select Invoice', value: -1});
    const request = this.dropdownInfoService.budgetBillEntryInfo(cont_id);
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.selectedInvoice.push({label: msg[i].invoice_no, value: msg[i].invoice_no});
           }
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
    }
  
  setBudgetEntry(invoice:string){
  // this.selectedInvoice = [];
  // this.selectedInvoice.push({ label: 'Select Invoice', value: -1});
  const request = this.budgetBillEntryService.getBillEntry(invoice);
  request.subscribe(
    (res ) => {
      
              const msg = JSON.parse(res) ;
              this.budgetBillEntry.invoice_no = msg[0][0];
              this.budgetBillPayment.invoice_no=msg[0][0];
              this.budgetBillEntry.bill_amount = msg[0][1];
              this.budgetBillEntry.cont_id = msg[0][2];
              this.budgetBillEntry.ait_product = msg[0][3];
              this.budgetBillPayment.vat_rate=msg[0][3];
              this.budgetBillEntry.vat_product = msg[0][4];
              this.budgetBillPayment.ait_rate = msg[0][4];

              // this.selectedInvoice.push({label: msg.invoice_no, value: msg.invoice_no});
        //  }

              this.getInvoiceListInfo(this.budgetBillEntry.cont_id);
            
        
    },
    err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);
  }

  setPaymentAmountWords(payment_amount) {

   
if(payment_amount){
  this.wording = this.budgetBillEntryService.convertGreaterThanThousand(payment_amount) + " Only."
    
  this.budgetBillPayment.pay_order_amount_in_words = this.wording;
}
else{
  this.wording = "";
  this.budgetBillPayment.pay_order_amount_in_words = this.wording;
}
    
    //console.log("\ninit: "+this.budgetBillEntry.bill_amount_in_words);

   

     
  }
  getBillPayment(budgetbillentry:Budgetbillentry,budgetbillpayment:Budgetbillpayment){

    const request = this.budgetBillPaymentService.getBillPaymentAmount(
      budgetbillentry.bill_amount,budgetbillentry.vat_product,this.incexc,
      budgetbillpayment.in_with_vat,budgetbillentry.ait_product,budgetbillpayment.in_with_tax,budgetbillpayment.penalty_amt
      ,budgetbillentry.invoice_no,budgetbillpayment.fiscal_year)
    request.subscribe(
      (res ) => {
        if(res.includes(',')){
          const array = res.split(',');
        this.budgetBillPayment.bill_after_vat=Number(array[0]);
        this.budgetBillPayment.accu_bill_after_vat=Number(array[1]);
        this.budgetBillPayment.curr_tds=Number(array[2]);
        this.budgetBillPayment.accu_tds=Number(array[3]);
        this.budgetBillPayment.pay_order_amt=Number(array[4]);
        this.setPaymentAmountWords( this.budgetBillPayment.pay_order_amt);
        }
  })
 
}

reloadPage(): void {
  let currentUrl = this.router.url;
  this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  this.router.onSameUrlNavigation = 'reload';
  this.router.navigate([currentUrl]);
}

saveBudgetBillPayment(budgetBillPayment) {

  if (!budgetBillPayment.invoice_no ) {
    this.severity = 'error';
    this.detailMsg = "Invoice No cannot be blank";
    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //this.invoice_no.nativeElement.focus();
    return;
  }

  if (!budgetBillPayment.sector_id ) {
    this.severity = 'error';
    this.detailMsg = "Sector cannot be blank";
    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //this.invoice_no.nativeElement.focus();
    return;
  }
  
  //console.log("data\n"+JSON.stringify(this.SectorYearwiseAllocationInfos));

  var bal;

  this.SectorYearwiseAllocationInfos.forEach(element => {
    if(element.id === budgetBillPayment.sector_id){
      //availableCADFund = element.amount;
      bal = element.amount-element.use;
      if(bal<budgetBillPayment.pay_order_amt){
        this.insufficientFund = true;  
        return;
      }else{
        this.insufficientFund = false;
      }
      
    }
  });

  if(this.insufficientFund){
    this.severity = 'error';
    this.detailMsg = "Available Fund: "+bal;
    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    return;
  }

 
  this.budgetBillPayment.entry_id = this.user.userId;
  var dataHeader = this.dataService.createMessageHeader("SAVE");
  this.jsonMessage = {
    dataHeader: dataHeader,
    payLoad: new Array(this.budgetBillPayment)
  }
  console.log( this.jsonMessage)
 this.budgetBillPaymentService.insertBudgetBillPayment(JSON.stringify(this.jsonMessage));
  this.reloadPage();


}


}
