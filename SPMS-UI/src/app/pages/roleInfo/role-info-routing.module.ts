import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleInfoInsertComponent } from './role-info-insert/role-info-insert.component';


const routes: Routes = [
  {path: '', component: RoleInfoInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleInfoRoutingModule { }
