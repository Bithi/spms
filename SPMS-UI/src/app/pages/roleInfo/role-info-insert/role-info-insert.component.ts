import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/models/role';
import { DataService } from 'src/app/data/data.service';
import { RoleInfoService } from 'src/app/services/role-info.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role-info-insert',
  templateUrl: './role-info-insert.component.html',
  styleUrls: ['./role-info-insert.component.scss']
})
export class RoleInfoInsertComponent implements OnInit {

  currentFileUpload: File;
  options: boolean = false;
  role:Role=new Role();
  jsonMessage:any;
  key:string = "toast";
    severity:string;
    detailMsg:string;

    roleStatus = [
      { value: '', label: 'Select user status' },
      { value: 'A', label: 'Active' },
      { value: 'I', label: 'Inactive' }
  ];

  constructor(
    private dataService: DataService,
    private roleInfoService: RoleInfoService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.roleInfoService.addToTheList.subscribe(
      data =>{
        
       this.role = data;
       this.options = false;
      }
    )
  }

  createRole(): void {

    if(!this.role.roleName){
      this.severity = 'error';
      this.detailMsg = "Role cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    if(!this.role.status){
      this.severity = 'error';
      this.detailMsg = "Status cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.role)
    }

    this.roleInfoService.insertRoleInfo(JSON.stringify(this.jsonMessage));

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['roleinfo']);
  // });
  this.reset(this.role);
  };


  updateRole(role): void {

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.role)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.roleInfoService.updateRoleInfo(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['roleinfo']);
  // });
  this.reset(this.role);
  };


  

  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.role  = new Role();
      form.reset({ roleName: this.role.roleName });
      form.reset({ status: this.role.status });
      form.reset({ options: false });
    }
}


deleteRole(role): void {
   
    

  var dataHeader = this.dataService.createMessageHeader("DELETE");
  
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(role)
  }
  this.roleInfoService.deleteRoleInfo(JSON.stringify(this.jsonMessage));
//   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
//     this.router.navigate(['roleinfo']);
// });
this.reset(this.role);
};

}
