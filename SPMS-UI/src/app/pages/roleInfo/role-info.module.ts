import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoleInfoRoutingModule } from './role-info-routing.module';
import { RoleInfoInsertComponent } from './role-info-insert/role-info-insert.component';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from 'src/app/layout/layout.module';
import { DropdownModule, ProgressSpinnerModule, TableModule } from 'primeng';
import { RoleInfoComponent } from './role-info/role-info.component';


@NgModule({
  declarations: [RoleInfoInsertComponent,RoleInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    RoleInfoRoutingModule
  ]
})
export class RoleInfoModule { }
