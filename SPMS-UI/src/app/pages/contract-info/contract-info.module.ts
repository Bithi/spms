import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractInfoRoutingModule } from './contract-info-routing.module';
import { ContractInfoComponent } from './contract-info/contract-info.component';
import { ContractInfoInsertComponent } from './contract-info-insert/contract-info-insert.component';
import { FormsModule } from '@angular/forms';
import { ConfirmationService, ConfirmDialogModule, DialogModule, DropdownModule, MessageService, ProgressSpinnerModule, RadioButtonModule, TableModule } from 'primeng';
import { LayoutModule } from 'src/app/layout/layout.module';

@NgModule({
  declarations: [ContractInfoComponent,ContractInfoInsertComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,ConfirmDialogModule,RadioButtonModule,DialogModule,ContractInfoRoutingModule
  ],
  exports: [ContractInfoComponent,ContractInfoInsertComponent],
  providers: [ConfirmationService,MessageService]
})

export class ContractInfoModule { }
