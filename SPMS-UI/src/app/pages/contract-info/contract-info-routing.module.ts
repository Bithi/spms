import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractInfoInsertComponent } from './contract-info-insert/contract-info-insert.component';


const routes: Routes = [
  {path: '', component: ContractInfoInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractInfoRoutingModule { }
