import { Component, OnInit, ViewChild } from '@angular/core';
import { ContractInfo } from 'src/app/models/contractinfo';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { DataService } from 'src/app/data/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { VendorInfoService } from 'src/app/services/vendor-info.service';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { User } from 'src/app/models/user';
import { ViewEncapsulation } from '@angular/compiler/src/core';
import { Table } from 'primeng';
import { ContSubItem } from 'src/app/models/ContSubItem';
// import { ContSubItemService } from 'src/app/services/contSubItem.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ContSubItemService } from 'src/app/services/contsubitem.service';


@Component({
  selector: 'app-contract-info',
  templateUrl: './contract-info.component.html',
  styleUrls: ['./contract-info.component.scss'],
  // encapsulation: ViewEncapsulation.None,
//   styles: [`
//   :host ::ng-deep .ui-button {
//       margin: .5em .5em .5em 0;
//       width: 140px;
//   }
//   @media screen and (max-width: 40em) {
//       :host ::ng-deep .ui-dialog {
//           width: 75vw !important;
//       }
//   }
// `]
})
export class ContractInfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  public user  : User;
  selecteContractList: SelectItem[];
  subItemList : SelectItem[];
  selecteModelList:SelectItem[];
  contractInfos: ContractInfo[];
  contractInfo: ContractInfo = new ContractInfo();
  vendor: VendorInfo = null;
  vendors: VendorInfo[];
  vendorServiceType = new Map();
  vendorNameSet = new Map();
  cols: any[];
  displayModal: boolean;
  displayBasic2: boolean = false;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  renew:boolean = false;
  moduleId:number;
  contractNo: string;
  public contSubItem: ContSubItem = new ContSubItem();
  jsonMessage: { dataHeader: any; payLoad: any; };
  constructor(
    private contractInfoService: ContractInfoService,
    private dataService: DataService,   private dropdownInfoService: DropdownInfoService,
    private vendorInfoService: VendorInfoService,
    public router: Router,private utility:Utility,private aroute: ActivatedRoute, private contSubItemService: ContSubItemService,
  ) { }

  // getContractInfo() {
  //   this.contractInfoService.getContractInfoList().subscribe(data => {
  //     this.contractInfos = data;
  //     //////////////console.log(data);
  //   });

  // }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  getContractInfo() {
    const request = this.contractInfoService.contractInfoJoin();
    request.subscribe(
      (res) => {
        //////////////console.log(res);
        const msg = JSON.parse(res) ;
        this.contractInfos =[] ;
        for(let i = 0; i< msg.length; i++) {
          this.contractInfo = new ContractInfo;
          this.contractInfo.contId= msg[i][0];
          this.contractInfo.contNo= msg[i][1];
          this.contractInfo.contName= msg[i][2];
          this.contractInfo.serviceTypeId= msg[i][3];
          this.contractInfo.contParty= msg[i][4];
          this.contractInfo.contValidStart= msg[i][5];
          this.contractInfo.contValidEnd= msg[i][6];
          this.contractInfo.tenderDt= msg[i][7];

          this.contractInfo.tenderNo= msg[i][8];
          this.contractInfo.totalMonth= msg[i][9];
          this.contractInfo.monthlyCharge= msg[i][10];
          this.contractInfo.totalCharge= msg[i][11];
          this.contractInfo.totalBr= msg[i][12];
          this.contractInfo.totalAtm= msg[i][13];
          this.contractInfo.totalDcDrConnectivity= msg[i][14];
          this.contractInfo.venId= msg[i][15];
          if(msg[i][16]=='ACT'){
            this.contractInfo.status='Active';
          }
          else if(msg[i][16]=='CAN'){
            this.contractInfo.status='Inactive';
          }
          if(msg[i][16]=='CLS'){
            this.contractInfo.status='Closed';
          }
          this.contractInfo.moduleId= msg[i][17];
          this.contractInfo.entryUser= msg[i][18];
          this.contractInfo.entryDate= msg[i][19];
          this.contractInfo.updateUser= msg[i][20];
          this.contractInfo.updateDt= msg[i][21];
          this.contractInfo.renewDate= msg[i][22];
          this.contractInfo.vat= msg[i][23];
          this.contractInfo.tax= msg[i][24];
          this.contractInfo.type= msg[i][25];
          this.contractInfo.warrantyPeriod= msg[i][26];
          this.contractInfo.refConNo= msg[i][27];
          this.contractInfo.remarks= msg[i][28];
          this.contractInfo.commDate= msg[i][29];
          this.contractInfo.contDate= msg[i][30];
          this.contractInfo.performReportDt= msg[i][31];
          this.contractInfo.inspectReportDt= msg[i][32];
          this.contractInfo.vendor= msg[i][33];
          // this.contractInfo.refConNo=msg[i][32];

          this.contractInfos[i] = this.contractInfo;
          // //////////////console.log( this.vendorList);
     }

     
    });

    
  }


  createContractInfo(): void {
    this.router.navigate(['/create']);
  };


  ngOnInit() {
    
    this.aroute
    .data
    .subscribe(v =>
       {
         if(v.module==="amc"){
          this.renew = true;
          
       }
       this.moduleId=v.moduleId;
      }
    
    );
    this.user=JSON.parse(localStorage.getItem('loggedinUser'));
    this.vendor = new VendorInfo();
    this.cols = [

      { field: 'contNo', header: 'Contract no' },
      { field: 'vendor', header: 'Vendor' },
      { field: 'type', header: 'Type' },
      { field: 'status', header: 'Status' },


    ];
    this.getServiceTypeList();
    this.getVendorList();
    this.getContractInfo();

    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract', value: -1 });
    this.getContractListInfo();

    // this.selecteModelList = [];
    // this.selecteModelList.push({ label: 'Select Model', value: -1 });
    // this.getModelList();
    this.getmodelBrandJoin();
    this.subItemList = [];
    this.subItemList.push({ label: 'Select Item', value: -1 });
    this.getItemList();
  }

  editContractInfo(contractInfo): void {
    this.contractInfoService.passTheValue(contractInfo);

  }
  renewContractInfo(contractInfo): void{
    if(!contractInfo.renewDate){
      this.severity = 'error';
      this.detailMsg = "renewDate cant be null";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    else if(!contractInfo.contValidEnd){
      this.severity = 'error';
      this.detailMsg = "Contract valid end date cant be null";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    this.displayBasic2=false;
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    contractInfo.moduleId = this.moduleId;


    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(contractInfo)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractInfoService.renewContractInfo(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/contractinfoListAmc', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractinfoAmc']);
  // });
  this.reloadPage();

  }
  showModalDialog(con) {
    this.contSubItem.contId = con.contId;
    this.contractNo=con.contNo;
    this.displayBasic2 = true;
}
  reloadPage(): void {
      let currentUrl = this.router.url;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([currentUrl]);
}
  
  

  getServiceTypeList() {
    const request = this.vendorInfoService.getVendorServiceType();
    request.subscribe(
      (res) => {
        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {

          this.vendorServiceType.set(msg[i].serviceTypeId, msg[i].serviceTypeName);
          
        }
        ////////////////console.log(this.vendorServiceType);

      },
      err => {
        return;
      }
    );
  }

  getVendorList(){
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    var jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.vendor)
    }
    const request = this.vendorInfoService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              ////////////////console.log(msg);
              this.vendors = msg.payLoad;
              ////////////////console.log(this.vendors);

              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]){
                
                  return;
              }

              for(let i=0; i<this.vendors.length; i++) {
                this.vendorNameSet.set(this.vendors[i].venId, this.vendors[i].venName);
              }
              ////////////////console.log(this.vendorNameSet);         
          
      },
      err => {
          return;
      }
 );
}

onRowSelect(event) {

  this.contractInfo = event.data;
  this.displayBasic2 = true;
  
  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }
  getmodelBrandJoin()
  {
    
  this.selecteModelList = [];
  this.selecteModelList.push({ label: 'Select Model ', value: -1 });
    const request = this.contSubItemService.getmodelBrandJoin();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.selecteModelList.push({label: msg[i][2]+' ('+msg[i][8]+')', value: msg[i][0]});
           }


              
              
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }
  getModelList() {

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    }

    const request = this.contSubItemService.getModelList(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
        const msg = JSON.parse(atob(res)).payLoad;
       
        for (let i = 0; i < msg.length; i++) {
          this.selecteModelList.push({ label: msg[i].modelName, value: msg[i].modelId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }

  getItemList() {

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    }

    const request = this.contSubItemService.getItemList(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res)).payLoad;
       
        for (let i = 0; i < msg.length; i++) {
          this.subItemList.push({ label: msg[i].itemName, value: msg[i].itemId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }

  itemAssign(contractInfo): void {
    if (!this.contSubItem.modelId) {
      this.severity = 'error';
      this.detailMsg = "Model cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contSubItem.itemId) {
      this.severity = 'error';
      this.detailMsg = "Item cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contSubItem.itemQuantity|| this.contSubItem.itemQuantity == 0) {
      this.severity = 'error';
      this.detailMsg = "Quantity cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contSubItem.totalValue || this.contSubItem.totalValue == 0) {
      this.severity = 'error';
      this.detailMsg = "Total value product cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

     // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contSubItem.entryDt = new Date();
    this.contSubItem.entryUser = this.user.userId;
    // this.contSubItem.updateDt = new Date('01/01/1970');
    // this.contSubItem.updateUser = "";

    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contSubItem)
    }
    this.contSubItemService.insertData(JSON.stringify(this.jsonMessage));
    this.reloadPage();
    // this.router.navigateByUrl('/contSubItemList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSubItem']);
    // });
  };

}
