import { Component, OnInit, ViewChild } from '@angular/core';
import { ContractInfo } from 'src/app/models/contractinfo';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { DataService } from 'src/app/data/data.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { VendorInfoService } from 'src/app/services/vendor-info.service';
import { User } from 'src/app/models/user';
declare var $: any;
@Component({
  selector: 'app-contract-info-insert',
  templateUrl: './contract-info-insert.component.html',
  styleUrls: ['./contract-info-insert.component.scss']
})
export class ContractInfoInsertComponent implements OnInit {
  public user: User;
  contractInfo: ContractInfo = new ContractInfo();
  vendorNameSet = new Map();
  cont: ContractInfo = new ContractInfo();
  selecteContractList: SelectItem[];
  selectedvendor: SelectItem[];
  selectservicetype: SelectItem[];
  contractNameSet = new Map();
  contractWithChild = new Map();
  contractSet = new Map();
  currentFileUpload: File;
  renew: boolean = false;
  moduleId: number;
  displayBasic2: boolean = false;
  amcslaForm: string = "pur";
  defaultValue: String[] = [];
  mySubscription: any;
  status = [
    { value: null, label: 'Select Status' },
    { value: 'ACT', label: 'Active' },
    { value: 'CAN', label: 'Inactive' },
    { value: 'CLS', label: 'Closed' }
  ];

  selectType = [
    { value: null, label: 'Select Type' },
    { value: 'AMC', label: 'AMC' },
    { value: 'SLA', label: 'SLA' },
    { value: 'conn', label: 'Connectivity' },
    { value: 'pur', label: 'Purchase' },
  ];

  jsonMessage: any;
  options: boolean = false;
  constructor(
    private contractInfoService: ContractInfoService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService,
    private vendorInfoService: VendorInfoService,
    private aroute: ActivatedRoute,
    private confirmationService: ConfirmationService

  ) {

  }

  key: string = "toast";
  severity: string;
  detailMsg: string;


  ngOnInit() {
    this.amcslaForm='pur';
    this.aroute
      .data
      .subscribe(v => {
        if (v.moduleName === "amc") {
          this.renew = true;
        }
        this.moduleId = v.moduleId;

      }

      );

    this.contractInfo.moduleId = this.moduleId;
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.selectedvendor = [];
    this.selectservicetype = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.selectservicetype.push({ label: 'Select Service', value: -1 });
    this.getVendorList();
    this.getServiceTypeList();
    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract Number', value: -1 });
    this.getContractListInfo();
    this.getChildBycon();
    window.dispatchEvent(new Event('resize'));
    var me = this;
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.contractInfoService.addToTheList.subscribe(
      data => {
        this.contractInfo = data;
        this.options = false;
        this.amcslaForm = this.contractInfo.type;


      }
    );
    if (this.aroute.queryParams) {
      this.aroute.queryParams.subscribe(params => {
        const request = this.dropdownInfoService.getVendors();
        request.subscribe(
          (res) => {

            const msg = JSON.parse(res);

            for (let i = 0; i < msg.length; i++) {
              this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
              //this.vendorNameSet.set(msg[i].venId, msg[i].venName);
              // //console.log( this.vendorList);
            }
            if(params.contractInfo){
              this.contractInfoService.passTheValue(JSON.parse(params.contractInfo));
            }
          





          },
          err => {
            // this.severity = 'error';
            // this.detailMsg = "Server Error: " + err.message;
            // // this.showProgressSpin = false;
            // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            return;
          }
        );


      });
    }

  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {
        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.cont = new ContractInfo();
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
          this.cont = msg[i];
          this.contractSet.set(msg[i].contId, this.cont);
          this.contractNameSet.set(msg[i].contId, msg[i].contNo);

        }



      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }
  getServiceTypeList() {

    const request = this.vendorInfoService.getVendorServiceType();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectservicetype.push({ label: msg[i].serviceTypeName, value: msg[i].serviceTypeId });
          // //console.log( this.vendorList);
        }





      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }

  getVendorList() {

    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
          //this.vendorNameSet.set(msg[i].venId, msg[i].venName);
          // //console.log( this.vendorList);
        }


      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }

  createContractInfo(): void {

    if (!this.contractInfo.contNo) {
      this.severity = 'error';
      this.detailMsg = "contract No cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.tenderDt)) {
      this.severity = 'error';
      this.detailMsg = "Tender date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.commDate)) {
      this.severity = 'error';
      this.detailMsg = "Commission date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.warrantyPeriod)) {
      this.severity = 'error';
      this.detailMsg = "Warranty Period cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    else if (!this.contractInfo.contValidStart) {
      this.severity = 'error';
      this.detailMsg = "Contract valid start date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (!this.contractInfo.contValidEnd) {
      this.severity = 'error';
      this.detailMsg = "Contract valid end date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn') &&(!this.contractInfo.totalDcDrConnectivity)) {
  
        this.severity = 'error';
        this.detailMsg = "Dc Dr connectivity cant be null";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    else if (this.contractInfo.venId == null) {

      this.severity = 'error';
      this.detailMsg = "vendor cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.serviceTypeId == null) {
      this.severity = 'error';
      this.detailMsg = "service type id cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }


    // else if (!this.contractInfo.status) {
    //   this.severity = 'error';
    //   this.detailMsg = "Status cant be null";
    //   this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //   return;
    // }
    if ((!this.contractInfo.totalCharge) || this.contractInfo.totalCharge.toString() == "") {
      //this.contractInfo.totalCharge = 0;
      this.severity = 'error';
      this.detailMsg = "Total Charge cant be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
   
    }
    if ((!this.contractInfo.vat) || this.contractInfo.vat.toString() == "") {
      this.contractInfo.vat = '0';
    }


    else if (!this.contractInfo.inspectReportDt) {
      this.severity = 'error';
      this.detailMsg = "Inspection Report Date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (!this.contractInfo.performReportDt) {
      this.severity = 'error';
      this.detailMsg = "Perform Report Date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    // else if(this.amcslaForm=='AMC/SLA' && !this.contractInfo.type){
    //   this.severity = 'error';
    //   this.detailMsg = "Type cant be null";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;
    // }
    this.contractInfo.entryDate = new Date();
    this.contractInfo.entryUser = this.user.userId;
    this.contractInfo.updateDt = new Date('01/01/1970');
    this.contractInfo.moduleId = this.moduleId;
    this.contractInfo.updateUser = "";
    this.contractInfo.type = this.amcslaForm;
    this.contractInfo.status='ACT';

    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contractInfo)
    }
    this.contractInfoService.insertContractInfo(JSON.stringify(this.jsonMessage));
    //    this.router.navigateByUrl('/contractinfoList', { skipLocationChange: true }).then(() => {
    //     this.router.navigate(['contractinfo']);
    // });

    this.reloadPage();

  };

  updateContractInfo(contractInfo): void {



    if (!this.contractInfo.contNo) {
      this.severity = 'error';
      this.detailMsg = "contract No cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.tenderDt)) {
      this.severity = 'error';
      this.detailMsg = "Tender date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.commDate)) {
      this.severity = 'error';
      this.detailMsg = "Commission date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn') &&(!this.contractInfo.totalDcDrConnectivity)) {
  
      this.severity = 'error';
      this.detailMsg = "Dc Dr connectivity cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.warrantyPeriod)) {
      this.severity = 'error';
      this.detailMsg = "Warranty Period cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    else if (!this.contractInfo.contValidStart) {
      this.severity = 'error';
      this.detailMsg = "Contract valid start date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (!this.contractInfo.contValidEnd) {
      this.severity = 'error';
      this.detailMsg = "Contract valid end date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.venId == null) {

      this.severity = 'error';
      this.detailMsg = "vendor cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.serviceTypeId == null) {
      this.severity = 'error';
      this.detailMsg = "service type id cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }


    // else if (!this.contractInfo.status) {
    //   this.severity = 'error';
    //   this.detailMsg = "Status cant be null";
    //   this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //   return;
    // }

    else if (!this.contractInfo.type) {
      this.severity = 'error';
      this.detailMsg = "Type cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if ((!this.contractInfo.totalCharge) || this.contractInfo.totalCharge.toString() == "") {
      this.contractInfo.totalCharge = 0;
    }
    if ((!this.contractInfo.vat) || this.contractInfo.vat.toString() == "") {
      this.contractInfo.vat = '0';
    }
    // var today = new Date();
    // var date = (today.getMonth()+1)+'/'+(today.getDate() + 1)+'/'+today.getFullYear();
    var dataHeader = this.dataService.createMessageHeader("UPDATE");
    this.contractInfo.updateUser = this.user.userId;
    this.contractInfo.updateDt = new Date();

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(contractInfo)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractInfoService.updateContractInfo(JSON.stringify(this.jsonMessage));
    this.reloadPage();
  };
  close() {
    if (!this.contractInfo.contNo) {
      this.severity = 'error';
      this.detailMsg = "contract No cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.tenderDt)) {
      this.severity = 'error';
      this.detailMsg = "Tender date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.commDate)) {
      this.severity = 'error';
      this.detailMsg = "Commission date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    
    else if ((this.amcslaForm == 'conn' || this.amcslaForm == 'pur') && (!this.contractInfo.warrantyPeriod)) {
      this.severity = 'error';
      this.detailMsg = "Warranty Period cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    else if (!this.contractInfo.contValidStart) {
      this.severity = 'error';
      this.detailMsg = "Contract valid start date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (!this.contractInfo.contValidEnd) {
      this.severity = 'error';
      this.detailMsg = "Contract valid end date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.venId == null) {

      this.severity = 'error';
      this.detailMsg = "vendor cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.serviceTypeId == null) {
      this.severity = 'error';
      this.detailMsg = "service type id cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }


    // else if (!this.contractInfo.status) {
    //   this.severity = 'error';
    //   this.detailMsg = "Status cant be null";
    //   this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //   return;
    // }

    else if (!this.contractInfo.type) {
      this.severity = 'error';
      this.detailMsg = "Type cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if ((!this.contractInfo.totalCharge) || this.contractInfo.totalCharge.toString() == "") {
      this.contractInfo.totalCharge = 0;
    }
    if ((!this.contractInfo.vat) || this.contractInfo.vat.toString() == "") {
      this.contractInfo.vat = '0';
    }

    this.confirmationService.confirm({
        message: 'Are you sure that you want to Close the contract?',
        accept: () => {
          var dataHeader = this.dataService.createMessageHeader("UPDATE");
          this.contractInfo.updateUser = this.user.userId;
          this.contractInfo.updateDt = new Date();
          this.contractInfo.status = 'CLS';
      
          this.jsonMessage = {
            dataHeader: dataHeader,
            payLoad: new Array(this.contractInfo)
          }
          //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
          this.contractInfoService.closeContractInfo(JSON.stringify(this.jsonMessage));
          this.amcslaForm='pur';
          this.reset(this.contractInfo);
        }
    });
}
 

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  deleteContractInfo(contractInfo): void {



    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(contractInfo)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractInfoService.deleteContractInfo(JSON.stringify(this.jsonMessage));

    this.amcslaForm='pur';
    this.reset(this.contractInfo);
    //this.router.navigate(['/create']);
  };
  selectFile(event) {
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);


  }
  upload() {
    var formdata: FormData = new FormData();
    formdata.append('file', this.currentFileUpload);
    var con: ContractInfo = new ContractInfo();
    con.moduleId = this.moduleId;
    con.entryDate = new Date();
    con.entryUser = JSON.parse(localStorage.getItem('loggedinUser')).userId;
    con.updateDt = new Date('01/01/1970');
    con.updateUser = "";
    this.contractInfoService.upload(formdata, con);

    this.amcslaForm='pur';
    this.reset(this.contractInfo);
  }

  reset(form) {

    if (this.options) {
      $('.custom-file-label').html('Choose file');

    }
    else {
      this.contractInfo = new ContractInfo();
      form.reset({ selectservicetype: this.contractInfo.serviceTypeId });
      form.reset({ VendorId: this.contractInfo.venId });
      form.reset({ status: this.contractInfo.status });
      form.reset({ options: false });
      // debugger;
      this.amcslaForm='pur';
      
    }



  }
  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
}

  renewContractInfo(contractInfo): void {
    if (!this.contractInfo.contNo) {
      this.severity = 'error';
      this.detailMsg = "contract No cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if ((this.amcslaForm == 'SLA') &&(!this.contractInfo.totalDcDrConnectivity)) {
      this.severity = 'error';
      this.detailMsg = "Dc Dr connectivity cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    // else if(!this.contractInfo.tenderDt){
    //   this.severity = 'error';
    //   this.detailMsg = "Tender date cant be null";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;
    // }
    else if (!this.contractInfo.contValidStart) {
      this.severity = 'error';
      this.detailMsg = "Contract valid start date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (!this.contractInfo.contValidEnd) {
      this.severity = 'error';
      this.detailMsg = "Contract valid end date cant be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.venId == null) {

      this.severity = 'error';
      this.detailMsg = "vendor cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if (this.contractInfo.serviceTypeId == null) {
      this.severity = 'error';
      this.detailMsg = "service type id cannot be null";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }


    // else if (!this.contractInfo.status) {
    //   this.severity = 'error';
    //   this.detailMsg = "Status cant be null";
    //   this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //   return;
    // }

    // else if(!this.contractInfo.type){
    //   this.severity = 'error';
    //   this.detailMsg = "Type cant be null";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;s
    // }
    this.contractInfo.entryDate = new Date();
    this.contractInfo.entryUser = this.user.userId;
    this.contractInfo.updateDt = new Date('01/01/1970');
    this.contractInfo.moduleId = this.moduleId;
    this.contractInfo.updateUser = "";
    contractInfo.renewDate = contractInfo.contValidStart;
    this.contractInfo.type = this.amcslaForm;

    this.displayBasic2 = false;
    var dataHeader = this.dataService.createMessageHeader("SAVE");




    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(contractInfo)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractInfoService.renewContractInfo(JSON.stringify(this.jsonMessage));
    this.reloadPage();
  }

  showModalDialog(con) {
    this.contractInfo = con;
    this.displayBasic2 = true;
  }


  editContractInfo(id): void {

    this.cont = this.contractSet.get(id); debugger

    this.contractInfoService.passTheValue(this.cont);

  }
  
  getChildBycon() {
    const request = this.contractInfoService.getChildBycon();
    request.subscribe(
      (res) => {

          const msg = JSON.parse(res);
          
          for (let i = 0; i < msg.length; i++) {
            this.contractWithChild.set(  msg[i][0],   msg[i][1]);
          }
          
          return
      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }
  splitDescription(theString: string) {
    return theString.split(';');
}
}
