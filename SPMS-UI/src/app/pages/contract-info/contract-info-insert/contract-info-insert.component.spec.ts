import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractInfoInsertComponent } from './contract-info-insert.component';

describe('ContractInfoInsertComponent', () => {
  let component: ContractInfoInsertComponent;
  let fixture: ComponentFixture<ContractInfoInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractInfoInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractInfoInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
