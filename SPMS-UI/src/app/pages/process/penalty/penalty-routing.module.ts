import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PenaltyComponent } from '../../process/penalty/penalty/penalty.component';


const routes: Routes = [
  {path: '', component: PenaltyComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PenaltyRoutingModule { }
