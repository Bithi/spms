import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { Utility } from 'src/app/util/utility';
import { PenaltyService } from 'src/app/services/penalty.service';
import { PenaltyProcess } from 'src/app/models/penaltyProcess';
import { BranchService } from 'src/app/services/branch.service';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { PenaltyCalc } from 'src/app/models/PenaltyCalc';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { NgForm } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-penalty',
  templateUrl: './penalty.component.html',
  styleUrls: ['./penalty.component.scss']
})
export class PenaltyComponent implements OnInit {
  vendors: {};
  vendorData:[];
  selectedvendor:SelectItem[];
  selectedcontract:SelectItem[];
  selectedcontractbranch: SelectItem[];
  contractinfos: {};
  contractNameSet = new Map();
  contractSet= new Map();
  contractbranches: {};
  date12: Date;
  twelvemonth=[
    {value:null,label:'Select Month'},
    {value:'1',label:'january'},
    {value:'2',label:'february'},
    {value:'3',label:'March'},
    {value:'4',label:'April'},
    {value:'5',label:'May'},
    {value:'6',label:'june'},
    {value:'7',label:'july'},
    {value:'8',label:'August'},
    {value:'9',label:'September'},
    {value:'10',label:'October'},
    {value:'11',label:'November'},
    {value:'12',label:'December'}
  ];
  penaltyCalcRep: PenaltyCalc = new PenaltyCalc();

  month : any[];
  vendorList : any[];
  key:string = "toast";
  severity:string;
  detailMsg:string;
  sites: SelectItem[];
  branch:SelectItem[];
  cols: any[];
  penalties:PenaltyCalc[] =[];
  penaltySingle:PenaltyCalc = new PenaltyCalc();
  penaltyProcess:PenaltyProcess = new PenaltyProcess();
  vendorSet = new Map();
  jsonMessage:any;
  constructor(     
    private dataService: DataService,
    public router: Router,
    private utility:Utility,
    private penaltyService: PenaltyService,
    private dropdownInfoService: DropdownInfoService,
    private branchService:BranchService,
    private confirmationService:ConfirmationService ) { 

    
  }

  ngOnInit() {
    this.penList();
    this.selectedvendor = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.getVendorList();
    this.getContracts();
    this.selectedcontract = [];
    this.selectedcontract.push({ label: 'Select Contract', value: -1 });
    this.cols = [
      { field: 'calcId', header: 'Serial'},
      { field: 'downid', header: 'DownId'},
      { field: 'downdate', header: 'Date' },
      { field: 'brCode', header: 'Branch code' },
      { field: 'venId', header: 'Vendor' },
      { field: 'contId', header: 'contract' }
    ];

  }
  penList(){
  var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: this.penalties
    }
    var request = this.penaltyService.getPenaltyList(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
       const msg = JSON.parse(atob(res));
       console.log(msg.payLoad);
       this.penalties = msg.payLoad;
       for(let i = 0; i<this.penalties.length; i++) {
        if( this.penalties[i].downid.indexOf("-")!=-1){
          this.penalties[i].downid=  this.penalties[i].downid.substring(0,  this.penalties[i].downid.indexOf("-"))+" (single)";
         }
       
       }
      
    }); 
  }
  getVendorList(){
   
    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;

              for(let i = 0; i< msg.length; i++) {
                this.selectedvendor.push({label: msg[i].venName, value: msg[i].venId});
                this.vendorSet.set(msg[i].venId, msg[i].venName);
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );


}


getContracts(){

    const request = this.dropdownInfoService.getContractInfoList();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.contractNameSet.set(msg[i].contId, msg[i].contNo);
                
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
}

getContractList(venId: string){
  //console.log(venId);
  
    // this.dropdownInfoService.getContracts(this.penaltyCalcRep.venId).subscribe(
    //   data => this.contractinfos = data
    // );
    this.selectedcontract = [];
    this.selectedcontract.push({ label: 'Select Contract', value: -1});

    if(venId){
    const request = this.dropdownInfoService.getContracts(venId);
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
            

              for(let i = 0; i< msg.length; i++) {
                this.selectedcontract.push({label: msg[i].contNo, value: msg[i].contId});
                this.contractNameSet.set(msg[i].contId, msg[i].contNo);
                
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }
  else{
    this.contractinfos=null;
    this.contractbranches=null;
  }
}

getBranchList(contractId: string){
  this.selectedcontractbranch = [];
  this.selectedcontractbranch.push({ label: 'Select Branch', value: -1});
  if(contractId){
    // this.dropdownInfoService.getContractBraches(contractId).subscribe(
    //   data => this.contractbranches = data
    // );

    const request = this.dropdownInfoService.getContractBraches(this.penaltyCalcRep.contId);
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              console.log("msg="+msg);
              for(let i = 0; i< msg.length; i++) {
                this.selectedcontractbranch.push({label: msg[i].brac, value: msg[i].brac});
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }else{
    this.contractbranches=null;
  }
}
  

  process(){

    // if(!this.penaltyProcess.month){
    //   this.severity = 'error';
    //   this.detailMsg = "Select a month at first.";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;
    // }
    // this.penaltyProcess.date.setTime(this.penaltyProcess.date.getTime() + (this.penaltyProcess.date.getTimezoneOffset() * 60 * 1000));
  // debugger;
  // var d = new Date(this.penaltyProcess.date);
  // this.penaltyProcess.date.setTime(d.getTime()+ (d.getTimezoneOffset() * 60 * 1000));
    // this.penaltyProcess.date=this.date12
    this.penaltyProcess.date=$('#date').val();
    this.penaltyProcess.userId =  JSON.parse(localStorage.getItem('loggedinUser')).userId;
    var dataHeader = this.dataService.createMessageHeader("PENALTY_PROCESS");
    var jsonMessage = {
      dataHeader : dataHeader,
        payLoad: [this.penaltyProcess]
    };
    const req =this.penaltyService.process(JSON.stringify(jsonMessage));
    req.subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
             this.severity = msg["dataHeader"].reponseStatus.toLowerCase();
             this.detailMsg = msg["dataHeader"].description;
             this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            // location.reload();
              return;
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
  );
 // location.reload();
}

reset(form) {
  this.penaltyProcess= new PenaltyProcess();
  $('#date').val("2018-01");
  // form.reset({ month: this.penaltyProcess.date });
  

  
}

reloadPage(){
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['penaltyProcess']);
});
}

deletePenalty(penalty) {
  this.confirmationService.confirm({
    message: 'Are you sure that you want to proceed?',
    header: 'Confirmation',
    icon: 'pi pi-exclamation-triangle',
    accept: () => {
      var dataHeader = this.dataService.createMessageHeader("DELETE");
  
      this.jsonMessage = {
          dataHeader : dataHeader,
          payLoad: new Array(penalty)
      }
      this.penaltyService.deletePenalty(JSON.stringify(this.jsonMessage));
      this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        this.router.navigate(['penaltyProcess']);
    });
      this.severity = 'info';
      this.detailMsg = "Record deleted";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    
    },
    reject: () => {
      this.severity = 'error';
      this.detailMsg = "You have rejected the action";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }
});

}

}
