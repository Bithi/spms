import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PenaltyRoutingModule } from './penalty-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { CalendarModule, ConfirmationService, ConfirmDialogModule, DropdownModule, TableModule, ToastModule } from 'primeng';
import { HttpClientModule } from '@angular/common/http';
import { PenaltyComponent } from './penalty/penalty.component';


@NgModule({
  declarations: [PenaltyComponent],
  imports: [
    PenaltyRoutingModule,
    CommonModule,
    LayoutModule,
    FormsModule,
    CalendarModule,
    HttpClientModule,
    ToastModule,
    DropdownModule,
    TableModule,
    ConfirmDialogModule
  ],
  exports: [PenaltyComponent],
  providers:[ConfirmationService]
})
export class PenaltyModule { }
