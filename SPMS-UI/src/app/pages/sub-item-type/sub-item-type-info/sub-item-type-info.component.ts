import { Component, OnInit, ViewChild } from '@angular/core';
import { SubItemType } from 'src/app/models/SubItemType';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { SubItemTypeService } from 'src/app/services/subitemtype.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-sub-item-type-info',
  templateUrl: './sub-item-type-info.component.html',
  styleUrls: ['./sub-item-type-info.component.scss']
})
export class SubItemTypeInfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  subItemType: SubItemType = null;

  subItemTypes: SubItemType[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };


  constructor(
    private subItemTypeService: SubItemTypeService,
    public router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit(): void {
    this.cols = [

      { field: 'itemName', header: 'Item Name' }

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.subItemType)
    }

    const request = this.subItemTypeService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.subItemTypes = msg.payLoad;

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }


  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['subItemTypeInsert']);
  });
  }

  editSubItemType(subItemType): void {
    this.subItemTypeService.passTheValue(subItemType);
  }


}
