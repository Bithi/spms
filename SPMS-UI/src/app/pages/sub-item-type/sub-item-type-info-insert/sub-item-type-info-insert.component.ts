import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { SubItemType } from 'src/app/models/SubItemType';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { SubItemTypeService } from 'src/app/services/subitemtype.service';
import { MessageService, SelectItem } from 'primeng/api';

@Component({
  selector: 'app-sub-item-type-info-insert',
  templateUrl: './sub-item-type-info-insert.component.html',
  styleUrls: ['./sub-item-type-info-insert.component.scss']
})
export class SubItemTypeInfoInsertComponent implements OnInit {

  public user: User;
  selectedTypeId: SelectItem[];

  currentFileUpload: File;
  options: boolean = false;
  subItemType:SubItemType=new SubItemType();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;


  constructor(
    private dataService: DataService,
    private subItemTypeService: SubItemTypeService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.subItemTypeService.addToTheList.subscribe(
      data =>{
        
       this.subItemType = data;
       this.options = false;
      }
    )


    this.selectedTypeId = [];
    this.selectedTypeId.push({ label: 'Select Site', value: -1 });

    this.getTypeId();
  }


  createSubItemType(): void {
    var str;
    var cleanStr;
    if(this.subItemType.itemName){
       str=this.subItemType.itemName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.subItemType.itemName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "Item name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    if(!this.subItemType.typeId){
      this.severity = 'error';
      this.detailMsg = "Site cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    // var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.subItemType.entryDt = new Date();
    this.subItemType.updateDt = new Date('01/01/1970');
    this.subItemType.updateUser = "";
    this.subItemType.entryUser = this.user.userId;
    
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.subItemType)
    }

    this.subItemTypeService.insertSubItemType(JSON.stringify(this.jsonMessage));

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['subItemTypeInsert']);
  // });
  this.reset(this.subItemType);
  };


  updateSubItemType(subItemType): void {


    var str;
    var cleanStr;
    if(this.subItemType.itemName){
       str=this.subItemType.itemName.toString();
       cleanStr=str.trim();
    }
    else{
      cleanStr=this.subItemType.itemName;
    }
    if(!cleanStr){
      this.severity = 'error';
      this.detailMsg = "Item name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    if(!this.subItemType.typeId){
      this.severity = 'error';
      this.detailMsg = "Site cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.subItemType.updateDt = new Date();
    this.subItemType.updateUser = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.subItemType)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.subItemTypeService.updateSubItemType(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['subItemTypeInsert']);
  // });
  this.reset(this.subItemType);
  };


  deleteSubItemType(subItemType): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(subItemType)
    }
    this.subItemTypeService.deleteSubItemType(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['subItemTypeInsert']);
  // });
  this.reset(this.subItemType);
   
  };

  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.subItemType  = new SubItemType();
      form.reset({ itemName: this.subItemType.itemName });
      form.reset({ options: false });
    }
}


getTypeId() {
  const request = this.subItemTypeService.getAllTypeId();
  request.subscribe(
    (res) => {

      const msg = JSON.parse(res);

      for (let i = 0; i < msg.length; i++) {
        debugger;
        this.selectedTypeId.push({ label: msg[i].shortName, value: msg[i].typeId });
        // console.log( this.vendorList);
      }





    },
    err => {
      // this.severity = 'error';
      // this.detailMsg = "Server Error: " + err.message;
      // // this.showProgressSpin = false;
      // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
  );


}





}
