import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubItemTypeInfoInsertComponent } from './sub-item-type-info-insert/sub-item-type-info-insert.component';


const routes: Routes = [
  {path: '', component: SubItemTypeInfoInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubItemTypeInfoRoutingModule { }
