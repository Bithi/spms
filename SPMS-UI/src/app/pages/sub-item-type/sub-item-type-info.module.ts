import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubItemTypeInfoRoutingModule } from './sub-item-type-info-routing.module';
import { SubItemTypeInfoComponent } from './sub-item-type-info/sub-item-type-info.component';
import { SubItemTypeInfoInsertComponent } from './sub-item-type-info-insert/sub-item-type-info-insert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';


@NgModule({
  declarations: [SubItemTypeInfoComponent, SubItemTypeInfoInsertComponent],
  imports: [
    CommonModule,
    SubItemTypeInfoRoutingModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,
  ]
})
export class SubItemTypeInfoModule { }
