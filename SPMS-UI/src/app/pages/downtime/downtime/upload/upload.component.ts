import { Component, OnInit } from '@angular/core';
import { FileUp } from 'src/app/models/fileUp';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, RequestOptions , Headers } from '@angular/http';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { UploadService } from 'src/app/services/upload.service';
import { Brand } from 'src/app/models/brand';
import { DowntimeService } from 'src/app/services/downtime.service';
import { Downtime } from 'src/app/models/downtime';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import {ConfirmationService} from 'primeng/api';
import { userInfo, UserInfo } from 'os';
import { User } from 'src/app/models/user';
declare var $ : any;
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(
    public router: Router,
    private dataService: DataService,
    private http: HttpClient,
    private messageService: MessageService,
    private utility:Utility,
    private uploadService: UploadService,
    private downtimeService: DowntimeService,
    private dropdownInfoService: DropdownInfoService,
    private confirmationService:ConfirmationService
    
    ) { }
    brand: Brand = null;
    vendors:VendorInfo[];
    brands: Brand[];
    display:boolean = false;
    cols: any[];
    jsonMessage: { dataHeader: any; payLoad: any; };
  FileUp: FileUp= new FileUp();
  key:string = "toast";
  severity:string;
  detailMsg:string;
  currentFileUpload: File;
  downtimes: Downtime[];
  lastdowntimes: Downtime[]=[];
  downtime: Downtime;
  vendorSet = new Map();
  user:User = new User();
  lastDown:Downtime;

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.getVendors();
    this.getDowntimes();
    this.cols = [
      { field: 'date', header: 'Date',data: true ,format: 'dd/MM/yyyy' },
      { field: 'month', header: 'Month' },
      { field: 'branchCode', header: 'Branch code' },
      { field: 'vendor', header: 'Vendor' },
      { field: 'problem', header: 'Problem' }
      
    ];
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.getLastDowntimeRow(this.user.userId);

  }

  getVendors(){
    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;

              for(let i = 0; i< msg.length; i++) {
                this.vendorSet.set(msg[i].venId, msg[i].venName);
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );

  }
  
  getDowntimes() {
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.downtimes)
    }
    var request = this.downtimeService.getDowntimeList(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
       const msg = JSON.parse(atob(res));
        
        this.downtimes=msg.payLoad;
    });

  }
    ngOnDestroy(): void {
        document.body.className = '';
    }
    selectFile(event) {  
      this.currentFileUpload = event.target.files.item(0);
      $('.custom-file-label').html(this.currentFileUpload.name);

    }
    upload(){
      var formdata: FormData = new FormData();
      formdata.append('file', this.currentFileUpload);  
      this.uploadService.upload(formdata);
  
    }
    reset(form) {
    
        $('.custom-file-label').html('Choose file');
  
      
  
      
  }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['upload']);
  });
  }

  deleteDowntime(downtime) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        var dataHeader = this.dataService.createMessageHeader("DELETE");
    
        this.jsonMessage = {
            dataHeader : dataHeader,
            payLoad: new Array(downtime)
        }
        this.downtimeService.deleteDowntime(JSON.stringify(this.jsonMessage));
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['upload']);
      });
        this.severity = 'info';
        this.detailMsg = "Record deleted";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      
      },
      reject: () => {
        this.severity = 'error';
        this.detailMsg = "You have rejected the action";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      }
  });

}

getLastDowntimeRow(user: string){
  const request = this.downtimeService.getLastDowntimeRow(user);
  request.subscribe(
    (res) => {
      const msg = JSON.parse(res);
      
      for(var i=0;i<msg.length;i++){
        this.lastDown = new Downtime();
        this.lastDown.id=msg[i][0];
        this.lastDown.month=msg[i][1];
        this.lastDown.date=msg[i][2];
        this.lastDown.branchCode=msg[i][3];
        this.lastDown.vendor=msg[i][4];
        this.lastDown.problem=msg[i][5];
        this.lastDown.downTime=msg[i][6];
        this.lastDown.upTime=msg[i][7];
        this.lastDown.totalTime=msg[i][8];
        this.lastDown.followedBy=msg[i][9];
        this.lastDown.concernOfficer=msg[i][10];
        this.lastDown.entryUser=msg[i][11];
        this.lastDown.entryDate=msg[i][12];
        this.lastDown.updateUser=msg[i][13];
        this.lastDown.updateDt=msg[i][14];
       
        this.lastdowntimes[i]=this.lastDown;
      }
    },
    err => {
      this.severity = 'error';
      this.detailMsg = "Server Error: " + err.message;
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
  );
}
LastDataPage(){
  this.display=true;
}
}