import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DowntimeRoutingModule } from './downtime-routing.module';
import { UploadComponent } from './upload/upload.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ConfirmationService, ConfirmDialogModule, DialogModule, DropdownModule, ProgressSpinnerModule, TableModule, ToastModule } from 'primeng';


@NgModule({
  declarations: [UploadComponent],
  imports: [
    CommonModule,
    DowntimeRoutingModule,
    CommonModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    HttpModule,ToastModule,
    ProgressSpinnerModule,TableModule,DropdownModule,
    ConfirmDialogModule,DialogModule
  ],
  exports:[UploadComponent],
  providers:[ConfirmationService]
})
export class DowntimeModule { }
