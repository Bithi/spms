import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Contractbilltax } from 'src/app/models/Contractbilltax';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { ContractbilltaxService } from 'src/app/services/contractbilltax.service';

@Component({
  selector: 'app-contractbilltaxinsert',
  templateUrl: './contractbilltaxinsert.component.html',
  styleUrls: ['./contractbilltaxinsert.component.scss']
})
export class ContractbilltaxinsertComponent implements OnInit {
  public user: User;
  selectedTypeId: SelectItem[];

  currentFileUpload: File;
  options: boolean = false;
  contTax:Contractbilltax=new Contractbilltax();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;


  constructor(
    private dataService: DataService,
    private contractbilltaxService: ContractbilltaxService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.contractbilltaxService.addToTheList.subscribe(
      data =>{
        
       this.contTax = data;
       this.options = false;
      }
    )
  }

  createContBillTax(): void {

    if(!this.contTax.fiscal_year){
      this.severity = 'error';
      this.detailMsg = "Fiscal year cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contTax.entrydt = new Date(date);
    this.contTax.updatedt = new Date('01/01/1970');
    this.contTax.updateusr = "";
    this.contTax.entryusr = this.user.userId;
    
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contTax)
    }

    this.contractbilltaxService.insertContractBillTax(JSON.stringify(this.jsonMessage));

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbilltax']);
  // });
  this.reset(this.contTax);
  };

  updateContBillTax(contTax): void {


    if(!this.contTax.fiscal_year){
      this.severity = 'error';
      this.detailMsg = "Fiscal year cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    var today = new Date();
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contTax.updatedt = new Date(date);
    this.contTax.updateusr = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contTax)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractbilltaxService.updateContractBillTax(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });
    this.reset(this.contTax);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbilltax']);
  // });
  };


  deleteContBillTax(contTax): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(contTax)
    }
    this.contractbilltaxService.deleteContractBillTax(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbilltax']);
  // });
  this.reset(this.contTax);
  };


  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.contTax  = new Contractbilltax();
      form.reset({ status: this.contTax.fiscal_year});
      form.reset({ options: false });
    }

}
}
