import { Component, OnInit, ViewChild } from '@angular/core';
import { Contractbilltax } from 'src/app/models/Contractbilltax';
import { ContractbilltaxService } from 'src/app/services/contractbilltax.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-contractbilltaxinfo',
  templateUrl: './contractbilltaxinfo.component.html',
  styleUrls: ['./contractbilltaxinfo.component.scss']
})
export class ContractbilltaxinfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  contTax: Contractbilltax = null;

  contTaxs: Contractbilltax[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contractbilltaxService: ContractbilltaxService,
    public router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.cols = [

      { field: 'cont_tax_id', header: 'Contract Tax ID' },
      { field: 'cont_tax_rate', header: 'Contract Tax Rate' },
      { field: 'fiscal_year', header: 'Fiscal Year' },

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contTax)
    }

    const request = this.contractbilltaxService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {
        debugger;
        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.contTaxs = msg.payLoad;

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contractbilltax']);
  });
  }

  editContBillTax(contBill): void {
    this.contractbilltaxService.passTheValue(contBill);
  }

}
