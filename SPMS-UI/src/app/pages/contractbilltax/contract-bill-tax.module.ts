import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractBillTaxRoutingModule } from './contract-bill-tax-routing.module';
import { ContractbilltaxinfoComponent } from './contractbilltaxinfo/contractbilltaxinfo.component';
import { ContractbilltaxinsertComponent } from './contractbilltaxinsert/contractbilltaxinsert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';


@NgModule({
  declarations: [ContractbilltaxinfoComponent, ContractbilltaxinsertComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,ContractBillTaxRoutingModule
  ]
  
})
export class ContractBillTaxModule { }
