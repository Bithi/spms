import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractbilltaxinsertComponent } from './contractbilltaxinsert/contractbilltaxinsert.component';


const routes: Routes = [
  {path: '', component: ContractbilltaxinsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractBillTaxRoutingModule { }
