import { Component, OnInit, ViewChild } from '@angular/core';
import { Budgetbillentry } from 'src/app/models/Budgetbillentry';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { BudgetbillentryService } from 'src/app/services/budgetbillentry.service';
import { SelectItem, Table } from 'primeng';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { Utility } from 'src/app/util/utility';
import { BudgetMaster } from 'src/app/models/BudgetMaster';
import { BudgetMasterEntryService } from 'src/app/services/budgetMasterEntry.service';

@Component({
  selector: 'app-budgetMasterInfo',
  templateUrl: './budgetMasterInfo.component.html',
  styleUrls: ['./budgetMasterInfo.component.scss']
})
export class BudgetMasterInfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;

  budgetMasterInfo: BudgetMaster = null;
  contractNameSet = new Map();
  budgetMasterInfos: BudgetMaster[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  key = 'toast';
  severity: string;
  detailMsg: string;
  selectedVatList: SelectItem[];
  selectedAitList: SelectItem[];
  constructor(
    private budgetMasterEntryService: BudgetMasterEntryService,
    public router: Router,
    private dataService: DataService,
    private dropdownInfoService: DropdownInfoService,
    private utility: Utility
  ) { }
  getBudgetMasterInfo() {

    const request = this.budgetMasterEntryService.getBudgetMasterList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res);
        this.budgetMasterInfos = [];

        for (let i = 0; i < msg.length; i++) {
          // tslint:disable-next-line:new-parens
          this.budgetMasterInfo = new BudgetMaster;
          this.budgetMasterInfo.id = msg[i][0];
          this.budgetMasterInfo.amountAllocationDate = msg[i][1];
          this.budgetMasterInfo.amount = msg[i][2];
          this.budgetMasterInfo.year = msg[i][3];
          this.budgetMasterInfo.createdBy = msg[i][4];
          this.budgetMasterInfo.createdOn = msg[i][5];
          this.budgetMasterInfo.updatedBy = msg[i][6];
          this.budgetMasterInfo.updatedOn = msg[i][7];
         
          this.budgetMasterInfos[i] = this.budgetMasterInfo;
        }
      });

  }

  // getContractListInfo() {

  //   const request = this.dropdownInfoService.getContractInfos();
  //   request.subscribe(
  //     (res) => {

  //       const msg = JSON.parse(res);


  //       // tslint:disable-next-line:prefer-for-of
  //       for (let i = 0; i < msg.length; i++) {
  //         // this.selectedContract.push({ label: msg[i].contNo, value: msg[i].contId });
  //         this.contractNameSet.set(msg[i].contId, msg[i].contNo);
  //       }

  //     },
  //     err => {
  //       this.severity = 'error';
  //       this.detailMsg = 'Server Error: ' + err.message;
  //       this.utility.ShowToast(this.key, this.severity, this.detailMsg);
  //       return;
  //     }
  //   );


  // }

  // getVatProductList() {

  //   const request = this.dropdownInfoService.getVatProductList();
  //   request.subscribe(
  //     (res) => {

  //       const msg = JSON.parse(res);

  //       // tslint:disable-next-line:prefer-for-of
  //       for (let i = 0; i < msg.length; i++) {
  //         // tslint:disable-next-line:no-debugger
  //         debugger;
  //         this.selectedVatList.push({ label: msg[i].vatAitProductId, value: msg[i].productType });
  //         // this.contractNameSet.set(msg[i].contId, msg[i].contNo);
  //       }

  //     },
  //     err => {
  //       this.severity = 'error';
  //       this.detailMsg = 'Server Error: ' + err.message;
  //       this.utility.ShowToast(this.key, this.severity, this.detailMsg);
  //       return;
  //     }
  //   );


  // }

  // getAitProductList() {

  //   const request = this.dropdownInfoService.getAitProductList();
  //   request.subscribe(
  //     (res) => {

  //       const msg = JSON.parse(res);


  //       // tslint:disable-next-line:prefer-for-of
  //       for (let i = 0; i < msg.length; i++) {
  //         this.selectedAitList.push({ label: msg[i].vatAitProductId, value: msg[i].productType });

  //       }

  //     },
  //     err => {
  //       this.severity = 'error';
  //       this.detailMsg = 'Server Error: ' + err.message;
  //       this.utility.ShowToast(this.key, this.severity, this.detailMsg);
  //       return;
  //     }
  //   );


  // }

  ngOnInit(): void {

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    // this.getAitProductList();
    // this.getVatProductList();
   // this.getContractListInfo();
    this.getBudgetMasterInfo();

    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'amountAllocationDate', header: 'Fund Allocation Date' },
      { field: 'amount', header: 'Amount' },
      { field: 'year', header: 'Year' }
    ];


  }

  editBudgetMasterInfo(budgetMasterInfo): void {

    this.budgetMasterEntryService.passTheValue(budgetMasterInfo);
  }

  reloadPage(): void {
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }


}
