import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BudgetMasterRoutingModule } from './budget-master-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, MultiSelectModule, TableModule } from 'primeng';
import { BudgetMasterInfoComponent } from './budgetMasterInfo/budgetMasterInfo.component';
import { BudgetMasterInsertComponent } from './budgetMasterInsert/budgetMasterInsert.component';


@NgModule({
  declarations: [BudgetMasterInfoComponent, BudgetMasterInsertComponent],
  imports: [
    CommonModule,
    BudgetMasterRoutingModule,CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,MultiSelectModule
  ]
})
export class BudgetMasterModule { }
