import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Budgetbillentry } from 'src/app/models/Budgetbillentry';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { BudgetbillentryService } from 'src/app/services/budgetbillentry.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ItemPerBill } from 'src/app/models/ItemPerBill';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { ContractInfo } from 'src/app/models/contractinfo';
import { BudgetMaster } from 'src/app/models/BudgetMaster';
import { BudgetMasterEntryService } from 'src/app/services/budgetMasterEntry.service';
//import { BudgetbillentryinfoComponent } from '../budgetbillentryinfo/budgetbillentryinfo.component';

@Component({
  selector: 'app-budgetMasterInsert',
  templateUrl: './budgetMasterInsert.component.html',
  styleUrls: ['./budgetMasterInsert.component.scss']
})
export class BudgetMasterInsertComponent implements OnInit {

  // @ViewChild('invoice_no',{static:false}) invoice_no: ElementRef;
  // @ViewChild('invoice_dt') invoice_dt: ElementRef;
  // @ViewChild('bill_amount') bill_amount: ElementRef;
  // @ViewChild('cont_id') con_id: ElementRef;
  // @ViewChild('delivary_date') delivary_date: ElementRef;

  public user: User;
  vendorNameSet = new Map();
  cont_id: string;
  selectedvendor: SelectItem[];
  selectedBillEnt: SelectItem[];
  selectedContract: SelectItem[];
  options: boolean = false;
  budgetBillEntry: Budgetbillentry = new Budgetbillentry();
  jsonMessage: any;
  key: string = "toast";
  severity: string;
  detailMsg: string;
  wording: string;
  contractInfos: ContractInfo[];
  contractInfo: ContractInfo = new ContractInfo();
  budgetMaster: BudgetMaster = new BudgetMaster();
  selectedVatList: SelectItem[];
  selectedAitList: SelectItem[];

  yearList: any[];

  constructor(
    private dataService: DataService
    ,private budgetMasterEntryService: BudgetMasterEntryService
    ,private utility: Utility
    ,public router: Router
    ,private dropdownInfoService: DropdownInfoService
    , private contractInfoService: ContractInfoService
  ) { }

  ngOnInit() {

    this.yearList = [
      { label: '2010', value: '2010' },
      { label: '2011', value: '2011' },
      { label: '2012', value: '2012' },
      { label: '2013', value: '2013' },
      { label: '2014', value: '2014' },
      { label: '2015', value: '2015' },
      { label: '2016', value: '2016' },
      { label: '2017', value: '2017' },
      { label: '2018', value: '2018' },
      { label: '2019', value: '2019' },
      { label: '2020', value: '2020' },
      { label: '2021', value: '2021' },
      { label: '2022', value: '2022' },
      { label: '2023', value: '2023' },
      { label: '2024', value: '2024' },
      { label: '2025', value: '2025' },
      { label: '2026', value: '2026' },
      { label: '2027', value: '2027' },
      { label: '2028', value: '2028' },
      { label: '2029', value: '2029' },
      { label: '2030', value: '2030' },
      { label: '2031', value: '2031' },
      { label: '2032', value: '2032' },
      { label: '2033', value: '2033' },
      { label: '2034', value: '2034' },
      { label: '2035', value: '2035' },
      { label: '2036', value: '2036' },
      { label: '2037', value: '2037' },
      { label: '2038', value: '2038' },
      { label: '2039', value: '2039' },
      { label: '2040', value: '2040' },
      { label: '2041', value: '2041' },
      { label: '2042', value: '2042' },
      { label: '2043', value: '2043' },
      { label: '2044', value: '2044' },
      { label: '2045', value: '2045' },
      { label: '2046', value: '2046' },
      { label: '2047', value: '2047' },
      { label: '2048', value: '2048' },
      { label: '2049', value: '2049' },
      { label: '2050', value: '2050' }
  ];

   
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    
    this.budgetMasterEntryService.addToTheList.subscribe(
      data => {
        this.budgetMaster = data;
        this.options = false;
        //this.amcslaForm = this.contractInfo.type;

      }
    );

  }


  saveBudgetMasterEntry(budgetMaster) {

    //console.log("\nBill Budget Entry:\n" + JSON.stringify(budgetBillEntry));s
    this.budgetMaster = budgetMaster;

    if (!this.budgetMaster.amountAllocationDate || this.budgetMaster.amountAllocationDate == null ) {
      this.severity = 'error';
      this.detailMsg = "Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.invoice_dt.nativeElement.focus();
      return;
    }

    if (!this.budgetMaster.amount || this.budgetMaster.amount == null) {
      this.severity = 'error';
      this.detailMsg = "Amount cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    if (!this.budgetMaster.year || this.budgetMaster.year == null ) {
      this.severity = 'error';
      this.detailMsg = "Year cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    this.budgetMaster.createdBy = this.user.userId;
    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(budgetMaster)
    }


    this.budgetMasterEntryService.insertBudgetMasterEntry(JSON.stringify(this.jsonMessage));

  }

  updateBudgetMasterInfo(budgetMaster): void {

    this.budgetMaster = budgetMaster;

    if (!this.budgetMaster.amountAllocationDate || this.budgetMaster.amountAllocationDate == null ) {
      this.severity = 'error';
      this.detailMsg = "Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.invoice_dt.nativeElement.focus();
      return;
    }

    if (!this.budgetMaster.amount || this.budgetMaster.amount == null) {
      this.severity = 'error';
      this.detailMsg = "Amount cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    if (!this.budgetMaster.year || this.budgetMaster.year == null ) {
      this.severity = 'error';
      this.detailMsg = "Year cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    this.budgetMaster.updatedBy = this.user.userId;
    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(budgetMaster)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.budgetMasterEntryService.updateBudgetMasterInfo(JSON.stringify(this.jsonMessage));
    
    this.reloadPage();
  };

  deleteBudgetBillInfo(budgetBillEntry): void {

    console.log("Del Data\n" + JSON.stringify(budgetBillEntry.budget_bill_ref));

    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(budgetBillEntry)
    }

    this.budgetMasterEntryService.deleteBudgetMasterInfo(JSON.stringify(this.jsonMessage));

    this.reloadPage();

  };

  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  setBillAmountWords(bill_amount) {
    
    this.wording = "";
    this.budgetBillEntry.bill_amount_in_words = this.wording;
    //console.log("\ninit: "+this.budgetBillEntry.bill_amount_in_words);

    if (bill_amount == 0 || !bill_amount) {
      console.log("\nzero or blank...\n");
      this.wording = "";
    }

    else {
      //console.log("\nB:-"+bill_amount);
      bill_amount = parseInt(bill_amount, 10);
      
      this.wording = this.budgetMasterEntryService.convertGreaterThanThousand(bill_amount) + " Only."
    }
    this.budgetMaster.amount_in_words = this.wording;
  }



}


