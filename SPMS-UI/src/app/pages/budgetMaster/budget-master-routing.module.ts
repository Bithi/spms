import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetMasterInsertComponent } from './budgetMasterInsert/budgetMasterInsert.component';


const routes: Routes = [
  {path: '', component: BudgetMasterInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetMasterRoutingModule { }
