import { Component, OnInit } from '@angular/core';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { Menu } from 'src/app/models/menu';
import { MenuInfoService } from 'src/app/services/menu-info.service';
import { MessageService, SelectItem } from 'primeng/api';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-info-insert',
  templateUrl: './menu-info-insert.component.html',
  styleUrls: ['./menu-info-insert.component.scss']
})
export class MenuInfoInsertComponent implements OnInit {
  parentMenuPath = new Map();
  selectedmenu:SelectItem[];
  currentFileUpload: File;
  options: boolean = false;

  menu: Menu = new Menu();
  jsonMessage:any;
  key:string = "toast";
    severity:string;
    detailMsg:string;

  constructor(
    private dataService: DataService,
    private menuInfoService: MenuInfoService,
    private utility:Utility,
    private dropdownInfoService: DropdownInfoService,
    public router:Router
  ) { }

  ngOnInit() {
    this.selectedmenu = [];
    this.selectedmenu.push({ label: 'Select Parent Menu', value: -1});
    this.getParentMenuList();

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.menuInfoService.addToTheList.subscribe(
      data =>{
        
       this.menu = data;
       this.options = false;
      }
    )
  }

  createMenu(): void {       
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.menu)
    }

    this.menuInfoService.insertMenuInfo(JSON.stringify(this.jsonMessage));

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['menuinfo']);
  // });
  this.reset(this.menu);
  };


  updateMenu(menu): void {

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.menu)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.menuInfoService.updateMenuInfo(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['menuinfo']);
  });
  };


  getParentMenuList(){
   
    const request = this.dropdownInfoService.getMenuList();
    request.subscribe(
      (res ) => {
              const msg = JSON.parse(res) ;
              const menu =msg;
              var parentMenuName = "";
              for(let i = 0; i< msg.length; i++) {
                // var parentId = msg[i].parentMenuId;
                // for(let k = 0; k< menu.length; k++) {
                //   if(menu[k].m_id === parentId){
                //     parentMenuName = '('+ menu[k].menuName+')';
                //     if(parentMenuName ==="(root)"){
                //       parentMenuName = ""
                //     }
                    
                //   }
                // }
                debugger;
                this.selectedmenu.push({label: msg[i][2].substring(1), value: msg[i][0]});
                this.parentMenuPath.set(msg[i][0], msg[i][2].substring(1));
                // this.selectedmenu.push({label: msg[i].menuName, value: msg[i].m_id});
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
}




selectFile(event) {  
  this.currentFileUpload = event.target.files.item(0);
  $('.custom-file-label').html(this.currentFileUpload.name);
}




reset(form) {

  if(this.options){
    $('.custom-file-label').html('Choose file');

  }
  else{
    this.menu  = new Menu();
    form.reset({ menuName: this.menu.menuName });
    form.reset({ parentMenuId: this.menu.parentMenuId });
    form.reset({ options: false });
  }
  
}


deleteMenu(menu): void {
   
    

  var dataHeader = this.dataService.createMessageHeader("DELETE");
  
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(menu)
  }
  this.menuInfoService.deleteMenuInfo(JSON.stringify(this.jsonMessage));
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['menuinfo']);
});
 
};

}
