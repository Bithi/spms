import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuInfoInsertComponent } from './menu-info-insert/menu-info-insert.component';


const routes: Routes = [
  {path: '', component: MenuInfoInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuInfoRoutingModule { }
