import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuInfoRoutingModule } from './menu-info-routing.module';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from 'src/app/layout/layout.module';
import { DropdownModule, ProgressSpinnerModule, TableModule } from 'primeng';
import { MenuInfoInsertComponent } from './menu-info-insert/menu-info-insert.component';
import { MenuInfoComponent } from './menu-info/menu-info.component';


@NgModule({
  declarations: [MenuInfoInsertComponent, MenuInfoComponent],
  imports: [
    CommonModule,
    MenuInfoRoutingModule,
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule
  ]
})
export class MenuInfoModule { }
