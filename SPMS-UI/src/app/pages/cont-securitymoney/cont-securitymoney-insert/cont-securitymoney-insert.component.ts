import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { User } from 'src/app/models/user';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ContSecurityMoney } from 'src/app/models/ContSecurityMoney';
import { ContSecurityMoneyService } from 'src/app/services/contsecuritymoney.service';
import { Table } from 'primeng';

declare var $: any;


@Component({
  selector: 'app-cont-securitymoney-insert',
  templateUrl: './cont-securitymoney-insert.component.html',
  styleUrls: ['./cont-securitymoney-insert.component.scss']
})
export class ContSecurityMoneyInsertComponent implements OnInit {
  public user: User;
  @ViewChild(Table) dt: Table;
  selecteContractList: SelectItem[];
  securityType = [
    { label: 'Select Security Type', value: -1 },
    { label: 'Bank Guarantee', value: 'BG' },
    { label: 'Pay Order', value: 'PO' }
  ]
   
  public ContSecurityMoney: ContSecurityMoney = new ContSecurityMoney();
  jsonMessage: any;
  options: boolean = false;

  key: string = "toast";
  severity: string;
  detailMsg: string;
  moment: any;

  constructor(
    private dataService: DataService,
    private contSecurityMoneyService: ContSecurityMoneyService,
    public router: Router,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.contSecurityMoneyService.addToTheList.subscribe(
      data => {
        this.ContSecurityMoney = data;
        this.options = false;
      }
    )

    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract', value: -1 });
    this.getContractListInfo();


  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }
  create(): void {

    if (!this.ContSecurityMoney.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.securityType) {
      this.severity = 'error';
      this.detailMsg = "Security Type cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.issueBank) {
      this.severity = 'error';
      this.detailMsg = "Issue Bank cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.issueBranch) {
      this.severity = 'error';
      this.detailMsg = "Issue Branch cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    if (!this.ContSecurityMoney.issueAmount|| this.ContSecurityMoney.issueAmount == 0) {
      this.severity = 'error';
      this.detailMsg = "Issue Amount cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.ContSecurityMoney.issueDate) {
      this.severity = 'error';
      this.detailMsg = "Issue Date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.ContSecurityMoney.periodSecurity || this.ContSecurityMoney.periodSecurity == 0) {
      this.severity = 'error';
      this.detailMsg = "Period Security cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.returnDate) {
      this.severity = 'error';
      this.detailMsg = "Return Date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    this.ContSecurityMoney.entryDt = new Date();
    this.ContSecurityMoney.entryUser = this.user.userId;
    this.ContSecurityMoney.updateDt = new Date('01/01/1970');   
    this.ContSecurityMoney.updateUser = "";

    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.ContSecurityMoney)
    }
    this.contSecurityMoneyService.insertData(JSON.stringify(this.jsonMessage));

    this.reset(this.ContSecurityMoney);
    // this.router.navigateByUrl('/contSecurityMoneyList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSecurityMoney']);
    // });
  };


  update(ContSecurityMoney): void {

    debugger;
    if (!this.ContSecurityMoney.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.securityType) {
      this.severity = 'error';
      this.detailMsg = "Security Type cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.issueBank) {
      this.severity = 'error';
      this.detailMsg = "Issue Bank cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.issueBranch) {
      this.severity = 'error';
      this.detailMsg = "Issue Branch cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    if (!this.ContSecurityMoney.issueAmount|| this.ContSecurityMoney.issueAmount == 0) {
      this.severity = 'error';
      this.detailMsg = "Issue Amount cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.ContSecurityMoney.issueDate) {
      this.severity = 'error';
      this.detailMsg = "Issue Date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.ContSecurityMoney.periodSecurity || this.ContSecurityMoney.periodSecurity == 0) {
      this.severity = 'error';
      this.detailMsg = "Period Security cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.ContSecurityMoney.returnDate) {
      this.severity = 'error';
      this.detailMsg = "Return Date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    this.ContSecurityMoney.updateDt = new Date();
    this.ContSecurityMoney.updateUser = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(ContSecurityMoney)
    }
    this.contSecurityMoneyService.updateData(JSON.stringify(this.jsonMessage));
    this.reset(this.ContSecurityMoney);
    // this.router.navigateByUrl('/contSecurityMoneyList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSecurityMoney']);
    // });
  };


  delete(ContSecurityMoney): void {
    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(ContSecurityMoney)
    }
    this.contSecurityMoneyService.deleteData(JSON.stringify(this.jsonMessage));
    this.reset(this.ContSecurityMoney);
    // this.router.navigateByUrl('/contSecurityMoneyList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSecurityMoney']);
    // });
  }



  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  reset(form) {

    if (this.options) {
      $('.custom-file-label').html('Choose file');

    }
    else {
      this.ContSecurityMoney = new ContSecurityMoney();
    }



  }


}
