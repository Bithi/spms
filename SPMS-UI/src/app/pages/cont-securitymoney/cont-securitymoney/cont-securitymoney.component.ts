import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ContSecurityMoney } from 'src/app/models/ContSecurityMoney';
import { DataService } from 'src/app/data/data.service';
import { ContSecurityMoneyService } from 'src/app/services/contsecuritymoney.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-cont-securitymoney',
  templateUrl: './cont-securitymoney.component.html',
  styleUrls: ['./cont-securitymoney.component.scss']
})
export class ContSecurityMoneyComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  cols: any[];

  dataL: ContSecurityMoney;

  contSecurityMoney: ContSecurityMoney = new ContSecurityMoney();

  dataList: ContSecurityMoney[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contSecurityMoneyService: ContSecurityMoneyService,
    public router: Router,
    private dataService: DataService,
  ) { }


  getSecurityMoney() {

    var request = this.contSecurityMoneyService.getSecurityMoneyList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.dataList =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.dataL = new ContSecurityMoney;
         
        debugger;
          this.dataL.contId= msg[i][0];
          this.dataL.securityType= msg[i][1]=='BG'?'Bank Gaurantee':'Pay Order';
          this.dataL.issueBank= msg[i][2];
          this.dataL.issueBranch= msg[i][3];
          this.dataL.issueAmount= msg[i][4];
          this.dataL.issueDate= msg[i][5];
          this.dataL.periodSecurity= msg[i][6];
          this.dataL.returnDate= msg[i][7];
          this.dataL.entryUser= msg[i][9];
          this.dataL.entryDt= msg[i][8];
       
       
       
          this.dataL.updateUser= msg[i][10];
          
          this.dataL.updateDt= msg[i][11];
          this.dataL.contSecurityId= msg[i][12]; 
          this.dataL.contNo= msg[i][13];

          this.dataList[i] = this.dataL;
          // console.log( this.vendorList);
     }
    });

  }


  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

   //this.getList();
   this.getSecurityMoney();

    this.cols = [
      { field: 'contSecurityId', header: 'Id' },
      { field: 'contNo', header: 'Contract Number' },
      { field: 'securityType', header: 'Security Type' },
      { field: 'issueBank', header: 'Issue Bank' },
      { field: 'issueBranch', header: 'Issue Branch' },
      { field: 'issueAmount', header: 'Issue Amount' }
    ];

  }
  

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contSecurityMoney']);
  });
}

  editData(contSecurityMoney): void {
     this.contSecurityMoneyService.passTheValue(contSecurityMoney);
  }

}
