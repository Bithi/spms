import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContSecurityMoneyInsertComponent } from './cont-securitymoney-insert/cont-securitymoney-insert.component';


const routes: Routes = [
  {path: '', component: ContSecurityMoneyInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContSecuritymoneyRoutingModule { }
