import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContSecuritymoneyRoutingModule } from './cont-securitymoney-routing.module';
import { ContSecurityMoneyComponent } from './cont-securitymoney/cont-securitymoney.component';
import { ContSecurityMoneyInsertComponent } from './cont-securitymoney-insert/cont-securitymoney-insert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CalendarModule, DropdownModule, ProgressSpinnerModule, TableModule, ToastModule } from 'primeng';

@NgModule({
  declarations: [ContSecurityMoneyComponent, ContSecurityMoneyInsertComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    HttpModule,ToastModule,TableModule,DropdownModule,CalendarModule,
    ProgressSpinnerModule,ContSecuritymoneyRoutingModule
  ],
  exports: [ContSecurityMoneyComponent, ContSecurityMoneyInsertComponent]
})

export class ContSecuritymoneyModule { }
