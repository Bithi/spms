import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { UserInfoService } from 'src/app/services/user-info.service';
import { Location, formatDate } from '@angular/common';
import { VendorInfoService } from 'src/app/services/vendor-info.service';
import { User } from 'src/app/models/user';
import { ResourceLoader } from '@angular/compiler';
import { AmcNotification } from 'src/app/models/AmcNotification';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { AmcNotificationService } from 'src/app/services/amcnotification.service';
declare var $: any;


@Component({
  selector: 'app-amc-notification-insert',
  templateUrl: './amc-notification-insert.component.html',
  styleUrls: ['./amc-notification-insert.component.scss']
})
export class AmcNotificationInsertComponent implements OnInit {
  public myDate;
  public user: User;

  selecteContractList: SelectItem[];
  currentFileUpload: File;
  public amcNotification: AmcNotification = new AmcNotification();
  jsonMessage: any;
  options: boolean = false;

  key: string = "toast";
  severity: string;
  detailMsg: string;
  moment: any;

  constructor(
    private dataService: DataService,
    private amcNotService: AmcNotificationService,
    public router: Router,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit() {


    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.amcNotService.addToTheList.subscribe(
      data => {
        this.amcNotification = data;
        this.options = false;
      }
    )

    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract Number', value: -1 });
    this.getContractListInfo();

  }
 
  
  phonenumber(event): boolean
{
  const charCode = (event.which) ? event.which : event.keyCode;
  var phoneno = /^\d{10}$/;
  if((charCode.value.match(phoneno)))
        {
      return true;
        }
      else
        {
        return false;
        }
}
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
        }





      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }





  createVendor(): void {

    if (!this.amcNotification.contractId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.vendorMobile) {
      this.severity = 'error';
      this.detailMsg = "Vendor mobile number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.amcNotification.vendorEmail) {
      this.severity = 'error';
      this.detailMsg = "Vendor email id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.amcNotification.gmItMobile) {
      this.severity = 'error';
      this.detailMsg = "GM IT mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.gmItEmail) {
      this.severity = 'error';
      this.detailMsg = "GM IT email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dgmItProcMobile) {
      this.severity = 'error';
      this.detailMsg = "DGM IT PRocurement mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dgmItProcEmail) {
      this.severity = 'error';
      this.detailMsg = "DGM IT PRocurement email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dealingOffMobile) {
      this.severity = 'error';
      this.detailMsg = "Deailing office mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dealingOffEmail) {
      this.severity = 'error';
      this.detailMsg = "Dealing office email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm1ItprocMob) {
      this.severity = 'error';
      this.detailMsg = "AGM 1 IT PRocurement mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm1ItprocEmail) {
      this.severity = 'error';
      this.detailMsg = "AGM 1 IT PRocurement email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm2ItprocMob) {
      this.severity = 'error';
      this.detailMsg = "AGM 2 IT PRocurement mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm2ItprocEmail) {
      this.severity = 'error';
      this.detailMsg = "AGM 2 IT PRocurement email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.notifBefore) {
      this.severity = 'error';
      this.detailMsg = "Seding notification before day should be given";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.notifIntervalDay) {
      this.severity = 'error';
      this.detailMsg = "Seding Notification interval day should be given";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
   


    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.amcNotification.entryDate = new Date();
    this.amcNotification.entryUser = this.user.userId;
    this.amcNotification.lastNotifDate = new Date('01/01/1970'); 
   
    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.amcNotification)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.amcNotService.insertNotification(JSON.stringify(this.jsonMessage));
    this.reset(this.amcNotification);

    // this.router.navigateByUrl('/notificationList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['notification']);
    // });
  };


  update(vendor): void {

    if (!this.amcNotification.contractId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.vendorMobile) {
      this.severity = 'error';
      this.detailMsg = "Vendor mobile number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.amcNotification.vendorEmail) {
      this.severity = 'error';
      this.detailMsg = "Vendor email id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.amcNotification.gmItMobile) {
      this.severity = 'error';
      this.detailMsg = "GM IT mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.gmItEmail) {
      this.severity = 'error';
      this.detailMsg = "GM IT email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dgmItProcMobile) {
      this.severity = 'error';
      this.detailMsg = "DGM IT PRocurement mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dgmItProcEmail) {
      this.severity = 'error';
      this.detailMsg = "DGM IT PRocurement email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dealingOffMobile) {
      this.severity = 'error';
      this.detailMsg = "Deailing office mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.dealingOffEmail) {
      this.severity = 'error';
      this.detailMsg = "Dealing office email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm1ItprocMob) {
      this.severity = 'error';
      this.detailMsg = "AGM 1 IT PRocurement mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm1ItprocEmail) {
      this.severity = 'error';
      this.detailMsg = "AGM 1 IT PRocurement email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm2ItprocMob) {
      this.severity = 'error';
      this.detailMsg = "AGM 2 IT PRocurement mobile cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.agm2ItprocEmail) {
      this.severity = 'error';
      this.detailMsg = "AGM 2 IT PRocurement email cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.notifBefore) {
      this.severity = 'error';
      this.detailMsg = "Seding notification before day should be given";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.amcNotification.notifIntervalDay) {
      this.severity = 'error';
      this.detailMsg = "Seding Notification interval day should be given";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
   

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(vendor)
    }
    this.amcNotService.updateNotification(JSON.stringify(this.jsonMessage));
    this.reset(this.amcNotification);
    // this.router.navigateByUrl('/notificationList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['notification']);
    // });
  };


  delete(vendor): void {
    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(vendor)
    }
    this.amcNotService.deleteNotification(JSON.stringify(this.jsonMessage));
    this.reset(this.amcNotification);
    // this.router.navigateByUrl('/notificationList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['notification']);
    // });
  }



  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  reset(form) {

    if (this.options) {
      $('.custom-file-label').html('Choose file');

    }
    else {
      this.amcNotification = new AmcNotification();
      //form.reset({ options: false });
    }



  }


}
