import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserInfoInsertComponent } from '../user-info/user-info-insert/user-info-insert.component';
import { AmcNotificationInsertComponent } from './amc-notification-insert/amc-notification-insert.component';
import { AmcNotificationComponent } from './amc-notification/amc-notification.component';

const routes: Routes = [
    {path: '', component: AmcNotificationInsertComponent},
    ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmcNotificationRoutingModule { }
