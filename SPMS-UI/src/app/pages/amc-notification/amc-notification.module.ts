import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { AmcNotificationComponent } from './amc-notification/amc-notification.component';
import { AmcNotificationInsertComponent } from './amc-notification-insert/amc-notification-insert.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { LayoutModule } from 'src/app/layout/layout.module';
import { AmcNotificationRoutingModule } from './amc-notification-routing.module';

@NgModule({
  declarations: [AmcNotificationComponent, AmcNotificationInsertComponent],
  imports: [
    CommonModule,
    AmcNotificationRoutingModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    HttpModule,ToastModule,TableModule,DropdownModule,CalendarModule,
    ProgressSpinnerModule
  ],
  exports: [AmcNotificationComponent, AmcNotificationInsertComponent]
})
export class AmcNotificationModule { }
