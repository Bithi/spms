import { Component, OnInit, ViewChild } from '@angular/core';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { AmcNotificationService } from 'src/app/services/amcnotification.service';
import { AmcNotification } from 'src/app/models/AmcNotification';
import { Table } from 'primeng';

@Component({
  selector: 'app-amc-notification',
  templateUrl: './amc-notification.component.html',
  styleUrls: ['./amc-notification.component.scss']
})
export class AmcNotificationComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  cols: any[];

  vendorType = new Map();
  vendorServiceType = new Map();
  notification: AmcNotification = null;

  notifications: AmcNotification[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private amcNotificationService: AmcNotificationService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility
  ) { }

  ngOnInit() {
   
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

   this.getNotifications();

    this.cols = [
      { field: 'contractNo', header: 'Contract Number' },
      { field: 'vendorMobile', header: 'Vendor Mobile' },
      { field: 'vendorEmail', header: 'Vendor Email' },
      { field: 'notifBefore', header: 'Notify Before' },
      { field: 'lastNotifDate', header: 'Last Notification Date' }
    ];

  }
  getNotifications() {
   
    const request = this.amcNotificationService.getcontAmcJoin();
    request.subscribe(
      (res) => {
        const msg = JSON.parse(res) ;
        this.notifications =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.notification = new AmcNotification();
          this.notification.contractId= msg[i][0];
          this.notification.vendorMobile= msg[i][1];
          this.notification.vendorEmail= msg[i][2];
          this.notification.gmItMobile= msg[i][3];
          this.notification.gmItEmail= msg[i][4];
          this.notification.dgmItProcMobile= msg[i][5];
          this.notification.dgmItProcEmail= msg[i][6];
          this.notification.dealingOffMobile= msg[i][7];
          this.notification.dealingOffEmail= msg[i][8];
          this.notification.agm1ItprocMob= msg[i][9];
          this.notification.agm1ItprocEmail= msg[i][10];
  
          this.notification.notifBefore= msg[i][11];
          this.notification.notifIntervalDay= msg[i][12];
          this.notification.lastNotifDate= msg[i][13];
          this.notification.entryUser= msg[i][14];
          this.notification.entryDate= msg[i][15];
          this.notification.amcNotifId= msg[i][16];
          this.notification.agm2ItprocMob= msg[i][17];
          this.notification.agm2ItprocEmail= msg[i][18];
          this.notification.contractNo= msg[i][19];
          this.notifications[i] = this.notification;
       
        }


      },
      err => {
        
        return;
      }
    );

  }
  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['notification']);
  });
  }
 
  


  

  // getServiceTypeList() {
  //   const request = this.vendorInfoService.getVendorServiceType();
  //   request.subscribe(
  //     (res) => {

  //       const msg = JSON.parse(res);

  //       for (let i = 0; i < msg.length; i++) {
  //         this.vendorServiceType.set(msg[i].serviceTypeId, msg[i].serviceTypeName);
          
  //       }

  //     },
  //     err => {
        
  //       return;
  //     }
  //   );


  // }

  editVendor(vendor): void {
    this.amcNotificationService.passTheValue(vendor);
  }
  
}
