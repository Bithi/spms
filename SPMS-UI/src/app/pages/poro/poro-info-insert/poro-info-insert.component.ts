import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Poro } from 'src/app/models/poro';
import { PoroService } from 'src/app/services/poro.service';
import { DataService } from 'src/app/data/data.service';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { isNumber, isString } from 'util';
import { Location } from '@angular/common';


@Component({
  selector: 'app-poro-info-insert',
  templateUrl: './poro-info-insert.component.html',
  styleUrls: ['./poro-info-insert.component.scss']
})
export class PoroInfoInsertComponent implements OnInit {

  poro:Poro=new Poro();
  isReadonly = false;

  toggleReadonly() {
    this.isReadonly = true;
  }
  jsonMessage:any;
  constructor(
    private poroInfoService: PoroService,
    private dataService: DataService,
    public router:Router,
    private messageService:MessageService,
    private utility:Utility

  ) { }

   key:string = "toast";
    severity:string;
    detailMsg:string;

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.poroInfoService.addToTheList.subscribe(
      data => this.poro = data
    )
    
  }
  createPoro(): void {

    if(!this.poro.poroPrefix){
      this.severity = 'error';
      this.detailMsg = "poroPrefix cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
      
    }else if(this.poro.poroPrefix.length>2){
      this.severity = 'error';
      this.detailMsg = "poroPrefix connot be more than 2";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    else if(!this.poro.poroName){
      this.severity = 'error';
      this.detailMsg = "Poro name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    } 
   else if(this.poro.poroName.length>30){
      this.severity = 'error';
      this.detailMsg = "Poro name connot be more than 30 character";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    } 
    else if(!this.poro.poroName){
      this.severity = 'error';
      this.detailMsg = "PoroType cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    else if(this.poro.poroType.length>1){
      this.severity = 'error';
      this.detailMsg = "Poro type connot be more than 1 character";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    } 
    else if(!this.poro.poroType.length){
      this.severity = 'error';
      this.detailMsg = "PoroType cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }else if(this.poro.poroType.length>1){
      this.severity = 'error';
      this.detailMsg = "Poro type connot be more than 1 character";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return; 
    }  
 
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.poro)
    }
     this.poroInfoService.insertPoroInfo(JSON.stringify(this.jsonMessage));
    
  };


  updatePoro(poro): void {
   
 
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(poro)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.poroInfoService.updatePoroInfo(JSON.stringify(this.jsonMessage));
   
  //   request.subscribe(
  //     (data ) => {
  //       const msg = JSON.parse( atob(data) );
  //      // alert("Employee created successfully.");
  //      debugger;
  //      console.log(msg.payload);
  //     });

    //this.router.navigate(['/create']);
  };

  deletePoro(poro): void {
   
    

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(poro)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.poroInfoService.deletePoroInfo(JSON.stringify(this.jsonMessage));
   
  //   request.subscribe(
  //     (data ) => {
  //       const msg = JSON.parse( atob(data) );
  //      // alert("Employee created successfully.");
  //      debugger;
  //      console.log(msg.payload);
  //     });

    //this.router.navigate(['/create']);
  };




  focusoutHandlerPoroPrefix(event): void{
  
    var x = event.target.value;
    if(x == ' '){
      this.severity = 'error';
      this.detailMsg = "Poro prefix cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
     
    }else if(x.length>2){
      this.severity = 'error';
      this.detailMsg = "Poro prefix connot be more than 2";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      
    }
    return;
    
  }
  focusoutHandlerPoroName(event): void{
  
    var x = event.target.value;
    if(!x){
      this.severity = 'error';
      this.detailMsg = "Poro name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      
    } 
    if(x.length>30){
      this.severity = 'error';
      this.detailMsg = "Poro name connot be more than 30 character";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    
    }   
    return;
  }


  focusoutHandlerPoroType(event): void{
  
    var x = event.target.value;
    if(!x){
      this.severity = 'error';
      this.detailMsg = "PoroType cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }else if(x.length>1){
      this.severity = 'error';
      this.detailMsg = "Poro type connot be more than 1 character";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    
    }  
    return;  
  }

  focusoutHandlerGmoId(event): void{
    
    var x = event.target.value;
    
    if(!x){
      this.severity = 'error';
      this.detailMsg = "Gmo Id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      
      var len = Math.log10(x) + 1;
      
    }if(x.length > 2){
      this.severity = 'error';
      this.detailMsg = "Gmo Id  connot be more than 2 character";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
   
    }    
    return;
  }
  

}
