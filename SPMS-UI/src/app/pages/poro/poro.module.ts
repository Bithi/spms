import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PoroInfoInsertComponent } from './poro-info-insert/poro-info-insert.component';
import { NgModule } from '@angular/core';
import { PoroListComponent } from './poro-list/poro-list.component';
import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import { LayoutModule } from 'src/app/layout/layout.module';



@NgModule({
  declarations: [PoroInfoInsertComponent,PoroListComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule
    
  ],
  exports: [PoroInfoInsertComponent,PoroListComponent]
})
export class POROModule { }
