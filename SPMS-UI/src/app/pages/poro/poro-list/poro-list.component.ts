import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { PoroService } from 'src/app/services/poro.service';
import { DataService } from 'src/app/data/data.service';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { Observable } from 'rxjs';
import { Poro } from 'src/app/models/poro';





@Component({
  selector: 'app-poro-list',
  templateUrl: './poro-list.component.html',
  styleUrls: ['./poro-list.component.scss']
})
export class PoroListComponent implements OnInit {

  poroInfos: Poro[];
  poros: Poro[];
  poro:Poro = null;
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private poroService: PoroService,
    private dataService: DataService,
      public router: Router,
      private messageService: MessageService,
      private utility:Utility
  ) { }

  isReadonly = false;

  toggleReadonly() {
    this.isReadonly = true;
  }

  // getUsers(){
  //   var me = this;
  //   this.poroService.getPorosList().subscribe(data => {
  //     debugger;
  //     me.poros=data;
  //   });
  // }



  getPoroList(){
   
    var request = this.poroService.getPoros();
      request.subscribe(
          (res) => {
              const msg = JSON.parse(atob(res));
              this.poroInfos=msg.payLoad;
          },
          err => {

          }
      );
  // });

}






  createEmployee(): void {
    this.router.navigate(['/create']);  
  };
  poroPrefix: string;
  poroName: string;
  poroType:string;
  gmoId: string;
  poroCode: string
  gmoCode: string;

  ngOnInit() {
    this.cols = [
      { field: 'poroPrefix', header: 'Poro Perfix' },
      { field: 'poroName', header: 'Poro Name' },
      { field: 'poroType', header: 'Poro Type' },
      { field: 'gmoId', header: 'Gmo ID' },
      { field: 'poroCode', header: 'Poro Code' },
      { field: 'gmoCode', header: 'Gmo Code' }
  ];
    //this.getUsers();
  
        
  }
  
  
  editPoro(poro): void{

    this.poroService.passTheValue(poro);
    
  };




 
}
