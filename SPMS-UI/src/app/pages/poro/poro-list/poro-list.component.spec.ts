import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoroListComponent } from './poro-list.component';

describe('PoroListComponent', () => {
  let component: PoroListComponent;
  let fixture: ComponentFixture<PoroListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoroListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
