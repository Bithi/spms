import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleWiseUserInsertComponent } from './role-wise-user-insert.component';

describe('RoleWiseUserInsertComponent', () => {
  let component: RoleWiseUserInsertComponent;
  let fixture: ComponentFixture<RoleWiseUserInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleWiseUserInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleWiseUserInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
