import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleWiseUserInsertComponent } from './role-wise-user-insert/role-wise-user-insert.component';


const routes: Routes = [
  {path: '', component: RoleWiseUserInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleWiseUserRoutingModule { }
