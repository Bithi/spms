import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleWiseUserComponent } from './role-wise-user.component';

describe('RoleWiseUserComponent', () => {
  let component: RoleWiseUserComponent;
  let fixture: ComponentFixture<RoleWiseUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleWiseUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleWiseUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
