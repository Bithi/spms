import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoleWiseUserRoutingModule } from './role-wise-user-routing.module';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from 'src/app/layout/layout.module';
import { DropdownModule, ProgressSpinnerModule, TableModule } from 'primeng';
import { RoleWiseUserInsertComponent } from './role-wise-user-insert/role-wise-user-insert.component';
import { RoleWiseUserComponent } from './role-wise-user/role-wise-user.component';


@NgModule({
  declarations: [RoleWiseUserInsertComponent,RoleWiseUserComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    RoleWiseUserRoutingModule
  ]
})
export class RoleWiseUserModule { }
