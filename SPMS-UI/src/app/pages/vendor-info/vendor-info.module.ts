import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorInfoRoutingModule } from './vendor-info-routing.module';
import { VendorInfoComponent } from './vendor-info/vendor-info.component';
import { VendorInsertComponent } from './vendor-insert/vendor-insert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CalendarModule, DropdownModule, ProgressSpinnerModule, TableModule, ToastModule } from 'primeng';


@NgModule({
  declarations: [VendorInfoComponent, VendorInsertComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    HttpModule,ToastModule,TableModule,DropdownModule,CalendarModule,
    ProgressSpinnerModule,
    VendorInfoRoutingModule
  ]
})
export class VendorInfoModule { }
