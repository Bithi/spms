import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorInsertComponent } from './vendor-insert/vendor-insert.component';


const routes: Routes = [ 
   {path: '', component: VendorInsertComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorInfoRoutingModule { }
