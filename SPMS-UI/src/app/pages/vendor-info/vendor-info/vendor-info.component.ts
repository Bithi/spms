import { Component, OnInit, ViewChild } from '@angular/core';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { VendorInfoService } from 'src/app/services/vendor-info.service';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { Table } from 'primeng';

@Component({
  selector: 'app-vendor-info',
  templateUrl: './vendor-info.component.html',
  styleUrls: ['./vendor-info.component.scss']
})
export class VendorInfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  cols: any[];

  vendorType = new Map();
  vendorServiceType = new Map();
  vendor: VendorInfo = null;

  vendors: VendorInfo[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private vendorInfoService: VendorInfoService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility
  ) { }

  ngOnInit() {
    this.vendor = new VendorInfo();
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.getVendorTypeList();
    this.getServiceTypeList();
    this.getVendorList();

    this.cols = [
      { field: 'venName', header: 'Vendor Name' },
      { field: 'venTypeId', header: 'Vendor Type' },
      { field: 'serviceTypeId', header: 'Service Type' },
      { field: 'venAdd', header: 'Vendor Address' }
    ];

  }

  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);

}
  getVendorList() {
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.vendor)
    }

    
    const request = this.vendorInfoService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.vendors = msg.payLoad;
        console.log(this.vendors);

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }


  getVendorTypeList() {
    const request = this.vendorInfoService.getVendorType();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.vendorType.set(msg[i].venTypeId, msg[i].venTypeName);
          
        }

      },
      err => {
       
        return;
      }
    );


  }



  getServiceTypeList() {
    const request = this.vendorInfoService.getVendorServiceType();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.vendorServiceType.set(msg[i].serviceTypeId, msg[i].serviceTypeName);
          
        }

      },
      err => {
        
        return;
      }
    );


  }

  editVendor(vendor): void {
    this.vendorInfoService.passTheValue(vendor);
  }

}
