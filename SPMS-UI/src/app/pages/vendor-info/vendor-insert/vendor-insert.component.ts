import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { UserInfoService } from 'src/app/services/user-info.service';
import { Location, formatDate } from '@angular/common';
import { VendorInfoService } from 'src/app/services/vendor-info.service';
import { User } from 'src/app/models/user';
import { ResourceLoader } from '@angular/compiler';
declare var $: any;


@Component({
  selector: 'app-vendor-insert',
  templateUrl: './vendor-insert.component.html',
  styleUrls: ['./vendor-insert.component.scss']
})
export class VendorInsertComponent implements OnInit {
  public myDate;
  public user: User;

  selectedvendortype: SelectItem[];

  selectservicetype: SelectItem[];

  selectcontractnumber: SelectItem[];
  currentFileUpload: File;
  public vendor: VendorInfo = new VendorInfo();
  jsonMessage: any;
  options: boolean = false;

  key: string = "toast";
  severity: string;
  detailMsg: string;
  moment: any;

  constructor(
    private dataService: DataService,
    public router: Router,
    private utility: Utility,
    private vendorInforService: VendorInfoService
  ) { }

  ngOnInit() {


    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.vendorInforService.addToTheList.subscribe(
      data => {
        this.vendor = data;
        this.options = false;
      }
    )

    this.selectedvendortype = [];
    this.selectedvendortype.push({ label: 'Select Vendor Type', value: -1 });

    this.selectservicetype = [];
    this.selectservicetype.push({ label: 'Select Service Type', value: -1 });
    this.selectcontractnumber = [];
    this.selectcontractnumber.push({ label: 'Select Contract Number', value: -1 });

    this.getVendorTypeList();
    this.getServiceTypeList();
  }



  getVendorTypeList() {
    const request = this.vendorInforService.getVendorType();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectedvendortype.push({ label: msg[i].venTypeName, value: msg[i].venTypeId });
          // console.log( this.vendorList);
        }





      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }



  getServiceTypeList() {
    const request = this.vendorInforService.getVendorServiceType();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectservicetype.push({ label: msg[i].serviceTypeName, value: msg[i].serviceTypeId });
          // console.log( this.vendorList);
        }





      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }




  createVendor(): void {

    if (!this.vendor.venName) {
      this.severity = 'error';
      this.detailMsg = "Vendor name cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.vendor.venTradeLicenseNo) {
      this.severity = 'error';
      this.detailMsg = "Trade license number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.vendor.venTypeId) {
      this.severity = 'error';
      this.detailMsg = "Vendor type id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    } else if (this.vendor.venTypeId.length > 5) {
      this.severity = 'error';
      this.detailMsg = "Vendor type id lenght cann't be more than 5";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.vendor.serviceTypeId) {
      this.severity = 'error';
      this.detailMsg = "Service type id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    } else if (this.vendor.serviceTypeId.length > 5) {
      this.severity = 'error';
      this.detailMsg = "Service type id length cann't be more than 5";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.vendor.bin_no) {
      this.severity = 'error';
      this.detailMsg = "Bin No cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }




    // var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.vendor.entryDt = new Date();
    this.vendor.userId = this.user.userId;
    this.vendor.updatedt = new Date('01/01/1970');
    this.vendor.machineIp = "";
    this.vendor.updateusr = "";


    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.vendor)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.vendorInforService.insertVendorInfo(JSON.stringify(this.jsonMessage));

    this.reloadPage();
  //   this.router.navigateByUrl('/vendorList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['vendor']);
  // });
  };
  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);

}

  update(vendor): void {

    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.vendor.updatedt = new Date();
    this.vendor.updateusr = this.user.userId;
    this.vendor.machineIp = "";

    if (!this.vendor.venName) {
      this.severity = 'error';
      this.detailMsg = "Vendor name cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.vendor.venTypeId) {
      this.severity = 'error';
      this.detailMsg = "Vendor type id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    } else if (this.vendor.venTypeId.length > 5) {
      this.severity = 'error';
      this.detailMsg = "Vendor type id lenght cann't be more than 5";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.vendor.serviceTypeId) {
      this.severity = 'error';
      this.detailMsg = "Service type id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    } else if (this.vendor.serviceTypeId.length > 5) {
      this.severity = 'error';
      this.detailMsg = "Service type id length cann't be more than 5";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }




    // if (!this.vendor.venTradeLicenseNo) {
    //   this.severity = 'error';
    //   this.detailMsg = "Vendor phone cannot be empty";
    //   this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //   return;
    // }


    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(vendor)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.vendorInforService.updateVendorInfo(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/vendorList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['vendor']);
  // });
  this.reloadPage();
  };


  delete(vendor): void {
    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(vendor)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.vendorInforService.deleteVendorInfo(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });
  //   this.router.navigateByUrl('/vendorList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['vendor']);
  // });
  this.reloadPage();
  }



  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  selectFile(event) {
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);


  }
  upload() {
    var formdata: FormData = new FormData();
    formdata.append('file', this.currentFileUpload);
    this.vendorInforService.upload(formdata);
  }

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.vendor  = new VendorInfo();
      form.reset({ selectservicetype: this.vendor.serviceTypeId });
      form.reset({ selectedvendortype: this.vendor.venTypeId });
      form.reset({ options: false });
    }
    

    
}


}
