import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FileUploadInsertComponent } from './file-upload-insert/file-upload-insert.component';


const routes: Routes = [
  {path: '', component: FileUploadInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileUploadRoutingModule { }
