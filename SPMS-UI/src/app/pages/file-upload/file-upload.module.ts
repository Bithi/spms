import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileUploadRoutingModule } from './file-upload-routing.module';
import { FileUploadInsertComponent } from './file-upload-insert/file-upload-insert.component';
import { FileUploadListComponent } from './file-upload-list/file-upload-list.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CalendarModule, ConfirmDialogModule, DropdownModule, MessageModule, MessageService, ProgressSpinnerModule, TableModule, ToastModule, TooltipModule } from 'primeng';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [FileUploadInsertComponent, FileUploadListComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,TooltipModule,
    HttpModule,ToastModule,TableModule,DropdownModule,CalendarModule,
    ProgressSpinnerModule,ConfirmDialogModule,MessageModule,FileUploadRoutingModule
  ],
  providers: [MessageService],
})
export class FileUploadModule { }
