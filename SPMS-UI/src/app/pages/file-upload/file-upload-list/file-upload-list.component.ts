import { Component, OnInit } from '@angular/core';
import { FileUp } from 'src/app/models/fileUp';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { FileuploadService } from 'src/app/services/fileupload.service';
import { ConfirmationService, Message } from 'primeng/api';
import {MessageService} from 'primeng/api';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-file-upload-list',
  templateUrl: './file-upload-list.component.html',
  styleUrls: ['./file-upload-list.component.scss'],
  providers: [ConfirmationService]
})

export class FileUploadListComponent implements OnInit {
  fileUp: FileUp = null;
  msgs: Message[] = [];
  key: string = "toast";
  severity: string;
  detailMsg: string;
  position: string;
  files: FileUp[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  constructor(
    public router: Router,
    private dataService: DataService,
    private fileUploadService: FileuploadService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private utility: Utility) { }


    getAllUploadFile() {

      var request = this.fileUploadService.getFileUploadList();
      request.subscribe(
        (res) => {
          // debugger;
          // const msg = JSON.parse(atob(res));
          // this.contractbranches = msg.payLoad;
          const msg = JSON.parse(res) ;
          this.files =[] ;
  
          for(let i = 0; i< msg.length; i++) {
            this.fileUp = new FileUp;
           
            this.fileUp.fileId= msg[i][0];
            this.fileUp.contId= msg[i][1];
            this.fileUp.fileType= msg[i][2];
            this.fileUp.fileLoc= msg[i][3];
            this.fileUp.fileName=  this.fileUp.fileLoc.substring(this.fileUp.fileLoc.lastIndexOf("@") + 1);
            this.fileUp.entryusr= msg[i][4];
            this.fileUp.entrydt= msg[i][5];
            this.fileUp.updateusr= msg[i][6];
            this.fileUp.updatedt= msg[i][7];
            this.fileUp.contNo= msg[i][8];
            // this.fileUp.fileName= msg[i][9];
  
            this.files[i] = this.fileUp;
            
            // console.log( this.vendorList);
       }
      });
  
    }



  ngOnInit(): void {
    this.getAllUploadFile();
    this.cols = [

      { field: 'fileId', header: 'File ID' },
      { field: 'contNo', header: 'Contract Number' },
      { field: 'fileType', header: 'File Type' },
      { field: 'fileName', header: 'Download' }

    ];

    
  }
  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['fileUpload']);
  });
  }

  download(fileForm){ 
    this.fileUploadService.download(fileForm.fileLoc);
  
  }
  
 


  delete(fileData) {
  this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
          // this.msgs = [{severity:'info', summary:'Confirmed', detail:'Record deleted'}];
          var dataHeader = this.dataService.createMessageHeader("DELETE");

          this.jsonMessage = {
            dataHeader: dataHeader,
            payLoad: new Array(fileData)
          }
          this.fileUploadService.delete(JSON.stringify(this.jsonMessage));
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['fileUpload']);
          });
          
      },
      reject: () => {
          // this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
          this.severity = 'error';
          this.detailMsg = "You have rejected";
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
      }
  });
 
}

}
