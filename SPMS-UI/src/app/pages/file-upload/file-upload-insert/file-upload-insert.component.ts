import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api/selectitem';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { Utility } from 'src/app/util/utility';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { User } from 'src/app/models/user';
import { FileUp } from 'src/app/models/fileUp';
import { FileuploadService } from 'src/app/services/fileupload.service';
declare var $ : any;
@Component({
  selector: 'app-file-upload-insert',
  templateUrl: './file-upload-insert.component.html',
  styleUrls: ['./file-upload-insert.component.scss']
})
export class FileUploadInsertComponent implements OnInit {

  selecteContractList: SelectItem[];
  currentFileUpload: File;
  jsonMessage: any;
  options: boolean = false;
  files: FileUp[];
  key: string = "toast";
  severity: string;
  detailMsg: string;
  moment: any;
  count: number;
  public user: User;
  fileUp:FileUp = new FileUp();
  
  selectedFileType = [
    { value: '', label: 'Select Type' },
    { value: 'C', label: 'Contract' },
    { value: 'D', label: 'Document' },
    { value: 'O', label: 'Others' }
];

  constructor(
    private dataService: DataService,
    public router: Router,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService,
    private fileUploadService: FileuploadService
  ) { }

  ngOnInit() {


    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    

    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract Number', value: -1 });
    this.getContractListInfo();
   

  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
        }





      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }


  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);

  }
  upload(fileUp){

    if (!this.fileUp.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.fileUp.fileType) {
      this.severity = 'error';
      this.detailMsg = "File Type cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.currentFileUpload) {
      this.severity = 'error';
      this.detailMsg = "File cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    else if(this.currentFileUpload.size>10000000){
      this.severity = 'error';
      this.detailMsg = "Max file size should be 10MB";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
 
    // var today = new Date();
  // var date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();
  this.fileUp.entrydt = new Date();
  this.fileUp.entryusr = JSON.parse(localStorage.getItem('loggedinUser')).userId; 
  this.fileUp.updatedt = new Date('01/01/1970');   
  this.fileUp.updateusr = "";
  this.fileUp.fileLoc=fileUp.contId+"@"+fileUp.fileType+"@";
    var formdata: FormData = new FormData();
    formdata.append('file', this.currentFileUpload); 
    this.fileUploadService.upload(formdata,fileUp);
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['fileUpload']);
  });
}



  
  reset(form) {
    this.fileUp  = new FileUp();
      $('.custom-file-label').html('Choose file');

    

    
}




}
