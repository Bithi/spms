import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadInsertComponent } from './file-upload-insert.component';

describe('FileUploadInsertComponent', () => {
  let component: FileUploadInsertComponent;
  let fixture: ComponentFixture<FileUploadInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
