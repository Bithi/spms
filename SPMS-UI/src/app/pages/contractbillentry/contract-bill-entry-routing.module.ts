import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractbillentryinsertComponent } from './contractbillentryinsert/contractbillentryinsert.component';


const routes: Routes = [
  {path: '', component: ContractbillentryinsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractBillEntryRoutingModule { }
