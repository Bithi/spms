import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractBillEntryRoutingModule } from './contract-bill-entry-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, MultiSelectModule, TableModule } from 'primeng';
import { ContractbillentryinfoComponent } from './contractbillentryinfo/contractbillentryinfo.component';
import { ContractbillentryinsertComponent } from './contractbillentryinsert/contractbillentryinsert.component';


@NgModule({
  declarations: [ContractbillentryinfoComponent, ContractbillentryinsertComponent],
  imports: [
    CommonModule,
    ContractBillEntryRoutingModule,CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,MultiSelectModule
  ]
})
export class ContractBillEntryModule { }
