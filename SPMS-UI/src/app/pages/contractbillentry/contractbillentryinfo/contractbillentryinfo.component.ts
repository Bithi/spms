import { Component, OnInit, ViewChild } from '@angular/core';
import { Contractbillentry } from 'src/app/models/Contractbillentry';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { ContractbillentryService } from 'src/app/services/contractbillentry.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-contractbillentryinfo',
  templateUrl: './contractbillentryinfo.component.html',
  styleUrls: ['./contractbillentryinfo.component.scss']
})
export class ContractbillentryinfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;

  contEntry: Contractbillentry = null;

  contEntries: Contractbillentry[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contractbillentryService: ContractbillentryService,
    public router: Router,
    private dataService: DataService
  ) { }



  getContractBillEntry() {

    var request = this.contractbillentryService.getContractBillEntryList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.contEntries =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.contEntry = new Contractbillentry;
         
          this.contEntry.entry_id= msg[i][0];
          this.contEntry.cont_id= msg[i][1];
          this.contEntry.bill_no= msg[i][2];
          this.contEntry.bill_date= msg[i][3];
          this.contEntry.bill_recieve_date= msg[i][4];
          this.contEntry.part_full= msg[i][5];
          this.contEntry.send_audit_date= msg[i][6];
          this.contEntry.return_from_audit_date= msg[i][7];
          this.contEntry.entryusr= msg[i][8];
          this.contEntry.entrydt= msg[i][9];
       
       
       
          this.contEntry.updateusr= msg[i][10];

          this.contEntry.updatedt= msg[i][11];

          this.contEntry.billamount= msg[i][12];
          
          this.contEntry.contNo= msg[i][13];

          this.contEntries[i] = this.contEntry;
          // console.log( this.vendorList);
     }
    });

  }




  ngOnInit(): void {

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.getContractBillEntry();
    this.cols = [

      // { field: 'entry_id', header: 'Entry ID' },
      { field: 'contNo', header: 'Contract Number' },
      { field: 'bill_no', header: 'Bill No.' },
      { field: 'billamount', header: 'Amount' },
       { field: 'bill_date', header: 'Bill Date' },
      // { field: 'bill_recieve_date', header: 'Bill Recieve Date' },
      // { field: 'part_full', header: 'Partial/Full' },
      // { field: 'send_audit_date', header: 'Send Audit Date' },
      // { field: 'return_from_audit_date', header: 'Return From Audit Date' },

    ];

  }


  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contractbillentry']);
  });
  }
  editContBillEntry(contBillEntry): void {
    this.contractbillentryService.passTheValue(contBillEntry);
  }

}
