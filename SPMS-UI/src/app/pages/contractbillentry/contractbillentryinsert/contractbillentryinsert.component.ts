import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Contractbillentry } from 'src/app/models/Contractbillentry';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { ContractbillentryService } from 'src/app/services/contractbillentry.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ItemPerBill } from 'src/app/models/ItemPerBill';

@Component({
  selector: 'app-contractbillentryinsert',
  templateUrl: './contractbillentryinsert.component.html',
  styleUrls: ['./contractbillentryinsert.component.scss']
})
export class ContractbillentryinsertComponent implements OnInit {

  public user: User;
  contractNameSet = new Map();
  contractNameWithNumberSet= new Map();
  contractTypeSet = new Map();
  vendorNameSet = new Map();
  venID:string;
  cont_id:string;
  selectedvendor: SelectItem[];
  selectedBillEnt: SelectItem[];
  selecteItemIdList: SelectItem[];
  defaultValue:String[] = [];
  currentFileUpload: File;
  options: boolean = false;
  contractBillEntry:Contractbillentry=new Contractbillentry();
  itemPerBill:ItemPerBill = null;
  itemPerBillList:ItemPerBill[] = [];
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  selectedItems: string[] = [];
  
  partFull= [
    {label: 'Select an option', value: -1},
    {label: 'Partial', value: 'PART'},
    {label: 'Full', value: 'FULL'}
  ];


  constructor(
    private dataService: DataService,
    private contractbillentryService: ContractbillentryService,
    private utility:Utility,
    public router:Router,
    private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit(): void {
    this.defaultValue=["Choose Items"];
    
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    //
    this.selectedvendor=[];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.getVendorList();
      this.contractbillentryService.addToTheList.subscribe(
      data =>{
      this.contractBillEntry = data;
      this.options = false;
      this.selectedItems=[];

      this.getAllByEntry(this.contractBillEntry.entry_id);
      
      //  this.selectedScopes = [];
      //  this.selecteItemIdList.filter(opt=>opt.value.id == 'CONSUBIT0021').map((item) => this.selectedScopes.push(item.value));
      // this.selectedItems=[this.selecteItemIdList[0].value]
      }
    )


   

    // this.selecteItemIdList = [];
    // this.selecteItemIdList.push({ label: 'Select Item Id', value: -1 });

    // this.getContractListInfo();
  }
  getVendorList() {
   
    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        
        for (let i = 0; i < msg.length; i++) {
         
          this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
          this.vendorNameSet.set(msg[i].venId, msg[i].venName);
          // //console.log( this.vendorList);
        }

    


      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }
  getAllByEntry(entry:string) {
   
    const request = this.contractbillentryService.getAllByEntry(entry);
    request.subscribe(
      (res) => {

          const msg = JSON.parse(res);
          this.venID=msg[13];
          this.contractBillEntry.cont_id=msg[1];
          this.getCon(this.venID);
          this.getItem(msg[1]);
          this.getItemByEntry(entry);
          this.cont_id=msg[1];
          return
      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }
  // getContractBillEntry() {
  //   const request = this.contractbillentryService.getAllContIdBillEntry();
  //   request.subscribe(
  //     (res) => {
  
  //       const msg = JSON.parse(res);
  
  //       for (let i = 0; i < msg.length; i++) {
  //         this.selectedBillEnt.push({ label: msg[i].contName, value: msg[i].contId });
  //         // console.log( this.vendorList);
  //       }
  
  
  
  
  
  //     },
  //     err => {
  //       // this.severity = 'error';
  //       // this.detailMsg = "Server Error: " + err.message;
  //       // // this.showProgressSpin = false;
  //       // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
  //       return;
  //     }
  //   );
  // }
  getItemByEntry(entry: string){
    this.defaultValue=[];
    this.selectedItems=[];
    if((this.contractTypeSet.get(this.contractBillEntry.cont_id)==='conn')||(this.contractTypeSet.get(this.contractBillEntry.cont_id)==='SLA')){
      this.selectedItems.push('Connectivity');
     }
     else{
      const request = this.contractbillentryService.getItemByEntry(entry);
      request.subscribe(
        (res) => {
          const msg = JSON.parse(res);
          // if(msg.length>1)
          // this.selecteItemIdList.push({ label: 'All', value: 0 });
           for (let i = 0; i < msg.length; i++) {
            // this.selecteItemIdList.push({ label: msg[i].itemId, value:msg[i].contSubItemId });
             this.defaultValue.push(this.contractNameSet.get(msg[i][8]));
            this.selectedItems.push(msg[i][8]);
          }
  
        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
     }
    
  }

  getItem(cont_id: string) {
   
    this.selectedItems=[];
    if((this.contractTypeSet.get(this.contractBillEntry.cont_id)==='conn')||(this.contractTypeSet.get(this.contractBillEntry.cont_id)==='SLA')){
      //this.selectedItems.push('Connectivity');
     }
     else{
      this.selecteItemIdList = [];
      //  this.selecteItemIdList.push({ label: 'Select Item Id', value: -1 });
      
      const request = this.contractbillentryService.getItem(cont_id);
      request.subscribe(
        (res) => {
          const msg = JSON.parse(res);
          // if(msg.length>1)
          // this.selecteItemIdList.push({ label: 'All', value: 0 });
       
           for (let i = 0; i < msg.length; i++) {
            this.selecteItemIdList.push({ label: msg[i][2]+"-"+msg[i][3], value:msg[i][1] });
            this.contractNameSet.set(msg[i][1], msg[i][2]+"-"+msg[i][3]);
          }
  
        },
        err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          this.utility.ShowToast(this.key, this.severity, this.detailMsg);
          return;
        }
      );
     }
    
  }
  getCon(venID){
    this.selectedBillEnt = [];
    if(!this.contractBillEntry.entry_id)
    this.selectedBillEnt.push({ label: 'Select Contract ID', value: -1 });

    const request = this.dropdownInfoService.getContracts(venID);
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        for (let i = 0; i < msg.length; i++) {
          this.selectedBillEnt.push({ label: msg[i].contNo, value: msg[i].contId });
          this.contractNameWithNumberSet.set(msg[i].contId, msg[i].contNo);
          this.contractTypeSet.set(msg[i].contId, msg[i].type);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
 
  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectedBillEnt.push({ label: msg[i].contNo, value: msg[i].contId });
          this.contractTypeSet.set(msg[i].contId, msg[i].type);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }
  createContBillEntry(): void {
      var me = this;
      // if(!this.contractBillEntry.entry_id){
      //   this.severity = 'error';
      //   this.detailMsg = "Bill entry cannot be empty";
      //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      //   return;
      // }
    if(!this.contractBillEntry.cont_id){
      this.severity = 'error';
      this.detailMsg = "Contract id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    // if((this.contractTypeSet.get(this.contractBillEntry.cont_id)==='conn')||(this.contractTypeSet.get(this.contractBillEntry.cont_id)==='SLA')){
    //  this.selectedItems.push('Connectivity');
    // }
    if(((this.contractTypeSet.get(this.contractBillEntry.cont_id)==='pur')||(this.contractTypeSet.get(this.contractBillEntry.cont_id)==='AMC'))&&(this.selectedItems.length==0)){
      this.severity = 'error';
      this.detailMsg = "Minimum one item should be chosen";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    this.contractBillEntry.updateusr = "";
    this.contractBillEntry.entryusr = this.user.userId;
    this.selectedItems.forEach(function(entry) {
      me.itemPerBill = new ItemPerBill();
      me.itemPerBill.itemId =  entry ; 
      me.itemPerBillList.push(me.itemPerBill);
  });
    
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contractBillEntry),
        others: this.itemPerBillList
    }
    
    this.contractbillentryService.insertContractBillEntry(JSON.stringify(this.jsonMessage));
    // this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    //       this.router.navigate(['contractbillentry']);
    //   });
    this.reset(this.contractBillEntry);
  };


  updateContBillEntry(contractBillEntry): void {

    var me = this;
    if(!this.cont_id){
      this.severity = 'error';
      this.detailMsg = "Contract id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    this.contractBillEntry.cont_id=this.cont_id;
    if(((this.contractTypeSet.get(this.contractBillEntry.cont_id)==='pur')||(this.contractTypeSet.get(this.contractBillEntry.cont_id)==='AMC'))&&(this.selectedItems.length==0)){
      this.severity = 'error';
      this.detailMsg = "Minimum one item should be chosen";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    this.selectedItems.forEach(function(entry) {
      me.itemPerBill = new ItemPerBill();
      me.itemPerBill.itemId =  entry ; 
      me.itemPerBillList.push(me.itemPerBill);
  });
    var today = new Date();
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contractBillEntry.updatedt = new Date(date);
    this.contractBillEntry.updateusr = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contractBillEntry),
      others: this.itemPerBillList
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contractbillentryService.updateContractBillEntry(JSON.stringify(this.jsonMessage));
// debugger;
    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbillentry']);
  // });
  this.reset(this.contractBillEntry);
  };


  deleteContBillEntry(contractBillEntry): void {
    var me = this;
    var dataHeader = this.dataService.createMessageHeader("DELETE");
    this.selectedItems.forEach(function(entry) {
      me.itemPerBill = new ItemPerBill();
      me.itemPerBill.itemId =  entry ; 
      me.itemPerBillList.push(me.itemPerBill);
  });
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(contractBillEntry),
        others: this.itemPerBillList
    }
    this.contractbillentryService.deleteContractBillEntry(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbillentry']);
  // });
  this.reset(this.contractBillEntry);
  };


  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
      this.contractBillEntry  = new Contractbillentry();
       this.selecteItemIdList=[];
      this.defaultValue=[];
       this.defaultValue.push("Choose Items");
      this.selectedItems=[];
      form.reset({ status: this.contractBillEntry.part_full});
      form.reset({ options: false });
    

}




}
