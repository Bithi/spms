import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserInfoComponent } from './user-info/user-info.component';
import { UserInfoInsertComponent } from './user-info-insert/user-info-insert.component';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { LayoutModule } from 'src/app/layout/layout.module';
import { UserInfoService } from 'src/app/services/user-info.service';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { DefaultComponent } from './default/default.component';
import { RouterModule, Routes } from '@angular/router';
import { UserInfoRoutingModule } from './user-info-routing.module';

// export const ROUTES: Routes = [
//   { path: 'user', component: UserInfoInsertComponent },
//   { path: 'userList', component: UserInfoComponent, pathMatch:'full' },
//   { path: 'default', component: DefaultComponent, pathMatch:'full' },
//   { path: 'passwordReset', component: PasswordResetComponent, pathMatch:'full' }
// ];


@NgModule({
  imports: [
    CommonModule,
    UserInfoRoutingModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,
  ],
  declarations: [UserInfoComponent, UserInfoInsertComponent, PasswordResetComponent, DefaultComponent],
  exports: [
    UserInfoComponent, UserInfoInsertComponent, PasswordResetComponent, DefaultComponent
],
providers:[UserInfoService]
})
// export class DashboardModule { }


// @NgModule({
//   imports: [
//     RouterModule.forChild(ROUTES),
//     CommonModule,
//     UserInfoRoutingModule,
//     LayoutModule,
//     FormsModule,TableModule,DropdownModule,
//   ],
//   declarations: [UserInfoComponent, UserInfoInsertComponent, PasswordResetComponent, DefaultComponent],
//   exports: [
//     UserInfoComponent, UserInfoInsertComponent, PasswordResetComponent, DefaultComponent
// ],
// providers:[UserInfoService]
// })
export class UserInfoModule { }


// @NgModule({
//   declarations: [UserInfoComponent, UserInfoInsertComponent, PasswordResetComponent, DefaultComponent],
//   imports: [
//     CommonModule,
//     UserInfoRoutingModule,
//     LayoutModule,
//     FormsModule,TableModule,DropdownModule,
//   ],
//   providers: [UserInfoService],
//   exports: [UserInfoComponent,UserInfoInsertComponent, PasswordResetComponent, DefaultComponent]
// })
// export class UserInfoModule { }
