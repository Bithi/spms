import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/util/auth.guard';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { DefaultComponent } from './default/default.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { UserInfoInsertComponent } from './user-info-insert/user-info-insert.component';
import { UserInfoComponent } from './user-info/user-info.component';

const routes: Routes = [
  { path: '', component: UserInfoInsertComponent },
  // { path: 'user', component: UserInfoInsertComponent },
  { path: 'userList', component: UserInfoComponent },
  { path: 'default', component: DefaultComponent,canActivate: [ AuthGuard ], data: { path: 'default' }},
  { path: 'passwordReset', component: PasswordResetComponent },
  // {path: '**', component: PageNotFoundComponent}
    ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserInfoRoutingModule { }
