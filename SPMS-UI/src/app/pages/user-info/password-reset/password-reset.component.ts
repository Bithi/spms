import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Utility
} from 'src/app/util/utility';
import {
  DataService
} from 'src/app/data/data.service';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  MessageService
} from 'primeng/api';
import {
  UserInfoService
} from 'src/app/services/user-info.service';
import {
  User
} from 'src/app/models/user';
import {
  Location
} from '@angular/common';
import {
  DropdownInfoService
} from 'src/app/services/dropdown-info.service';
import { NgForm } from '@angular/forms';
import * as CryptoJS from 'crypto-js'; 
@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  public user: User = new User();
  constructor(
    private userInfoService: UserInfoService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private dropdownInfoService: DropdownInfoService

  ) { }
  key: string = "toast";
    severity: string;
    detailMsg: string;
    users: User[];
    jsonMessage: any;
    myForm: any;
    currPass:any;
  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

  }
  changePass(){
    var us = this.user;
    debugger;
    if (!this.currPass) {
      this.severity = 'error';
      this.detailMsg = "Current password cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
  } 
   if(!us.newPassword){
    this.severity = 'error';
    this.detailMsg = "New password cannot be empty";
    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    return;

  }
  

  var encryptedPassword=CryptoJS.SHA256(this.currPass);//hashing the key using SHA256

  if(encryptedPassword!=this.user.password){
    this.severity = 'error';
    this.detailMsg = "Current password mismatched";
    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    return;

  }
  else {
    var encryptednewPassword=CryptoJS.SHA256(us.newPassword);
   if(encryptednewPassword==us.password){
    this.severity = 'error';
    this.detailMsg = "New password cannot be same as Old Password";
    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    return;

  }
    this.user.password=this.user.newPassword;
    }
    var dataHeader = this.dataService.createMessageHeader("PASSUPDATE");
                        this.jsonMessage = {
                            dataHeader: dataHeader,
                            payLoad: new Array(this.user)
                        }
this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
localStorage.removeItem('isLoggedin');
localStorage.removeItem('loggedinUser');
localStorage.removeItem('roleWiseMenu');
localStorage.removeItem('firstPage');
  this.router.navigate(['']);

}
focusoutHandlerPassword(event): void {
  var x = event.target.value;
  if (!x) {
      this.severity = 'error';
      this.detailMsg = "Password cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
  }
}

  
}
