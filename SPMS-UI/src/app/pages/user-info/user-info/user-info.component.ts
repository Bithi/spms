import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UserInfoService } from 'src/app/services/user-info.service';
import { Table } from 'primeng';


@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  @ViewChild(Table) dt: Table;
  cols: any[];

  user: User=null;

  users: User[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private userInfoService: UserInfoService,
    private dataService: DataService,
      public router: Router,
      private messageService: MessageService,
      private utility:Utility
  ) { }

  ngOnInit() {
    this.user = new User();
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.getUserList();
      this.cols = [
        { field: 'Id', header: 'ID' },
        { field: 'userId', header: 'User ID' },
        { field: 'userName', header: 'User Name' },
        { field: 'userStatus', header: 'User Status' }
    ];

  }

  getUserList(){
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    var jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.user)
    }

    //this.userInfoService.showUserInfo(jsonMessage);
     //this.userInfoService.showUserInfo(JSON.stringify(jsonMessage)).subscribe(
       //respnse =>this.handleSuccessfulResponse(respnse),
    //);
    const request = this.userInfoService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse( atob(res) );
              console.log(msg);
              this.users = msg.payLoad;
              console.log(this.users);

              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]){
                
               
                  // this.severity = 'error';
                  // this.detailMsg = "Invalid Cridential";
                  // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  // localStorage.removeItem('isLoggedin');
                  // this.showProgressSpin = false;
                  return;
              }
              // this.showProgressSpin = false;
              // localStorage.setItem('isLoggedin', 'true');
              // localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
              // this.router.navigate(['/dashboard']);
              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
}


  handleSuccessfulResponse(response)
  {
      // this.User=response;
  }


    ngOnDestroy(): void {
        document.body.className = '';
    }

    createEmployee(): void {
      this.router.navigate(['/insert']); 
    }; 


    editEmployee(user): void{
      this.userInfoService.passTheValue(user);
    };
}
