import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import {
    Utility
} from 'src/app/util/utility';
import {
    DataService
} from 'src/app/data/data.service';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    MessageService
} from 'primeng/api';
import {
    UserInfoService
} from 'src/app/services/user-info.service';
import {
    User
} from 'src/app/models/user';
import {
    Location
} from '@angular/common';
import {
    DropdownInfoService
} from 'src/app/services/dropdown-info.service';
import { NgForm } from '@angular/forms';


@Component({
    selector: 'app-user-info-insert',
    templateUrl: './user-info-insert.component.html',
    styleUrls: ['./user-info-insert.component.scss']
})
export class UserInfoInsertComponent implements OnInit {


    matchUserName: User[];
    public person: any = {};
    public user: User = new User();
    users: User[];
    jsonMessage: any;
    myForm: any;

    userStatus = [
        { value: '', label: 'Select user status' },
        { value: 'A', label: 'Active' },
        { value: 'I', label: 'Inactive' },
        { value: 'L', label: 'Locked' }
    ];



    constructor(
        private userInfoService: UserInfoService,
        private dataService: DataService,
        public router: Router,
        private messageService: MessageService,
        private utility: Utility,
        private activatedRoute: ActivatedRoute,
        private location: Location,
        private dropdownInfoService: DropdownInfoService
    ) { }

    key: string = "toast";
    severity: string;
    detailMsg: string;

    ngOnInit() {
        window.dispatchEvent(new Event('resize'));
        document.body.className = 'hold-transition skin-blue sidebar-mini';

        this.userInfoService.addToTheList.subscribe(
            data => this.user = data
        )
    }


    createEmployee() {
        var us = this.user;
        if (!this.user.userId) {
            this.severity = 'error';
            this.detailMsg = "User id cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        } else if (this.user.userId.length > 8) {
            this.severity = 'error';
            this.detailMsg = "User id connot be more than 8";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }

        if (!this.user.userName) {
            this.severity = 'error';
            this.detailMsg = "User name cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }

        // if (!this.user.password) {
        //     this.severity = 'error';
        //     this.detailMsg = "Password cannot be empty";
        //     this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        //     return;
        // }

        // if (this.user.password.indexOf(' ') > -1) {
        //     //var str = this.user.password.replace(/\s/g, "");
        //     var strr = this.user.password;
        //     var str1 = "";
        //     if (strr != null) {
        //         str1 = strr.trim();
        //     }
        //     if (str1 == "") {
        //         this.severity = 'error';
        //         this.detailMsg = 'Only spaces can not be used as a password';
        //         this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        //         return;
        //     }
        // }

    //    if(this.user.officeCode){
    //     if (this.user.officeCode.length > 10) {
    //         this.severity = 'error';
    //         this.detailMsg = "Office Code lenght cannot be more than 10";
    //         this.utility.ShowToast(this.key, this.severity, this.detailMsg);
    //         return;
    //     }
    //    }

    if(!this.user.userStatus){
        this.severity = 'error';
        this.detailMsg = "User status cann't be empty";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
    }else if (this.user.userStatus.length > 1) {
                this.severity = 'error';
                this.detailMsg = "User status lenght cannot be more than 1";
                this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                return;
            }


        if (this.user.userId) {

            const request = this.dropdownInfoService.matchUserId(this.user.userId);
            var errOcc = false;
            request.subscribe(
                (res) => {
                    if (res) {

                        console.log("Name already exist!" + us.userName);
                        this.severity = 'error';
                        this.detailMsg = "User ID already exists";
                        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                        errOcc = true;

                    }
                    else {
                        console.log(JSON.stringify(this.user));
                        // if (!errOcc) {
                        var dataHeader = this.dataService.createMessageHeader("SAVE");
                        this.jsonMessage = {
                            dataHeader: dataHeader,
                            payLoad: new Array(us)
                        }

                        this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
                        // this.user =new User();

                    };
                    this.router.navigateByUrl('/userList', { skipLocationChange: true }).then(() => {
                        this.router.navigate(['user']);
                    }); 
                    return;

                },
                err => {
                    errOcc = true;
                    return;

                }
            );


        }

    }


    focusoutHandler(event): void {

        var x = event.target.value;
        if (!x) {
            this.severity = 'error';
            this.detailMsg = "User id cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        } else if (x.length > 8) {
            this.severity = 'error';
            this.detailMsg = "User id connot be more than 8";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }

        if (x) {
            const request = this.dropdownInfoService.matchUserId(x);
            request.subscribe(
                (res) => {
                    if (res) {

                        //console.log("Name already exist!");
                        this.severity = 'error';
                        this.detailMsg = "User ID already exists";
                        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                        return;
                    }
                },
                err => {
                    // this.severity = 'error';
                    // this.detailMsg = "Server Error: " + err.message;
                    // // this.showProgressSpin = false;
                    // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    return;
                }
            );
        }

    }

    focusoutHandlerUserName(event): void {

        var x = event.target.value;
        if (!x) {
            this.severity = 'error';
            this.detailMsg = "User name cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }

    }


    focusoutHandlerPassword(event): void {

        var x = event.target.value;
        if (!x) {
            this.severity = 'error';
            this.detailMsg = "Password cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }
    }

    focusoutHandlerOfficeCode(event): void {

        var x = event.target.value;
        if (!x) {
            this.severity = 'error';
            this.detailMsg = "Office Code cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        } else if (x.length > 10) {
            this.severity = 'error';
            this.detailMsg = "Office Code lenght cannot be more than 10";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }
    }

    focusoutHandlerUserStatus(event): void {

        var x = event.target.value;
        if (!x) {
            this.severity = 'error';
            this.detailMsg = "User status cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        } else if (x.length > 1) {
            this.severity = 'error';
            this.detailMsg = "User status lenght cannot be more than 1";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }
    }


    update(user): void {
        // if (!user.userId) {
        //     this.severity = 'error';
        //     this.detailMsg = "User id cannot be empty";
        //     this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        //     return;
        // } else if (user.userId.length > 8) {
        //     this.severity = 'error';
        //     this.detailMsg = "User id connot be more than 8";
        //     this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        //     return;
        // }

        if (!user.userName) {
            this.severity = 'error';
            this.detailMsg = "User name cannot be empty";
            this.utility.ShowToast(this.key, this.severity, this.detailMsg);
            return;
        }

        // if (!user.password) {
        //     this.severity = 'error';
        //     this.detailMsg = "Password cannot be empty";
        //     this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        //     return;
        // }

        if(user.officeCode){
            if (user.officeCode.length > 10) {
                this.severity = 'error';
                this.detailMsg = "Office Code lenght cannot be more than 10";
                this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                return;
            }
           }
    
                if(!user.userStatus){
                    this.severity = 'error';
                    this.detailMsg = "User status cann't be empty";
                    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                    return;
                }
                else if (user.userStatus.length > 1) {
                    this.severity = 'error';
                    this.detailMsg = "User status lenght cannot be more than 1";
                    this.utility.ShowToast(this.key, this.severity, this.detailMsg);
                    return;
                }

        var dataHeader = this.dataService.createMessageHeader("UPDATE");

        this.jsonMessage = {
            dataHeader: dataHeader,
            payLoad: new Array(user)
        }
        //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
        this.userInfoService.updateUserInfo(JSON.stringify(this.jsonMessage));

        this.router.navigateByUrl('/userList', { skipLocationChange: true }).then(() => {
            this.router.navigate(['user']);
        });
    };

    delete(user): void {
        var dataHeader = this.dataService.createMessageHeader("DELETE");

        this.jsonMessage = {
            dataHeader: dataHeader,
            payLoad: new Array(user)
        }
        //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
        this.userInfoService.deleteUserInfo(JSON.stringify(this.jsonMessage));

        //   request.subscribe(
        //     (data ) => {
        //       const msg = JSON.parse( atob(data) );
        //      // alert("Employee created successfully.");
        //      debugger;
        //      console.log(msg.payload);
        //     });
        this.router.navigateByUrl('/userList', { skipLocationChange: true }).then(() => {
            this.router.navigate(['user']);
        });
    }

    passReset(user){
        var dataHeader = this.dataService.createMessageHeader("RESET");

        this.jsonMessage = {
            dataHeader: dataHeader,
            payLoad: new Array(user)
        }
        //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
        this.userInfoService.resetPass(JSON.stringify(this.jsonMessage));

        this.router.navigateByUrl('/userList', { skipLocationChange: true }).then(() => {
            this.router.navigate(['user/passwordReset']);
        });

    }

    reset(form) {
        this.user = new User();
        form.reset({ userStatus: this.user.userStatus });
    }

}