import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContyearwiserateinsertComponent } from './contyearwiserateinsert/contyearwiserateinsert.component';


const routes: Routes = [
  {path: '', component: ContyearwiserateinsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractYearWiseRateRoutingModule { }
