import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Contyearwiserate } from 'src/app/models/Contyearwiserate';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { ContyearwiserateService } from 'src/app/services/contyearwiserate.service';

@Component({
  selector: 'app-contyearwiserateinsert',
  templateUrl: './contyearwiserateinsert.component.html',
  styleUrls: ['./contyearwiserateinsert.component.scss']
})
export class ContyearwiserateinsertComponent implements OnInit {

  public user: User;
  selectedRate: SelectItem[];

  currentFileUpload: File;
  options: boolean = false;
  contRate:Contyearwiserate=new Contyearwiserate();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;


  contractType= [
    {value:null,label:'Select Type'},
    {value:'AMC',label:'AMC'},
    {value:'SLA',label:'SLA'},
    {value:'conn',label:'Connectivity'},
    {value:'pur',label:'Purchase'},
  ];


  constructor(
    private dataService: DataService,
    private contyearwiserateService: ContyearwiserateService,
    private utility:Utility,
    public router:Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.contyearwiserateService.addToTheList.subscribe(
      data =>{
       this.contRate = data;
       this.options = false;
      }
    )


    this.selectedRate = [];
    this.selectedRate.push({ label: 'Select Contract ID', value: -1 });

    this.getAllContIdForRate();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  getAllContIdForRate() {
    const request = this.contyearwiserateService.getAllContForRate();
    request.subscribe(
      (res) => {
  
        const msg = JSON.parse(res);
  
        for (let i = 0; i < msg.length; i++) {
          this.selectedRate.push({ label: msg[i].contNo, value: msg[i].contId });
          // console.log( this.vendorList);
        }
  
  
  
  
  
      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }


  createContRate(): void {

    if(!this.contRate.cont_id){
      this.severity = 'error';
      this.detailMsg = "Contract id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    if(!this.contRate.contract_type){
      this.severity = 'error';
      this.detailMsg = "Contract type cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

   



    var today = new Date();
    //var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    //var date = today.toUTCString();
    //this.vendor.entryDt = new Date(date);
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contRate.entrydt = new Date(date);
    this.contRate.updatedt = new Date('01/01/1970');
    this.contRate.updateusr = "";
    this.contRate.entryusr = this.user.userId;
    
     
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contRate)
    }

    this.contyearwiserateService.insertContyearwiserate(JSON.stringify(this.jsonMessage));
    this.reset(this.contRate);
  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contYearWiseRate']);
  // });
    
  };


  updateContYearwiseRate(contRate): void {


    if(!this.contRate.cont_id){
      this.severity = 'error';
      this.detailMsg = "Contract id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    if(!this.contRate.contract_type){
      this.severity = 'error';
      this.detailMsg = "Contract type cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

   



    var today = new Date();
    var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contRate.updatedt = new Date(date);
    this.contRate.updateusr = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contRate)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.contyearwiserateService.updateContyearwiseRate(JSON.stringify(this.jsonMessage));

    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

  //   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contYearWiseRate']);
  // });
  this.reset(this.contRate);
  };


  deleteContYearwiseRate(contRate): void {

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(contRate)
    }
    this.contyearwiserateService.deleteContyearwiseRate(JSON.stringify(this.jsonMessage));
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contYearWiseRate']);
  });
   
  };


  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
  }


  

  reset(form) {
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      this.contRate  = new Contyearwiserate();
      form.reset({ contact_type: this.contRate.contract_type});
      form.reset({ options: false });
    }

}



}
