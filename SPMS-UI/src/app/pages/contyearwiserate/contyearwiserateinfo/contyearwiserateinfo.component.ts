import { Component, OnInit, ViewChild } from '@angular/core';
import { Contyearwiserate } from 'src/app/models/Contyearwiserate';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { ContyearwiserateService } from 'src/app/services/contyearwiserate.service';
import { Table } from 'primeng';

@Component({
  selector: 'app-contyearwiserateinfo',
  templateUrl: './contyearwiserateinfo.component.html',
  styleUrls: ['./contyearwiserateinfo.component.scss']
})
export class ContyearwiserateinfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  contRate: Contyearwiserate = null;

  contRates: Contyearwiserate[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contyearwiserateService: ContyearwiserateService,
    public router: Router,
    private dataService: DataService
  ) { }


  getYearWiseRate() {

    var request = this.contyearwiserateService.getContYearWiseRate();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.contRates =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.contRate = new Contyearwiserate;
          this.contRate.id= msg[i][0];
          this.contRate.cont_id= msg[i][1];
          this.contRate.rate= msg[i][2];
          this.contRate.start_date= msg[i][3];
          this.contRate.end_date= msg[i][4];
          this.contRate.contract_type= msg[i][5];
          this.contRate.entryusr= msg[i][6];
          this.contRate.entrydt= msg[i][7];
          this.contRate.updateusr= msg[i][8];
          this.contRate.updatedt= msg[i][9];
          this.contRate.contNo= msg[i][10];

          this.contRates[i] = this.contRate;
          
          // console.log( this.vendorList);
     }
    });

  }


  ngOnInit(): void {

    this.getYearWiseRate();
    
    this.cols = [

      { field: 'id', header: 'ID' },
      { field: 'contNo', header: 'Contract Number' },
      { field: 'rate', header: 'Rate' },
      { field: 'start_date', header: 'Start Date' },
      { field: 'end_date', header: 'End Date' },
      { field: 'contract_type', header: 'Contract Type' },

    ];

    
  }


  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contYearWiseRate']);
  });
  }

  editContractRate(contRate): void {
    this.contyearwiserateService.passTheValue(contRate);
  }
}
