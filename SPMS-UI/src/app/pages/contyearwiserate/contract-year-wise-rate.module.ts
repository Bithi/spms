import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractYearWiseRateRoutingModule } from './contract-year-wise-rate-routing.module';
import { ContyearwiserateinfoComponent } from './contyearwiserateinfo/contyearwiserateinfo.component';
import { ContyearwiserateinsertComponent } from './contyearwiserateinsert/contyearwiserateinsert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, TableModule } from 'primeng';


@NgModule({
  declarations: [ContyearwiserateinfoComponent, ContyearwiserateinsertComponent],
  imports: [
    CommonModule,
    ContractYearWiseRateRoutingModule,
    CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,
  ]
})
export class ContractYearWiseRateModule { }
