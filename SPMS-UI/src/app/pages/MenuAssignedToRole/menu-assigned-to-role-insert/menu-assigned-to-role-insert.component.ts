import { Component, OnInit } from '@angular/core';
import { MessageService, SelectItem } from 'primeng/api';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { MenuAssignedToRoleService } from 'src/app/services/menuassignedtorole-info.service';
import { MenuAssigndedToRole } from 'src/app/models/menuassignedtorole';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-assigned-to-role-insert',
  templateUrl: './menu-assigned-to-role-insert.component.html',
  styleUrls: ['./menu-assigned-to-role-insert.component.scss']
})

export class MenuAssignedToRoleInsertComponent implements OnInit {

  currentFileUpload: File;
  options: boolean = false;
  menuAssigndedToRole: MenuAssigndedToRole = new MenuAssigndedToRole();
  selectedmenu:SelectItem[];
  selectedrole:SelectItem[];
  parentMenuPath = new Map();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;

  constructor(
    private dataService: DataService,
    private menuAssignedToRoleService: MenuAssignedToRoleService,
    private utility:Utility,
    public router:Router
  ) { }

  ngOnInit() {
    this.selectedmenu = [];
    this.selectedmenu.push({ label: 'Select Menu', value: -1});
    this.getParentMenuList();
    this.selectedrole = [];
    this.selectedrole.push({ label: 'Select Role', value: -1});
    this.getTotalRoleList();

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.menuAssignedToRoleService.addToTheList.subscribe(
      data =>{
        
       this.menuAssigndedToRole = data;
       this.options = false;
      }
    )
    
  }
 
  getParentMenuList(){

    const request = this.menuAssignedToRoleService.getMenus();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.selectedmenu.push({label: msg[i][1], value: msg[i][0]});
                this.parentMenuPath.set(msg[i][0], msg[i][2]);
                // console.log( this.vendorList);
          }

              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );

console.log(this.parentMenuPath);
}


getTotalRoleList(){
   
  const request = this.menuAssignedToRoleService.getMyRoleList();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;

            for(let i = 0; i< msg.length; i++) {
              this.selectedrole.push({label: msg[i].roleName, value: msg[i].r_id});
              // console.log( this.vendorList);
         }
            
        
    },
    err => {
        return;
    }
);


}





createMenu(): void {   
  
  
  if(!this.menuAssigndedToRole.m_id){
    this.severity = 'error';
    this.detailMsg = "Menu cannot be null";
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
  }

  if(!this.menuAssigndedToRole.r_id){
    this.severity = 'error';
    this.detailMsg = "Role cannot be null";
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
  }

      
  var dataHeader = this.dataService.createMessageHeader("SAVE");
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(this.menuAssigndedToRole)
  }

  this.menuAssignedToRoleService.insertMenuAssignedToRole(JSON.stringify(this.jsonMessage));
//   this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
//     this.router.navigate(['menuassignedtorole']);
// });
this.reset(this.menuAssigndedToRole);
};

selectFile(event) {  
  this.currentFileUpload = event.target.files.item(0);
  $('.custom-file-label').html(this.currentFileUpload.name);
}




reset(form) {

  if(this.options){
    $('.custom-file-label').html('Choose file');

  }
  else{
    this.menuAssigndedToRole  = new MenuAssigndedToRole();
    form.reset({ m_id: this.menuAssigndedToRole.m_id });
    form.reset({ r_id: this.menuAssigndedToRole.r_id });
    form.reset({ options: false });
  }
  
}


updateMenuAssignedToRole(menuAssignedToRole): void {

  var dataHeader = this.dataService.createMessageHeader("UPDATE");

  this.jsonMessage = {
    dataHeader: dataHeader,
    payLoad: new Array(this.menuAssigndedToRole)
  }
  //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
  this.menuAssignedToRoleService.updateMenuAssigendToRoleInfo(JSON.stringify(this.jsonMessage));

  //   request.subscribe(
  //     (data ) => {
  //       const msg = JSON.parse( atob(data) );
  //      // alert("Employee created successfully.");
  //      debugger;
  //      console.log(msg.payload);
  //     });

  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['menuassignedtorole']);
});
};


deleteMenuAssignedToRole(menuAssigndedToRole): void {    

  var dataHeader = this.dataService.createMessageHeader("DELETE");
  
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(menuAssigndedToRole)
  }
  this.menuAssignedToRoleService.deleteMenuAssignedToRoleInfo(JSON.stringify(this.jsonMessage));
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['menuassignedtorole']);
});
 
};

}