import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuAssignedToRoleInsertComponent } from './menu-assigned-to-role-insert/menu-assigned-to-role-insert.component';


const routes: Routes = [
  {path: '', component: MenuAssignedToRoleInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuAssignedToRoleRoutingModule { }
