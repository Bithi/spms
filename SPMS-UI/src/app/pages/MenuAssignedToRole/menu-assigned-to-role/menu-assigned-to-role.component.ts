import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuAssigndedToRole } from 'src/app/models/menuassignedtorole';
import { MenuAssignedToRoleService } from 'src/app/services/menuassignedtorole-info.service';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { SelectItem, Table } from 'primeng';

@Component({
  selector: 'app-menu-assigned-to-role',
  templateUrl: './menu-assigned-to-role.component.html',
  styleUrls: ['./menu-assigned-to-role.component.scss']
})
export class MenuAssignedToRoleComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  menuAssigndedToRole: MenuAssigndedToRole = new MenuAssigndedToRole;

   menuAssigndedToRoleInfos: MenuAssigndedToRole[]=[] ;
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  selectedmenu:SelectItem[];
  selectedrole:SelectItem[];
  parentMenuPath = new Map();
  roleSet = new Map();
  roleSetFilter = new Map();

  constructor(
    private menuAssignedToRoleService: MenuAssignedToRoleService,
    public router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit(): void {
    // this.getParentMenuList();
    // this.getRole();
    this.cols = [

      // { field: 'id', header: 'ID' },//sortable: true, filter: true, filterMatchMode: 'contains', allowToggle: true, style: { 'width': '20px', 'vertical-align': 'top'}},
      { field: 'menuName', header: 'Menu'},//,sortable: true, filter: true, filterMatchMode: 'contains', allowToggle: true, style: { 'width': '20px', 'vertical-align': 'top' }},
      { field: 'roleName', header: 'Role'}//,sortable: true, filter: true, filterMatchMode: 'contains', allowToggle: true, style: { 'width': '20px', 'vertical-align': 'top' }}

    ];

    
    const request = this.menuAssignedToRoleService.menuAssignedToRoleJoin();
    request.subscribe(
      (res) => {
       
        const msg = JSON.parse(res) ;
        this.menuAssigndedToRoleInfos =[] ;
        for(let i = 0; i< msg.length; i++) {
          this.menuAssigndedToRole = new MenuAssigndedToRole;
          this.menuAssigndedToRole.id= msg[i][0];
          this.menuAssigndedToRole.m_id= msg[i][1];
          this.menuAssigndedToRole.menuName = msg[i][2].substring(6);
          this.menuAssigndedToRole.r_id= msg[i][3];
          this.menuAssigndedToRole.roleName = msg[i][4];

          this.menuAssigndedToRoleInfos[i] = this.menuAssigndedToRole;
          // console.log( this.vendorList);
     }
        
        // for(let i = 0; i<  this.menuAssigndedToRoleInfos.length; i++) {
        //   this.menuAssigndedToRoleInfos[i].menuName = this.parentMenuPath.get(this.menuAssigndedToRoleInfos[i].m_id);
        //   // this.menuAssigndedToRoleInfos[i].r_id = msg.payLoad[i].r_id;
        // }
        
        // this.menuAssigndedToRoleInfos = msg.payLoad;
        
        // console.log(this.menuAssigndedToRoleInfos);

        // if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
        //   return;
        // }
       



      },
      err => {
        
        return;
      }
    );
    
  }
  getRole(){
    const request = this.menuAssignedToRoleService.getMyRoleList();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.roleSet.set(msg[i].r_id, msg[i].roleName);
                this.roleSetFilter.set(msg[i].roleName, msg[i].r_id);
                
                // console.log( this.vendorList);
           }
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
  );


}

  getParentMenuList(){
  
    const request = this.menuAssignedToRoleService.getMenus();
    request.subscribe(
      (res ) => {
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
               this.parentMenuPath.set(msg[i][0], msg[i][2].substring(6));
                // console.log( this.vendorList);
          }

              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );


}

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['menuWIseRole']);
  });
  }

  editMenuAssignedToRole(menuAssignedToRole): void {
    this.menuAssignedToRoleService.passTheValue(menuAssignedToRole);
  }

   getByValue(map, searchValue) {
    for (let [key, value] of map.entries()) {
      if (value===searchValue){
        return key;
      }
        
    }
  }

}
