import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuAssignedToRoleRoutingModule } from './menu-assigned-to-role-routing.module';
import { MenuAssignedToRoleInsertComponent } from './menu-assigned-to-role-insert/menu-assigned-to-role-insert.component';
import { MenuAssignedToRoleComponent } from './menu-assigned-to-role/menu-assigned-to-role.component';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from 'src/app/layout/layout.module';
import { DropdownModule, FieldsetModule, KeyFilterModule, ProgressSpinnerModule, TableModule } from 'primeng';


@NgModule({
  declarations: [MenuAssignedToRoleInsertComponent, MenuAssignedToRoleComponent],
  imports: [
    CommonModule,
    MenuAssignedToRoleRoutingModule,
    FormsModule,
    LayoutModule,
    TableModule,KeyFilterModule,FieldsetModule,
    DropdownModule,
    ProgressSpinnerModule
  ]
})
export class MenuAssignedToRoleModule { }
