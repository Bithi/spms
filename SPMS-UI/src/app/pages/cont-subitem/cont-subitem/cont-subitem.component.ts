import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ContSubItem } from 'src/app/models/ContSubItem';
import { DataService } from 'src/app/data/data.service';
import { Table } from 'primeng';
import { ContSubItemService } from 'src/app/services/contsubitem.service';

@Component({
  selector: 'app-cont-subitem',
  templateUrl: './cont-subitem.component.html',
  styleUrls: ['./cont-subitem.component.scss']
})
export class ContSubItemComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  cols: any[];
  data: ContSubItem = null;

  dataList: ContSubItem[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contSubItemService: ContSubItemService,
    public router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

   this.getList();

    this.cols = [
      { field: 'cont_no', header: 'Contract Number' },
      { field: 'itemname', header: 'Item' },
      { field: 'modelname', header: 'Model' },
      { field: 'itemQuantity', header: 'Item Quantity' },
      { field: 'totalValue', header: 'Total Value' }
    ];

  }
  getList() {

    const request = this.contSubItemService.getcontsubitemModelJoin();
    request.subscribe(
      (res) => {
        const msg = JSON.parse(res) ;
        this.dataList =[] ;


        for(let i = 0; i< msg.length; i++) {
          this.data = new ContSubItem;
        
          this.data.contId        = msg[i][0];
          this.data.itemId         = msg[i][1];
         
          this.data.itemQuantity    = msg[i][2];
          this.data.totalValue      = msg[i][3];
          this.data.entryUser       = msg[i][4];
          this.data.entryDt         = msg[i][5];
          this.data.updateUser     = msg[i][6];
          this.data.updateDt       = msg[i][7];
          this.data.contSubItemId  = msg[i][8];
          this.data.modelId         = msg[i][9];
          this.data.cont_no     = msg[i][10];
          this.data.itemname    = msg[i][11];
          this.data.modelname    = msg[i][12];
       

          this.dataList[i] = this.data;
       
        }


      },
      err => {
        
        return;
      }
    );
  }

  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);

}

  editData(vendor): void {
    this.contSubItemService.passTheValue(vendor);
  }
  
}
