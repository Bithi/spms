import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { User } from 'src/app/models/user';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ContSubItem } from 'src/app/models/ContSubItem';
import { ContSubItemService } from 'src/app/services/contsubitem.service';

declare var $: any;


@Component({
  selector: 'app-cont-subitem-insert',
  templateUrl: './cont-subitem-insert.component.html',
  styleUrls: ['./cont-subitem-insert.component.scss']
})
export class ContSubItemInsertComponent implements OnInit {
  public myDate;
  public user: User;

  selecteContractList: SelectItem[];
  subItemList : SelectItem[];
  selecteModelList:SelectItem[];
  public contSubItem: ContSubItem = new ContSubItem();
  jsonMessage: any;
  options: boolean = false;

  key: string = "toast";
  severity: string;
  detailMsg: string;
  moment: any;

  constructor(
    private dataService: DataService,
    private contSubItemService: ContSubItemService,
    public router: Router,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit() {


    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.contSubItemService.addToTheList.subscribe(
      data => {
        this.contSubItem = data;
        this.options = false;
      }
    )

    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract', value: -1 });
    this.getContractListInfo();

    // this.selecteModelList = [];
    // this.selecteModelList.push({ label: 'Select Model', value: -1 });
    // this.getModelList();
    this.getmodelBrandJoin();
    this.subItemList = [];
    this.subItemList.push({ label: 'Select Item', value: -1 });
    this.getItemList();

  }

  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);

}

  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }
  getmodelBrandJoin()
  {
    
  this.selecteModelList = [];
  this.selecteModelList.push({ label: 'Select Model ', value: -1 });
    const request = this.contSubItemService.getmodelBrandJoin();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.selecteModelList.push({label: msg[i][2]+' ('+msg[i][8]+')', value: msg[i][0]});
           }


              
              
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }
  getModelList() {

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    }

    const request = this.contSubItemService.getModelList(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
        const msg = JSON.parse(atob(res)).payLoad;
       
        for (let i = 0; i < msg.length; i++) {
          this.selecteModelList.push({ label: msg[i].modelName, value: msg[i].modelId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }

  getItemList() {

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    }

    const request = this.contSubItemService.getItemList(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res)).payLoad;
       
        for (let i = 0; i < msg.length; i++) {
          this.subItemList.push({ label: msg[i].itemName, value: msg[i].itemId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );
  }
  create(): void {

    if (!this.contSubItem.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contSubItem.modelId) {
      this.severity = 'error';
      this.detailMsg = "Model cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contSubItem.itemId) {
      this.severity = 'error';
      this.detailMsg = "Item cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contSubItem.itemQuantity|| this.contSubItem.itemQuantity == 0) {
      this.severity = 'error';
      this.detailMsg = "Quantity cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contSubItem.totalValue || this.contSubItem.totalValue == 0) {
      this.severity = 'error';
      this.detailMsg = "Total value product cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contSubItem.entryDt = new Date();
    this.contSubItem.entryUser = this.user.userId;
    // this.contSubItem.updateDt = new Date('01/01/1970');
    // this.contSubItem.updateUser = "";

    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contSubItem)
    }
    this.contSubItemService.insertData(JSON.stringify(this.jsonMessage));

   this.reloadPage();
    // this.router.navigateByUrl('/contSubItemList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSubItem']);
    // });
  };


  update(contSubItem): void {

    if (!this.contSubItem.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contSubItem.modelId) {
      this.severity = 'error';
      this.detailMsg = "Model cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contSubItem.itemId) {
      this.severity = 'error';
      this.detailMsg = "Item cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contSubItem.itemQuantity) {
      this.severity = 'error';
      this.detailMsg = "Quantity cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contSubItem.totalValue || this.contSubItem.totalValue == 0) {
      this.severity = 'error';
      this.detailMsg = "Total value product cannot be empty or zero";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contSubItem.updateDt = new Date();
    this.contSubItem.updateUser = this.user.userId;

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(contSubItem)
    }
    this.contSubItemService.updateData(JSON.stringify(this.jsonMessage));
    this.reloadPage();
    // this.router.navigateByUrl('/contSubItemList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSubItem']);
    // });
  };


  delete(vendor): void {
    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(vendor)
    }
    this.contSubItemService.deleteData(JSON.stringify(this.jsonMessage));
    this.reloadPage();
    // this.router.navigateByUrl('/contSubItemList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contSubItem']);
    // });
  }



  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  reset(form) {

    if (this.options) {
      $('.custom-file-label').html('Choose file');

    }
    else {
      this.contSubItem = new ContSubItem();
    }



  }


}
