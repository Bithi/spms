import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContSubitemRoutingModule } from './cont-subitem-routing.module';
import { ContSubItemComponent } from './cont-subitem/cont-subitem.component';
import { ContSubItemInsertComponent } from './cont-subitem-insert/cont-subitem-insert.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CalendarModule, DropdownModule, ProgressSpinnerModule, TableModule, ToastModule } from 'primeng';
import { ContPaymentMethodRoutingModule } from '../cont-payment-method/cont-payment-method-routing.module';


@NgModule({
  declarations: [ContSubItemComponent, ContSubItemInsertComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    HttpModule,ToastModule,TableModule,DropdownModule,CalendarModule,
    ProgressSpinnerModule,ContSubitemRoutingModule
  ],
  exports: [ContSubItemComponent, ContSubItemInsertComponent]
})
export class ContSubitemModule { }
