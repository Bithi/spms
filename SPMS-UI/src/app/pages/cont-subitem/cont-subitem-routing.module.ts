import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContSecurityMoneyInsertComponent } from '../cont-securitymoney/cont-securitymoney-insert/cont-securitymoney-insert.component';
import { ContSubItemInsertComponent } from './cont-subitem-insert/cont-subitem-insert.component';


const routes: Routes = [
  {path: '', component: ContSubItemInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContSubitemRoutingModule { }
