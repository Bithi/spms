import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContPaymentMethodInsertComponent } from './cont-payment-method-insert/cont-payment-method-insert.component';


const routes: Routes = [
  {path: '', component: ContPaymentMethodInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContPaymentMethodRoutingModule { }
