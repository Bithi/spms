import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ContPaymentMethodService } from 'src/app/services/contpaymentmethod.service';
import { ContPaymentMethod } from 'src/app/models/ContPaymentMethod';
import { Table } from 'primeng';

@Component({
  selector: 'app-cont-payment-method',
  templateUrl: './cont-payment-method.component.html',
  styleUrls: ['./cont-payment-method.component.scss']
})
export class ContPaymentMethodComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  cols: any[];

  dataList: ContPaymentMethod[];

  dataL: ContPaymentMethod;

  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contPaymentMethodService: ContPaymentMethodService,
    public router: Router
  ) { }



  getPaymentMethod() {

    var request = this.contPaymentMethodService.getPaymentMethodList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.dataList =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.dataL = new ContPaymentMethod;
         
        debugger;
          this.dataL.id= msg[i][0];
          this.dataL.contId= msg[i][1];
          this.dataL.partFull= msg[i][2];
          this.dataL.pmtSchedule= msg[i][3];
          this.dataL.ttValueProduct= msg[i][4];
          this.dataL.paymentDate= msg[i][5];
          this.dataL.paymentAmount= msg[i][6];
          this.dataL.dueAmount= msg[i][7];
          this.dataL.dueDate= msg[i][8];
          this.dataL.entryDt= msg[i][9];
       
       
       
          this.dataL.entryUser= msg[i][10];
          
          this.dataL.contNo= msg[i][11];

          this.dataList[i] = this.dataL;
          // console.log( this.vendorList);
     }
    });

  }



  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

   //this.getList();
   this.getPaymentMethod();

    this.cols = [
      { field: 'contNo', header: 'Contract Number' },
      { field: 'paymentDate', header: 'Payment Date' },
      { field: 'paymentAmount', header: 'Payment Amount' },
      { field: 'dueDate', header: 'Due Date' },
      { field: 'dueAmount', header: 'Due Amount' }
    ];

  }
  // getList() {
  //   this.contPaymentMethodService.getList().subscribe(data => {
  //     this.dataList = data;
  //   });

  // }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contPaymentMethod']);
  });
   
  }
  

  editData(vendor): void {
    this.contPaymentMethodService.passTheValue(vendor);
  }

}
