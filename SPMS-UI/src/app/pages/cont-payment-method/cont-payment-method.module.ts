import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContPaymentMethodRoutingModule } from './cont-payment-method-routing.module';
import { ContPaymentMethodComponent } from './cont-payment-method/cont-payment-method.component';
import { ContPaymentMethodInsertComponent } from './cont-payment-method-insert/cont-payment-method-insert.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CalendarModule, DropdownModule, ProgressSpinnerModule, TableModule, ToastModule } from 'primeng';
import { LayoutModule } from 'src/app/layout/layout.module';


@NgModule({
  declarations: [ContPaymentMethodComponent, ContPaymentMethodInsertComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ContPaymentMethodRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpClientModule,ToastModule,TableModule,DropdownModule,CalendarModule,
    ProgressSpinnerModule
  ],
  exports: [ContPaymentMethodComponent, ContPaymentMethodInsertComponent]
})
export class ContPaymentMethodModule { }
