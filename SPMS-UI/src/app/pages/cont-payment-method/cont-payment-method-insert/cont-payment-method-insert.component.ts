import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { User } from 'src/app/models/user';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ContPaymentMethod } from 'src/app/models/ContPaymentMethod';
import { ContPaymentMethodService } from 'src/app/services/contpaymentmethod.service';
declare var $: any;


@Component({
  selector: 'app-cont-payment-method-insert',
  templateUrl: './cont-payment-method-insert.component.html',
  styleUrls: ['./cont-payment-method-insert.component.scss']
})
export class ContPaymentMethodInsertComponent implements OnInit {
  public myDate;
  public user: User;

  selecteContractList: SelectItem[];
  partFull= [
    {label: 'Select an option', value: -1},
    {label: 'Partial', value: 'PART'},
    {label: 'Full', value: 'FULL'}
  ];
  paymentSchedule= [
    {label: 'Select a schedule', value: -1},
    {label: 'Half Yearly', value: 'H'},
    {label: 'Monthly', value: 'M'},
    {label: 'Yearly', value: 'Y'}
  ];
  public contPaymentMethod: ContPaymentMethod = new ContPaymentMethod();
  jsonMessage: any;
  options: boolean = false;

  key: string = "toast";
  severity: string;
  detailMsg: string;
  moment: any;

  constructor(
    private dataService: DataService,
    private contPaymentMethodService: ContPaymentMethodService,
    public router: Router,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit() {


    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.contPaymentMethodService.addToTheList.subscribe(
      data => {
        this.contPaymentMethod = data;
        this.options = false;
      }
    )
      // this.contPaymentMethod.dueAmount= this.contPaymentMethod.ttValueProduct-this.contPaymentMethod.paymentAmount;
    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract', value: -1 });
    this.getContractListInfo();

  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selecteContractList.push({ label: msg[i].contNo, value: msg[i].contId });
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }





  create(): void {
    this.contPaymentMethod.dueAmount= this.contPaymentMethod.ttValueProduct-this.contPaymentMethod.paymentAmount;
    if (!this.contPaymentMethod.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contPaymentMethod.partFull) {
      this.severity = 'error';
      this.detailMsg = "Partial or full option cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contPaymentMethod.pmtSchedule) {
      this.severity = 'error';
      this.detailMsg = "Payment schedule cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contPaymentMethod.ttValueProduct) {
      this.severity = 'error';
      this.detailMsg = "Total value product cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contPaymentMethod.paymentAmount) {
      this.severity = 'error';
      this.detailMsg = "Payment amount cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contPaymentMethod.paymentDate) {
      this.severity = 'error';
      this.detailMsg = "Payment date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    
    if (!this.contPaymentMethod.dueDate) {
      this.severity = 'error';
      this.detailMsg = "Due date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    

    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.contPaymentMethod.entryDt = new Date();
    this.contPaymentMethod.entryUser = this.user.userId;
   
    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.contPaymentMethod)
    }
    this.contPaymentMethodService.insertData(JSON.stringify(this.jsonMessage));
    this.reset(this.contPaymentMethod);

    // this.router.navigateByUrl('/contPaymentMethodList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contPaymentMethod']);
    // });
  };


  update(contPaymentMethod): void {
    this.contPaymentMethod.dueAmount= this.contPaymentMethod.ttValueProduct-this.contPaymentMethod.paymentAmount;
    if (!this.contPaymentMethod.contId) {
      this.severity = 'error';
      this.detailMsg = "Contract number cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contPaymentMethod.partFull) {
      this.severity = 'error';
      this.detailMsg = "Partial or full option cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contPaymentMethod.pmtSchedule) {
      this.severity = 'error';
      this.detailMsg = "Payment schedule cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.contPaymentMethod.ttValueProduct) {
      this.severity = 'error';
      this.detailMsg = "Total value product cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contPaymentMethod.paymentAmount) {
      this.severity = 'error';
      this.detailMsg = "Payment amount cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
    if (!this.contPaymentMethod.paymentDate) {
      this.severity = 'error';
      this.detailMsg = "Payment date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
  
    if (!this.contPaymentMethod.dueDate) {
      this.severity = 'error';
      this.detailMsg = "Due date cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(contPaymentMethod)
    }
    this.contPaymentMethodService.updateData(JSON.stringify(this.jsonMessage));
    this.reset(this.contPaymentMethod);
    // this.router.navigateByUrl('/contPaymentMethodList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contPaymentMethod']);
    // });
  };


  delete(vendor): void {
    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(vendor)
    }
    this.contPaymentMethodService.deleteData(JSON.stringify(this.jsonMessage));
    this.reset(this.contPaymentMethod);
    // this.router.navigateByUrl('/contPaymentMethodList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['contPaymentMethod']);
    // });
  }



  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  reset(form) {

    if (this.options) {
      $('.custom-file-label').html('Choose file');

    }
    else {
      this.contPaymentMethod = new ContPaymentMethod();
    }



  }


}
