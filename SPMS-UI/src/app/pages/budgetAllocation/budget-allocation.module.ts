import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BudgetAllocationRoutingModule } from './budget-allocation-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, MultiSelectModule, TableModule } from 'primeng';
import { BudgetAllocationInfoComponent } from './budgetAllocationInfo/budgetAllocationInfo.component';
import { BudgetAllocationInsertComponent } from './budgetAllocationInsert/budgetAllocationInsert.component';


@NgModule({
  declarations: [BudgetAllocationInfoComponent, BudgetAllocationInsertComponent],
  imports: [
    CommonModule,
    BudgetAllocationRoutingModule,CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,MultiSelectModule
  ]
})
export class BudgetAllocationModule { }
