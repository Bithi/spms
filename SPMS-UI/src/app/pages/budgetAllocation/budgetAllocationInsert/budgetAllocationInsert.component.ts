import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Budgetbillentry } from 'src/app/models/Budgetbillentry';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { BudgetbillentryService } from 'src/app/services/budgetbillentry.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ItemPerBill } from 'src/app/models/ItemPerBill';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { ContractInfo } from 'src/app/models/contractinfo';
import { BudgetMaster } from 'src/app/models/BudgetMaster';
import { BudgetMasterEntryService } from 'src/app/services/budgetMasterEntry.service';
import { BudgetSectorwiseAllocation } from 'src/app/models/BudgetSectorwiseAllocation';
import { BudgetAllocationEntryService } from 'src/app/services/budgetAllocationEntry.service';
import { CADFundInfo } from 'src/app/models/CADFundInfo';
import { SectorwiseAllocationInfo } from 'src/app/models/SectorwiseAllocationInfo';
//import { BudgetAllocationMap } from 'src/app/models/BudgetAllocationMap';
import { element } from 'protractor';
//import { BudgetbillentryinfoComponent } from '../budgetbillentryinfo/budgetbillentryinfo.component';

@Component({
  selector: 'app-budgetMasterAllocation',
  templateUrl: './budgetAllocationInsert.component.html',
  styleUrls: ['./budgetAllocationInsert.component.scss']
})
export class BudgetAllocationInsertComponent implements OnInit {

  // @ViewChild('invoice_no',{static:false}) invoice_no: ElementRef;
  // @ViewChild('invoice_dt') invoice_dt: ElementRef;
  // @ViewChild('bill_amount') bill_amount: ElementRef;
  // @ViewChild('cont_id') con_id: ElementRef;
  // @ViewChild('delivary_date') delivary_date: ElementRef;

  public user: User;
  vendorNameSet = new Map();
  cont_id: string;
  selectedvendor: SelectItem[];
  selectedBillEnt: SelectItem[];
  selectedContract: SelectItem[];
  options: boolean = false;
  budgetBillEntry: Budgetbillentry = new Budgetbillentry();
  jsonMessage: any;
  key: string = "toast";
  severity: string;
  detailMsg: string;
  wording: string;
  contractInfos: ContractInfo[];
  contractInfo: ContractInfo = new ContractInfo();
  budgetMaster: BudgetMaster = new BudgetMaster();
  budgetAllocation: BudgetSectorwiseAllocation = new BudgetSectorwiseAllocation();
  selectedVatList: SelectItem[];
  selectedAitList: SelectItem[];

  yearList: any[];
  sectorList: any[];

  CADFundInfo: CADFundInfo = new CADFundInfo(); 
  SectorwiseAllocationInfo: SectorwiseAllocationInfo = new SectorwiseAllocationInfo(); 
  //SectorYearwiseAllocationInfo: BudgetAllocationMap = new BudgetAllocationMap(); 

  CADFundInfos: CADFundInfo[];
  SectorwiseAllocationInfos: SectorwiseAllocationInfo[];
  //SectorYearwiseAllocationInfos: BudgetAllocationMap[];




  constructor(
    private dataService: DataService
    ,private budgetMasterEntryService: BudgetMasterEntryService
    ,private budgetAllocationService: BudgetAllocationEntryService
    ,private utility: Utility
    ,public router: Router
    ,private dropdownInfoService: DropdownInfoService
    , private contractInfoService: ContractInfoService
  ) { }

  ngOnInit() {

    this.yearList = [
      { label: '2010', value: '2010' },
      { label: '2011', value: '2011' },
      { label: '2012', value: '2012' },
      { label: '2013', value: '2013' },
      { label: '2014', value: '2014' },
      { label: '2015', value: '2015' },
      { label: '2016', value: '2016' },
      { label: '2017', value: '2017' },
      { label: '2018', value: '2018' },
      { label: '2019', value: '2019' },
      { label: '2020', value: '2020' },
      { label: '2021', value: '2021' },
      { label: '2022', value: '2022' },
      { label: '2023', value: '2023' },
      { label: '2024', value: '2024' },
      { label: '2025', value: '2025' },
      { label: '2026', value: '2026' },
      { label: '2027', value: '2027' },
      { label: '2028', value: '2028' },
      { label: '2029', value: '2029' },
      { label: '2030', value: '2030' },
      { label: '2031', value: '2031' },
      { label: '2032', value: '2032' },
      { label: '2033', value: '2033' },
      { label: '2034', value: '2034' },
      { label: '2035', value: '2035' },
      { label: '2036', value: '2036' },
      { label: '2037', value: '2037' },
      { label: '2038', value: '2038' },
      { label: '2039', value: '2039' },
      { label: '2040', value: '2040' },
      { label: '2041', value: '2041' },
      { label: '2042', value: '2042' },
      { label: '2043', value: '2043' },
      { label: '2044', value: '2044' },
      { label: '2045', value: '2045' },
      { label: '2046', value: '2046' },
      { label: '2047', value: '2047' },
      { label: '2048', value: '2048' },
      { label: '2049', value: '2049' },
      { label: '2050', value: '2050' }
  ];

  this.sectorList = [];
  this.sectorList.push({ label: 'Select Sector', value: -1 });
  this.getSector();

    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    
    this.budgetMasterEntryService.addToTheList.subscribe(
      data => {
        this.budgetAllocation = data;
        this.options = false;
        //this.amcslaForm = this.contractInfo.type;

      }
    );

    this.getYearWiseCADFund();
    this.getYearWiseAllocation();

  }

  getSector(){

    const request = this.dropdownInfoService.getBudgetSector();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;

              console.log("Sector\n"+JSON.stringify(msg));

              for(let i=0; i<msg.length; i++){
                this.sectorList.push({ label: msg[i][1], value: msg[i][0] });
              }
  
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
  );

  }

  getCADAllocationByYear(data){

    var dataHeader = this.dataService.createMessageHeader("");

    var amount=0;

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: [{'year':data}]
    }

    const request = this.budgetAllocationService.getCADAllocationByYear(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
        
        const msg = JSON.parse(atob(res));
       
        amount = msg.payLoad[0].amount;

        console.log(amount);
        
      });

    return amount;

  }

  getBudgetAllocationByYear(data){

    var dataHeader = this.dataService.createMessageHeader("");

    var amount = 0;

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: [{'year':data}]
    }

    const request = this.budgetAllocationService.getBudgetAllocationByYear(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
        
        const msg = JSON.parse(atob(res));
       
        amount = msg.payLoad[0].amount;
        
        
      });

      return amount;

    
  }

  getAvailableFundByYear(data){

    var dataHeader = this.dataService.createMessageHeader("");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: [{'year':data}]
    }

    const request = this.budgetAllocationService.getAvailableFundByYear(JSON.stringify(this.jsonMessage));
    request.subscribe(
      (res) => {
        
        const msg = JSON.parse(atob(res));

        console.log("available balance:\n"+msg.payLoad[0].amount);
       
        return msg.payLoad[0].amount;
        
      });

  }

  getYearWiseCADFund() {

    const request = this.budgetAllocationService.getFundRealtedList("getYearWiseCADFund");
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res);
        this.CADFundInfos = [];

       

        for (let i = 0; i < msg.length; i++) {
          // tslint:disable-next-line:new-parens
          this.CADFundInfo = new CADFundInfo;
          this.CADFundInfo.amount = msg[i][0];
          this.CADFundInfo.year = msg[i][1];

          this.CADFundInfos[i] = this.CADFundInfo;
        }
      });

      //console.log("DATA:"+JSON.stringify(this.budgetAllocationInfos));

  }


  getYearWiseAllocation() {

    const request = this.budgetAllocationService.getFundRealtedList("getYearWiseAllocation");
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res);
        this.SectorwiseAllocationInfos = [];

        for (let i = 0; i < msg.length; i++) {
          // tslint:disable-next-line:new-parens
          this.SectorwiseAllocationInfo = new SectorwiseAllocationInfo;
          this.SectorwiseAllocationInfo.amount = msg[i][0];
          this.SectorwiseAllocationInfo.year = msg[i][1];

          this.SectorwiseAllocationInfos[i] = this.SectorwiseAllocationInfo;
        }
      });

      //console.log("DATA:"+JSON.stringify(this.budgetAllocationInfos));

  }



  saveBudgetAllocationEntry(budgetAllocation) {

    //console.log("\nBill Budget Entry:\n" + JSON.stringify(budgetBillEntry));s
    
    this.budgetAllocation = budgetAllocation;

    if (!this.budgetAllocation.sectorId || this.budgetAllocation.sectorId == null ) {
      this.severity = 'error';
      this.detailMsg = "Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.invoice_dt.nativeElement.focus();
      return;
    }

    if (!this.budgetAllocation.amount || this.budgetAllocation.amount == null) {
      this.severity = 'error';
      this.detailMsg = "Amount cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    if (!this.budgetAllocation.year || this.budgetAllocation.year == null ) {
      this.severity = 'error';
      this.detailMsg = "Year cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

   var availableCADFund;
   var usedFund;
   var availableFund;

   console.log("data\n"+this.CADFundInfos);
   console.log("data\n"+this.SectorwiseAllocationInfos);
   

   this.CADFundInfos.forEach(element => {
    if(element.year === budgetAllocation.year){
      availableCADFund = element.amount;
    }else{
      availableCADFund = 0;
    }
   });

   this.SectorwiseAllocationInfos.forEach( element => {
    if(element.year === budgetAllocation.year){
      usedFund = element.amount;
    }
   });

   availableFund = availableCADFund - usedFund;

    if(budgetAllocation.amount>availableFund){
      this.severity = 'error';
      this.detailMsg = "Not Enough Fund Available! Fund remains: "+availableFund;
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }


    

  

    this.budgetAllocation.createdBy = this.user.userId;
    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(budgetAllocation)
    }


    this.budgetAllocationService.insertBudgetAllocationEntry(JSON.stringify(this.jsonMessage));

  }



  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  setBillAmountWords(bill_amount) {
    
    this.wording = "";
    this.budgetAllocation.amount_in_words = this.wording;
    //console.log("\ninit: "+this.budgetBillEntry.bill_amount_in_words);

    //this.getCADAllocationByYear(bill_amount);

    if (bill_amount == 0 || !bill_amount) {
      console.log("\nzero or blank...\n");
      this.wording = "";
    }

    else {
      //console.log("\nB:-"+bill_amount);
      bill_amount = parseInt(bill_amount, 10);
      
      this.wording = this.budgetMasterEntryService.convertGreaterThanThousand(bill_amount) + " Only."
    }
    this.budgetAllocation.amount_in_words = this.wording;
  }



}


