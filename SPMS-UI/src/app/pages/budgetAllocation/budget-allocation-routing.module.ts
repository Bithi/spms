import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetAllocationInsertComponent } from './budgetAllocationInsert/budgetAllocationInsert.component';


const routes: Routes = [
  {path: '', component: BudgetAllocationInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetAllocationRoutingModule { }
