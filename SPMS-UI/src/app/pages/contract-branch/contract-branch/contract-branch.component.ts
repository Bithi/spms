import { Component, OnInit, ViewChild } from '@angular/core';
import { ContractBranch } from 'src/app/models/contractbranch';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { ContractBranchService } from 'src/app/services/contractbranch.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { Table } from 'primeng';



@Component({
  selector: 'app-contract-branch',
  templateUrl: './contract-branch.component.html',
  styleUrls: ['./contract-branch.component.scss']
})
export class ContractBranchComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  contractbranches: ContractBranch[];
  contractbranch: ContractBranch = null;
  cols: any[];
  branches = new Map();
  contractNameSet = new Map();
  vendorSet = new Map();
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private contractBranchService: ContractBranchService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService,
    private contractInfoService: ContractInfoService
  ) { }
  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['contractbranch']);
  });
}
   
  getContractBranch() {

    var request = this.contractBranchService.getContractBranchList();
    request.subscribe(
      (res) => {
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res) ;
        this.contractbranches =[] ;

        for(let i = 0; i< msg.length; i++) {
          this.contractbranch = new ContractBranch;
         
          this.contractbranch.contractId= msg[i][0];
          this.contractbranch.brac= msg[i][1];
          this.contractbranch.brcAtm= msg[i][2];
          this.contractbranch.bw= msg[i][3];
          this.contractbranch.monthlyCharge= msg[i][4];
          this.contractbranch.id= msg[i][5];
          this.contractbranch.entryUser= msg[i][6];
          this.contractbranch.entryDate= msg[i][7];
          this.contractbranch.updateUser= msg[i][8];
          this.contractbranch.updateDt= msg[i][9];
          this.contractbranch.brName= msg[i][10];
          this.contractbranch.vendor=this.vendorSet.get(msg[i][0])
       
       
       
          this.contractbranch.status= msg[i][11];
          
          this.contractbranch.contractName= msg[i][12];

          this.contractbranches[i] = this.contractbranch;
          // console.log( this.vendorList);
     }
    });

  }
  createContractBranch(): void {
    this.router.navigate(['/create']);
  };


  ngOnInit() {
    this.getContractInfojoin();
    this.getContractBranch();
    this.getBranchName();
    this.getContractInfo();
    this.cols = [
      { field: 'vendor', header: 'Vendor' },
      { field: 'contractName', header: 'Contract' },
      { field: 'brac', header: 'Branch Code' },
      { field: 'brcAtm', header: 'Branch ATM' },
      { field: 'brName', header: 'Branch Name' },
      { field: 'monthlyCharge', header: 'Monthly Charge' }


    ];

  }

  editContractBranch(Contractbranch): void {
    this.contractBranchService.passTheValue(Contractbranch);

  }

  getBranchName() {

    const request = this.dropdownInfoService.getBranchNameList();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          // this.selecteBranchName.push({label: msg[i].branName + "-" + msg[i].branCode, value:msg[i].branCode});
          // this.brName[msg[i].branCode] = msg[i].branName;
          this.branches.set(msg[i].branCode, msg[i].branName + "-" + msg[i].branCode);

        }
        //console.log(this.branches);

      },
      err => {

        return;
      }
    );


  }

  getContractInfo() {
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    }

    

    this.contractInfoService.getList(JSON.stringify(this.jsonMessage)).subscribe(
      (res) => {
        const msg = JSON.parse(atob(res));
        debugger;
        for (let i = 0; i < msg.payLoad.length; i++) {
                  // console.log( this.vendorList);
                  
                  this.contractNameSet.set(msg.payLoad[i].contId, msg.payLoad[i].contNo);
                }

      },
      err => {
        return;
      }
    );


}
getContractInfojoin() {
  const request = this.contractInfoService.contractInfoJoin();
  request.subscribe(
    (res) => {

     
      const msg = JSON.parse(res) ;
      for(let i = 0; i< msg.length; i++) {
        this.vendorSet.set(msg[i][0], msg[i][30]);

   }
  });
}

}
