import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractBranchComponent } from './contract-branch.component';

describe('ContractBranchComponent', () => {
  let component: ContractBranchComponent;
  let fixture: ComponentFixture<ContractBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
