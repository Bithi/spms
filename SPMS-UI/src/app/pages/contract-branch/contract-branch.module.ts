import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractBranchRoutingModule } from './contract-branch-routing.module';
import { ContractBranchComponent } from './contract-branch/contract-branch.component';
import { ContractBranchInsertComponent } from './contract-branch-insert/contract-branch-insert.component';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from 'src/app/layout/layout.module';
import { DropdownModule, ProgressSpinnerModule, TableModule } from 'primeng';

@NgModule({
  declarations: [ ContractBranchComponent, ContractBranchInsertComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    ContractBranchRoutingModule
  ],
  exports: [ ContractBranchComponent, ContractBranchInsertComponent]
})


export class ContractBranchModule { }
