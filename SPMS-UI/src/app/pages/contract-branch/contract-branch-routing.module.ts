import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractBranchInsertComponent } from './contract-branch-insert/contract-branch-insert.component';


const routes: Routes = [
  {path: '', component: ContractBranchInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractBranchRoutingModule { }
