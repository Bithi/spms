import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractBranchInsertComponent } from './contract-branch-insert.component';

describe('ContractBranchInsertComponent', () => {
  let component: ContractBranchInsertComponent;
  let fixture: ComponentFixture<ContractBranchInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractBranchInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractBranchInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
