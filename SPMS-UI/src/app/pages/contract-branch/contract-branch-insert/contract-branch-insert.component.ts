import { Component, OnInit } from '@angular/core';
import { ContractBranchService } from 'src/app/services/contractbranch.service';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { ContractBranch } from 'src/app/models/contractbranch';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { User } from 'src/app/models/user';
declare var $ : any;

@Component({
  selector: 'app-contract-branch-insert',
  templateUrl: './contract-branch-insert.component.html',
  styleUrls: ['./contract-branch-insert.component.scss']
})
export class ContractBranchInsertComponent implements OnInit {

  contractBranch:ContractBranch=new ContractBranch();
  selecteContractList:SelectItem[];
  selecteBranchName:SelectItem[];
 
  vendorSet = new Map();
  selectStatus:SelectItem[];
  currentFileUpload: File;
  public user  : User;
  jsonMessage:any;
  brName:any= {};
  options: boolean = false;
  optionsForBranchAndATM: boolean = false;
  status=[
    {value:null,label:'Select Status'},
    {value:'ACT',label:'Active'},
    {value:'CAN',label:'Inactive'}
  ];
  constructor(
    private contractBranchInfoService: ContractBranchService,
    private dataService: DataService,
    public router:Router,
    private messageService:MessageService,
    private utility:Utility,
    private dropdownInfoService: DropdownInfoService

  ) { }

    key:string = "toast";
    severity:string;
    detailMsg:string;


  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('loggedinUser'));
    this.getVendors();
    this.selecteContractList = [];
    this.selecteContractList.push({ label: 'Select Contract', value: -1});
    this.getContractListInfo();
    this.selecteBranchName = [];
    this.selecteBranchName.push({ label: 'Select Branch', value: -1});
    this.getBranchName();
    this.optionsForBranchAndATM = false;
    
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.contractBranchInfoService.addToTheList.subscribe(
      data =>{
        debugger;
       this.contractBranch = data;
       this.options = false;
        if((this.contractBranch.brName!=null) && (this.contractBranch.brcAtm!=null)){
          this.optionsForBranchAndATM = true;
        }else{
          this.optionsForBranchAndATM = false;
        }
      }
    )

  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  getContractListInfo(){
   
    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;

              for(let i = 0; i< msg.length; i++) {
                this.selecteContractList.push({label: msg[i].contNo, value: msg[i].contId});
           }


              
              
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );


}
getVendors(){
  const request = this.dropdownInfoService.getVendors();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;

            for(let i = 0; i< msg.length; i++) {
              this.vendorSet.set(msg[i].venId, msg[i].venName);
              // console.log( this.vendorList);
         }


            
            
        
    },
    err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);

}

getBranchName(){
   
  const request = this.dropdownInfoService.getBranchNameList();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;
            for(let i = 0; i< msg.length; i++) {
              this.selecteBranchName.push({label: msg[i].branName + "-" + msg[i].branCode, value:msg[i].branCode});
              this.brName[msg[i].branCode] = msg[i].branName;
              
         }
    },
    err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        // this.showProgressSpin = false;
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);


}

  createContractBranch(): void {

    if(!this.contractBranch.contractId){
      this.severity = 'error';
      this.detailMsg = "contract id cannot be empty.";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;

    }
    
    if(!this.contractBranch.status){
      this.severity = 'error';
      this.detailMsg = "status cannot be null";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;

    }

    if(this.optionsForBranchAndATM){
      if(!this.contractBranch.brcAtm){
        this.severity = 'error';
        this.detailMsg = "Branch code cann't be empty";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
      if(!this.contractBranch.brName){
        this.severity = 'error';
        this.detailMsg = "Branch name cann't be empty";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    }
    else
    {
      if(!this.contractBranch.brac){
        this.severity = 'error';
        this.detailMsg = "Branch value cann't be empty";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    }

    
    // if(!this.contractBranch.brcAtm){
    //   this.severity = 'error';
    //   this.detailMsg = "brac atm cannot be null";
    //   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    //   return;

    // }
    // var today = new Date();
    // var date = (today.getMonth()+1)+'/'+(today.getDate()+1)+'/'+today.getFullYear();
    this.contractBranch.entryDate = new Date();
    this.contractBranch.entryUser = this.user.userId; 
    this.contractBranch.updateDt = new Date('01/01/1970');   
    this.contractBranch.updateUser = "";
    if(!this.optionsForBranchAndATM){
      this.contractBranch.brName= this.brName[this.contractBranch.brac];
    }else{
      this.contractBranch.brName = this.contractBranch.brName;
      this.contractBranch.brcAtm = this.contractBranch.brcAtm;
    }
    
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contractBranch)
    }
     this.contractBranchInfoService.insertContractBranchInfo(JSON.stringify(this.jsonMessage));
  //    this.router.navigateByUrl('/contractbranchList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbranch']);
  // });
  this.reset(this.contractBranch);
  };

  updateContract(contractBranch): void {
      
    if(!this.contractBranch.contractId){
      this.severity = 'error';
      this.detailMsg = "contract id cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;

    }
    
    if(!this.contractBranch.status){
      this.severity = 'error';
      this.detailMsg = "status cannot be null";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }


    if(this.optionsForBranchAndATM){
      if(!this.contractBranch.brcAtm){
        this.severity = 'error';
        this.detailMsg = "Branch code cann't be empty";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
      if(!this.contractBranch.brName){
        this.severity = 'error';
        this.detailMsg = "Branch name cann't be empty";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    }
    else
    {
      var num = parseInt(this.contractBranch.brac);
      if(num===-1){
        this.severity = 'error';
        this.detailMsg = "Branch value cann't be empty";
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    }



    this.contractBranch.updateUser=JSON.parse(localStorage.getItem('loggedinUser')).userId;
    this.contractBranch.updateDt=new Date();
    if(!this.optionsForBranchAndATM){
      this.contractBranch.brName= this.brName[this.contractBranch.brac];
      this.contractBranch.brcAtm = "";
      this.contractBranch.brName = "";
    }else{
      this.contractBranch.brName = this.contractBranch.brName;
      this.contractBranch.brcAtm = this.contractBranch.brcAtm;
      this.contractBranch.brac = "";
    }
 
    var dataHeader = this.dataService.createMessageHeader("UPDATE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.contractBranch)
    }
    debugger;
    this.contractBranchInfoService.updateContractBranchInfo(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/contractbranchList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbranch']);
  // });
  this.reset(this.contractBranch);
  };

  deleteContract(ContractBranch): void {
   
    

    var dataHeader = this.dataService.createMessageHeader("DELETE");
    
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(ContractBranch)
    }
    this.contractBranchInfoService.deleteContractBranchInfo(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/contractbranchList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['contractbranch']);
  // });
  this.reset(this.contractBranch);
  };

 

  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
    

  }
  upload(){
    var formdata: FormData = new FormData();
    formdata.append('file', this.currentFileUpload);  
    this.contractBranchInfoService.upload(formdata);

  }
  selectchange(args){
    console.log(args.target.options[args.target.selectedIndex].text);
    this.contractBranch.brName=args.target.options[args.target.selectedIndex].text; 
  }

  reset(form) {
    console.log(this.options);
  
    if(this.options){
      $('.custom-file-label').html('Choose file');

    }
    else{
      // this.contractBranch  = new ContractBranch();
      if(!this.optionsForBranchAndATM){
        this.optionsForBranchAndATM=false;
        // form.reset({ optionsForBranchAndATM: false });

     
      }
      else{
          this.optionsForBranchAndATM=true;
      }
      this.contractBranch  = new ContractBranch();

      
    }
    

    
}
}
