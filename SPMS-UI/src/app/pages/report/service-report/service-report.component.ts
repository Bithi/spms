import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data/data.service';
import { PenaltyReport } from 'src/app/models/PenaltyReport';
import { PenaltyReportService } from 'src/app/services/penaltyReport.service';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-service-report',
  templateUrl: './service-report.component.html',
  styleUrls: ['./service-report.component.scss']
})
export class ServiceReportComponent implements OnInit {key:string = "toast";
severity:string;
detailMsg:string;

public penaltyReport: PenaltyReport = new PenaltyReport();


constructor(
private penaltyReportService: PenaltyReportService,
private dataService: DataService,
private utility:Utility,
) { }

ngOnInit() {
}







export(){



var dataHeader = this.dataService.createMessageHeader("SERVICE_REPORT");
this.penaltyReport.user=JSON.parse(localStorage.getItem('loggedinUser')).userId;
var jsonMessage = {
  dataHeader : dataHeader,
  //payLoad: [this.downtime]
  payLoad: [this.penaltyReport]
}

this.penaltyReportService.generateServiceReport(jsonMessage);

  

}

reset(form) {
this.penaltyReport= new PenaltyReport();
form.reset({ selectedVendor: this.penaltyReport.venId });
form.reset({ selectedContractId: this.penaltyReport.contId });
form.reset({ startDate: this.penaltyReport.startDate });
form.reset({ endDate: this.penaltyReport.endDate });

}


}
