import { Component, OnInit } from '@angular/core';
import { MessageService, SelectItem } from 'primeng/api';
import { PenaltyReportService } from 'src/app/services/penaltyReport.service';
import { PenaltyReport } from 'src/app/models/PenaltyReport';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';

@Component({
  selector: 'app-penalty-report',
  templateUrl: './penalty-report.component.html',
  styleUrls: ['./penalty-report.component.scss']
})
export class PenaltyReportComponent implements OnInit {

  key:string = "toast";
    severity:string;
    detailMsg:string;

  selectedContractId:SelectItem[];
  selectedVendor:SelectItem[];
  public penaltyReport: PenaltyReport = new PenaltyReport();
  

  constructor(
    private penaltyReportService: PenaltyReportService,
    private dropdownInfo: DropdownInfoService,
    private dataService: DataService,
    private utility:Utility,
  ) { }

  ngOnInit() {
    this.selectedContractId = [];
    this.selectedContractId.push({ label: 'Select Contract ID', value: -1});
    this.selectedVendor = [];
    this.selectedVendor.push({ label: 'Select Vendor ID', value: -1});
    this.getContractIdList();
    this.getVendorIdList();
  }
  getContractList(venId: string){
    //console.log(venId);
    
      // this.dropdownInfoService.getContracts(this.penaltyCalcRep.venId).subscribe(
      //   data => this.contractinfos = data
      // );
      this.selectedContractId = [];
      //this.selectedcontract.push({ label: 'Select Contract Number', value: -1});
  
      if(venId){
      const request = this.dropdownInfo.getContracts(venId);
      request.subscribe(
        (res ) => {
            
                const msg = JSON.parse(res) ;
                // console.log(msg);
                for(let i = 0; i< msg.length; i++) {
                 
                  this.selectedContractId.push({label: msg[i].contNo, value: msg[i].contId});
                  // console.log( this.vendorList);
             }
            
        }
   );
    }
  }

  getContractIdList(){
    const request = this.penaltyReportService.getCotractId();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
 
              for(let i = 0; i< msg.length; i++) {
                this.selectedContractId.push({label: msg[i].contNo, value: msg[i].contId});
                // console.log( this.vendorList);
           }
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
 
 
 }

 getVendorIdList(){
  const request = this.penaltyReportService.getVendorId();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;

            for(let i = 0; i< msg.length; i++) {
              this.selectedVendor.push({label: msg[i].venName, value: msg[i].venId});
              // console.log( this.vendorList);
         }
    },
    err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);


}


export(){
  if(!this.penaltyReport.venId){
    this.severity = 'error';
    this.detailMsg = "Vendor can't be empty.";
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
  }    
  

  if(!this.penaltyReport.contId){
    this.severity = 'error';
    this.detailMsg = "Contract Number can't be empty.";
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
  }    


  
  var dataHeader = this.dataService.createMessageHeader("PENALTY_REPORT");
  var jsonMessage = {
      dataHeader : dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.penaltyReport]
  }

  this.penaltyReportService.generatePenaltyReport(jsonMessage);

      

}

reset(form) {
  this.penaltyReport= new PenaltyReport();
  form.reset({ selectedVendor: this.penaltyReport.venId });
  form.reset({ selectedContractId: this.penaltyReport.contId });
  form.reset({ startDate: this.penaltyReport.startDate });
  form.reset({ endDate: this.penaltyReport.endDate });
  
}


}
