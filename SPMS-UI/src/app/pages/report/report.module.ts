import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { DropdownModule, TableModule, ToastModule } from 'primeng';
import { DowntimeReportComponent } from './downtime-report/downtime-report.component';
import { PenaltyCalcComponent } from './penalty-calc/penalty-calc.component';
import { PenaltyReportComponent } from './penalty-report/penalty-report.component';
import { AmcReportComponent } from './amc-report/amc-report.component';
import { ConnectivityReportComponent } from './connectivity-report/connectivity-report.component';
import { FindValidEndComponent } from './find-valid-end/find-valid-end.component';
import { ServiceReportComponent } from './service-report/service-report.component';


@NgModule({
  declarations: [DowntimeReportComponent, PenaltyCalcComponent, PenaltyReportComponent, AmcReportComponent, ConnectivityReportComponent, FindValidEndComponent, ServiceReportComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    HttpClientModule,
    HttpModule,ToastModule,TableModule,DropdownModule,
    ReactiveFormsModule,
    ReportRoutingModule
  ],
  exports: [
    DowntimeReportComponent, PenaltyCalcComponent, PenaltyReportComponent, AmcReportComponent, ConnectivityReportComponent, FindValidEndComponent
],
})
export class ReportModule { }
