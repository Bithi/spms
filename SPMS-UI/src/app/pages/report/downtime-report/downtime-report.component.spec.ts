import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DowntimeReportComponent } from './downtime-report.component';

describe('DowntimeReportComponent', () => {
  let component: DowntimeReportComponent;
  let fixture: ComponentFixture<DowntimeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DowntimeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DowntimeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
