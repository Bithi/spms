import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { Downtime } from 'src/app/models/downtime';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import * as moment from 'moment';
import { DowntimeCalcRepo } from 'src/app/models/DowntimeCalcRepo';
import { DowntimeService } from 'src/app/services/downtime.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';

declare var $: any;
@Component({
  selector: 'app-downtime-report',
  templateUrl: './downtime-report.component.html',
  styleUrls: ['./downtime-report.component.scss']
})
export class DowntimeReportComponent implements OnInit {
  downtimeList: Downtime[];
  downtime: Downtime = new Downtime();
  DowntimeCalcRepo: DowntimeCalcRepo = new DowntimeCalcRepo();
  cols: any[];
  vendorNameSet = new Map();

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.getDownTimeList();
    this.getVendorList();

    this.cols = [
      { field: 'id', header: 'ID', filter: true, filterMatchMode: 'contains' },
      { field: 'month', header: 'Month' },
      { field: 'date', header: 'Date', dateField: true },
      { field: 'vendor', header: 'Vendor' }
    ];
    // this.setDataTableOptions();
    // $("#example1").DataTable();
    // $('#example2').DataTable({
    //   "paging": true,
    //   "lengthChange": true,
    //   "searching": true,
    //   "ordering": true,
    //   "info": true,
    //   "autoWidth": true,

    // });




  }


  constructor(
    private downtimeService: DowntimeService,
    private reportService: ReportService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService

  ) { }
  export() {
    var dataHeader = this.dataService.createMessageHeader("DOWNTIME_REPORT");
    var jsonMessage = {
      dataHeader: dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.DowntimeCalcRepo]
    }

    this.reportService.generateDownTimeDetailsReportRequest(jsonMessage);



  }

  format(date) {
    return moment(date).format('lll');
  }

  getDownTimeList() {
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    };
    const req = this.downtimeService.getDowntimeList(JSON.stringify(jsonMessage));
    req.subscribe(
      (res) => {
        const msg = JSON.parse(atob(res));
        this.downtimeList = msg.payLoad;
        //localStorage.setItem('isLoggedin', 'true');
        //localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
        return;

      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }

  getVendorList() {

    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          // console.log( this.vendorList);
          this.vendorNameSet.set(msg[i].venId, msg[i].venName);
        }
        //console.log(this.vendorNameSet);

      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }

}
