import { Component, OnInit } from '@angular/core';
import { PenaltyReport } from 'src/app/models/PenaltyReport';
import { SelectItem } from 'primeng';
import { PenaltyReportService } from 'src/app/services/penaltyReport.service';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { SubItemTypeService } from 'src/app/services/subitemtype.service';
import { User } from 'src/app/models/user';
import { ContSubItemService } from 'src/app/services/contsubitem.service';

@Component({
  selector: 'app-amc-report',
  templateUrl: './amc-report.component.html',
  styleUrls: ['./amc-report.component.scss']
})
export class AmcReportComponent implements OnInit {
  public user  : User;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  options: boolean = true;
  subItemList : SelectItem[];
selectedContractId:SelectItem[];
selectedTypeId: SelectItem[];
selectedVendor:SelectItem[];
jsonMessage: any;
in_action=[
  {value:null,label:'Select Filter'},
  {value:'AMC_REPORT_ON_DATE',label:'AMC REPORT ON DATE'},
  {value:'AMC_REPORT_ON_VEN',label:'AMC REPORT ON VENDOR'},
  {value:'AMC_REPORT_ON_ITEM',label:'AMC REPORT ON ITEM'},
  {value:'AMC_REPORT_ON_SITE',label:'AMC REPORT ON SITE'},
  {value:'AMC_REPORT_ON_WAR',label:'AMC REPORT ON WARRANTY'},
];

selectedWar=[
  {value:null,label:'SELECT STATUS'},
  {value:'Under Amc',label:'UNDER AMC'},
  {value:'Under Warranty',label:'UNDER WARRANTY'},
  {value:'Not in AMC/WAR',label:'NOT IN AMC/WAR'}
];

public penaltyReport: PenaltyReport = new PenaltyReport();


constructor(
  private penaltyReportService: PenaltyReportService,
  private dropdownInfo: DropdownInfoService,
  private dataService: DataService,
  private dropdownInfoService: DropdownInfoService,
  private contSubItemService: ContSubItemService,
  private subItemTypeService: SubItemTypeService,
  private utility:Utility,
) { }

ngOnInit() {
  this.user = JSON.parse(localStorage.getItem('loggedinUser'));
  this.selectedVendor = [];
    this.selectedVendor.push({ label: 'SELECT VENDOR', value: -1});
  this.getVendorList();
  this.subItemList = [];
  this.subItemList.push({ label: 'SELECT ITEM', value: -1 });
  this.getItemList();
  this.selectedTypeId = [];
  this.selectedTypeId.push({ label: 'SELECT SITE', value: -1 });

  this.getTypeId();
}

getTypeId() {
  const request = this.subItemTypeService.getAllTypeId();
  request.subscribe(
    (res) => {

      const msg = JSON.parse(res);

      for (let i = 0; i < msg.length; i++) {
        this.selectedTypeId.push({ label: msg[i].shortName, value: msg[i].typeId });
        // console.log( this.vendorList);
      }





    },
    err => {
      // this.severity = 'error';
      // this.detailMsg = "Server Error: " + err.message;
      // // this.showProgressSpin = false;
      // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
  );


}

getItemList() {

  var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

  this.jsonMessage = {
    dataHeader: dataHeader,
    payLoad: new Array()
  }

  const request = this.contSubItemService.getItemList(JSON.stringify(this.jsonMessage));
  request.subscribe(
    (res) => {

      const msg = JSON.parse(atob(res)).payLoad;
     
      for (let i = 0; i < msg.length; i++) {
        this.subItemList.push({ label: msg[i].itemName, value: msg[i].itemId });
      }

    },
    err => {
      this.severity = 'error';
      this.detailMsg = "Server Error: " + err.message;
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }
  );
}

getVendorList(){
   
  const request = this.dropdownInfoService.getVendors();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;

            for(let i = 0; i< msg.length; i++) {
              this.selectedVendor.push({label: msg[i].venName, value: msg[i].venId});
              // console.log( this.vendorList);
         }


            
            
        
    },
    err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);


}




export(){
this.penaltyReport.user=this.user.userName
var dataHeader = this.dataService.createMessageHeader("AMC_REPORT");
var jsonMessage = {
    dataHeader : dataHeader,
    //payLoad: [this.downtime]
    payLoad: [this.penaltyReport]
}

this.penaltyReportService.generateAMCReport(jsonMessage);

    

}

reset(form) {
this.penaltyReport= new PenaltyReport();
form.reset({ selectedVendor: this.penaltyReport.venId });
form.reset({ selectedContractId: this.penaltyReport.contId });
form.reset({ startDate: this.penaltyReport.startDate });
form.reset({ endDate: this.penaltyReport.endDate });

}

}
