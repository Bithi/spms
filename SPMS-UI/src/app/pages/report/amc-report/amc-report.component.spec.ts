import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmcReportComponent } from './amc-report.component';

describe('AmcReportComponent', () => {
  let component: AmcReportComponent;
  let fixture: ComponentFixture<AmcReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmcReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmcReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
