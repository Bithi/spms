import { Component, OnInit } from '@angular/core';
import { PenaltyCalc } from 'src/app/models/PenaltyCalc';
import { DataService } from 'src/app/data/data.service';
import { PenaltycalcService } from 'src/app/services/penaltycalc.service';
import { ReportService } from 'src/app/services/report.service';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VendorInfoService } from 'src/app/services/vendor-info.service';
import { ContactInfoService } from 'src/app/services/contact-info.service';
import { ContractInfo } from 'src/app/models/contractinfo';
import { ContractBranchInfoService } from 'src/app/services/contractbranch-info.service';
import { ContractBranch } from 'src/app/models/contractbranch';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ContractInfoService } from 'src/app/services/contractinfo.service';


@Component({
  selector: 'app-penalty-calc',
  templateUrl: './penalty-calc.component.html',
  styleUrls: ['./penalty-calc.component.scss']
})
export class PenaltyCalcComponent implements OnInit {

  vendors: {};
  vendorData: [];
  selectedvendor: SelectItem[];
  selectedcontract: SelectItem[];
  selectedcontractbranch: SelectItem[];
  branches = new Map();
  contractinfos: {};
  contractbranches: {};
  twelvemonth = [
    { value: null, label: 'Select Month' },
    { value: '1', label: 'january' },
    { value: '2', label: 'february' },
    { value: '3', label: 'March' },
    { value: '4', label: 'April' },
    { value: '5', label: 'May' },
    { value: '6', label: 'june' },
    { value: '7', label: 'july' },
    { value: '8', label: 'August' },
    { value: '9', label: 'September' },
    { value: '10', label: 'October' },
    { value: '11', label: 'November' },
    { value: '12', label: 'December' }
  ];

  mypenaltyCalcList: PenaltyCalc[];
  penaltycalc: PenaltyCalc = null;
  cols: any[];
  penaltyCalcRep: PenaltyCalc = new PenaltyCalc();
  vendor: VendorInfo = new VendorInfo();
  vendorNameSet = new Map();
  contractNameSet = new Map();


  constructor(
    private dataService: DataService,
    private penaltycalcService: PenaltycalcService,
    private reportService: ReportService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility,
    private http: HttpClient,
    private vendorInfoService: VendorInfoService,
    private contactInfoService: ContactInfoService,
    private contractBranchInfoService: ContractBranchInfoService,
    private dropdownInfoService: DropdownInfoService,
    private contractInfoService: ContractInfoService
  ) { }

  ngOnInit() {
    this.selectedvendor = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });

    this.dropdownInfoService.getVendors().subscribe(
      data => this.vendors = data
    );

    this.getVendorList();
    this.getBranchName();
    this.getContractInfo()
    // this.getContractList(this.penaltyCalcRep.venId);
    // this.getBranchList(this.penaltyCalcRep.contId);
    this.penaltycalc = new PenaltyCalc();
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    // this.getVendorList().subscribe(

    //   data => this.vendorList = data
    //   );
    // console.log(this.vendorList);

    this.getPenaltyCalcList();

    this.cols = [
      { field: 'contId', header: 'Contract ID' },
      { field: 'venId', header: 'Vendor ID' },
      { field: 'brCode', header: 'Branch Code' },
      { field: 'month', header: 'Month' }
    ];
  }


  getPenaltyCalcList() {
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");
    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.penaltycalc)
    };
    const req = this.penaltycalcService.getPenaltyCalcList(JSON.stringify(jsonMessage));
    req.subscribe(
      (res) => {
        const msg = JSON.parse(atob(res));
        this.mypenaltyCalcList = msg.payLoad;
        //localStorage.setItem('isLoggedin', 'true');
        //localStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
        return;

      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }
  getVendorList() {

    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
          // console.log( this.vendorList);
          this.vendorNameSet.set(msg[i].venId, msg[i].venName);
        }
        //console.log(this.vendorNameSet);

      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }


  export() {
    console.log(this.penaltyCalcRep.month);
    var dataHeader = this.dataService.createMessageHeader("PENALTYCALC_REPORT");
    var jsonMessage = {
      dataHeader: dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.penaltyCalcRep]
    }

    this.reportService.generatePenaltyCalcReportRequest(jsonMessage);



  }

  // getVendorList(): Observable<VendorInfo[]>{

  //   return this.http.get<VendorInfo[]>(this._url);


  //   //this.vendorList
  // }



  selectChangeHandler(event: any) {
    this.vendor = event.value;

    //this.getContactList(jsonMessage);
  }

  getContractList(venId: string) {
    //console.log(venId);

    // this.dropdownInfoService.getContracts(this.penaltyCalcRep.venId).subscribe(
    //   data => this.contractinfos = data
    // );
    this.selectedcontract = [];
    this.selectedcontract.push({ label: 'Select Contract', value: -1 });

    if (venId) {
      const request = this.dropdownInfoService.getContracts(venId);
      request.subscribe(
        (res) => {

          const msg = JSON.parse(res);


          for (let i = 0; i < msg.length; i++) {
            this.selectedcontract.push({ label: msg[i].contNo, value: msg[i].contId });
            // console.log( this.vendorList);
          }





        },
        err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
        }
      );
    }
    else {
      this.contractinfos = null;
      this.contractbranches = null;
    }
  }

  getBranchList(contractId: string) {
    this.selectedcontractbranch = [];
    this.selectedcontractbranch.push({ label: 'Select Branch', value: -1 });
    if (contractId) {
      // this.dropdownInfoService.getContractBraches(contractId).subscribe(
      //   data => this.contractbranches = data
      // );

      const request = this.dropdownInfoService.getContractBraches(this.penaltyCalcRep.contId);
      request.subscribe(
        (res) => {

          const msg = JSON.parse(res);
          console.log("msg=" + msg);
          for (let i = 0; i < msg.length; i++) {
            this.selectedcontractbranch.push({ label: msg[i].brac, value: msg[i].brac });
            // console.log( this.vendorList);
          }

        },
        err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
        }
      );
    } else {
      this.contractbranches = null;
    }
  }

  getBranchName() {

    const request = this.dropdownInfoService.getBranchNameList();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          // this.selecteBranchName.push({label: msg[i].branName + "-" + msg[i].branCode, value:msg[i].branCode});
          // this.brName[msg[i].branCode] = msg[i].branName;
          this.branches.set(msg[i].branCode, msg[i].branName + "-" + msg[i].branCode);

        }
        //console.log(this.branches);

      },
      err => {

        return;
      }
    );


  }

  getContractInfo() {
    const request = this.contractInfoService.getContractInfoList();
    request.subscribe(
      (msg) => {
        //console.log(msg);
        for (let i = 0; i < msg.length; i++) {
          // console.log( this.vendorList);
          this.contractNameSet.set(msg[i].contId, msg[i].contNo);
        }
        //console.log(this.contractNameSet);

      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }

}
