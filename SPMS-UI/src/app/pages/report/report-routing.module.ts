import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/util/auth.guard';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { AmcReportComponent } from './amc-report/amc-report.component';
import { ConnectivityReportComponent } from './connectivity-report/connectivity-report.component';
import { DowntimeReportComponent } from './downtime-report/downtime-report.component';
import { PenaltyCalcComponent } from './penalty-calc/penalty-calc.component';
import { PenaltyReportComponent } from './penalty-report/penalty-report.component';
import { ServiceReportComponent } from './service-report/service-report.component';

// const routes: Routes = [  
//   {
//    path: '',    
//    component: ConnectivityReportComponent,
//    children: [
//     {path: 'amcReport', component: AmcReportComponent},
//    ]
//   }
//  ];
const routes: Routes = [
  // {path: '', component: DowntimeReportComponent},
  {path: '', component:  ConnectivityReportComponent},
  {path: 'amcReport', component: AmcReportComponent,canActivate: [ AuthGuard ]},
  {path: 'export', component: PenaltyReportComponent,canActivate: [ AuthGuard ]},
  {path: 'penaltyReport', component: PenaltyCalcComponent,canActivate: [ AuthGuard ]},
  {path: 'serviceReport', component: ServiceReportComponent,canActivate: [ AuthGuard ]}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
