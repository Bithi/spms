import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectivityReportComponent } from './connectivity-report.component';

describe('ConnectivityReportComponent', () => {
  let component: ConnectivityReportComponent;
  let fixture: ComponentFixture<ConnectivityReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectivityReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectivityReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
