import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng';
import { PenaltyReport } from 'src/app/models/PenaltyReport';
import { PenaltyReportService } from 'src/app/services/penaltyReport.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-connectivity-report',
  templateUrl: './connectivity-report.component.html',
  styleUrls: ['./connectivity-report.component.scss']
})
export class ConnectivityReportComponent implements OnInit {

  key:string = "toast";
  severity:string;
  detailMsg:string;

public penaltyReport: PenaltyReport = new PenaltyReport();


constructor(
  private penaltyReportService: PenaltyReportService,
  private dropdownInfo: DropdownInfoService,
  private dataService: DataService,
  private utility:Utility,
) { }

ngOnInit() {
}







export(){



var dataHeader = this.dataService.createMessageHeader("CONNECTIVITY_REPORT");
var jsonMessage = {
    dataHeader : dataHeader,
    //payLoad: [this.downtime]
    payLoad: [this.penaltyReport]
}

this.penaltyReportService.generateConnectivityReport(jsonMessage);

    

}

reset(form) {
this.penaltyReport= new PenaltyReport();
form.reset({ selectedVendor: this.penaltyReport.venId });
form.reset({ selectedContractId: this.penaltyReport.contId });
form.reset({ startDate: this.penaltyReport.startDate });
form.reset({ endDate: this.penaltyReport.endDate });

}

}
