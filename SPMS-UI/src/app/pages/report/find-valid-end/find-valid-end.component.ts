import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data/data.service';
import { FindValidEnd } from 'src/app/models/FindValidEnd';
import { PenaltyReportService } from 'src/app/services/penaltyReport.service';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-find-valid-end',
  templateUrl: './find-valid-end.component.html',
  styleUrls: ['./find-valid-end.component.scss']
})
export class FindValidEndComponent implements OnInit {

  key:string = "toast";
  severity:string;
  detailMsg:string;

  public findValidEnd: FindValidEnd = new FindValidEnd();

  constructor(
    private penaltyReportService: PenaltyReportService,
    private dataService: DataService,
    private utility:Utility,
  ) { }

  ngOnInit(): void {
  }

  export(){

    var dataHeader = this.dataService.createMessageHeader("VALIDEND_REPORT");
    var jsonMessage = {
        dataHeader : dataHeader,
        //payLoad: [this.downtime]
        payLoad: [this.findValidEnd]
    }
    
    this.penaltyReportService.generateValidEnd(jsonMessage);
        
}


reset(form) {
  this.findValidEnd = new FindValidEnd();
  form.reset({ fromDate: this.findValidEnd.fromDate });
  form.reset({ toDate: this.findValidEnd.toDate });
  
  }

}
