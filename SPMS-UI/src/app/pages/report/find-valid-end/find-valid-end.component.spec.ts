import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindValidEndComponent } from './find-valid-end.component';

describe('FindValidEndComponent', () => {
  let component: FindValidEndComponent;
  let fixture: ComponentFixture<FindValidEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindValidEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindValidEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
