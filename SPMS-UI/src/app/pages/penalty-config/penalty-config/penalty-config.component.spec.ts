import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenaltyConfigComponent } from './penalty-config.component';

describe('PenaltyConfigComponent', () => {
  let component: PenaltyConfigComponent;
  let fixture: ComponentFixture<PenaltyConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenaltyConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenaltyConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
