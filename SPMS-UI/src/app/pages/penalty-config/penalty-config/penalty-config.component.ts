import { Component, OnInit, ViewChild } from '@angular/core';
import { PenaltyConfig } from 'src/app/models/penaltyconfig';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { MessageService} from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { PenaltyConfigService } from 'src/app/services/penaltyconfig.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { Table } from 'primeng';


@Component({
  selector: 'app-penalty-config',
  templateUrl: './penalty-config.component.html',
  styleUrls: ['./penalty-config.component.scss']
})
export class PenaltyConfigComponent implements OnInit {
  @ViewChild(Table) dt: Table;
  penaltyConfigs: PenaltyConfig[];
  penaltyConfig:  PenaltyConfig = null;
  vendorNameSet = new Map();
  contractNameSet = new Map();
  cols: any[];
  


 
  
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private penaltyConfigService: PenaltyConfigService,
    private dataService: DataService,
      public router: Router,
      private messageService: MessageService,
      private utility:Utility,
      private dropdownInfoService: DropdownInfoService,
      private contractInfoService: ContractInfoService
  
  ) { }



  getPenaltyConfig(){
   
    var request = this.penaltyConfigService.showPenaltyConfig();
      request.subscribe(
          (res) => {
            const msg = JSON.parse(res) ;
            this.penaltyConfigs =[] ;
            for(let i = 0; i< msg.length; i++) {
              this.penaltyConfig = new PenaltyConfig;
             
              this.penaltyConfig.contractId= msg[i][0];
    
              this.penaltyConfig.vendorId= msg[i][1];
              this.penaltyConfig.condType= msg[i][2];
              this.penaltyConfig.minDowntime= msg[i][3];
              this.penaltyConfig.maxDowntime= msg[i][4];
              this.penaltyConfig.penaltyRate= msg[i][5];
              this.penaltyConfig.configId= msg[i][6];
              this.penaltyConfig.entryUser= msg[i][7];
              this.penaltyConfig.entryDate= msg[i][8];
              this.penaltyConfig.updateUser= msg[i][9];
              this.penaltyConfig.updateDt= msg[i][10];
              this.penaltyConfig.contno= msg[i][11];
              this.penaltyConfig.venName= msg[i][12];
              this.penaltyConfigs[i] = this.penaltyConfig;
            }
          },
          err => {

          }
      );
  // });

}

  


  // getPenaltyConfig(){
  //   this.penaltyConfigService.getPenaltyConfigList().subscribe(data => {
  //     this.penaltyConfigs=data;
  //   });

  // }


  createPenaltyConfig(): void {
    this.router.navigate(['/create']);  
  };

  
  ngOnInit() {

    this.getPenaltyConfig(); 
    // this.getVendorList();
    
    // this.getContractInfo(); 

    this.cols = [
  
      { field: 'contno', header: 'Contract' },
      { field: 'venName', header: 'Vendor' },
      { field: 'minDowntime', header: 'Min Downtime' },
      { field: 'maxDowntime', header: 'Max Downtime' },
      { field: 'penaltyRate', header: 'Penalty Rate' }
  ];
      
  }
  
  // editPenaltyConfig(penaltyConfig): void{
   
  //   console.log(penaltyConfig.contractId);
  //   //this.penaltyConfigService.passTheValue(penaltyConfig);
  //    if(penaltyConfig.maxDowntime==-1){
  //     penaltyConfig.maxDowntime=null;
  //    }
  //    if(penaltyConfig.penaltyRate==-1){
  //     penaltyConfig.penaltyRate=null;
  //    }
  //   this.pConf.penaltyConfig = penaltyConfig;
   
  //   this.pConf.getContractList(penaltyConfig.vendorId);
    
  // };
  editPenaltyConfig(penaltyConfig): void {
   
    //  this.penaltyConfigService.getContractList(penaltyConfig);
      this.penaltyConfigService.passTheValue(penaltyConfig);

  }

  getVendorList() {

    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          // console.log( this.vendorList);
          this.vendorNameSet.set(msg[i].venId, msg[i].venName);
        }
        //console.log(this.vendorNameSet);

      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }

  // getContractInfo() {
  //   const request = this.contractInfoService.getContractInfoList();
  //   request.subscribe(
  //     (msg) => {
  //       //console.log(msg);
  //       for (let i = 0; i < msg.length; i++) {
  //         // console.log( this.vendorList);
  //         this.contractNameSet.set(msg[i].contId, msg[i].contNo);
  //       }
  //       //console.log(this.contractNameSet);

  //     },
  //     err => {
  //       // this.severity = 'error';
  //       // this.detailMsg = "Server Error: " + err.message;
  //       // // this.showProgressSpin = false;
  //       // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
  //       return;
  //     }
  //   );
  // }
  getContractInfo() {
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array()
    }

    

    this.contractInfoService.getList(JSON.stringify(this.jsonMessage)).subscribe(
      (res) => {
        const msg = JSON.parse(atob(res));
        for (let i = 0; i < msg.payLoad.length; i++) {
                  // console.log( this.vendorList);
                  
                  this.contractNameSet.set(msg.payLoad[i].contId, msg.payLoad[i].contNo);
                }

      },
      err => {
        return;
      }
    );


}

}


