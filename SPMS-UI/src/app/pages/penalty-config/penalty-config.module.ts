import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PenaltyConfigRoutingModule } from './penalty-config-routing.module';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from 'src/app/layout/layout.module';
import { DropdownModule, TableModule } from 'primeng';
import { PenaltyConfigComponent } from './penalty-config/penalty-config.component';
import { PenaltyConfigInsertComponent } from './penalty-config-insert/penalty-config-insert.component';


@NgModule({
  declarations: [PenaltyConfigComponent, PenaltyConfigInsertComponent],
  imports: [
    CommonModule,
    PenaltyConfigRoutingModule,
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule
  ]
})
export class PenaltyConfigModule { }
