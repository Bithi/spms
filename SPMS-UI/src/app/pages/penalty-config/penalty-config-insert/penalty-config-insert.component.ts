import { Component, OnInit } from '@angular/core';
import { PenaltyConfigService } from 'src/app/services/penaltyconfig.service';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { Utility } from 'src/app/util/utility';
import { PenaltyConfig } from 'src/app/models/penaltyconfig';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { VendorInfo } from 'src/app/models/vendorinfo';
import { FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';
declare var $: any;
@Component({
  selector: 'app-penalty-config-insert',
  templateUrl: './penalty-config-insert.component.html',
  styleUrls: ['./penalty-config-insert.component.scss']
})
export class PenaltyConfigInsertComponent implements OnInit {
  subscription: Subscription;
  public user: User;
  contractNameSet = new Map();
  selectedNewMonth: string = '';

  vendors: {};
  vendorData: [];
  selectedvendor: SelectItem[];
  selectedcontract: SelectItem[];
  selectedcontractbranch: SelectItem[];
  contractinfos: {};
  contractbranches: {};
  selectedCont: String;
  countries: SelectItem[];
  selectedCountry: SelectItem;
  Poptions: boolean;
  options: boolean;
  condition = [
    { value: null, label: 'Select Condition' },
    { value: 'M', label: 'Monthly' },
    { value: 'D', label: 'Daily' }
  ];

  constructor(
    private penaltyConfigInfoService: PenaltyConfigService,
    private dataService: DataService,
    public router: Router,
    private messageService: MessageService,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService


  ) { }


  penaltyConfig: PenaltyConfig = new PenaltyConfig();

  vendor: VendorInfo = new VendorInfo();
  jsonMessage: any;


  key: string = "toast";
  severity: string;
  detailMsg: string;


  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    //console.log("hello " + this.user.userId);

    this.selectedvendor = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.getVendorList();
    this.options = false;
    this.Poptions = false;
    this.selectedcontract = [];
    this.selectedcontract.push({ label: 'Select Contract', value: -1 });
    // this.dropdownInfoService.getVendors().subscribe(
    //     data => this.vendors = data
    //   );
    this.penaltyConfigInfoService.addToTheList.subscribe(
      data => {
        this.penaltyConfig = data;
        this.getContractList(data.vendorId);
        if ((data.maxDowntime == -1) || (data.maxDowntime == null)) {
          this.options = true;
        }
        else {
          this.options = false;
        }
        if ((data.penaltyRate == -1) || (data.penaltyRate == null)) {
          this.Poptions = true;
        }
        else {
          this.Poptions = false;
        }
        if ((data.minDowntime == -1) || (data.minDowntime == null)) {
          this.penaltyConfig.minDowntime = null;
        }
        
      });
    // this.selectedcontract.push({ label: 'Select Contract', value: -1});
    // this.getContractList(this.penaltyConfig.vendorId);

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    //   this.penaltyConfigInfoService.addToTheList.subscribe(
    //   data => this.penaltyConfig = data
    // )

  }

  createPenaltyConfig(): void {

    if (this.selectedCont != null) {
      this.penaltyConfig.contractId = this.selectedCont.toString();
    }

    if (!this.penaltyConfig.vendorId) {
      this.severity = 'error';
      this.detailMsg = "Vendor id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.penaltyConfig.contractId) {
      this.severity = 'error';
      this.detailMsg = "Contract id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.penaltyConfig.minDowntime) {
      this.penaltyConfig.minDowntime = -1;
    }

    if (!this.penaltyConfig.maxDowntime) {
      this.penaltyConfig.maxDowntime = -1;
    }


    if (!this.penaltyConfig.penaltyRate) {
      this.penaltyConfig.penaltyRate = -1;
    }




    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.penaltyConfig.entryDate = new Date();
    this.penaltyConfig.updateDt = new Date('01/01/1970');
    this.penaltyConfig.entryUser = this.user.userId;
    this.penaltyConfig.updateUser = "";


    var dataHeader = this.dataService.createMessageHeader("SAVE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.penaltyConfig)
    }

    this.penaltyConfigInfoService.insertPenaltyConfigInfo(JSON.stringify(this.jsonMessage));
  //   this.router.navigateByUrl('/penaltyconfigList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['penaltyconfig']);
  // });
  this.reset(this.penaltyConfig);
  };

  updatePenaltyConfig(penaltyConfig): void {

    if (!this.penaltyConfig.vendorId) {
      this.severity = 'error';
      this.detailMsg = "Vendor id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }


    if (!this.penaltyConfig.contractId) {
      this.severity = 'error';
      this.detailMsg = "Contract id cannot be empty";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.penaltyConfig.minDowntime) {
      this.penaltyConfig.minDowntime = -1;
    }

    if (!this.penaltyConfig.maxDowntime||(this.options===true)) {
      this.penaltyConfig.maxDowntime = -1;
    }


    if (!this.penaltyConfig.penaltyRate||(this.Poptions===true)) {
      this.penaltyConfig.penaltyRate = -1;
    }




    // var today = new Date();
    // var date = (today.getMonth() + 1) + '/' + (today.getDate() + 1) + '/' + today.getFullYear();
    this.penaltyConfig.updateDt = new Date();
    this.penaltyConfig.updateUser = this.user.userId;

    //this.penaltyConfig.contractId = this.selectedCont.toString();
    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.penaltyConfig)
    };
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.penaltyConfigInfoService.updatePenaltyConfigInfo(JSON.stringify(this.jsonMessage));
    // this.router.navigateByUrl('/penaltyconfigList', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['penaltyconfig']);
    // });
    this.reset(this.penaltyConfig);
    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

    //this.router.navigate(['/create']);
  };

  deletePenaltyConfig(penaltyConfig): void {



    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(penaltyConfig)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.penaltyConfigInfoService.deletePenaltyConfigInfo(JSON.stringify(this.jsonMessage));
    this.reset(this.penaltyConfig);
  //   this.router.navigateByUrl('/penaltyconfigList', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['penaltyconfig']);
  // });
    //   request.subscribe(
    //     (data ) => {
    //       const msg = JSON.parse( atob(data) );
    //      // alert("Employee created successfully.");
    //      debugger;
    //      console.log(msg.payload);
    //     });

    //this.router.navigate(['/create']);
  };
  getVendorList() {

    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
          // console.log( this.vendorList);
        }





      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }


  numberOnly(event): boolean {

    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  getContractList(venId: string) {
    this.selectedcontract = [];
    if (venId) {
      const request = this.dropdownInfoService.getContracts(venId);
      request.subscribe(
        (res) => {

          const msg = JSON.parse(res);
          for (let i = 0; i < msg.length; i++) {

            this.selectedcontract.push({ label: msg[i].contNo, value: msg[i].contId });
            this.contractNameSet.set(msg[i].contId, msg[i].contNo);
          }



        }
      );
    }
    else {
      this.contractinfos = null;
      this.contractbranches = null;
    }
    // this.selectedCont = this.penaltyConfig.contractId.toString();
  }

  selectChangeHandler(event: any) {
    this.vendor = event.value;

    //this.getContactList(jsonMessage);
  }
  getBranchList(contractId: string) {
    this.selectedcontractbranch = [];
    this.selectedcontractbranch.push({ label: 'Select Branch', value: -1 });
    if (contractId) {
      // this.dropdownInfoService.getContractBraches(contractId).subscribe(
      //   data => this.contractbranches = data
      // );

      const request = this.dropdownInfoService.getContractBraches(contractId);
      request.subscribe(
        (res) => {

          const msg = JSON.parse(res);

          for (let i = 0; i < msg.length; i++) {
            this.selectedcontractbranch.push({ label: msg[i].brc, value: msg[i].brc });
            // console.log( this.vendorList);
          }

        },
        err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
        }
      );
    } else {
      this.contractbranches = null;
    }
  }

  // onChange(event: any) {
  //   this.selectedNewMonth = event.value;
  //   this.penaltyConfig.contractId = this.selectedNewMonth;
  //   console.log(this.selectedNewMonth);
  // }
  reset(form) {

    this.penaltyConfig = new PenaltyConfig();
    this.Poptions = false;
    this.options = false;
    this.selectedvendor = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });
    this.getVendorList();
    this.selectedcontract = [];


  }
  noLimit(){
    // this.penaltyConfig.maxDowntime=null;
  }
  withLimit(max){
    // debugger;
  }
}




