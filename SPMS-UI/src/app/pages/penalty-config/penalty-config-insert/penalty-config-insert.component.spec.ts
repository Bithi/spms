import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenaltyConfigInsertComponent } from './penalty-config-insert.component';

describe('PenaltyConfigInsertComponent', () => {
  let component: PenaltyConfigInsertComponent;
  let fixture: ComponentFixture<PenaltyConfigInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenaltyConfigInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenaltyConfigInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
