import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PenaltyConfigInsertComponent } from './penalty-config-insert/penalty-config-insert.component';


const routes: Routes = [
  {path: '', component: PenaltyConfigInsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PenaltyConfigRoutingModule { }
