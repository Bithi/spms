import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DynamicDialogRef, MessageService, SelectItem } from 'primeng';
import { DataService } from 'src/app/data/data.service';
import { Chart } from 'src/app/models/Chart';
import { ContractInfo } from 'src/app/models/contractinfo';
import { PenaltyReport } from 'src/app/models/PenaltyReport';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { Utility } from 'src/app/util/utility';
import * as Chartt from 'chart.js';
// import ChartDataLabels from 'chartjs-plugin-datalabels';
import ChartDataLabels from 'chartjs-plugin-labels';
import { DatePipe } from '@angular/common';
import { PenaltyReportService } from 'src/app/services/penaltyReport.service';
import { User } from 'src/app/models/user';
@Component({
  selector: 'app-dashborad',
  templateUrl: './dashborad.component.html',
  styleUrls: ['./dashborad.component.scss'],
  providers: [DatePipe]
})
export class DashboradComponent implements OnInit, OnDestroy {
  blankAmc: boolean = true;
  blankSla: boolean = true;
  connectivityDt: any = new Object();
  amcData: any = new Map();
  amcDataPrev: any = new Map();
  slaData: any = new Map();
  slaDataPrev: any = new Map();
  connData: any = new Map();
  chartOptionsForConn: any = new Object();
  chartOptionsForAMC: any = new Object();
  chartOptionsForAMCPrev: any = new Object();
  chartOptionsForSLA: any = new Object();
  chartOptionsForSLAPrev: any = new Object();
  countslaData: any = new Object();
  countslaDataPrev: any = new Object();
  countamcData: any = new Object();
  countamcDataPrevData: any = new Object();
  fdate: any;
  tdate: any;
  duration: number = 90;
  myCar: any = new Object();
  display: boolean = false;
  display2: boolean = false;
  display3: boolean = false;
  displayAMCPrev: boolean = false;
  displaySLAPrev: boolean = false;
  slaChartReport: Chart = new Chart();
  slaChartReportPrev: Chart = new Chart();
  connChartReport: Chart = new Chart();
  slaChartForClick: any = [];
  slaChartForClickPrev: any = [];
  connChartForClick: any = [];
  amcChartReport: Chart = new Chart();
  amcChartForClick: any = [];
  amcChartForClickPrev: any = [];
  output: any = [];
  outputPrev: any = [];
  output2: any = [];
  output2Prev: any = [];
  output3: any = [];
  cols: any[];
  colsAmc: any[];
  colsConn: any[];
  caption: string;
  cont:ContractInfo=new ContractInfo();
  contractSet = new Map();
  dateField=new Date();
  public user: User;
  public penaltyReport: PenaltyReport = new PenaltyReport();
  durationOption = [
    { label: '3 months', value: '90' },
    { label: '6 months', value: '180' },
    { label: '9 months', value: '270' },
    { label: '12 months', value: '360' }
  ];
  chartJs = Chartt;
   chartLabelPlugin = ChartDataLabels;
   plugin = ChartDataLabels; 
  constructor(
    private dataService: DataService,
    private dashboardService: DashboardService,
    private utility: Utility,
    private dropdownInfoService: DropdownInfoService,
    private contractInfoService: ContractInfoService,
    public router: Router, public messageService: MessageService,
    private datePipe: DatePipe,
    private penaltyReportService: PenaltyReportService,
  ) {



  }
  ref: DynamicDialogRef;
  ngOnInit() {
    var today = new Date();
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.chartJs.plugins.unregister(this.chartLabelPlugin);
    this.getCountSLAChartValidity();
    this.getCountAMCChartValidity();
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    this.penaltyReport.user=  this.user.userId;
    if(!this.fdate){
      this.fdate = this.datePipe.transform(this.dateField, 'yyyy-MM-dd');
    }
    if(!this.tdate){
      this.tdate = this.fdate;
    }
   this.blankAmc=  true;
   this.blankSla=  true;
    this.getCountAMCChartValidityPrevData(this.fdate,this.tdate);
    this.getCountSLAChartValidityPrevData(this.fdate,this.tdate)
    this.getContractListInfo();
    this.cols = [
      { field: 'contractNo', header: 'Contract' },
      { field: 'endValidity', header: 'End' },
      { field: 'startValidity', header: 'Start' }

    ];
    this.colsAmc = [
      { field: 'contractNo', header: 'Contract' },
      { field: 'endValidity', header: 'End' },
      { field: 'startValidity', header: 'Start' }

    ];

    this.colsConn = [
      { field: 'contractNo', header: 'Contract' },
      { field: 'endValidity', header: 'End' },
      { field: 'startValidity', header: 'Start' }

    ];

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }
  showAmcPrevData(fdate,tdate){
   
    this.getCountAMCChartValidityPrevData(this.fdate,this.fdate);
    this.getCountSLAChartValidityPrevData(this.fdate,this.fdate);
    this.getCountAMCChartValidity();
    this.getCountSLAChartValidity();
    
  }

 
  
  getCountSLAChartValidity() {
    this.countslaData = new Object();
    const request = this.dashboardService.getCountSLAChartValidity();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        for (let i = 0; i < msg.length; i++) {
          if(msg[i].validity==='a: 0 to 3 months') {
            msg[i].color='#802b00';
          }
          else if(msg[i].validity==='b: 3 to 6 months') {
            msg[i].color='#ff9966';
          }
          else if(msg[i].validity==='c: 6 to 9 months') {
            msg[i].color='#4d4dff';
          }
          else if(msg[i].validity==='d: 9 to 12 months') {
            msg[i].color='#eb34db';
          }
          else if(msg[i].validity==='f: already expired') {
            msg[i].color='red';
          }
          else if(msg[i].validity==='e: 12 months to above'){
            msg[i].color='green';
          }
        }
        var col2 = [] ;
        this.output= [];
        // console.log(msg);
        var flags = [], l = msg.length, i, j, countslaDataOutputLabel = new Map(), countslaDataOutput = new Map();
        for (i = 0; i < l; i++) {

          if (flags[msg[i].validity]) continue;
          flags[msg[i].validity] = true;
          this.output.push(msg[i].validity);


        }
        // 3 month / 6 month
       
        for (j = 0; j < this.output.length; j++) {
          var label = []; var dataForlabel = [], labelString = "", slaChartReportList = []; this.slaChartReport = new Chart();
          for (i = 0; i < l; i++) {
            if (this.output[j] === msg[i].validity) {
              labelString = labelString + (msg[i].contractNo + "(" + msg[i].endValidity + "),\n");
              if (dataForlabel.length == 0) {
                dataForlabel.push(msg[i].count);
                col2.push(msg[i].color);
              }
              this.slaChartReport = msg[i];
              slaChartReportList.push(this.slaChartReport);
            }

          }
          label.push(labelString);

          //console.log("Data=========")
          //console.log(dataForlabel);

          countslaDataOutputLabel.set(j, label);
          countslaDataOutput.set(j, dataForlabel);

          this.slaData.set(this.output[j], slaChartReportList);
        }
        j
        // console.log(countslaDataOutput);
        let datasForLabel = Array.from(countslaDataOutputLabel.values());
        let datas = Array.from(countslaDataOutput.values());
        

        // this.countslaData.labels=output.map(function(a) {return ( a);});
        var k;

        this.countslaData = {
          labels: this.output.map(function (a) { return (a.substring(0,1).toUpperCase()+a.substring(1,a.length)); }),
          datasets: [
            {
              data: datas.map(function (a) { return a; }),
              backgroundColor: col2.map(function (a) { return (a); }),
              // hoverBackgroundColor: col2.map(function (a) { return (a); })
            }]
        };
        // this.chartOptionsForAMC = {
        //   legend: { display: false },
        //   scales: { xAxes: [{ display: false }] }
        // }

        this.chartOptionsForSLA = {
          
        title: {
          display: true,
          text: 'SLA Current Chart',
          fontSize: 16
      },
      legend: {
        position: 'right'
      },
      plugins: {
        labels: {
          render: function (args) {
            return args.label.substring(0,1).toUpperCase()+':'+args.value;
          },
          position: 'outside',
          fontColor: 'black',
          FontFamily: 'Arial',
          FontWeight: 'bold',
          fontSize: 8,
          usePointStyle: true,
        }
       
        }
      }


      },
      err => {
        return;
      }
    );
  }

  getCountSLAChartValidityPrevData(fdate:String,tdate:String) {
      this.countslaDataPrev = new Object();
      const request = this.dashboardService.getCountSLAChartValidityPrevData(fdate,tdate);
      request.subscribe(
        (res) => {
  
          const msg = JSON.parse(res);
          if(msg.length==0){
            this.blankSla = true;
           }
           else{
            this.blankSla = false;
          for (let i = 0; i < msg.length; i++) {
            if(msg[i].validity==='a: 0 to 3 months') {
              msg[i].color='#802b00';
            }
            else if(msg[i].validity==='b: 3 to 6 months') {
              msg[i].color='#ff9966';
            }
            else if(msg[i].validity==='c: 6 to 9 months') {
              msg[i].color='#4d4dff';
            }
            else if(msg[i].validity==='d: 9 to 12 months') {
              msg[i].color='#eb34db';
            }
            else if(msg[i].validity==='f: already expired') {
              msg[i].color='red';
            }
            else if(msg[i].validity==='e: 12 months to above'){
              msg[i].color='green';
            }
              
          }
          this.outputPrev= [];
          // console.log(msg);
          var flags = [], l = msg.length, i, j, countslaDataOutputLabelPrev = new Map(), countslaDataOutputPrev = new Map();
          for (i = 0; i < l; i++) {
  
            if (flags[msg[i].validity]) continue;
            flags[msg[i].validity] = true;
            this.outputPrev.push(msg[i].validity);
  
  
          }
          var col = [] ;
          // 3 month / 6 month
          for (j = 0; j < this.outputPrev.length; j++) {
            var label = []; var dataForlabel = [], labelString = "", slaChartReportList = []; this.slaChartReport = new Chart();
            for (i = 0; i < l; i++) {
              if (this.outputPrev[j] === msg[i].validity) {
                labelString = labelString + (msg[i].contractNo + "(" + msg[i].endValidity + "),\n");
                if (dataForlabel.length == 0) {
                  dataForlabel.push(msg[i].count);
                  col.push(msg[i].color);
                }
                this.slaChartReport = msg[i];
                slaChartReportList.push(this.slaChartReport);
              }
  
            }
            label.push(labelString);
  
            //console.log("Data=========")
            //console.log(dataForlabel);
  
            countslaDataOutputLabelPrev.set(j, label);
            countslaDataOutputPrev.set(j, dataForlabel);
            this.slaDataPrev.set(this.outputPrev[j], slaChartReportList);
          }
          j
          // console.log(countslaDataOutput);
          let datasForLabel = Array.from(countslaDataOutputLabelPrev.values());
          let datas = Array.from(countslaDataOutputPrev.values());
          // this.countslaData.labels=output.map(function(a) {return ( a);});
          var k;
          this.countslaDataPrev = {
            labels: this.outputPrev.map(function (a) { return (a.substring(0,1).toUpperCase()+a.substring(1,a.length)); }),
            datasets: [
              {
                label: this.outputPrev.map(function (a) { return (a); }),
                data: datas.map(function (a) { return a; }),
                backgroundColor: col.map(function (a) { 
              
                   return (a);
                },
              
              )
             
                // hoverBackgroundColor: col.map(function (a) { 
                //   return (a);
                //  })
              }]
          };
         
          this.chartOptionsForSLAPrev = {
            title: {
              display: true,
              text: 'SLA Previous Chart',
              fontSize: 8
          },
            legend: {
              position: 'right'
            },
          
            plugins: {
              labels: {
                render: function (args) {
                  return args.label.substring(0,1).toUpperCase()+':'+args.value;
                },
                position: 'outside',
                fontColor: 'black',
                FontFamily: 'Arial',
                FontWeight: 'bold',
                fontSize: 8,
              }
             
              }
            }
          }
  
        },
        err => {
          return;
        }
      );
    
  }

  getCountAMCChartValidity() {
    this.countamcData = new Object();
    const request = this.dashboardService.getCountAMCChartValidity();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        for (let i = 0; i < msg.length; i++) {
          if(msg[i].validity==='a: 0 to 3 months') {
            msg[i].color='#802b00';
          }
          else if(msg[i].validity==='b: 3 to 6 months') {
            msg[i].color='#ff9966';
          }
          else if(msg[i].validity==='c: 6 to 9 months') {
            msg[i].color='#4d4dff';
          }
          else if(msg[i].validity==='d: 9 to 12 months') {
            msg[i].color='#eb34db';
          }
          else if(msg[i].validity==='f: already expired') {
            msg[i].color='red';
          }
          else if(msg[i].validity==='e: 12 months to above'){
            msg[i].color='green';
          }
            
        }
        // console.log(msg);
        this.output2= [];
        var flags = [], l = msg.length, i, j, countamcDataOutputLabel = new Map(), countamcDataOutput = new Map();
        for (i = 0; i < l; i++) {
          if (flags[msg[i].validity]) continue;
          flags[msg[i].validity] = true;
          this.output2.push(msg[i].validity);


        }
        var col = [] ;
        // 3 month / 6 month
        for (j = 0; j < this.output2.length; j++) {
          var label = []; var dataForlabel = [], labelString = "", amcChartReportList = []; this.amcChartReport = new Chart();
          for (i = 0; i < l; i++) {
            if (this.output2[j] === msg[i].validity) {
              labelString = labelString + (msg[i].contractNo + "(" + msg[i].endValidity + "),\n");
              if (dataForlabel.length == 0) {
                dataForlabel.push(msg[i].count);
                col.push(msg[i].color);
              }
              this.amcChartReport = msg[i];
              amcChartReportList.push(this.amcChartReport);
            }

          }
          label.push(labelString);

          //console.log("Data=========")
          //console.log(dataForlabel);

          countamcDataOutputLabel.set(j, label);
          countamcDataOutput.set(j, dataForlabel);
          this.amcData.set(this.output2[j], amcChartReportList);
        }
        j
        // console.log(countslaDataOutput);
        let datasForLabel = Array.from(countamcDataOutputLabel.values());
        let datas = Array.from(countamcDataOutput.values());
        // this.countslaData.labels=output.map(function(a) {return ( a);});
        var k;
        this.countamcData = {
          labels: this.output2.map(function (a) { return (a.substring(0,1).toUpperCase()+a.substring(1,a.length)); }),
          datasets: [
            {
              label: this.output2.map(function (a) { return (a); }),
              data: datas.map(function (a) { return a; }),
              backgroundColor: col.map(function (a) { 
            
                 return (a);
              },
            
            )
           
              // hoverBackgroundColor: col.map(function (a) { 
              //   return (a);
              //  })
            }]
        };
       
        this.chartOptionsForAMC = {
          title: {
            display: true,
            text: 'AMC Current Chart',
            fontSize: 16,
        },
        legend: {
          position: 'right'
        },
        
          plugins: {
            labels: {
              render: function (args) {
                return args.label.substring(0,1).toUpperCase()+':'+args.value;
              },
              position: 'outside',
              fontColor: 'black',
              FontFamily: 'Arial',
              FontWeight: 'bold',
              fontSize: 8,
            }
           
            }
          }
        





      },
      err => {
        return;
      }
    );
  }
  
  getCountAMCChartValidityPrevData(fdate:String,tdate:String) {
    this.countamcDataPrevData = new Object();
    const request = this.dashboardService.getCountAMCChartValidityPrevData(fdate,tdate);
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        if(msg.length==0){
         this.blankAmc = true;
        }
        else{
          this.blankAmc = false;
          for (let i = 0; i < msg.length; i++) {
            if(msg[i].validity==='a: 0 to 3 months') {
              msg[i].color='#802b00';
            }
            else if(msg[i].validity==='b: 3 to 6 months') {
              msg[i].color='#ff9966';
            }
            else if(msg[i].validity==='c: 6 to 9 months') {
              msg[i].color='#4d4dff';
            }
            else if(msg[i].validity==='d: 9 to 12 months') {
              msg[i].color='#eb34db';
            }
            else if(msg[i].validity==='f: already expired') {
              msg[i].color='red';
            }
            else if(msg[i].validity==='e: 12 months to above'){
              msg[i].color='green';
            }
              
          }
          this.output2Prev= [];
          // console.log(msg);
          var flags = [], l = msg.length, i, j, countamcDataOutputLabelPrev = new Map(), countamcDataOutputPrev = new Map();
          for (i = 0; i < l; i++) {
  
            if (flags[msg[i].validity]) continue;
            flags[msg[i].validity] = true;
            this.output2Prev.push(msg[i].validity);
  
  
          }
          var col = [] ;
          // 3 month / 6 month
          for (j = 0; j < this.output2Prev.length; j++) {
            var label = []; var dataForlabel = [], labelString = "", amcChartReportList = []; this.amcChartReport = new Chart();
            for (i = 0; i < l; i++) {
              if (this.output2Prev[j] === msg[i].validity) {
                labelString = labelString + (msg[i].contractNo + "(" + msg[i].endValidity + "),\n");
                if (dataForlabel.length == 0) {
                  dataForlabel.push(msg[i].count);
                  col.push(msg[i].color);
                }
                this.amcChartReport = msg[i];
                amcChartReportList.push(this.amcChartReport);
              }
  
            }
            label.push(labelString);
  
            //console.log("Data=========")
            //console.log(dataForlabel);
  
            countamcDataOutputLabelPrev.set(j, label);
            countamcDataOutputPrev.set(j, dataForlabel);
            this.amcDataPrev.set(this.output2Prev[j], amcChartReportList);
          }
          j
          // console.log(countslaDataOutput);
          let datasForLabel = Array.from(countamcDataOutputLabelPrev.values());
          let datas = Array.from(countamcDataOutputPrev.values());
          // this.countslaData.labels=output.map(function(a) {return ( a);});
          var k;
          this.countamcDataPrevData = {
            labels: this.output2Prev.map(function (a) { return (a.substring(0,1).toUpperCase()+a.substring(1,a.length)); }),
            datasets: [
              {
                label: this.output2Prev.map(function (a) {  return (a); }),
                data: datas.map(function (a) {return a; }),
                backgroundColor: col.map(function (a) { 
              
                   return (a);
                },
              
              )
             
                // hoverBackgroundColor: col.map(function (a) { 
                //   return (a);
                //  })
              }]
          };
         
          this.chartOptionsForAMCPrev = {
            title: {
              display: true,
              text: 'AMC Previous Chart',
              fontSize: 16
          },
            legend: {
              position: 'right'
            },
          
            plugins: {
              
              labels: {
                render: function (args) {
                  return args.label.substring(0,1).toUpperCase()+':'+args.value;
                },
                position: 'outside',
                fontColor: 'black',
                FontFamily: 'Arial',
                FontWeight: 'bold',
                fontSize: 8,
              }
             
              }
            }
          }



      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }



  getCountCONNChartValidity() {
    this.connectivityDt = new Object();
    const request = this.dashboardService.getConnectivityChartValidity();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        for (let i = 0; i < msg.length; i++) {
          if(msg[i].validity==='0 to 3 months') {
            msg[i].color='#802b00';
          }
          else if(msg[i].validity==='3 to 6 months') {
            msg[i].color='#ff9966';
          }
          else if(msg[i].validity==='6 to 9 months') {
            msg[i].color='#4d4dff';
          }
          else if(msg[i].validity==='9 to 12 months') {
            msg[i].color='#eb34db';
          }
          else if(msg[i].validity==='already expired') {
            msg[i].color='red';
          }
          else if(msg[i].validity==='12 months to above'){
            msg[i].color='green';
          }
        }
        var col2 = [] ;
        this.output3= [];
        // console.log(msg);
        var flags = [], l = msg.length, i, j, connectivityDtOutputLabel = new Map(), connectivityDtOutput = new Map();
        for (i = 0; i < l; i++) {

          if (flags[msg[i].validity]) continue;
          flags[msg[i].validity] = true;
          this.output3.push(msg[i].validity);


        }
        // 3 month / 6 month
       
        for (j = 0; j < this.output3.length; j++) {
          var label = []; var dataForlabel = [], labelString = "", connChartReportList = []; this.connChartReport = new Chart();
          for (i = 0; i < l; i++) {
            if (this.output3[j] === msg[i].validity) {
              labelString = labelString + (msg[i].contractNo + "(" + msg[i].endValidity + "),\n");
              if (dataForlabel.length == 0) {
                dataForlabel.push(msg[i].count);
                col2.push(msg[i].color);
              }
              this.connChartReport = msg[i];
              connChartReportList.push(this.connChartReport);
            }

          }
          label.push(labelString);

          //console.log("Data=========")
          //console.log(dataForlabel);

          connectivityDtOutputLabel.set(j, label);
          connectivityDtOutput.set(j, dataForlabel);

          this.connData.set(this.output3[j], connChartReportList);
        }
        j
        // console.log(countslaDataOutput);
        let datasForLabel = Array.from(connectivityDtOutputLabel.values());
        let datas = Array.from(connectivityDtOutput.values());
        

        // this.countslaData.labels=output.map(function(a) {return ( a);});
        var k;

        this.connectivityDt = {
          labels: this.output3.map(function (a) { return (a.substring(0,1).toUpperCase()+a.substring(1,a.length)); }),
          datasets: [
            {
              data: datas.map(function (a) { return a; }),
              backgroundColor: col2.map(function (a) { return (a); }),
              // hoverBackgroundColor: col2.map(function (a) { return (a); })
            }]
        };
        // this.chartOptionsForAMC = {
        //   legend: { display: false },
        //   scales: { xAxes: [{ display: false }] }
        // }

        this.chartOptionsForConn = {
          legend: {
            position: 'right'
        },
        title: {
          display: true,
          text: 'Connectivity Chart',
          fontSize: 16
      },
      
      plugins: {
        labels: {
          render: function (args) {
            return args.label.substring(0,1).toUpperCase()+':'+args.value;
          },
          position: 'outside',
          fontColor: 'black',
          FontFamily: 'Arial',
          FontWeight: 'bold',
          fontSize: 8,
        }
       
        }
      }

        // console.log(this.countslaData);



      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );
  }

  selectData(e: any) {
    this.caption = '';
    this.display2 = false;
    this.display = true;
    this.display3 =false;
    this.displayAMCPrev=false;
    this.displaySLAPrev=false;
    this.slaChartForClick = this.slaData.get(this.output[e.element._index]);
    if (this.slaChartForClick.length > 0) {
      this.caption = this.slaChartForClick[0].validity.toString();
    }
    this.penaltyReport.clause = this.caption;
   


  }

  selectDataPrev(e: any) {
    this.caption = '';
    this.display2 = false;
    this.display = false;
    this.display3 =false;
    this.displayAMCPrev=false;
    this.displaySLAPrev=true;
    this.slaChartForClickPrev = this.slaDataPrev.get(this.outputPrev[e.element._index]);
    if (this.slaChartForClickPrev.length > 0) {
      this.caption = this.slaChartForClickPrev[0].validity.toString();
    }
    this.penaltyReport.clause = this.caption;
   


  }

 

  selectDataAmc(e: any) {
    this.caption = '';
    this.display = false;
    this.display2 = true;
    this.display3 =false;
    this.displayAMCPrev=false;
    this.displaySLAPrev=false;
    this.amcChartForClick = this.amcData.get(this.output2[e.element._index]);
    this.penaltyReport.clause = this.output2[e.element._index];
    




    if (this.amcChartForClick.length > 0) {

      this.caption = this.amcChartForClick[0].validity.toString();
    }
  }

  selectDataAmcPrev(e: any) {
    this.caption = '';
    this.display = false;
    this.display2 = false;
    this.display3 =false;
    this.displayAMCPrev = true;
    this.displaySLAPrev=false;
    this.amcChartForClickPrev = this.amcDataPrev.get(this.output2Prev[e.element._index]);
    this.penaltyReport.clause = this.output2Prev[e.element._index];
    

    if (this.amcChartForClickPrev.length > 0) {

      this.caption = this.amcChartForClickPrev[0].validity.toString();
    }
  }

  getRandomColor() {
    return '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
  }
  changeDateFormat(date: Date) {

    const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    return date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear();
  }
  

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);
        
        for (let i = 0; i < msg.length; i++) {
          this.cont=new ContractInfo();
          this.cont = msg[i];
          // debugger;
          this.contractSet.set(msg[i].contNo, this.cont);
          
        }



      },
      err => {
      
        return;
      }
    );


  }
  conDetail(cont){
    this.cont = this.contractSet.get(cont);
    // this.contractInfoService.passTheValue(this.cont);
    this.router.navigate([`${'/contractinfoAmc'.split('?')[0]}`], { queryParams: {contractInfo:  JSON.stringify(this.cont)}, skipLocationChange: true})
    ;
   
  }
  AMCreportPreview(type:string){
    debugger;
    this.penaltyReport.reportType=type;
    var dataHeader = this.dataService.createMessageHeader("AMCEX_REPORT");
    var jsonMessage = {
      dataHeader: dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.penaltyReport]
    }

    this.dashboardService.generateAMCExReport(jsonMessage);

  }
  AMCreportPrevPreview(type:string){
    this.penaltyReport.reportType=type;
    var dataHeader = this.dataService.createMessageHeader("AMCEX_REPORT");
     this.penaltyReport.startDate=this.fdate;
     this.penaltyReport.endDate=this.fdate;
    var jsonMessage = {
      dataHeader: dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.penaltyReport]
    }
   
    this.dashboardService.generateAMCExPrevReport(jsonMessage);

  }

  SLAreportPrevPreview(){
    var dataHeader = this.dataService.createMessageHeader("SLAEX_REPORT");
     this.penaltyReport.startDate=this.fdate;
     this.penaltyReport.endDate=this.fdate;
    var jsonMessage = {
      dataHeader: dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.penaltyReport]
    }
    this.dashboardService.generateSLAExPrevReport(jsonMessage);

  }
  

  

  SLAreportPreview(){
    var dataHeader = this.dataService.createMessageHeader("SLAEX_REPORT");
    debugger;
    var jsonMessage = {
      dataHeader: dataHeader,
      //payLoad: [this.downtime]
      payLoad: [this.penaltyReport]
    }

    this.dashboardService.generateSLAExReport(jsonMessage);
  }

  showPrevReport(fdate:any){
    var dataHeader = this.dataService.createMessageHeader("AMC_REPORT");
    this.penaltyReport.reportType='AMC_PREV_REPORT_ON_DATE';
    this.penaltyReport.endDate=fdate;
    var jsonMessage = {
    dataHeader : dataHeader,
    //payLoad: [this.downtime]
    payLoad: [this.penaltyReport]
}
this.penaltyReportService.generateAMCReport(jsonMessage);
  }
showReport(){
  var dataHeader = this.dataService.createMessageHeader("AMC_REPORT");
  this.penaltyReport.reportType='AMC_PREV_REPORT_ON_DATE';
  this.penaltyReport.endDate=null;
  var jsonMessage = {
  dataHeader : dataHeader,
  //payLoad: [this.downtime]
  payLoad: [this.penaltyReport]
}

this.penaltyReportService.generateAMCReport(jsonMessage);
  }

 
}
