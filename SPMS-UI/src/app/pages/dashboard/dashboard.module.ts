import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboradComponent } from './dashborad/dashborad.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { ChartModule, Dialog, DialogModule, DropdownModule, Table, TableModule } from 'primeng';
import { FormsModule } from '@angular/forms';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import { MatCardModule, MatGridListModule } from '@angular/material';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/util/auth.guard';


@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,FormsModule,
    LayoutModule,ChartModule,DropdownModule,DynamicDialogModule,TableModule,DialogModule,MatGridListModule,MatCardModule
  ],
  declarations: [DashboradComponent],
  exports: [
    DashboradComponent
],
providers:[ContractInfoService]
})
export class DashboardModule { }
