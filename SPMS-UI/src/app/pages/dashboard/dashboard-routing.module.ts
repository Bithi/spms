import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboradComponent} from './dashborad/dashborad.component';
import { UserInfoInsertComponent } from '../user-info/user-info-insert/user-info-insert.component';
import { AuthGuard } from 'src/app/util/auth.guard';

const routes: Routes = [
    {path: '', component: DashboradComponent,canActivate: [ AuthGuard ], data: { path: 'dashboard' } },
    // { path: 'user', component: UserInfoInsertComponent },
    ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
