import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BudgetBillEntryRoutingModule } from './budget-bill-entry-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule, MultiSelectModule, TableModule } from 'primeng';
import { BudgetbillentryinfoComponent } from './budgetbillentryinfo/budgetbillentryinfo.component';
import { BudgetbillentryinsertComponent } from './budgetbillentryinsert/budgetbillentryinsert.component';


@NgModule({
  declarations: [BudgetbillentryinfoComponent, BudgetbillentryinsertComponent],
  imports: [
    CommonModule,
    BudgetBillEntryRoutingModule,CommonModule,
    LayoutModule,
    FormsModule,TableModule,DropdownModule,MultiSelectModule
  ]
})
export class BudgetBillEntryModule { }
