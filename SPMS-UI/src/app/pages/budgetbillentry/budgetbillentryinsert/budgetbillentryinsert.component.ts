import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { MessageService, SelectItem } from 'primeng/api';
import { Budgetbillentry } from 'src/app/models/Budgetbillentry';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { BudgetbillentryService } from 'src/app/services/budgetbillentry.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { ItemPerBill } from 'src/app/models/ItemPerBill';
import { ContractInfoService } from 'src/app/services/contractinfo.service';
import { ContractInfo } from 'src/app/models/contractinfo';
//import { BudgetbillentryinfoComponent } from '../budgetbillentryinfo/budgetbillentryinfo.component';

@Component({
  selector: 'app-budgetbillentryinsert',
  templateUrl: './budgetbillentryinsert.component.html',
  styleUrls: ['./budgetbillentryinsert.component.scss']
})
export class BudgetbillentryinsertComponent implements OnInit {

  // @ViewChild('invoice_no',{static:false}) invoice_no: ElementRef;
  // @ViewChild('invoice_dt') invoice_dt: ElementRef;
  // @ViewChild('bill_amount') bill_amount: ElementRef;
  // @ViewChild('cont_id') con_id: ElementRef;
  // @ViewChild('delivary_date') delivary_date: ElementRef;

  public user: User;
  vendorNameSet = new Map();
  cont_id: string;
  selectedvendor: SelectItem[];
  selectedBillEnt: SelectItem[];
  selectedContract: SelectItem[];
  options: boolean = false;
  budgetBillEntry: Budgetbillentry = new Budgetbillentry();
  jsonMessage: any;
  key: string = "toast";
  severity: string;
  detailMsg: string;
  wording: string;
  contractInfos: ContractInfo[];
  contractInfo: ContractInfo = new ContractInfo();
  selectedVatList: SelectItem[];
  selectedAitList: SelectItem[];

  constructor(
    private dataService: DataService
    ,private budgetbillentryService: BudgetbillentryService
    ,private utility: Utility
    ,public router: Router
    ,private dropdownInfoService: DropdownInfoService
    , private contractInfoService: ContractInfoService
  ) { }

  ngOnInit() {

   
    this.user = JSON.parse(localStorage.getItem('loggedinUser'));
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    
    this.selectedvendor = [];
    this.selectedvendor.push({ label: 'Select Vendor', value: -1 });

    this.selectedContract = [];
    this.selectedContract.push({ label: 'Select Contract', value: -1 });

    this.selectedVatList = [];
    this.selectedVatList.push({ label: 'Select Vat Product', value: -1 });

    this.selectedAitList = [];
    this.selectedAitList.push({ label: 'Select Ait Product', value: -1 });

    this.getContractInfo();
    this.getVendorList();
    this.getContractListInfo();
    this.getVatProductList();
    this.getAitProductList();


    this.budgetbillentryService.addToTheList.subscribe(
      data => {
        this.budgetBillEntry = data;
        this.options = false;
        //this.amcslaForm = this.contractInfo.type;

      }
    );

  }

  getVendorList() {

    const request = this.dropdownInfoService.getVendors();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        //console.log("CData\n" + JSON.stringify(msg));

        for (let i = 0; i < msg.length; i++) {

          this.selectedvendor.push({ label: msg[i].venName, value: msg[i].venId });
          this.vendorNameSet.set(msg[i].venId, msg[i].venName);
          // //console.log( this.vendorList);
        }




      },
      err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
      }
    );


  }

  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);


        for (let i = 0; i < msg.length; i++) {
          this.selectedContract.push({ label: msg[i].contNo, value: msg[i].contId });
          //this.contractTypeSet.set(msg[i].contId, msg[i].type);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }

  getVatProductList() {

    const request = this.dropdownInfoService.getVatProductList();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        for (let i = 0; i < msg.length; i++) {
          this.selectedVatList.push({ label: msg[i].productType, value: msg[i].vatAitProductId });
          // this.contractNameSet.set(msg[i].contId, msg[i].contNo);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }

  getAitProductList() {

    const request = this.dropdownInfoService.getAitProductList();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);


        for (let i = 0; i < msg.length; i++) {
          this.selectedAitList.push({ label: msg[i].productType, value: msg[i].vatAitProductId });
          
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = "Server Error: " + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }


  getContractInfo() {

    console.log("Inside getContractInfo\n");
    const request = this.contractInfoService.contractInfoJoin();
    request.subscribe(
      (res) => {
        //////////////console.log(res);
        const msg = JSON.parse(res);
        this.contractInfos = [];
        for (let i = 0; i < msg.length; i++) {
          this.contractInfo = new ContractInfo;
          this.contractInfo.contId = msg[i][0];
          this.contractInfo.contNo = msg[i][1];
          this.contractInfo.contName = msg[i][2];
          this.contractInfo.serviceTypeId = msg[i][3];
          this.contractInfo.contParty = msg[i][4];
          this.contractInfo.contValidStart = msg[i][5];
          this.contractInfo.contValidEnd = msg[i][6];
          this.contractInfo.tenderDt = msg[i][7];
          this.contractInfo.tenderNo = msg[i][8];
          this.contractInfo.totalMonth = msg[i][9];
          this.contractInfo.monthlyCharge = msg[i][10];
          this.contractInfo.totalCharge = msg[i][11];
          this.contractInfo.totalBr = msg[i][12];
          this.contractInfo.totalAtm = msg[i][13];
          this.contractInfo.totalDcDrConnectivity = msg[i][14];
          this.contractInfo.venId = msg[i][15];

          if (msg[i][16] == 'ACT') {
            this.contractInfo.status = 'Active';
          }
          else if (msg[i][16] == 'CAN') {
            this.contractInfo.status = 'Inactive';
          }
          if (msg[i][16] == 'CLS') {
            this.contractInfo.status = 'Closed';
          }
          this.contractInfo.moduleId = msg[i][17];
          this.contractInfo.entryUser = msg[i][18];
          this.contractInfo.entryDate = msg[i][19];
          this.contractInfo.updateUser = msg[i][20];
          this.contractInfo.updateDt = msg[i][21];
          this.contractInfo.renewDate = msg[i][22];
          this.contractInfo.vat = msg[i][23];
          this.contractInfo.tax = msg[i][24];
          this.contractInfo.type = msg[i][25];
          this.contractInfo.warrantyPeriod = msg[i][26];
          this.contractInfo.refConNo = msg[i][27];
          this.contractInfo.remarks = msg[i][28];
          this.contractInfo.commDate = msg[i][29];
          this.contractInfo.contDate = msg[i][30];
          this.contractInfo.performReportDt = msg[i][31];
          this.contractInfo.inspectReportDt = msg[i][32];
          this.contractInfo.vendor = msg[i][33];
          // this.contractInfo.refConNo=msg[i][32];

          this.contractInfos[i] = this.contractInfo;
          // //////////////console.log( this.vendorList);
        }
        //console.log("Data\n" + this.contractInfos);
      });

  }

  setContractData(contId) {
    //console.log("Select:\n" + contId);

    this.contractInfos.forEach(element => {

      if (element.contId === contId) {

        element.commDate ?
          element.commDate = JSON.parse(JSON.stringify(element.commDate)).substring(0, 10) : element.commDate;
        element.contDate ?
          element.contDate = JSON.parse(JSON.stringify(element.contDate)).substring(0, 10) : element.contDate;
        element.performReportDt ?
          element.performReportDt = JSON.parse(JSON.stringify(element.performReportDt)).substring(0, 10) : element.performReportDt;
        element.inspectReportDt ?
          element.inspectReportDt = JSON.parse(JSON.stringify(element.inspectReportDt)).substring(0, 10) : element.inspectReportDt;

        this.budgetBillEntry.cont_id = element.contId;
        this.budgetBillEntry.agreement_sign_dt = element.contDate;
        this.budgetBillEntry.warranty = element.warrantyPeriod;
        this.budgetBillEntry.ven_id = element.venId;
        this.budgetBillEntry.performReportDt = element.performReportDt;
        this.budgetBillEntry.inspectReportDt = element.inspectReportDt;
        this.budgetBillEntry.commDate = element.commDate;

        this.budgetBillEntry.amnt_of_contract_for_bill = element.totalCharge;

        var expirationDate = new Date(this.budgetBillEntry.agreement_sign_dt);
        const date = new Date(expirationDate);
        date.setMonth(date.getMonth() + parseInt(this.budgetBillEntry.warranty));
        date.toISOString().slice(0, 10);
        this.budgetBillEntry.warranty_valid_upto = JSON.parse(JSON.stringify(date)).substring(0, 10);


      }

    });



    //console.log("Cont Data: \n"+JSON.stringify(this.budgetBillEntry.agreement_sign_dt));


    this.selectedvendor.forEach(element => {
      if (this.budgetBillEntry.ven_id === element.value) {
        console.log("Vendor Name :\n" + element.value);
      }
    })

    //get total bill amount for this contract

    var dataHeader = this.dataService.createMessageHeader("GET_BILL_AMNT_BY_CONTRACT");
     this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.budgetBillEntry)
    }

    const request = this.budgetbillentryService.getTotalBillByContract(JSON.stringify(this.jsonMessage));
    request.subscribe( (res) => {
      //////////////console.log(res);
      const msg = JSON.parse(res);

      this.budgetBillEntry.bill_amnt_by_cont = msg.budget_bill_amnt_by_cont;

      //IF CONT_BILL_ENTRY TABLE IS CONSIDERED FOR TOTAL CONTRACT AMOUNT CALCULATION----
      //this.budgetBillEntry.cont_bill_amnt_by_cont = msg.cont_bill_amnt_by_cont;

      console.log("Data\n"+JSON.stringify(this.budgetBillEntry));
    });




  }

  saveBudgetBillEntry(budgetBillEntry) {

    //console.log("\nBill Budget Entry:\n" + JSON.stringify(budgetBillEntry));s
    this.budgetBillEntry = budgetBillEntry;

    if (!this.budgetBillEntry.invoice_no || this.budgetBillEntry.invoice_no == null || this.budgetBillEntry.invoice_no == '') {
      this.severity = 'error';
      this.detailMsg = "Invoice No cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.invoice_no.nativeElement.focus();
      return;
    }

    if (!this.budgetBillEntry.invoice_dt || this.budgetBillEntry.invoice_dt == null ) {
      this.severity = 'error';
      this.detailMsg = "Invoice Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.invoice_dt.nativeElement.focus();
      return;
    }

    if (!this.budgetBillEntry.bill_amount || this.budgetBillEntry.bill_amount == null || this.budgetBillEntry.bill_amount=='') {
      this.severity = 'error';
      this.detailMsg = "Bill amount cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    if (!this.budgetBillEntry.vat_product || this.budgetBillEntry.vat_product == null ) {
      this.severity = 'error';
      this.detailMsg = "Vat product cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    if (!this.budgetBillEntry.ait_product || this.budgetBillEntry.ait_product == null) {
      this.severity = 'error';
      this.detailMsg = "AIT Product cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.bill_amount.nativeElement.focus();
      return;
    }

    if (!this.budgetBillEntry.cont_id || this.budgetBillEntry.cont_id == null || this.budgetBillEntry.cont_id=='') {
      this.severity = 'error';
      this.detailMsg = "Contact No must be selected";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      //this.con_id.nativeElement.focus();
      return;
    }

    if (!this.budgetBillEntry.delivery_dt || this.budgetBillEntry.delivery_dt == null ) {
      this.severity = 'error';
      this.detailMsg = "Delivary Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
     // this.delivary_date.nativeElement.focus();
      return;
    }

    this.budgetBillEntry.entry_id = this.user.userId;
    this.budgetBillEntry.update_id = "";
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.budgetBillEntry)
    }

    this.budgetbillentryService.insertBudgetBillEntry(JSON.stringify(this.jsonMessage));


  }

  updateBudgetBillInfo(budgetBillEntry): void {

    this.budgetBillEntry = budgetBillEntry;

    if (!this.budgetBillEntry.invoice_no || this.budgetBillEntry.invoice_no == null || this.budgetBillEntry.invoice_no == '') {
      this.severity = 'error';
      this.detailMsg = "Invoice No cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.budgetBillEntry.invoice_dt || this.budgetBillEntry.invoice_dt == null ) {
      this.severity = 'error';
      this.detailMsg = "Invoice Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.budgetBillEntry.bill_amount || this.budgetBillEntry.bill_amount == null || this.budgetBillEntry.bill_amount=='') {
      this.severity = 'error';
      this.detailMsg = "Bill amount cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.budgetBillEntry.cont_id || this.budgetBillEntry.cont_id == null || this.budgetBillEntry.cont_id=='') {
      this.severity = 'error';
      this.detailMsg = "Contact No must be selected";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    if (!this.budgetBillEntry.delivery_dt || this.budgetBillEntry.delivery_dt == null ) {
      this.severity = 'error';
      this.detailMsg = "Delivary Date cannot be blank";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      return;
    }

    this.budgetBillEntry.update_id = this.user.userId;
    var dataHeader = this.dataService.createMessageHeader("UPDATE");
    this.contractInfo.updateUser = this.user.userId;
    this.contractInfo.updateDt = new Date();

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(budgetBillEntry)
    }
    //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
    this.budgetbillentryService.updateBudgetBillEntry(JSON.stringify(this.jsonMessage));
    
    this.reloadPage();
  };

  deleteBudgetBillInfo(budgetBillEntry): void {

    console.log("Del Data\n" + JSON.stringify(budgetBillEntry.budget_bill_ref));

    var dataHeader = this.dataService.createMessageHeader("DELETE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(budgetBillEntry)
    }

    this.budgetbillentryService.deleteBudgetBillEntry(JSON.stringify(this.jsonMessage));

    this.reloadPage();

  };

  reloadPage(): void {
    let currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }

  setBillAmountWords(bill_amount) {

    if(!this.budgetBillEntry.cont_id || this.budgetBillEntry.cont_id===null || this.budgetBillEntry.cont_id===undefined){
      this.severity = 'error';
      this.detailMsg = "Contract must be select first!";
      this.utility.ShowToast(this.key, this.severity, this.detailMsg);
      this.budgetBillEntry.bill_amount = "";
      //this.bill_amount.nativeElement.focus();
      return;
    }

    this.wording = "";
    this.budgetBillEntry.bill_amount_in_words = this.wording;
    //console.log("\ninit: "+this.budgetBillEntry.bill_amount_in_words);

    if (bill_amount == 0 || !bill_amount) {
      console.log("\nzero or blank...\n");
      this.wording = "";
    }

    else {
      //console.log("\nB:-"+bill_amount);
      bill_amount = parseInt(bill_amount, 10);
      
      var total_budget_amnt = bill_amount+parseInt(this.budgetBillEntry.bill_amnt_by_cont, 10);

      var total_contract_amount = this.budgetBillEntry.amnt_of_contract_for_bill;
      
      
      //// in case CONT_BILL_ENTRY TABLE CONSIDERED
      //var total_contract_amount = this.budgetBillEntry.amnt_of_contract_for_bill+parseInt(this.budgetBillEntry.amnt_of_contract_for_bill, 10);

      if(total_contract_amount<total_budget_amnt){
        this.severity = 'error';
        this.detailMsg = "The Amount exceeds the total contract amount! Please Check..";
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        this.budgetBillEntry.bill_amount = "";
        //this.bill_amount.nativeElement.focus();
        return;
      }

      this.wording = this.budgetbillentryService.convertGreaterThanThousand(bill_amount) + " Only."
    }
    this.budgetBillEntry.bill_amount_in_words = this.wording;
  }

  setDeliverytTenorInWords(delivaryDate) {


    delivaryDate = new Date(delivaryDate);
    
    var currentDate = new Date();
    //var todaysDate = new Date(currentDate);
    var commDate = new Date(this.budgetBillEntry.agreement_sign_dt);
    
   // todaysDate.setDate(currentDate.getDate() - 1);

    var delivaryTimeLeft = delivaryDate.getTime() - commDate.getTime();

    //console.log("\nrr:" + delivaryTimeLeft);

    delivaryTimeLeft > 0 ? this.budgetBillEntry.delivary_time_left = this.budgetbillentryService.convertMillisToWords(delivaryTimeLeft) : this.budgetBillEntry.delivary_time_left = "Expired!"

    if(this.budgetBillEntry.delivary_time_left === ""){
      this.budgetBillEntry.delivary_time_left = "Today";
    }

    //console.log("A:\n"+ this.budgetBillEntry.delivary_time_left);

  }

}


