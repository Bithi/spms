import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BudgetbillentryinsertComponent } from './budgetbillentryinsert/budgetbillentryinsert.component';


const routes: Routes = [
  {path: '', component: BudgetbillentryinsertComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetBillEntryRoutingModule { }
