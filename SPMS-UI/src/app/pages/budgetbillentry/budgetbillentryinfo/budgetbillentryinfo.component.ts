import { Component, OnInit, ViewChild } from '@angular/core';
import { Budgetbillentry } from 'src/app/models/Budgetbillentry';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { BudgetbillentryService } from 'src/app/services/budgetbillentry.service';
import { SelectItem, Table } from 'primeng';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-budgetbillentryinfo',
  templateUrl: './budgetbillentryinfo.component.html',
  styleUrls: ['./budgetbillentryinfo.component.scss']
})
export class BudgetbillentryinfoComponent implements OnInit {
  @ViewChild(Table) dt: Table;

  budgetBillEntry: Budgetbillentry = null;
  contractNameSet = new Map();
  budgetBillEntries: Budgetbillentry[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  key = 'toast';
  severity: string;
  detailMsg: string;
  selectedVatList: SelectItem[];
  selectedAitList: SelectItem[];
  constructor(
    private budgetbillentryService: BudgetbillentryService,
    public router: Router,
    private dataService: DataService,
    private dropdownInfoService: DropdownInfoService,
    private utility: Utility
  ) { }
  getBudgetBillEntry() {

    const request = this.budgetbillentryService.getBudgetBillEntryList();
    request.subscribe(
      (res) => {
        // debugger;
        // const msg = JSON.parse(atob(res));
        // this.contractbranches = msg.payLoad;
        const msg = JSON.parse(res);
        this.budgetBillEntries = [];

        for (let i = 0; i < msg.length; i++) {
          // tslint:disable-next-line:new-parens
          this.budgetBillEntry = new Budgetbillentry;
          this.budgetBillEntry.invoice_no = msg[i][0];
          this.budgetBillEntry.invoice_dt = msg[i][1];
          this.budgetBillEntry.product_description = msg[i][2];
          this.budgetBillEntry.bill_amount = msg[i][3];
          this.budgetBillEntry.cont_id = msg[i][4];
          this.budgetBillEntry.work_order_no = msg[i][5];
          this.budgetBillEntry.delivery_dt = msg[i][6];
          this.budgetBillEntry.bill_recieve_dt = msg[i][7];
          this.budgetBillEntry.bill_to_audit_dt = msg[i][8];
          this.budgetBillEntry.audit_report_receive_dt = msg[i][9];
          this.budgetBillEntry.send_to_bdc_dt = msg[i][10];
          this.budgetBillEntry.payment_note_dt = msg[i][11];
          this.budgetBillEntry.from_bdc_dt = msg[i][12];
          this.budgetBillEntry.note_approve_dt = msg[i][13];
          this.budgetBillEntry.budget_bill_ref = msg[i][14];
          this.budgetBillEntry.entry_id = msg[i][15];
          this.budgetBillEntry.entry_dt = msg[i][16];
          this.budgetBillEntry.update_id = msg[i][17];
          this.budgetBillEntry.update_dt = msg[i][18];
          this.budgetBillEntry.vat_product = msg[i][19];
          this.budgetBillEntry.ait_product = msg[i][20];
          this.budgetBillEntry.agreement_sign_dt = msg[i][21];
          this.budgetBillEntry.ven_id = msg[i][22];
          this.budgetBillEntry.warranty = msg[i][23];
          this.budgetBillEntry.performReportDt = msg[i][24];
          this.budgetBillEntry.inspectReportDt = msg[i][25];
          this.budgetBillEntries[i] = this.budgetBillEntry;
        }
      });

  }

  getContractListInfo() {

    const request = this.dropdownInfoService.getContractInfos();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);


        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < msg.length; i++) {
          // this.selectedContract.push({ label: msg[i].contNo, value: msg[i].contId });
          this.contractNameSet.set(msg[i].contId, msg[i].contNo);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = 'Server Error: ' + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }

  getVatProductList() {

    const request = this.dropdownInfoService.getVatProductList();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);

        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < msg.length; i++) {
          // tslint:disable-next-line:no-debugger
          debugger;
          this.selectedVatList.push({ label: msg[i].vatAitProductId, value: msg[i].productType });
          // this.contractNameSet.set(msg[i].contId, msg[i].contNo);
        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = 'Server Error: ' + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }

  getAitProductList() {

    const request = this.dropdownInfoService.getAitProductList();
    request.subscribe(
      (res) => {

        const msg = JSON.parse(res);


        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < msg.length; i++) {
          this.selectedAitList.push({ label: msg[i].vatAitProductId, value: msg[i].productType });

        }

      },
      err => {
        this.severity = 'error';
        this.detailMsg = 'Server Error: ' + err.message;
        this.utility.ShowToast(this.key, this.severity, this.detailMsg);
        return;
      }
    );


  }

  ngOnInit(): void {

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    // this.getAitProductList();
    // this.getVatProductList();
    this.getContractListInfo();
    this.getBudgetBillEntry();

    this.cols = [


      { field: 'cont_id', header: 'Contract Number' },
      { field: 'invoice_no', header: 'Invioce Number' },
      { field: 'bill_amount', header: 'Amount' },
      { field: 'work_order_no', header: 'Work Order No' }
    ];


  }

  editContBillEntry(budgetBillEntry): void {

    this.budgetbillentryService.passTheValue(budgetBillEntry);
  }

  reloadPage(): void {
    const currentUrl = this.router.url;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([currentUrl]);
  }


}
