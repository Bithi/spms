import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = '/api/v1/users';

  constructor(private http: HttpClient) { }

  getUser(id: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
  getUserList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
