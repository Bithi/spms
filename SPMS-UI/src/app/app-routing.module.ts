import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboradComponent } from './pages/dashboard/dashborad/dashborad.component';
import { AuthGuard } from './util/auth.guard';
import { UserInfoComponent } from './pages/user-info/user-info/user-info.component';
import { UserInfoInsertComponent } from './pages/user-info/user-info-insert/user-info-insert.component';
import { VendorInsertComponent } from './pages/vendor-info/vendor-insert/vendor-insert.component';
import { VendorInfoComponent } from './pages/vendor-info/vendor-info/vendor-info.component';
import { ContractInfoInsertComponent } from './pages/contract-info/contract-info-insert/contract-info-insert.component';
import { ContractInfoComponent } from './pages/contract-info/contract-info/contract-info.component';
import { ContractBranchInsertComponent } from './pages/contract-branch/contract-branch-insert/contract-branch-insert.component';
import { ContractBranchComponent } from './pages/contract-branch/contract-branch/contract-branch.component';
import { UploadComponent } from './pages/downtime/downtime/upload/upload.component';
import { PenaltyConfigInsertComponent } from './pages/penalty-config/penalty-config-insert/penalty-config-insert.component';
import { PenaltyConfigComponent } from './pages/penalty-config/penalty-config/penalty-config.component';
import { PenaltyComponent } from './pages/process/penalty/penalty/penalty.component';
import { DowntimeReportComponent } from './pages/report/downtime-report/downtime-report.component';
import { PoroInfoInsertComponent } from './pages/poro/poro-info-insert/poro-info-insert.component';
import { PenaltyCalcComponent } from './pages/report/penalty-calc/penalty-calc.component';
import { PenaltyReportComponent } from './pages/report/penalty-report/penalty-report.component';
import { RoleInfoInsertComponent } from './pages/roleInfo/role-info-insert/role-info-insert.component';
import { MenuInfoInsertComponent } from './pages/menuInfo/menu-info-insert/menu-info-insert.component';
import { MenuAssignedToRoleInsertComponent } from './pages/MenuAssignedToRole/menu-assigned-to-role-insert/menu-assigned-to-role-insert.component';
import { ItemTypeInsertComponent } from './pages/itemtype/item-type-insert/item-type-insert.component';
import { AmcNotificationInsertComponent } from './pages/amc-notification/amc-notification-insert/amc-notification-insert.component';
import { AmcNotificationComponent } from './pages/amc-notification/amc-notification/amc-notification.component';
import { FileUploadListComponent } from './pages/file-upload/file-upload-list/file-upload-list.component';
import { FileUploadInsertComponent } from './pages/file-upload/file-upload-insert/file-upload-insert.component';
import { SubItemTypeInfoInsertComponent } from './pages/sub-item-type/sub-item-type-info-insert/sub-item-type-info-insert.component';
import { BrandInfoInsertComponent } from './pages/brand/brand-info-insert/brand-info-insert.component';
import { ContPaymentMethodInsertComponent } from './pages/cont-payment-method/cont-payment-method-insert/cont-payment-method-insert.component';
import { ContPaymentMethodComponent } from './pages/cont-payment-method/cont-payment-method/cont-payment-method.component';
import { ContSubItemInsertComponent } from './pages/cont-subitem/cont-subitem-insert/cont-subitem-insert.component';
import { ContSubItemComponent } from './pages/cont-subitem/cont-subitem/cont-subitem.component';
import { ModelCreateComponent } from './pages/model/model-create/model-create.component';
import { ModelListComponent } from './pages/model/model-list/model-list.component';
import { RoleWiseUserInsertComponent } from './pages/role-wise-user/role-wise-user-insert/role-wise-user-insert.component';
import { RoleWiseUserComponent } from './pages/role-wise-user/role-wise-user/role-wise-user.component';
import { ContSecurityMoneyInsertComponent } from './pages/cont-securitymoney/cont-securitymoney-insert/cont-securitymoney-insert.component';
import { ContSecurityMoneyComponent } from './pages/cont-securitymoney/cont-securitymoney/cont-securitymoney.component';
import { ContractbilltaxinsertComponent } from './pages/contractbilltax/contractbilltaxinsert/contractbilltaxinsert.component';
import { ContractbillentryinsertComponent } from './pages/contractbillentry/contractbillentryinsert/contractbillentryinsert.component';
import { ContyearwiserateinsertComponent } from './pages/contyearwiserate/contyearwiserateinsert/contyearwiserateinsert.component';
import { ContbilltranInsertComponent } from './pages/contbilltran/contbilltran-insert/contbilltran-insert.component';
import { BillAuthComponent } from './pages/bill-auth/bill-auth/bill-auth.component';
import { AmcReportComponent } from './pages/report/amc-report/amc-report.component';
import { ConnectivityReportComponent } from './pages/report/connectivity-report/connectivity-report.component';
import { FindValidEndComponent } from './pages/report/find-valid-end/find-valid-end.component';
import { PasswordResetComponent } from './pages/user-info/password-reset/password-reset.component';
import { DefaultComponent } from './pages/user-info/default/default.component';
import { LoginGuard } from './util/login.guard';
import { ServiceReportComponent } from './pages/report/service-report/service-report.component';



import { BudgetbillentryinsertComponent } from './pages/budgetbillentry/budgetbillentryinsert/budgetbillentryinsert.component';
import { BudgetbillpaymentComponent } from './pages/budgetbillpayment/budgetbillpayment/budgetbillpayment.component';


const routes: Routes = [

  // { path: '', component: LoginComponent,canActivate:[LoginGuard]},
  {
    path: '',
    children: [
      {path: '', component: LoginComponent, canActivate: [LoginGuard]}
    ]
  },
  // { path: 'SPMS', 
  //   loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
  //   canLoad: [AuthGuard], 
  //   data: { path: 'dashboard' } 
  // },

  { path: 'user', 
    loadChildren: () => import('./pages/user-info/user-info.module').then(m => m.UserInfoModule) ,
    canLoad: [AuthGuard],
    data: { path: 'user' }
  },
 
  // { path: '', component: LoginComponent },
  // { path: 'vendor', loadChildren: () => import('./pages/vendor-info/vendor-info.module').then(m => m.VendorInfoModule),canActivate: [AuthGuard], data: { path: 'vendor' } },
  
  // { path: 'default', component: DefaultComponent, canActivate: [AuthGuard], data: { path: 'default' } },
  { path: 'dashboard', loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule) ,
    canLoad: [AuthGuard], 
    data: { path: 'dashboard' }
  },

  // { path: '', loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule) ,
  //   canLoad: [AuthGuard], 
  //   data: { path: 'dashboard' }
  // },

  { path: 'notification', 
    loadChildren: () => import('./pages/amc-notification/amc-notification.module').then(m => m.AmcNotificationModule),
    canLoad: [AuthGuard], 
    data: { path: 'notification' } 
  },

  { path: 'billAuth', 
    loadChildren: () => import('./pages/bill-auth/bill-auth.module').then(m=>m.BillAuthModule),
    canLoad: [AuthGuard], 
    data: { path: 'billAuth' } 
  },
  
  { path: 'brand', 
    loadChildren: () => import('./pages/brand/brand.module').then(m=>m.BrandModule),
    canLoad: [AuthGuard], 
    data: { path: 'brand' } 
  },

  { path: 'contPaymentMethod', 
  loadChildren: () => import('./pages/cont-payment-method/cont-payment-method.module').then(m=>m.ContPaymentMethodModule),
  canLoad: [AuthGuard], 
  data: { path: 'contPaymentMethod' } 
  },

  { path: 'contSecurityMoney', 
  loadChildren: () => import('./pages/cont-securitymoney/cont-securitymoney.module').then(m=>m.ContSecuritymoneyModule),
  canLoad: [AuthGuard], 
  data: { path: 'contSecurityMoney' } 
  },

  { path: 'contSubItem', 
  loadChildren: () => import('./pages/cont-subitem/cont-subitem.module').then(m=>m.ContSubitemModule),
  canLoad: [AuthGuard], 
  data: { path: 'contSubItem' } 
  },

  { path: 'contbillchghead', 
  loadChildren: () => import('./pages/contbillchghead/contbillchghead.module').then(m=>m.ContbillchgheadModule),
  canLoad: [AuthGuard], 
  data: { path: 'contbillchghead' } 
  },

  { path: 'contBillTran', 
  loadChildren: () => import('./pages/contbilltran/contbilltran.module').then(m=>m.ContbilltranModule),
  canLoad: [AuthGuard], 
  data: { path: 'contBillTran' } 
  },

  { path: 'contractbranch', 
  loadChildren: () => import('./pages/contract-branch/contract-branch.module').then(m=>m.ContractBranchModule),
  canLoad: [AuthGuard], 
  data: { path: 'contractbranch' } 
  },

  { path: 'contractinfo', 
  loadChildren: () => import('./pages/contract-info/contract-info.module').then(m=>m.ContractInfoModule),
  canLoad: [AuthGuard], 
  data: { path: 'contractinfo', module: 'downtime', moduleId: '1' } 
  },

  { path: 'contractinfoAmc', 
  loadChildren: () => import('./pages/contract-info/contract-info.module').then(m=>m.ContractInfoModule),
  canLoad: [AuthGuard], 
  data: { path: 'contractinfoAmc', module: 'amc', moduleId: '2' }
  },


  { path: 'contractbillentry', 
  loadChildren: () => import('./pages/contractbillentry/contract-bill-entry.module').then(m=>m.ContractBillEntryModule),
  canLoad: [AuthGuard], 
  data: { path: 'contractbillentry' } 
  },

  { path: 'contractbilltax', 
  loadChildren: () => import('./pages/contractbilltax/contract-bill-tax.module').then(m=>m.ContractBillTaxModule),
  canLoad: [AuthGuard], 
  data: { path: 'contractbilltax' } 
  },

  { path: 'contYearWiseRate', 
  loadChildren: () => import('./pages/contyearwiserate/contract-year-wise-rate.module').then(m=>m.ContractYearWiseRateModule),
  canLoad: [AuthGuard], 
  data: { path: 'contYearWiseRate' } 
  },

  { path: 'upload', 
  loadChildren: () => import('./pages/downtime/downtime/downtime.module').then(m=>m.DowntimeModule),
  canLoad: [AuthGuard], 
  data: { path: 'upload' } 
  },

  { path: 'fileUpload', 
  loadChildren: () => import('./pages/file-upload/file-upload.module').then(m=>m.FileUploadModule),
  canLoad: [AuthGuard], 
  data: { path: 'fileUpload' } 
  },

  { path: 'itemType', 
  loadChildren: () => import('./pages/itemtype/item-type.module').then(m=>m.ItemTypeModule),
  canLoad: [AuthGuard], 
  data: { path: 'itemType' } 
  },

  { path: 'menuWIseRole', 
  loadChildren: () => import('./pages/MenuAssignedToRole/menu-assigned-to-role.module').then(m=>m.MenuAssignedToRoleModule),
  canLoad: [AuthGuard], 
  data: { path: 'menuWIseRole' } 
  },

  { path: 'menuinfo', 
  loadChildren: () => import('./pages/menuInfo/menu-info.module').then(m=>m.MenuInfoModule),
  canLoad: [AuthGuard], 
  data: { path: 'menuinfo' } 
  },

  { path: 'model', 
  loadChildren: () => import('./pages/model/model.module').then(m=>m.ModelModule),
  canLoad: [AuthGuard], 
  data: { path: 'model' } 
  },

  { path: 'penaltyconfig', 
  loadChildren: () => import('./pages/penalty-config/penalty-config.module').then(m=>m.PenaltyConfigModule),
  canLoad: [AuthGuard], 
  data: { path: 'penaltyconfig' } 
  },

  { path: 'penaltyProcess', 
  loadChildren: () => import('./pages/process/penalty/penalty.module').then(m=>m.PenaltyModule),
  canLoad: [AuthGuard], 
  data: { path: 'penaltyProcess' } 
  },

  { path: 'rolewiseuser', 
  loadChildren: () => import('./pages/role-wise-user/role-wise-user.module').then(m=>m.RoleWiseUserModule),
  canLoad: [AuthGuard], 
  data: { path: 'rolewiseuser' } 
  },

  { path: 'roleinfo', 
  loadChildren: () => import('./pages/roleInfo/role-info.module').then(m=>m.RoleInfoModule),
  canLoad: [AuthGuard], 
  data: { path: 'roleinfo' } 
  },

  { path: 'subItemTypeInsert', 
  loadChildren: () => import('./pages/sub-item-type/sub-item-type-info.module').then(m=>m.SubItemTypeInfoModule),
  canLoad: [AuthGuard], 
  data: { path: 'subItemTypeInsert' } 
  },

  { path: 'vendor', 
  loadChildren: () => import('./pages/vendor-info/vendor-info.module').then(m=>m.VendorInfoModule),
  canLoad: [AuthGuard], 
  data: { path: 'vendor' } 
  },


//ADDED 08/11/2023

  { path: 'budgetbillentry', 
  loadChildren: () => import('./pages/budgetbillentry/budget-bill-entry.module').then(m=>m.BudgetBillEntryModule),
  canLoad: [AuthGuard], 
  data: { path: 'budgetbillentry' } 
  },

  { path: 'billpayment', 
  loadChildren: () => import('./pages/budgetbillpayment/budgetbillpayment.module').then(m=>m.BudgetbillpaymentModule),
  canLoad: [AuthGuard], 
  data: { path: 'billpayment' } 
  },


  //ADDED IN 16-04-24

  { path: 'budgetMaster', 
  loadChildren: () => import('./pages/budgetMaster/budget-master.module').then(m=>m.BudgetMasterModule),
  canLoad: [AuthGuard], 
  data: { path: 'budgetMaster' } 
  },

  { path: 'budgetAllocation', 
  loadChildren: () => import('./pages/budgetAllocation/budget-allocation.module').then(m=>m.BudgetAllocationModule),
  canLoad: [AuthGuard], 
  data: { path: 'budgetAllocation' } 
  },

  // { path: 'connectivityReport', 
  // loadChildren: () => import('./pages/report/report.module').then(m=>m.ReportModule),
  // canLoad: [AuthGuard], 
  // // canActivate: [ AuthGuard ],
  // data: { path: 'connectivityReport' } 
  // },


  // { path: 'contractinfo', loadChildren: () => import('./pages/contract-info/contract-info.module').then(m => m.ContractInfoModule),canActivate: [AuthGuard], data: { path: 'contractinfo' } },
  // { path: 'contractbranch', loadChildren: () => import('./pages/contract-branch/contract-branch.module').then(m => m.ContractBranchModule),canActivate: [AuthGuard], data: { path: 'contractbranch' } },
  // { path: 'upload', loadChildren: () => import('./pages/downtime/downtime/downtime.module').then(m => m.DowntimeModule),canActivate: [AuthGuard], data: { path: 'upload' } },
  // { path: 'penaltyconfig', loadChildren: () => import('./pages/penalty-config/penalty-config.module').then(m => m.PenaltyConfigModule),canActivate: [AuthGuard], data: { path: 'penaltyconfig' } },
  // { path: 'penaltyProcess', loadChildren: () => import('./pages/process/penalty/penalty.module').then(m => m.PenaltyModule),canActivate: [AuthGuard], data: { path: 'penaltyProcess' } },
  // { path: 'poro', loadChildren: () => import('./pages/poro/poro.module').then(m => m.POROModule),canActivate: [AuthGuard], data: { path: 'poro' } },
  // { path: 'roleinfo', loadChildren: () => import('./pages/roleInfo/role-info.module').then(m => m.RoleInfoModule),canActivate: [AuthGuard], data: { path: 'roleinfo' } },
  // { path: 'menuinfo', loadChildren: () => import('./pages/menuInfo/menu-info.module').then(m => m.MenuInfoModule),canActivate: [AuthGuard], data: { path: 'menuinfo' } },
  // { path: 'menuWIseRole', loadChildren: () => import('./pages/MenuAssignedToRole/menu-assigned-to-role.module').then(m => m.MenuAssignedToRoleModule),canActivate: [AuthGuard], data: { path: 'menuWIseRole' } },
  // { path: 'itemType', loadChildren: () => import('./pages/itemtype/itemtype.module').then(m => m.ItemtypeModule),canActivate: [AuthGuard], data: { path: 'itemType' } },
  // { path: 'notification', loadChildren: () => import('./pages/amc-notification/amc-notification.module').then(m => m.AmcNotificationModule),canActivate: [AuthGuard], data: { path: 'notification' } },
  // { path: 'fileUpload', loadChildren: () => import('./pages/file-upload/file-upload.module').then(m => m.FileUploadModule),canActivate: [AuthGuard], data: { path: 'fileUpload' } },
  // { path: 'subItemTypeInsert', loadChildren: () => import('./pages/sub-item-type/sub-item-type.module').then(m => m.SubItemTypeModule),canActivate: [AuthGuard], data: { path: 'subItemTypeInsert' } },
 
  // { path: 'dashboard', component: DashboradComponent, canActivate: [AuthGuard], data: { path: 'dashboard' } },
  // { path: 'userList', component: UserInfoComponent, canActivate: [AuthGuard], data: { path: 'userList' } },
  // { path: 'user', component: UserInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'user' } },
 
 
 
  // { path: 'vendor', component: VendorInsertComponent, canActivate: [AuthGuard], data: { path: 'vendor' } },
  // { path: 'vendorList', component: VendorInfoComponent, canActivate: [AuthGuard], data: { path: 'vendorList' } },
  // { path: 'contractinfo', component: ContractInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'contractinfo', module: 'downtime', moduleId: '1' } },
  // { path: 'contractinfoList', component: ContractInfoComponent, canActivate: [AuthGuard], data: { path: 'contractinfoList', module: 'downtime', moduleId: '1' } },
  // { path: 'contractbranch', component: ContractBranchInsertComponent, canActivate: [AuthGuard], data: { path: 'contractbranch' } },
  // { path: 'contractbranchList', component: ContractBranchComponent, canActivate: [AuthGuard], data: { path: 'contractbranchList' } },
  // { path: 'upload', component: UploadComponent, canActivate: [AuthGuard], data: { path: 'upload' } },
  // { path: 'penaltyconfig', component: PenaltyConfigInsertComponent, canActivate: [AuthGuard], data: { path: 'penaltyconfig' } },
  // { path: 'penaltyconfigList', component: PenaltyConfigComponent, canActivate: [AuthGuard], data: { path: 'penaltyconfigList' } },
  // { path: 'penaltyProcess', component: PenaltyComponent, canActivate: [AuthGuard], data: { path: 'penaltyProcess' } },
  { path: 'export', component: PenaltyReportComponent, canActivate: [AuthGuard], data: { path: 'export' } },
  // { path: 'poro', component: PoroInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'poro' } },
  // { path: 'penaltyReport', component: PenaltyCalcComponent, canActivate: [AuthGuard], data: { path: 'penaltyReport' } },
  // { path: 'penaltyNewReport', component: PenaltyReportComponent, canActivate: [AuthGuard], data: { path: 'penaltyNewReport' } },
  // { path: 'roleinfo', component: RoleInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'roleinfo' } },
  // { path: 'menuinfo', component: MenuInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'menuinfo' } },
  // { path: 'menuWIseRole', component: MenuAssignedToRoleInsertComponent, canActivate: [AuthGuard], data: { path: 'menuWIseRole' } },
  // { path: 'itemType', component: ItemTypeInsertComponent, canActivate: [AuthGuard], data: { path: 'itemType' } },
  // { path: 'notification', component: AmcNotificationInsertComponent, canActivate: [AuthGuard], data: { path: 'notification' } },
  // { path: 'notificationList', component: AmcNotificationComponent, canActivate: [AuthGuard], data: { path: 'notificationList' } },
  // { path: 'fileUploadList', component: FileUploadListComponent, canActivate: [AuthGuard], data: { path: 'fileUploadList' } },
  // { path: 'fileUpload', component: FileUploadInsertComponent, canActivate: [AuthGuard], data: { path: 'fileUpload' } },
  // { path: 'subItemTypeInsert', component: SubItemTypeInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'subItemTypeInsert' } },
  // { path: 'brand', component: BrandInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'brand' } },
  // { path: 'contPaymentMethod', component: ContPaymentMethodInsertComponent, canActivate: [AuthGuard], data: { path: 'contPaymentMethod' } },
  // { path: 'contPaymentMethodList', component: ContPaymentMethodComponent, canActivate: [AuthGuard], data: { path: 'contPaymentMethodList' } },
  // { path: 'contSubItem', component: ContSubItemInsertComponent, canActivate: [AuthGuard], data: { path: 'contSubItem' } },
  // { path: 'contSubItemList', component: ContSubItemComponent, canActivate: [AuthGuard], data: { path: 'contSubItemList' } },

  // { path: 'model', component: ModelCreateComponent, canActivate: [AuthGuard], data: { path: 'model' } },
  // { path: 'modelList', component: ModelListComponent, canActivate: [AuthGuard], data: { path: 'modelListng' } },
  // { path: 'rolewiseuser', component: RoleWiseUserInsertComponent, canActivate: [AuthGuard], data: { path: 'rolewiseuser' } },
  // { path: 'rolewiseuserList', component: RoleWiseUserComponent, canActivate: [AuthGuard], data: { path: 'rolewiseuserList' } },

  // { path: 'contractinfoAmc', component: ContractInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'contractinfoAmc', module: 'amc', moduleId: '2' } },
  // { path: 'contractinfoListAmc', component: ContractInfoComponent, canActivate: [AuthGuard], data: { path: 'contractinfoList', module: 'amc', moduleId: '2' } },
  // { path: 'contSecurityMoney', component: ContSecurityMoneyInsertComponent, canActivate: [AuthGuard], data: { path: 'contSecurityMoney' } },
  // { path: 'contSecurityMoneyList', component: ContSecurityMoneyComponent, canActivate: [AuthGuard], data: { path: 'contSecurityMoneyList' } },
  // // { path: 'contbillchghead', component: ContbillchgheadinsertComponent, canActivate: [AuthGuard], data: { path: 'contbillchghead' } },
  // { path: 'contractbilltax', component: ContractbilltaxinsertComponent, canActivate: [AuthGuard], data: { path: 'contractbilltax' } },
  // { path: 'contractbillentry', component: ContractbillentryinsertComponent, canActivate: [AuthGuard], data: { path: 'contractbillentry' } },
  // { path: 'contYearWiseRate', component: ContyearwiserateinsertComponent, canActivate: [AuthGuard], data: { path: 'contYearWiseRate' } },
  // { path: 'contBillTran', component: ContbilltranInsertComponent, canActivate: [AuthGuard], data: { path: 'contBillTran' } },
  // { path: 'billAuth', component: BillAuthComponent, canActivate: [AuthGuard], data: { path: 'billAuth' } },
  { path: 'amcReport', component: AmcReportComponent, canActivate: [AuthGuard], data: { path: 'amcReport' } },
  { path: 'connectivityReport', component: ConnectivityReportComponent, canActivate: [AuthGuard], data: { path: 'connectivityReport' } },
  { path: 'serviceReport', component: ServiceReportComponent, canActivate: [AuthGuard], data: { path: 'serviceReport' } },
  { path: 'findValildEnd', component: FindValidEndComponent, data: { path: 'findValidEnd' } },
  { path: 'passwordReset', component: PasswordResetComponent, data: { path: 'passwordReset' } },
  { path: 'contBillTran', 
  loadChildren: () => import('./pages/budgetbillpayment/budgetbillpayment.module').then(m=>m.BudgetbillpaymentModule),
  canLoad: [AuthGuard], 
  data: {
  path: 'billpayment', component: BudgetbillpaymentComponent}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
