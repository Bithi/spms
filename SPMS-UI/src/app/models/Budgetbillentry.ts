export class Budgetbillentry {
    entry_id: string;
    entry_dt: Date;

    update_id: string;
    update_dt: Date;

    budget_bill_ref: number;
    
    invoice_no: string;
    invoice_dt: Date;

    product_description: string;

    bill_amount: any;
    
    cont_id: string;
    ven_id: string;
    
    work_order_no: string;
    agreement_sign_dt: Date;
    warranty_valid_upto: any;
    warranty: string;
    delivery_dt: Date;

    bill_recieve_dt: Date;

    
    performReportDt:Date;
    inspectReportDt:Date;

    bill_to_audit_dt: Date;
    audit_report_receive_dt:Date;
    send_to_bdc_dt: Date;
    payment_note_dt: Date;
    from_bdc_dt: Date;
    note_approve_dt: Date;

    //for display puppose
    bill_amount_in_words: string;
    delivary_time_left: string;

    isEdit: boolean = false;

    commDate: Date;
    
    vat_product: number;
    ait_product: number;

    
    bill_amnt_by_cont: string; 
    cont_bill_amnt_by_cont: string;// in case CONT_BILL_ENTRY TABLE CONSIDERED
    amnt_of_contract_for_bill: number; 
    
   
}


