export class Brand {
    brandId: string;
    itemId: string;
    brandName: string;
    entryDt: Date;
    entryUser: string;
    updateUser: string;
    updateDt: Date;
    itemName: String;
}


