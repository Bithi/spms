export class Budgetbillpayment {
    payment_id: number;
    invoice_no: string;
    penalty_amt: number;
    pay_order_no: number;
    pay_order_amt: number;
    pay_order_amount_in_words: string;
    vat_chalan_no: string;
    vat_chalan_dt: Date;
    ait_chalan_no: string;
    ait_chalan_dt: Date;
    form_no      : string;
    form_dt      : Date;
    entry_id     : string;
    entry_dt     : Date;
    payment_dt     : Date;
    fiscal_year :string;
    bill_after_vat:number;
    accu_bill_after_vat:number;
    curr_tds: number;
    accu_tds:number;
    vat_rate:number;
    ait_rate:number;
    in_with_vat:number=1;
    in_with_tax:number=1;
   
    sector_id:number;
    
    
   
}


