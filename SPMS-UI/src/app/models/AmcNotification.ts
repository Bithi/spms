export class AmcNotification {


    amcNotifId: number;
    contractId: string;
    vendorMobile: string;
    vendorEmail: string;
    gmItMobile: string;
    gmItEmail: string;
    dgmItProcMobile: string;
    dgmItProcEmail: string;
    dealingOffMobile: string;
    dealingOffEmail: string;
    agm1ItprocMob: string;
    agm1ItprocEmail: string;
    agm2ItprocMob: string;
    agm2ItprocEmail: string;
    notifBefore: number;
    notifIntervalDay: number;
    lastNotifDate: Date;
    entryUser: string;
    entryDate: Date;
    contractNo:string;

}