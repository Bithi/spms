export class Vat {
    vat_id: string;
    micro_gl_head: string;
    vat_rate: number;
    fiscal_year: string;
    entrydt: Date;
    entryusr: string;
    updatedt: Date;
    updateusr: string;
}


