export class SubItemType {
    itemId: string;
    itemName: string;
    entryDt: Date;
    entryUser: string;
    updateUser: string;
    updateDt: Date;
    typeId: string;
}


