export class ContSubItem {
    contSubItemId   : string;
    contId          : string;
    itemId          : string;
    modelId          : string;
    itemQuantity    : number;
    totalValue      : number;
    entryUser       : string;
    entryDt         : Date;
    updateUser      : string;
    updateDt        : Date;
    cont_no      : string;
    itemname      : string;
    modelname      : string;
}