export class ContPaymentMethod{

  
    id                          : number    ;
    contId                      : String    ;
    partFull                    : String    ;
    pmtSchedule                 : String    ;
    ttValueProduct              : number    ;
    paymentDate                 : Date      ;
    paymentAmount               : number    ;
    dueAmount                   : number    ;
    dueDate                     : Date      ;
    entryUser                   : String    ;
    entryDt                     : Date      ;
    contNo                      : String    ;
}