export class Contractbillentry {
    entry_id: string;
    cont_id: string;
    bill_no: string;
    bill_date: Date;
    bill_recieve_date: Date;
    part_full: string;
    send_audit_date: Date;
    return_from_audit_date: Date;
    entryusr: string;
    entrydt: Date;
    updateusr: string;
    updatedt: Date;   
    itemid: string;
    billamount: Number;
    contNo: string;
}


