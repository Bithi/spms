export class ContractBranch {
    
    id: number;
    contractId:string;
    brac: string;
    brcAtm:string;
    brName:string;
    bw: number;
    monthlyCharge: number;
    status:string;
    entryUser:string;
    entryDate:Date;
    updateUser:string;
    updateDt:Date;
    contractName:string;
    vendor:string;
}