export class Downtime {
    id: string;
    month: string;
    date: Date;
    branchCode: string;
    vendor: string;
    problem: string;
    downTime: Date;
    upTime: Date;
    totalTime: BigInteger;
    followedBy: string;
    concernOfficer: string;
    entryUser:string;
    entryDate:Date;
    updateUser:string;
    updateDt:Date;
}