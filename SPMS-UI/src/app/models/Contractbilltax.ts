export class Contractbilltax {
    cont_tax_id: string;
    cont_tax_rate: number;
    min_amt: number;
    max_amt: number;
    fiscal_year: string;
    entrydt: Date;
    entryusr: string;
    updatedt: Date;
    updateusr: string;
}


