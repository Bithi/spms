export class BudgetMaster {
    id: number;
    amountAllocationDate: Date;
    amount: number;
    year: string;
    createdBy: string;
    createdOn: Date;
    updatedBy: string;
    updatedOn: Date;

    //non db field
    amount_in_words: string;
    isEdit: boolean;
}