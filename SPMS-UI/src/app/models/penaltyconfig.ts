export class PenaltyConfig{

    configId: number;
    contractId: string;
    vendorId:string;
    condType:string;
    minDowntime: number;
    maxDowntime: number;
    penaltyRate:number;
    entryUser: string;
    entryDate: Date;
    updateUser: string;
    updateDt: Date;
    contno:  string;
    venName: string;

}

