export class BudgetSectorwiseAllocation {
    Id: number;
    sectorId: number;
    amount: string;
    year: string;
    createdOn: Date;
    createdBy: string;
    updatedBy: string;
    updatedOn: Date;
        //non db field
        amount_in_words: string;
        isEdit: boolean;
        sectorName: string;
   
}


