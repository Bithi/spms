export class Model {
    modelId: string;
    brandId: string;
    modelName: string;
    warrantyInMonth: number;
    entryDt: Date;
    entryUser: string;
    updateUser: string;
    updateDt: Date;
    brandName: string;
}