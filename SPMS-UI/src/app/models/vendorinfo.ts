export class VendorInfo{
    venId: string;
    venName: string;
    venTypeId: string;
    serviceTypeId: string;
    venAdd: string;
    venPhoneNumber: number;
    venTradeLicenseNo: string;
    venTinNo: number;
    userId: string;
    entryDt: Date;
    machineIp: string;
    updateusr: string;
    updatedt: Date;
    bin_no: string;
}