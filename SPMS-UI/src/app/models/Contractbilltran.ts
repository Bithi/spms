export class Contractbilltran {
    bill_tran_id: string;
    bill_no: string;
    cont_id: string;
    bill_part_full: string;
    bill_date: Date;
    chg_head: string;
    item_id: string;
    bill_period_start: Date;
    bill_period_end: Date;
    current_bill: number;
    vat: number;
    bill_after_vat: number;
    accum_bill_after_vat: number;
    current_income_tax: number;
    accum_income_tax: number;
    penalty: number;
    final_bill_vendor: number;
    contract_no: string;
    reference_no: string;
    reference_date: Date;
    narration: string;
    next_bill_date: Date;
    status: string;
    bill_initiate_id: string;
    bill_authorize_id: string;
    entryusr: string;
    entrydt: Date;
    updateusr: string;
    updatedt: Date;
    entry_id: string;
    chgHeadName: string;
}


