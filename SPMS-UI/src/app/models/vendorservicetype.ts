export class VendorServiceType{
    serviceTypeId: string;

    serviceTypeName: string;

    serviceTypeCode: string;
}