export class Poro {
    
    poroPrefix: string;
    poroName: string;
    poroType:string;
    gmoId: string;
    poroCode: string
    gmoCode: string;
}