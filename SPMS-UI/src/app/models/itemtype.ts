export class Itemtype {
    typeId: string;
    shortName: string;
    entryDate: Date;
    entryUser: string;
    description: string;
    category: string;
    updateUser: string;
    updateDt: Date;
}


