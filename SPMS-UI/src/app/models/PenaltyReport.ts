export class PenaltyReport{
    
    contId: string;
    venId: string;
    startDate: Date;
    endDate: Date;
    clause: string;
    item: string;
    site: string;
    status: string;
    reportType: string;
    user: string;
	 
}