export class SectorwiseAllocationInfo {
    amount: number;
    year: string;
}