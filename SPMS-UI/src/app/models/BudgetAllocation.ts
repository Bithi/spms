export class BudgetAllocation {
    id: number;
    sectorId: number;
    amount: number;
    year: string;
    createdBy: string;
    createdOn: Date;
    updatedBy: string;
    updatedOn: Date;

    //non db field
    amount_in_words: string;
    isEdit: boolean;
    
    sectorName: string;
}