export class ContractInfo{
    
    contId:string;
    contNo:string;
    contName:string;
    serviceTypeId:string;
    contParty:string;
    contValidStart:Date;
    contValidEnd:Date;
    tenderDt:Date;
    tenderNo:string;
    totalMonth:number;
    monthlyCharge:number;
    totalCharge:number;
    totalBr:number;
    totalAtm:number;
    totalDcDrConnectivity:number;
    venId:string;
    status:string;
    moduleId:number;
    entryUser:string;
    entryDate:Date;
    updateUser:string;
    updateDt:Date;
    renewDate:Date;
    vat:string;
    tax:string;
    type:string;
    warrantyPeriod:string;
    refConNo:string;
    commDate:Date;
    remarks:string;
    vendor:string;
    contDate:Date;
    performReportDt:Date;
    inspectReportDt:Date;
}

