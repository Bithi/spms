export class PenaltyCalc{
    calcId: number;
    contId: string;
    venId: string;
    brCode: string;
    month: string;
    monthlyCharge: number;
    netPenalty: number;
    entryusr:string;
    entrydt:string;
    updateusr:string;
    updatedt:string;
    brName:string;
    downid:string;
    downdate:string;
    bill_period_start:Date;
    bill_period_end:Date;

	
}