export class FileUp {
    fileId: number;
    contId: string;
    fileType: string;
    fileLoc: string;
    entryusr: string;
    entrydt: Date;
    updateusr: string;
    updatedt:Date;
    contNo: string;
    fileName: string;
}
