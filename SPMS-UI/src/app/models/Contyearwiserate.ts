export class Contyearwiserate {
    id: string;
    cont_id: string;
    rate: number;
    start_date: string;
    end_date: Date;
    contract_type: string;
    entryusr: string;
    entrydt: Date;
    updateusr: string;
    updatedt: Date;  
    contNo: string;
}


