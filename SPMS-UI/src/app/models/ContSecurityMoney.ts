export class ContSecurityMoney{
    contSecurityId      : string;
    contId              : string;
    securityType        : string;
    issueBank           : string;
    issueBranch         : string;
    issueAmount         : number;
    issueDate           : Date;
    periodSecurity      : number;
    returnDate          : Date;
    entryUser           : string;
    entryDt             : Date;
    updateUser          : string;
    updateDt            : Date;
    contNo              : string;

}