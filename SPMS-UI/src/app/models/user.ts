export class User {
    Id: number;
    userId: string;
    userName: string;
    password: string;
    newPassword: string;
    officeCode: string;
    userStatus: string;
    passwordChangeDate: Date;
    lastLoginDate:Date;
    loginAttempt: number;
}


