package com.sbl.spms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;


////@SpringBootApplication
//
//@EnableScheduling
//
//@SpringBootApplication
//@EnableEncryptableProperties
//@Configuration
//@EncryptablePropertySource("classpath:application.properties")
//@ImportResource("classpath:spring-beans.xml")
//@EnableScheduling
@EnableTransactionManagement
@SpringBootApplication
public class SpmsApplication extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	
	    return application.sources(SpmsApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpmsApplication.class, args);
	}

}
