package com.sbl.spms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.model.VendorType;
import com.sbl.spms.service.VendorInfoService;
import com.sbl.spms.service.VendorTypeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class VendorTypeController {
	
	@Autowired
	private VendorTypeService vendorTypeService;

	
	
	@GetMapping("/vendorType")
	public List<VendorType> get() throws Exception{
		return vendorTypeService.getLabels();
	}
}
