package com.sbl.spms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbl.spms.model.BRL;
import com.sbl.spms.service.BRLService;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;



@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class BRLController {
	

	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private BRLService brlService;
	
	
	@GetMapping("/getBrNameList")
	public List<BRL> getBRLList() throws Exception{
		return brlService.getLabels();

	}
	
	

}
