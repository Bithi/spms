package com.sbl.spms.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.service.UserInfoService;
import com.sbl.spms.service.VendorInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;



@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class UserInfoController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private UserInfoService userInfoService;
	
	@Autowired
	private VendorInfoService vendorInfoService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public String handleLoginRequest(@RequestBody String  encodedMessageString) throws Exception {
		log.info("User Login Request Accepted. Request : {}" , encodedMessageString);
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		
		/*
		 * TO_DO:H:Need to do Message Validation
		 */
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		DataHeader messageHeader = message.getDataHeader();
		
		try {
			if(messageHeader.getActionType().equals(ActionTypes.USER_LOGIN.toString())) {
				userInfoList = (List<UserInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<UserInfo>>(){}.getType());
				userInfoList.set(0, userInfoService.login(userInfoList.get(0).getUserId(),userInfoList.get(0).getPassword()));
			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
			
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage());
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			userInfoList=null;
			
		}
		if(messageHeader.getActionType().equals(ActionTypes.USER_LOGIN.toString())) {
			message.setPayLoad(userInfoList);
		}
		
		byte[] encoded = Base64.getEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
		
		
		return new String(encoded);
		//return messageProcessor.getResponseAsJson(message);
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/userInfo", method = RequestMethod.POST)
	@ResponseBody
	public String insertUserInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		UserInfo userInfo = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		userInfoList = (List<UserInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<UserInfo>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				userInfo = userInfoService.saveUserInfo(userInfoList.get(0));
				
				log.info("User Info Saved.");
				userInfoList.set(0, userInfo);
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				
				userInfo = userInfoService.updatePasswordOfUserInfo(userInfoList.get(0));
					log.info("User Info Updated.");
					userInfoList.set(0, userInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
				userInfo = userInfoService.deleteAllUserInfo(userInfoList.get(0));
				 log.info("User Info Deleted.");
				 userInfoList.set(0, userInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.PASSUPDATE.toString())) {
				
				userInfo = userInfoService.updatePasswordOfUserInfo(userInfoList.get(0));
					log.info("User password Updated.");
					userInfoList.set(0, userInfo);
					
				}
	else if(messageHeader.getActionType().equals(ActionTypes.RESET.toString())) {
				
				userInfo = userInfoService.resetPasswordOfUserInfo(userInfoList.get(0));
					log.info("User password Reset.");
					userInfoList.set(0, userInfo);
					
				}
			
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				userInfoList = userInfoService.getUserInfoList();
					
				}
			
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(userInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
		
	
	
	
	@GetMapping("/matchUserId/{userId}")
	public boolean getAllBrach(@PathVariable String userId) {
		return vendorInfoService.matchUserId(userId);
	}
	
	@GetMapping("/userList")
	public List<UserInfo> getUsers() throws Exception{
		return userInfoService.getLabels();
	}
	
}
