package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContSubItem;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.service.ContSubItemService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class ContSubItemController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private ContSubItemService service;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contSubItem", method = RequestMethod.POST)
	@ResponseBody
	public String requestHandler(@RequestBody String encodedMessageString) throws Exception {

		log.info("Contract Sub Item Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<ContSubItem> payloadList = new ArrayList<ContSubItem>();
		payloadList = (List<ContSubItem>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<ContSubItem>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {

			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				payloadList.set(0, service.saveData(payloadList.get(0)));
				log.info("Contract Sub Item  Saved.");

			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				payloadList.set(0, service.updateData(payloadList.get(0)));
				log.info("Contract Sub Item Updated.");

			} else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				payloadList.set(0, service.deleteData(payloadList.get(0)));
				log.info("Contract Sub Item Deleted.");

			} else if (messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {

				payloadList = service.getPaymentMethodList();

			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			log.error("", e);
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(payloadList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	@GetMapping("/ItemByContract/{contractId}")
	public List<Object[]> getAllItem(@PathVariable String contractId) {
		List<Object[]> employeeObj =  service.getItemByContract(contractId);
		if(employeeObj == null) {
			throw new RuntimeException("Item with id: "+contractId+" is not found.");
		}
		return employeeObj;
	}
	@GetMapping("/getcontsubitemModelJoin")
	public List<Object> getcon() throws Exception{
		return service.getcontsubitemModelJoin();
	}
}
