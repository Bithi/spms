package com.sbl.spms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.DownTime;
import com.sbl.spms.model.PenaltyCalc;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.service.ContractInfoService;
import com.sbl.spms.service.DowntimeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class DowntimeController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private DowntimeService dataUploadService;
	
	@Autowired
	private DowntimeService downtimeService;
	@Value("${file.store.location}")
	private String fileStoreLocation;
	
	 @RequestMapping("/uploadFile")
	public List<String> uploadFile(@RequestParam("file") MultipartFile file,@RequestParam(name="data") String jSONDownTime, HttpServletRequest request) {

		 DownTime downtime = (DownTime) messageProcessor.getObjectFromJsonString(jSONDownTime, new TypeToken<DownTime>() {}.getType());

		 Part filePart = null;
		 File fileStore = null;
		 String message = null;
		 String messageHeader = null;
		 List<String> messageList = new ArrayList<>();
		try {
			
			
			fileStore = new File(fileStoreLocation+"/DowntimeFile", file.getOriginalFilename());
			FileUtils.writeByteArrayToFile(fileStore, file.getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Retrieves <input type="file" name="file">
			FileInputStream fis = null;
			boolean status=false;
		    try {
		    	fis = new FileInputStream(fileStore);
		    	message=dataUploadService.readWrite(fis,downtime);
		    	if (message.indexOf("Error") > -1) {
					messageHeader="error";
					
				}
		    	else {
		    		messageHeader="success";
		    	}
			} catch (IOException e) {
				messageHeader="error";
				message=e.getMessage();
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				messageHeader="error";
				message=e.getMessage();
				e.printStackTrace();
			}
		   
		    messageList.add(messageHeader);
		    messageList.add(message);
			return messageList;

    }
	 
	 
	 @SuppressWarnings("unchecked")
		@RequestMapping(value = "/downtimeInfo", method = RequestMethod.POST)
		@ResponseBody
		public String handleDownTimeInfoRequest(@RequestBody String  encodedMessageString) {
			log.info("DownTime Info Request Accepted");
			byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
			String messageString = new String(decoded);
			
			Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
			
			/*
			 * TO_DO:H:Need to do Message Validation
			 */
			
			List<DownTime> downtimeList = new ArrayList<DownTime>();
			DownTime downtime = new DownTime();
			downtimeList = (List<DownTime>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<DownTime>>(){}.getType());
			
			DataHeader messageHeader = message.getDataHeader();
			try {
				
				if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
					downtimeList = dataUploadService.getDowntimeList();
					
				}
				else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
					downtime = dataUploadService.deleteDowntime(downtimeList.get(0));
					 log.info("User Info Deleted.");
					 downtimeList.set(0, downtime);
				}
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
				
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage());
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
				
			}
			message.setPayLoad(downtimeList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
		}
	
	 
	 
	 @GetMapping("/getLastDowntimeRow/{user}")
		public List<DownTime> getRow(@PathVariable String user) throws Exception{
			return downtimeService.lastDowntimeRow(user);
		}
	
}
