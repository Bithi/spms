package com.sbl.spms.controller;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.FileCopyUtils;

import javax.servlet.ServletContext;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.AMCNotification;
import com.sbl.spms.model.BRL;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.FileUp;
import com.sbl.spms.model.MediaTypeUtils;
import com.sbl.spms.service.ContractBranchService;
import com.sbl.spms.service.FileUploadService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;
@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class FileUploadController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private FileUploadService fileUploadService;
	@Value("${file.store.location}")
	private String fileStoreLocation;
    private static final String DEFAULT_FILE_NAME = "vendor_info.xls";
    @Autowired
    private ServletContext servletContext;
    
    
	@RequestMapping("/fileUpload")
	public List<String> uploadFile(@RequestParam("file") MultipartFile file,@RequestParam(name="data") String jSONFileUp, HttpServletRequest request) {
    	
    	FileUp fileup = (FileUp) messageProcessor.getObjectFromJsonString(jSONFileUp, new TypeToken<FileUp>() {}.getType());

		 Part filePart = null;
		 File fileStore = null;
		 String message = null;
		 String messageHeader = null;
		 List<String> messageList = new ArrayList<>();
		try {
			
			FileUp fileupRes = fileUploadService.fileInsertToDB(fileup,file);
			if(!fileupRes.equals(null)) {
				
				messageHeader="success";
				message = "File Uploaded";
			}

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			messageHeader="error";
			message=e.getMessage();
		} // Retrieves <input type="file" name="file">
 catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			messageHeader="error";
			message=e.getMessage();
		}
//		   
		   
		    messageList.add(messageHeader);
		    messageList.add(message);
			return messageList;

    }
	
	@RequestMapping("/fileUploadList")
	public List<FileUp> getFileList() throws Exception{
		return fileUploadService.getFileList();

	}
	
	@RequestMapping("/file/{fileName:.+}/**")
	public void downloadPDFResource(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("fileName") String fileName) throws IOException {

		final String path =
	            request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
	    final String bestMatchingPattern =
	            request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE).toString();

	    String arguments = new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);

	    String fName;
	    if (null != arguments && !arguments.isEmpty()) {
	    	fName = fileName + '/' + arguments;
	    } else {
	    	fName = fileName;
	    }

		File file = new File(fileStoreLocation + fName);
		if (file.exists()) {

			//get the mimetype
			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				//unknown mimetype so set the mimetype to application/octet-stream
				mimeType = "application/octet-stream";
			}

			response.setContentType(mimeType);

			/**
			 * In a regular HTTP response, the Content-Disposition response header is a
			 * header indicating if the content is expected to be displayed inline in the
			 * browser, that is, as a Web page or as part of a Web page, or as an
			 * attachment, that is downloaded and saved locally.
			 * 
			 */

			/**
			 * Here we have mentioned it to show inline
			 */
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

			 //Here we have mentioned it to show as attachment
			 //response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

			response.setContentLength((int) file.length());

			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

			FileCopyUtils.copy(inputStream, response.getOutputStream());

		}
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/deleteFile", method = RequestMethod.POST)
	@ResponseBody
	public String deleteFile(@RequestBody String encodedMessageString) throws Exception {

		log.info("File Delete Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<FileUp> fileUpList = new ArrayList<FileUp>();
		fileUpList = (List<FileUp>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<FileUp>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();
		fileUpList.get(0).setFileLoc(fileStoreLocation+fileUpList.get(0).getFileLoc());
		
		try {
			 if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				 fileUpList.set(0, fileUploadService.delete(fileUpList.get(0)));
				log.info("File Deleted.");

			} 
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			log.error("", e);
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(fileUpList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	
	@GetMapping("/getFileUploadWithContractInfoJoin")
	public List<Object> getcon() throws Exception{
		return fileUploadService.getFileUploadWithContractInfoJoin();
	}

}
