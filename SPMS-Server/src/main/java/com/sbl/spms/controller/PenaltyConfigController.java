package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.PenaltyConfig;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.service.ContractBranchService;
import com.sbl.spms.service.PenaltyConfigService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class PenaltyConfigController {
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private PenaltyConfigService penaltyConfigService;
	
	

//	@RequestMapping(value = "/getPenaltyConfigInfo", method = RequestMethod.GET)
//	@ResponseBody
//	public List<PenaltyConfig> get() throws Exception  {	
//		List<PenaltyConfig> penaltyConfigList = new ArrayList<PenaltyConfig>();	
//		penaltyConfigList = penaltyConfigService.getPenaltyConfigList();
//		return penaltyConfigList;
//	}
	
	
	@GetMapping("/getPenaltyConfigJoin")
	public List<Object> getpenaltycon() throws Exception{
		return penaltyConfigService.getPenaltyConfigJoin();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/penaltyConfig", method = RequestMethod.POST)
	@ResponseBody
	public String insertPenaltyConfig(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		PenaltyConfig penaltyConfig = new PenaltyConfig();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<PenaltyConfig> penaltConfigList = new ArrayList<PenaltyConfig>();
		penaltConfigList = (List<PenaltyConfig>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyConfig>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				penaltyConfig = penaltyConfigService.saveOrUpdatePenaltyConfig(penaltConfigList.get(0));
				
				log.info("Penalty Config Saved.");
				penaltConfigList.set(0, penaltyConfig);
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				
				penaltyConfig = penaltyConfigService.UpdatePenaltyConfigService(penaltConfigList.get(0));
					log.info("Penalty Config Updated.");
					penaltConfigList.set(0, penaltyConfig);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
				penaltyConfig = penaltyConfigService.DeletePenaltyConfigInfo(penaltConfigList.get(0));
				 log.info("Penalty Config Deleted.");
				 penaltConfigList.set(0, penaltyConfig);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				penaltConfigList = penaltyConfigService.getPenaltyConfigList();
					
				}
			
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(penaltConfigList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	
	
	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/insertPenaltyConfigInfo", method = RequestMethod.POST)
//	@ResponseBody
//	public String insertPenalty(@RequestBody String  encodedMessageString) throws Exception {
//		
//		log.info("Penalty Config Request Accepted. Request : {}" , encodedMessageString);
//		
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		System.err.print("---"+messageString+"---");
//		
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		
//		List<PenaltyConfig> penaltyConfigInfoList = new ArrayList<PenaltyConfig>();
//		penaltyConfigInfoList = (List<PenaltyConfig>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyConfig>>(){}.getType());
//		DataHeader messageHeader = message.getDataHeader();
//		String status = "Error occured";
//		List<String> messageList = new ArrayList<>();
//		String statusHeader = null;		
//		
//		try {
//			
//			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
//				
//				
//				System.err.print("-----"+penaltyConfigInfoList.get(0).getEntryUser()+" "
//						+penaltyConfigInfoList.get(0).getEntryDate()+" "
//						+penaltyConfigInfoList.get(0).getUpdateDt()+"-----");
//				
//				penaltyConfigInfoList.set(0, penaltyConfigService.saveOrUpdatePenaltyConfig(penaltyConfigInfoList.get(0)));
//				log.info("Poro Saved.");
//				if (status.indexOf("Error") > -1) {
//		    		statusHeader="error";
//					
//				}
//		    	else {
//		    		statusHeader="success";
//		    	}
//			}
//			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//		}catch (Exception e) {
//				// TODO Auto-generated catch block
//				statusHeader="error";
//				status=e.getMessage();
//				e.printStackTrace();
//				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
//			}
//		   
//		    messageList.add(statusHeader);
//		    messageList.add(status);
//		    message.setPayLoad(penaltyConfigInfoList);
//			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//			
//			return new String(encoded);
//	}
//	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/updatePenaltyConfigInfo", method = RequestMethod.POST)
//	@ResponseBody
//	public String updateContractBranchInfo(@RequestBody String  encodedMessageString) throws Exception {
//		log.info("Update Penalty config Accepted");
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		
//		/*
//		 * TO_DO:H:Need to do Message Validation
//		 */		
//		List<PenaltyConfig> penaltyConfigList = new ArrayList<PenaltyConfig>();
//		penaltyConfigList = (List<PenaltyConfig>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyConfig>>(){}.getType());
//		
//		DataHeader messageHeader = message.getDataHeader();
//		
//		
//	
//		try {
//		 if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
//				
//			 PenaltyConfig penaltyConfig = penaltyConfigService.UpdatePenaltyConfigService(penaltyConfigList.get(0));
//	        	//chargeConfigList.set(0, chargeConfig) ;
//				
//			}
//		 messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//		
//		}catch (Exception e) {			
//		
//				log.error(e.getLocalizedMessage());
//				//e.printStackTrace();
//				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
//				
//	
//		}
//			message.setPayLoad(penaltyConfigList);
//			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//						return new String(encoded);
//			}
// 
//    @SuppressWarnings("unchecked")
//	@RequestMapping(value = "/deletePenaltyConfigInfo", method = RequestMethod.POST)
//	@ResponseBody
//	public String deletePoro(@RequestBody String  encodedMessageString) throws Exception {
//		log.info("Delete Penalty config info");
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		
//		/*
//		 * TO_DO:H:Need to do Message Validation
//		 */
//		
//		List<PenaltyConfig> penaltyConfigList = new ArrayList<PenaltyConfig>();
//		penaltyConfigList = (List<PenaltyConfig>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyConfig>>(){}.getType());
//		
//		DataHeader messageHeader = message.getDataHeader();
//		
//		
//	
//		try {
//		 if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
//				
//			 PenaltyConfig penaltyConfig = penaltyConfigService.DeletePenaltyConfigInfo(penaltyConfigList.get(0));
//	        	//chargeConfigList.set(0, chargeConfig) ;
//				
//			}
//		 messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//		
//		}catch (Exception e) {
//			
//		
//				log.error(e.getLocalizedMessage());
//				//e.printStackTrace();
//				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
//				
//		}
//			message.setPayLoad(penaltyConfigList);
//			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//			
//			return new String(encoded);
//		
//	}

	
	

}
