package com.sbl.spms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContSecurityMoney;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.model.VendorServiceType;
import com.sbl.spms.service.ContractInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class ContractInfoController {
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private ContractInfoService contractInfoService;
	@Value("${file.store.location}")
	private String fileStoreLocation;

	@RequestMapping(value = "/getContractInfo", method = RequestMethod.GET)
	@ResponseBody
	public List<ContractInfo> get() throws Exception  {
	
		List<ContractInfo> contractInfoList = new ArrayList<ContractInfo>();	
		contractInfoList = contractInfoService.getContractInfoList();
		return contractInfoList;
	}
	@GetMapping("/getcontractInfoJoin")
	public List<Object> getcon() throws Exception{
		return contractInfoService.getcontractInfoJoin();
	}

	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contractInfo", method = RequestMethod.POST)
	@ResponseBody
	public String requestHandler(@RequestBody String encodedMessageString) throws Exception {
		log.info("insert contract info Accepted. Request : {}" , encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<ContractInfo> payloadList = new ArrayList<ContractInfo>();
		payloadList = (List<ContractInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<ContractInfo>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {

			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				payloadList.set(0, contractInfoService.saveOrUpdateContractInfo(payloadList.get(0)));
				log.info("Contract Info  Saved.");

			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				payloadList.set(0, contractInfoService.UpdateContractInfoService(payloadList.get(0)));
				log.info("Contract Info Updated.");

			} else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				payloadList.set(0, contractInfoService.DeleteContractInfo(payloadList.get(0)));
				log.info("Contract Info Deleted.");

			} else if (messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {

				payloadList = contractInfoService.getContractInfoList();

			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			log.error("", e);
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(payloadList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	
		@GetMapping("/contractInfoList")
		public List<ContractInfo> getContractId() throws Exception{
			return contractInfoService.getLabels();
		}
		
//		@GetMapping("/vendorServiceType")
//		public List<ContractInfo> getVendorServiceType() throws Exception{
//			return contractInfoService.getLabels();
//		}
		
 
		 @RequestMapping("/uploadContractInfoFile")
			public List<String> uploadFile(@RequestParam("file") MultipartFile file,@RequestParam(name="data") String jSONContractBranch, HttpServletRequest request) {
		    	
		    	ContractInfo conb = (ContractInfo) messageProcessor.getObjectFromJsonString(jSONContractBranch, new TypeToken<ContractInfo>() {}.getType());

				 Part filePart = null;
				 File fileStore = null;
				 String message = null;
				 String messageHeader = null;
				 List<String> messageList = new ArrayList<>();
				try {
					
					
					fileStore = new File(fileStoreLocation+"/ContractInfoFile", file.getOriginalFilename());
					FileUtils.writeByteArrayToFile(fileStore, file.getBytes());
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // Retrieves <input type="file" name="file">
					FileInputStream fis = null;
					boolean status=false;
				    try {
				    	fis = new FileInputStream(fileStore);
				    	message=contractInfoService.readWrite(fis,conb);
				    	if (message.indexOf("Error") > -1) {
							messageHeader="error";
							
						}
				    	else {
				    		messageHeader="success";
				    	}
					} catch (IOException e) {
						messageHeader="error";
						message=e.getMessage();
						//e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						messageHeader="error";
						message=e.getMessage();
						//e.printStackTrace();
					}
				   
				    messageList.add(messageHeader);
				    messageList.add(message);
					return messageList;

		    }

		 
		 @GetMapping("/contractID")
			public List<ContractInfo> getContractInfoList() throws Exception{
				return contractInfoService.getLabels();
			}
		
		 
		 @GetMapping("/childBycon")
			public List<Object> getChildByCont() {
			 List<Object> employeeObj = null;
				try {
					employeeObj = contractInfoService.getChildByCont();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				if(employeeObj == null) {
//					throw new RuntimeException("no data is not found.");
//				}
				return employeeObj;
			}
		

}
