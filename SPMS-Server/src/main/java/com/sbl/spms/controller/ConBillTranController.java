package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.AMCNotification;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contractbilltran;
import com.sbl.spms.model.ItemPerBill;
import com.sbl.spms.service.AMCNotificationService;
import com.sbl.spms.service.ConBillTranService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class ConBillTranController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private ConBillTranService service;


	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/conbilltran", method = RequestMethod.POST)
	@ResponseBody
	public String billTran(@RequestBody String encodedMessageString) throws Exception {

		log.info("Bill Tran Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<Contractbilltran> contractbilltranList = new ArrayList<Contractbilltran>();
		contractbilltranList = (List<Contractbilltran>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<Contractbilltran>>() {
				}.getType());
		
		List<String> cont = new ArrayList<String>();
//		cont = (List<String>) messageProcessor.getObjectListFromPayload(message.getOthers(),
//				new TypeToken<List<String>>() {
//				}.getType());
		
		DataHeader messageHeader = message.getDataHeader();

		try {

			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				contractbilltranList.set(0, service.saveBillTran(contractbilltranList.get(0)));
				log.info("Bill Tran Saved.");

			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				contractbilltranList.set(0, service.updateBillTran(contractbilltranList.get(0)));
				log.info("Bill Tran Updated.");

			} else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				contractbilltranList.set(0, service.deleteBillTran(contractbilltranList.get(0)));
				log.info("Bill Tran  Deleted.");

			} 
			else if (messageHeader.getActionType().equals(ActionTypes.AUTH.toString())) {
				contractbilltranList.set(0, service.authBillTran(contractbilltranList.get(0)));
				log.info("Bill Tran Authorized.");

			}
			else if (messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {

				contractbilltranList = service.getContractbilltran();

			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			log.error("", e);
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(contractbilltranList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	@GetMapping("/duplicateEntry/{entry}")
	public boolean duplicateEntryExists(@PathVariable String entry) throws Exception{
		return service.getDuplicateEntryExists(entry);
	}
	
	@GetMapping("/getBillTranByEntry/{entry}")
	public Contractbilltran getAllItem(@PathVariable String entry) {
		Contractbilltran contractbilltran =  service.getBillTranByEntry(entry);
		if(contractbilltran == null) {
			throw new RuntimeException("Tran with entry: "+entry+" is not found.");
		}
		return contractbilltran;
	}
	
	@GetMapping("/getInitialBillTran")
	public List<Contractbilltran> getAllBillTranId() throws Exception{
		return service.getInitialBillTran();
	}
	
	
	@GetMapping("/getContractBillTransactionJoin")
	public List<Object> getcon() throws Exception{
		return service.getContractBillTransactionJoin();
	}
}
