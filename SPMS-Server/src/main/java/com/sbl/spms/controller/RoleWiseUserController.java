package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Menu;
import com.sbl.spms.model.MenuAssignedToRole;
import com.sbl.spms.model.Role;
import com.sbl.spms.model.RoleWiseUser;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.service.MenuAssigendToRoleService;
import com.sbl.spms.service.RoleInfoService;
import com.sbl.spms.service.RoleWiseUserService;
import com.sbl.spms.service.UserInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class RoleWiseUserController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private RoleWiseUserService roleWiseUserService;
	
	
	@Autowired
	private RoleInfoService roleInfoService;
	
	@Autowired
	private UserInfoService userInfoService;
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/roleWiseUser", method = RequestMethod.POST)
	@ResponseBody
	public String handleRoleWiseUser(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		RoleWiseUser roleWiseUser = new RoleWiseUser();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<RoleWiseUser> roleWiseUserList = new ArrayList<RoleWiseUser>();
		roleWiseUserList = (List<RoleWiseUser>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<RoleWiseUser>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				roleWiseUser = roleWiseUserService.saveOrUpdateRoleWiseUser(roleWiseUserList.get(0));
				
				log.info("Role Wise User Saved.");
				roleWiseUserList.set(0, roleWiseUser);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				roleWiseUserList = roleWiseUserService.getRoleWiseUserList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					roleWiseUser = roleWiseUserService.updateRoleWiseUser(roleWiseUserList.get(0));
						log.info("Role Wise User  Updated.");
						roleWiseUserList.set(0, roleWiseUser);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						roleWiseUser = roleWiseUserService.DeleteRoleWiseUser(roleWiseUserList.get(0));
						 log.info("Menu Info Deleted.");
						 roleWiseUserList.set(0, roleWiseUser);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(roleWiseUserList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
//	@GetMapping("/userList")
//	public List<UserInfo> getUserlist() throws Exception{
//		return userInfoService.getUserInfoList();
//	}
	
//	@GetMapping("/roleList")
//	public List<Role> getRoles() throws Exception{
//		return roleInfoService.getLabels();
//	}
//	

}
