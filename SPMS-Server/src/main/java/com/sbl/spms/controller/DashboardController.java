package com.sbl.spms.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbl.spms.model.Chart;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.service.DashboardService;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")

public class DashboardController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private DashboardService dashboardService;
	
	@GetMapping("/getConnectivityChart")
	
	public List<Chart> getConnectivityChart() throws Exception{
		return dashboardService.getConnectivityChart();
	}
//	@GetMapping("/getAMCChartValidity/{duration}")
//	
//	public List<Chart> getAMCChartValidity(@PathVariable int duration) throws Exception{
//		return dashboardService.getAMCChartValidity(duration);
//	}
	
//	@GetMapping("/getSLAChartValidity/{duration}")
//
//	public List<Chart> getSLAChartValidity(@PathVariable int duration) throws Exception{
//	return dashboardService.getSLAChartValidity(duration);
//	}
	
	@GetMapping("/getCountSLAChartValidity")

	public List<Chart> getCountSLAChartValidity() throws Exception{
	return dashboardService.getCountSLAChartValidity();
	}
	
	@GetMapping("/getConnectivityChartValidity")

	public List<Chart> getConnectivityChartValidity() throws Exception{
	return dashboardService.getConnectivityChartValidity();
	}
	
	@GetMapping("/getCountAMCChartValidity")

	public List<Chart> getCountAMCChartValidity() throws Exception{
	return dashboardService.getCountAMCChartValidity();
	}
	@GetMapping("/getCountAMCChartValidityPrevData/{date}")

	public List<Chart> getCountAMCChartValidityPrevData(@PathVariable String date) throws Exception{
		String[] splited = date.split("\\s+");
	return dashboardService.getCountAMCChartValidityPrevData(splited[0],splited[1]);
	}
	
	@GetMapping("/getCountSLAChartValidityPrevData/{date}")

	public List<Chart> getCountSLAChartValidityPrevData(@PathVariable String date) throws Exception{
		String[] splited = date.split("\\s+");
	return dashboardService.getCountSLAChartValidityPrevData(splited[0],splited[1]);
	}
	
}
