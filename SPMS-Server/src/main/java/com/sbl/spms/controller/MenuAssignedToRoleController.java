package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Menu;
import com.sbl.spms.model.MenuAssignedToRole;
import com.sbl.spms.model.Role;
import com.sbl.spms.service.MenuAssigendToRoleService;
import com.sbl.spms.service.RoleInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class MenuAssignedToRoleController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private MenuAssigendToRoleService menuAssigendToRoleService;
	
	
	@Autowired
	private RoleInfoService roleInfoService;
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertMenuAssignedToRole", method = RequestMethod.POST)
	@ResponseBody
	public String insertMenuInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		MenuAssignedToRole menuAssignedToRole = new MenuAssignedToRole();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<MenuAssignedToRole> menuAssignedToRoleList = new ArrayList<MenuAssignedToRole>();
		menuAssignedToRoleList = (List<MenuAssignedToRole>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<MenuAssignedToRole>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				menuAssignedToRole = menuAssigendToRoleService.saveOrUpdateMenuAssignedToRoleInfo(menuAssignedToRoleList.get(0));
				
				log.info("Role Info Saved.");
				menuAssignedToRoleList.set(0, menuAssignedToRole);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				menuAssignedToRoleList = menuAssigendToRoleService.getMenuAssignedToRoleList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					menuAssignedToRole = menuAssigendToRoleService.updateMenuAssignedToRoleInfo(menuAssignedToRoleList.get(0));
						log.info("Menu assigned to role Updated.");
						menuAssignedToRoleList.set(0, menuAssignedToRole);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						menuAssignedToRole = menuAssigendToRoleService.DeleteMenuAssignedToRoleInfo(menuAssignedToRoleList.get(0));
						 log.info("Menu Info Deleted.");
						 menuAssignedToRoleList.set(0, menuAssignedToRole);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(menuAssignedToRoleList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	@GetMapping("/roles")
	public List<Role> get() throws Exception{
		return roleInfoService.getLabels();
	}
	
	@GetMapping("/menuAssignedToRoleList")
	public List<MenuAssignedToRole> getRoleId() throws Exception{
		return menuAssigendToRoleService.getLabels();
	}
	
	@GetMapping("/menuAssignedToRoleJoin")
	public List<Object> getAllItem() {
		List<Object> employeeObj = null;
		try {
			employeeObj = menuAssigendToRoleService.getMenuAssignedToRole();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return employeeObj;
	
	}

}
