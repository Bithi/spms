package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.MenuAssignedToRole;
import com.sbl.spms.model.Role;
import com.sbl.spms.service.ItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class ItemTypeController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private ItemTypeService itemTypeService;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/itemType", method = RequestMethod.POST)
	@ResponseBody
	public String itemType(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		ItemType itemType = new ItemType();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<ItemType> itemTypeList = new ArrayList<ItemType>();
		itemTypeList = (List<ItemType>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<ItemType>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				itemType = itemTypeService.saveOrUpdateItemType(itemTypeList.get(0));
				
				log.info("ItemType Info Saved.");
				itemTypeList.set(0, itemType);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				itemTypeList = itemTypeService.getitemTypeList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					itemType = itemTypeService.updateItemType(itemTypeList.get(0));
						log.info("ItemType Updated.");
						itemTypeList.set(0, itemType);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						itemType = itemTypeService.DeleteItemType(itemTypeList.get(0));
						 log.info("ItemType Deleted.");
						 itemTypeList.set(0, itemType);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(itemTypeList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	
	
	@GetMapping("/itemTypeList")
	public List<ItemType> getRoleId() throws Exception{
		return itemTypeService.getLabels();
	}
}
