package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.BudgetBillEntry;
import com.sbl.spms.model.BudgetMaster;
import com.sbl.spms.model.BudgetSectorwiseAllocation;
import com.sbl.spms.service.BudgetBillEntryService;
import com.sbl.spms.service.BudgetSectorwiseAllocationService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class BudgetSectorwiseAllocationController {

	@Autowired
	private DataProcessor<?> messageProcessor;

	@Autowired
	private BudgetSectorwiseAllocationService budgetSectorwiseAllocationService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/budgetAllocationEntry", method = RequestMethod.POST)
	@ResponseBody
	public String budgetAllocationEntry(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		BudgetSectorwiseAllocation budgetAllocationEntry = new BudgetSectorwiseAllocation();
		// ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<BudgetSectorwiseAllocation> budgetAllocationEntryList = new ArrayList<BudgetSectorwiseAllocation>();

		budgetAllocationEntryList = (List<BudgetSectorwiseAllocation>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<BudgetSectorwiseAllocation>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {
			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {


				budgetAllocationEntry = budgetSectorwiseAllocationService.saveBudgetAllocationEntry(budgetAllocationEntryList.get(0));
				
				log.info("Info Saved.");
				budgetAllocationEntryList.set(0, budgetAllocationEntry);
			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				budgetAllocationEntryList.set(0,
						budgetSectorwiseAllocationService.UpdateBudgetAllocationInfoService(budgetAllocationEntryList.get(0)));
				log.info("Info Updated.");
			}

//			else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
//				budgetAllocationEntryList.set(0, budgetSectorwiseAllocationService.DeleteBudgetMasterInfo(budgetAllocationEntryList.get(0)));
//				log.info("Info Deleted.");
//			}
			
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(budgetAllocationEntryList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	@GetMapping("/getBudgetSectorwiseAllocationList")
	public List<Object> getSectorList() throws Exception {
		return budgetSectorwiseAllocationService.getSectorwiseBudgetAllocationList();
	}
	
	@GetMapping("/getBudgetAllocationList")
	public List<Object> getBudgetAllocationList() throws Exception {
		return budgetSectorwiseAllocationService.getBudgetAllocationInfo();
	}
	
	@GetMapping("/getBudgetSectorList")
	public List<Object> getBudgetSectorList() throws Exception {
		return budgetSectorwiseAllocationService.getBudgetSectorInfo();
	}
	
//	@GetMapping("/getCADAllocationByYear")
//	public Double getCADAllocationByYear() throws Exception {
//		return budgetSectorwiseAllocationService.getBudgetSectorInfo();
//	}
	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/getCADAllocationByYear", method = RequestMethod.POST)
//	@ResponseBody
//	public String getCADAllocationByYear(@RequestBody String encodedMessageString) throws Exception {
//
//		log.info("Request Accepted. Request : {}", encodedMessageString);
//
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		BudgetSectorwiseAllocation budgetAllocationEntry = new BudgetSectorwiseAllocation();
//		// ItemPerBill itemPerBill = new ItemPerBill();
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
//		}.getType());
//
////		List<BudgetSectorwiseAllocation> budgetAllocationEntryList = new ArrayList<BudgetSectorwiseAllocation>();
//
//		List<JSONObject> budgetAllocationEntryList = (List<JSONObject>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
//				new TypeToken<List<JSONObject>>() {
//				}.getType());
//		DataHeader messageHeader = message.getDataHeader();
//
//		double amount = 0;
//		try {
//			
//			amount = budgetSectorwiseAllocationService
//					.getCADAllocationByYear(budgetAllocationEntryList.get(0).getString("year"));
//						
//			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
//					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
//					"404", e.getMessage());
//		}
//
//		Map<String, String> json = new HashMap<String, String>();
//		json.put("amount", String.valueOf(amount));
//		
//		message.setPayLoad(Arrays.asList(json));
//		
//		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//
//		return new String(encoded);
//	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getBudgetAllocationByYear", method = RequestMethod.POST)
	@ResponseBody
	public String getBudgetAllocationByYear(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		BudgetSectorwiseAllocation budgetAllocationEntry = new BudgetSectorwiseAllocation();
		// ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<BudgetSectorwiseAllocation> budgetAllocationEntryList = new ArrayList<BudgetSectorwiseAllocation>();

		budgetAllocationEntryList = (List<BudgetSectorwiseAllocation>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<BudgetSectorwiseAllocation>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {
			
			
			budgetAllocationEntry.setAmount(budgetSectorwiseAllocationService
					.getBudgetAllocationByYear(budgetAllocationEntryList.get(0).getYear()));
			
			budgetAllocationEntryList.set(0, budgetAllocationEntry);
						
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}
		
		message.setPayLoad(budgetAllocationEntryList);
		
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}


	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getCADAllocationByYear", method = RequestMethod.POST)
	@ResponseBody
	public String getCADAllocationByYear(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		BudgetSectorwiseAllocation budgetAllocationEntry = new BudgetSectorwiseAllocation();
		// ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<BudgetSectorwiseAllocation> budgetAllocationEntryList = new ArrayList<BudgetSectorwiseAllocation>();

		budgetAllocationEntryList = (List<BudgetSectorwiseAllocation>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<BudgetSectorwiseAllocation>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {
			
			
			budgetAllocationEntry.setAmount(budgetSectorwiseAllocationService
					.getCADAllocationByYear(budgetAllocationEntryList.get(0).getYear()));
			
			budgetAllocationEntryList.set(0, budgetAllocationEntry);
						
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}
		
		message.setPayLoad(budgetAllocationEntryList);
		
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getAvailableFundByYear", method = RequestMethod.POST)
	@ResponseBody
	public String getAvailableFundByYear(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		BudgetSectorwiseAllocation budgetAllocationEntry = new BudgetSectorwiseAllocation();
		// ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<BudgetSectorwiseAllocation> budgetAllocationEntryList = new ArrayList<BudgetSectorwiseAllocation>();

		budgetAllocationEntryList = (List<BudgetSectorwiseAllocation>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<BudgetSectorwiseAllocation>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		double availableCADFund = 0;
		
		double usedFund = 0;
		
		double availableFund = 0;

		try {
			
			availableCADFund = budgetSectorwiseAllocationService
					.getCADAllocationByYear(budgetAllocationEntryList.get(0).getYear());
			
			usedFund = budgetSectorwiseAllocationService
					.getBudgetAllocationByYear(budgetAllocationEntryList.get(0).getYear());
			
			
			availableFund = availableCADFund - usedFund;
			
			if(availableFund<0) {
				availableFund = 0;
			}
			
			budgetAllocationEntry.setAmount(availableFund);
			
			budgetAllocationEntryList.set(0, budgetAllocationEntry);
						
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}
		
		message.setPayLoad(budgetAllocationEntryList);
		
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	@GetMapping("/getSectorAndYearWiseAllocation")
	public List<Object> getSectorAndYearWiseAllocation() throws Exception {
		return budgetSectorwiseAllocationService.getSectorAndYearWiseAllocation();
	}
	

	
	@GetMapping("/getYearWiseAllocation")
	public List<Object> getYearWiseAllocation() throws Exception {
		return budgetSectorwiseAllocationService.getYearWiseAllocation();
	}
	
	
	@GetMapping("/getYearWiseCADFund")
	public List<Object> getYearWiseCADFund() throws Exception {
		return budgetSectorwiseAllocationService.getYearWiseCADFund();
	}

	
	

}
