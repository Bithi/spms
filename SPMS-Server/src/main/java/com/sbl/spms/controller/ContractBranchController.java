
package com.sbl.spms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.service.ContractBranchService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class ContractBranchController {
	
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private ContractBranchService contractBranchInfoService;
	@Value("${file.store.location}")
	private String fileStoreLocation;
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contractBranch", method = RequestMethod.POST)
	@ResponseBody
	public String insertContractBranchInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		ContractBranch contractBranch = new ContractBranch();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<ContractBranch> contractBranchInfoList = new ArrayList<ContractBranch>();
		contractBranchInfoList = (List<ContractBranch>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<ContractBranch>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
			    contractBranch = contractBranchInfoService.saveOrUpdateContractBranch(contractBranchInfoList.get(0));
				
				log.info("Contract Branch Saved.");
				contractBranchInfoList.set(0, contractBranch);
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				
				 contractBranch = contractBranchInfoService.UpdateContractBranch(contractBranchInfoList.get(0));
					log.info("Contract Branch Updated.");
					contractBranchInfoList.set(0, contractBranch);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
				 contractBranch = contractBranchInfoService.DeleteContractBranchInfo(contractBranchInfoList.get(0));
				 log.info("Contract Branch Deleted.");
				 contractBranchInfoList.set(0, contractBranch);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				contractBranchInfoList = contractBranchInfoService.getContactBranchList();
					
				}
			
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(contractBranchInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}

	    @RequestMapping("/uploadContractBranchFile")
		public List<String> uploadFile(@RequestParam("file") MultipartFile file,@RequestParam(name="data") String jSONContractBranch, HttpServletRequest request) {
	    	
	    	ContractBranch conb = (ContractBranch) messageProcessor.getObjectFromJsonString(jSONContractBranch, new TypeToken<ContractBranch>() {}.getType());

			 Part filePart = null;
			 File fileStore = null;
			 String message = null;
			 String messageHeader = null;
			 List<String> messageList = new ArrayList<>();
			try {
				
				
				fileStore = new File(fileStoreLocation+"/ContractBranchFile", file.getOriginalFilename());
				FileUtils.writeByteArrayToFile(fileStore, file.getBytes());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // Retrieves <input type="file" name="file">
				FileInputStream fis = null;
				boolean status=false;
			    try {
			    	fis = new FileInputStream(fileStore);
			    	message=contractBranchInfoService.readWrite(fis,conb);
			    	if (message.indexOf("Error") > -1) {
						messageHeader="error";
						
					}
			    	else {
			    		messageHeader="success";
			    	}
				} catch (IOException e) {
					messageHeader="error";
					message=e.getMessage();
					//e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					messageHeader="error";
					message=e.getMessage();
					//e.printStackTrace();
				}
			   
			    messageList.add(messageHeader);
			    messageList.add(message);
				return messageList;

	    }
	    
	    @GetMapping("/getcontractBranchJoin")
		public List<Object> getcon() throws Exception{
			return contractBranchInfoService.getcontractBranchJoin();
		}
	    
	    @SuppressWarnings("unchecked")
		@RequestMapping(value = "/branchList", method = RequestMethod.POST)
		@ResponseBody
		public String getContractBranchList(@RequestBody String  encodedMessageString) {
			log.info("Penalty Process Request Accepted. Request : {}" , encodedMessageString);
			byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
			String messageString = new String(decoded);
			
			Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
			String status = null;
			
			/*
			 * TO_DO:H:Need to do Message Validation
			 */
			List<ContractBranch> contractBranchList = new ArrayList<ContractBranch>();
			DataHeader messageHeader = message.getDataHeader();
			
			try {
				if(messageHeader.getActionType().equals(ActionTypes.BRANCH_LIST.toString())) {
					contractBranchList = (List<ContractBranch>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<ContractBranch>>(){}.getType());
					contractBranchList= contractBranchInfoService.getBranchList();
				}
				
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), status);
				
			}
			catch (Exception e) {
				log.error(e.getLocalizedMessage());
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
				contractBranchList=null;
				
			}
			if(messageHeader.getActionType().equals(ActionTypes.BRANCH_LIST.toString())) {
				message.setPayLoad(contractBranchList);
			}
			
			byte[] encoded = Base64.getEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
			
		}

}
