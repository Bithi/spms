package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.Contractbilltax;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.service.ContbillchgheadService;
import com.sbl.spms.service.ContractbilltaxService;
import com.sbl.spms.service.SubItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
class ContractbilltaxController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	
	@Autowired
	private ContractbilltaxService contractbilltaxService;
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contractbilltax", method = RequestMethod.POST)
	@ResponseBody
	public String ContractBillTax(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Contractbilltax contTax = new Contractbilltax();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Contractbilltax> contList = new ArrayList<Contractbilltax>();
		contList = (List<Contractbilltax>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Contractbilltax>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				contTax = contractbilltaxService.saveOrUpdateContBillTax(contList.get(0));
				
				log.info("Info Saved.");
				contList.set(0, contTax);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				contList = contractbilltaxService.getContBillTax();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					contTax = contractbilltaxService.updateContBillTax(contList.get(0));
						log.info("Info Updated.");
						contList.set(0, contTax);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						contTax = contractbilltaxService.deleteContBillTax(contList.get(0));
						 log.info("Info Deleted.");
						 contList.set(0, contTax);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(contList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
}
