package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contractbilltax;
import com.sbl.spms.model.Contyearwiserate;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.service.ContbillchgheadService;
import com.sbl.spms.service.ContractbillentryService;
import com.sbl.spms.service.ContractbilltaxService;
import com.sbl.spms.service.ContyearwiserateService;
import com.sbl.spms.service.SubItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class ContyearwiserateController {
	
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	
	@Autowired
	private ContyearwiserateService contyearwiseService;
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contyearwiserate", method = RequestMethod.POST)
	@ResponseBody
	public String ContYearWiseRate(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Contyearwiserate contRate = new Contyearwiserate();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Contyearwiserate> contList = new ArrayList<Contyearwiserate>();
		contList = (List<Contyearwiserate>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Contyearwiserate>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				contRate = contyearwiseService.saveOrUpdateContYearwiseRate(contList.get(0));
				
				log.info("Info Saved.");
				contList.set(0, contRate);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				contList = contyearwiseService.getContYearwiseRate();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					contRate = contyearwiseService.updateContyearwiserate(contList.get(0));
						log.info("Info Updated.");
						contList.set(0, contRate);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						contRate = contyearwiseService.deleteContYearwiseRate(contList.get(0));
						 log.info("Info Deleted.");
						 contList.set(0, contRate);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(contList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	@GetMapping("/contforrate")
	public List<ContractInfo> get() throws Exception{
		return contyearwiseService.getAllId();
	}
	
	
	@GetMapping("/getContYearWiseRate")
	public List<Object> getcon() throws Exception{
		return contyearwiseService.getContYearWiseRate();
	}

}
