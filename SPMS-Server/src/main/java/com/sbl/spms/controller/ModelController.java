package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.MenuAssignedToRole;
import com.sbl.spms.model.Model;
import com.sbl.spms.model.Role;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.service.BrandService;
import com.sbl.spms.service.ItemTypeService;
import com.sbl.spms.service.ModelService;
import com.sbl.spms.service.SubItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class ModelController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	
	@Autowired
	private ModelService modelService;
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/model", method = RequestMethod.POST)
	@ResponseBody
	public String itemType(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Model model = new Model();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Model> modelList = new ArrayList<Model>();
		modelList = (List<Model>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Model>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				model = modelService.saveOrUpdateModel(modelList.get(0));
				if(model==null) {
					
					modelList.set(0, model);
				}
				else {
					log.info("model Info Saved.");
					modelList.set(0, model);
				}
				
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				modelList = modelService.getModelList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					model = modelService.updateModel(modelList.get(0));
					if(model==null) {
						
						modelList.set(0, model);
					}
					else {
						log.info("model Info Updated.");
						modelList.set(0, model);
					}
					
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						model = modelService.deleteModel(modelList.get(0));
						 log.info("Model Deleted.");
						 modelList.set(0, model);
					}
							
			
			if(modelList.get(0)==null) {
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_DUPLICATE.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
		    	
			}
			else
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(modelList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	  @GetMapping("/getmodelBrandJoin")
			public List<Object> getcon() throws Exception{
				return modelService.getmodelBrandJJoin();
			}
	  
	 
	 
}
