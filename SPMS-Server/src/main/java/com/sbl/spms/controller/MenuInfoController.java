package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Menu;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.service.MenuInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class MenuInfoController {
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private MenuInfoService menuInfoService;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertMenuInfo", method = RequestMethod.POST)
	@ResponseBody
	public String insertMenuInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Menu menuInfo = new Menu();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Menu> menuInfoList = new ArrayList<Menu>();
		menuInfoList = (List<Menu>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Menu>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				menuInfo = menuInfoService.saveOrUpdateMenuInfo(menuInfoList.get(0));
				
				log.info("Menu Info Saved.");
				menuInfoList.set(0, menuInfo);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				menuInfoList = menuInfoService.getMenuList();
					
				}
        
        else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
            
            menuInfo = menuInfoService.updateMenuInfo(menuInfoList.get(0));
                log.info("Role Updated.");
                menuInfoList.set(0, menuInfo);
                
            }else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
                
                menuInfo = menuInfoService.DeleteMenuInfo(menuInfoList.get(0));
                 log.info("Menu Info Deleted.");
                 menuInfoList.set(0, menuInfo);
                    
                }

						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(menuInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	@GetMapping("/menus")
	public List<Object> get() throws Exception{
		return menuInfoService.getParentMenu();
	}
	
	@GetMapping("/menuList")
	public List<Menu> getRoleId() throws Exception{
		return menuInfoService.getLabels();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/showMenu", method = RequestMethod.POST)
	@ResponseBody
	public String showMenu(@RequestBody String  encodedMessageString) throws Exception{
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		UserInfo userInfo = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Menu> menuInfoList = new ArrayList<Menu>();
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		userInfoList = (List<UserInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<UserInfo>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SHOW_MENU.toString())) {
				
				menuInfoList = menuInfoService.showMenu(userInfoList.get(0));
					
				}
			else
				menuInfoList=null;
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(menuInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
}
