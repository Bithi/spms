package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.DownTime;
import com.sbl.spms.model.PenaltyCalc;
import com.sbl.spms.model.PenaltyProcess;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.Vendor;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.service.ContractBranchService;
import com.sbl.spms.service.DowntimeService;
import com.sbl.spms.service.PenaltyService;
import com.sbl.spms.service.UserInfoService;
import com.sbl.spms.service.VendorInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class PenaltyController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private PenaltyService penaltyService;
	@Autowired
	private VendorInfoService vendorService;
	@Autowired
	private ContractBranchService contractBranchService;
	
	@Autowired
	private DowntimeService dataUploadService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/penaltyProcess", method = RequestMethod.POST)
	@ResponseBody
	public String handlepenaltyProcessRequest(@RequestBody String  encodedMessageString) {
		log.info("Penalty Process Request Accepted. Request : {}" , encodedMessageString);
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		PenaltyCalc penCalc= new PenaltyCalc();
		List<PenaltyCalc> penaltyCalcList = new ArrayList<PenaltyCalc>();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		String status = "";
		
		/*
		 * TO_DO:H:Need to do Message Validation
		 */
		List<PenaltyProcess> penaltyProcessList = new ArrayList<PenaltyProcess>();
		DataHeader messageHeader = message.getDataHeader();
		
		try {
			if(messageHeader.getActionType().equals(ActionTypes.PENALTY_PROCESS.toString())) {
				penaltyProcessList = (List<PenaltyProcess>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyProcess>>(){}.getType());
				status= penaltyService.process(penaltyProcessList.get(0));
			}
			if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				penaltyCalcList = penaltyService.getPenaltyCalcList();
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				penaltyCalcList = (List<PenaltyCalc>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyCalc>>(){}.getType());
				penCalc = penaltyService.deletePenalty(penaltyCalcList.get(0));
				 log.info("Penalty Info Deleted.");
				 penaltyCalcList.set(0, penCalc);
			}
			
			
			if(status.indexOf("FAIL") > -1) {
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", status);
			}
			else {
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), status);
			}
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage());
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			penaltyCalcList=null;
			penaltyProcessList=null;
			
		}
		if(messageHeader.getActionType().equals(ActionTypes.PENALTY_PROCESS.toString())) {
			message.setPayLoad(penaltyProcessList);
		}
		else  {
			message.setPayLoad(penaltyCalcList);
		}
		
		byte[] encoded = Base64.getEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
		
		return new String(encoded);
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/penaltycalc", method = RequestMethod.POST)
	@ResponseBody
	public String handlePenaltyCalcInfoRequest(@RequestBody String  encodedMessageString) {
		log.info("DownTime Info Request Accepted");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		/*
		 * TO_DO:H:Need to do Message Validation
		 */
		
		List<PenaltyCalc> penaltyCalcList = new ArrayList<PenaltyCalc>();
		penaltyCalcList = (List<PenaltyCalc>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyCalc>>(){}.getType());
		
		DataHeader messageHeader = message.getDataHeader();
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				penaltyCalcList = dataUploadService.getPenaltyCalcList();
				
				
			}
			/*else if(messageHeader.getActionType().equals(ActionTypes.SELECT.toString())) {
				
				BranchInfo branch = branchService.getBranchInfo(branchInfoList.get(0).getBrCode());
				branchInfoList.set(0, branch) ;
				
				log.info("Branch Saved.");
			}*/
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
			
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			
		}
		message.setPayLoad(penaltyCalcList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
		
		return new String(encoded);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getNetPenaltyForBillPay", method = RequestMethod.POST)
	@ResponseBody
	public String getNetPenaltyForBillPay(@RequestBody String  encodedMessageString) {
		log.info("NetPenalty Request Accepted");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		/*
		 * TO_DO:H:Need to do Message Validation
		 */
		
		List<PenaltyCalc> penaltyCalcList = new ArrayList<PenaltyCalc>();
		penaltyCalcList = (List<PenaltyCalc>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyCalc>>(){}.getType());
		
		DataHeader messageHeader = message.getDataHeader();
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				penaltyCalcList.get(0).setNetPenalty(penaltyService.getNetPenaltyForBillPay(penaltyCalcList.get(0)));
				
				
			}
			/*else if(messageHeader.getActionType().equals(ActionTypes.SELECT.toString())) {
				
				BranchInfo branch = branchService.getBranchInfo(branchInfoList.get(0).getBrCode());
				branchInfoList.set(0, branch) ;
				
				log.info("Branch Saved.");
			}*/
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
			
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			
		}
		message.setPayLoad(penaltyCalcList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
		
		return new String(encoded);
	}
	
	
}
