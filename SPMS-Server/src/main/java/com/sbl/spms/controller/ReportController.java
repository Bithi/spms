package com.sbl.spms.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.integration.handler.MessageProcessor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.model.DownTime;
import com.sbl.spms.model.DowntimeCalcRep;
import com.sbl.spms.model.FindValidEnd;
import com.sbl.spms.model.PenaltyCalc;
import com.sbl.spms.model.PenaltyReport;
import com.sbl.spms.service.ReportService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;


@RestController
@RequestMapping("/v1")
@CrossOrigin(origins="*")
@Slf4j
public class ReportController {
	@Autowired
	private DataProcessor<?> dataProcessor;
	@Autowired
	private ReportService reportService;
	
//	@RequestMapping(value = "/DownTimeCalcReport", method = RequestMethod.POST)
//	@ResponseBody
//	public void handleDownTimeCalcReportRequest(@RequestBody String encodedMessageString,HttpServletResponse response) throws JRException, IOException {
//		log.info("Report Request Accepted.");
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		DownTime reportCriteria = (DownTime) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<DownTime>>(){}.getType()).get(0);
//		reportService.getDowntimeReport(reportCriteria, response.getOutputStream());
//		
//	}
	
	@RequestMapping(value = "/DownTimeCalcReport", method = RequestMethod.POST)
	@ResponseBody
	public void handleDownTimeCalcReportRequestOne(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("Report Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		DowntimeCalcRep reportCriteria = (DowntimeCalcRep) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<DowntimeCalcRep>>(){}.getType()).get(0);
		reportService.getDowntimeReport(reportCriteria, response.getOutputStream());
	}
	
	
	@RequestMapping(value = "/PenaltyCalcReport", method = RequestMethod.POST)
	@ResponseBody
	public void handlePenaltyCalcReportRequestOne(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("Penalty Report Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyCalc reportCriteria = (PenaltyCalc) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyCalc>>(){}.getType()).get(0);
		reportService.getPenaltyCalcReport(reportCriteria, response.getOutputStream());
	}
	
	
	@RequestMapping(value = "/penaltyReport", method = RequestMethod.POST)
	@ResponseBody
	public void penaltyReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("Penalty Report Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getPenaltyReport(reportCriteria, response.getOutputStream());
	}
	
	@RequestMapping(value = "/AmcReport", method = RequestMethod.POST)
	@ResponseBody
	public void amcReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("Amc Report Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getAmcReport(reportCriteria, response.getOutputStream());
	}
	
	@RequestMapping(value = "/ConnectivityReport", method = RequestMethod.POST)
	@ResponseBody
	public void connectivityReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("Connectivity Report Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getConnectivityReport(reportCriteria, response.getOutputStream());
	}
	
	
	
	@RequestMapping(value = "/findValidEnd", method = RequestMethod.POST)
	@ResponseBody
	public void contractValidityEndReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("Contract Validity End Report Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		FindValidEnd reportCriteria = (FindValidEnd) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<FindValidEnd>>(){}.getType()).get(0);
		reportService.getContractValidityEndReport(reportCriteria, response.getOutputStream());
	}
	
	@RequestMapping(value = "/AmcExReport", method = RequestMethod.POST)
	@ResponseBody
	public void amcExpirationReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("amcExpirationReport  Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getAmcExpirationReport(reportCriteria, response.getOutputStream());
	}
	@RequestMapping(value = "/AmcExPrevReport", method = RequestMethod.POST)
	@ResponseBody
	public void amcExpirationPrevReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("amcExpirationReport  Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getAmcExpirationPrevReport(reportCriteria, response.getOutputStream());
	}
	
	@RequestMapping(value = "/SlaExReport", method = RequestMethod.POST)
	@ResponseBody
	public void slaExpirationReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("slaExpirationReport  Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getSlaExpirationReport(reportCriteria, response.getOutputStream());
	}
	
	@RequestMapping(value = "/SlaExPrevReport", method = RequestMethod.POST)
	@ResponseBody
	public void slaExpirationPrevReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("slaExpirationReport  Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getSlaExpirationPrevReport(reportCriteria, response.getOutputStream());
	}
	
	
	@RequestMapping(value = "/ConnExReport", method = RequestMethod.POST)
	@ResponseBody
	public void connExpirationReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("ConnExpirationReport  Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getConnExpirationReport(reportCriteria, response.getOutputStream());
	}
	
	@RequestMapping(value = "/ServiceReport", method = RequestMethod.POST)
	@ResponseBody
	public void getServiceReport(@RequestBody String encodedMessageString, HttpServletResponse response) throws JRException, IOException {
		log.info("slaExpirationReport  Request Accepted.");
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) dataProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		PenaltyReport reportCriteria = (PenaltyReport) dataProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PenaltyReport>>(){}.getType()).get(0);
		reportService.getServiceReport(reportCriteria, response.getOutputStream());
	}
	
	
}
