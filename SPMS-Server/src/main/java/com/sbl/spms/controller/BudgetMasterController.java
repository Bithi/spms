package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.BudgetBillEntry;
import com.sbl.spms.model.BudgetMaster;
import com.sbl.spms.service.BudgetMasterService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class BudgetMasterController {

	@Autowired
	private DataProcessor<?> messageProcessor;

	@Autowired
	private BudgetMasterService budgetMasterService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/budgetMasterEntry", method = RequestMethod.POST)
	@ResponseBody
	public String BudgetBillEntry(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		BudgetMaster budgetMasterEntry = new BudgetMaster();
		// ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<BudgetMaster> budgetMasterEntryList = new ArrayList<BudgetMaster>();

		budgetMasterEntryList = (List<BudgetMaster>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<BudgetMaster>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {
			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {


				budgetMasterEntry = budgetMasterService.saveBudgetMasterEntry(budgetMasterEntryList.get(0));
				
				log.info("Info Saved.");
				budgetMasterEntryList.set(0, budgetMasterEntry);
			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				budgetMasterEntryList.set(0,
						budgetMasterService.UpdateBudgetMasterInfoService(budgetMasterEntryList.get(0)));
				log.info("Info Updated.");
			}

			else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				budgetMasterEntryList.set(0, budgetMasterService.DeleteBudgetMasterInfo(budgetMasterEntryList.get(0)));
				log.info("Info Deleted.");
			}
			
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(budgetMasterEntryList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}

	
	
	
	
	@GetMapping("/getBudgetMasterList")
	public List<Object> getcon() throws Exception {
		return budgetMasterService.getBudgetMasterInfo();
	}
//	
//	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/getBillAmountByContract", method = RequestMethod.POST)
//	@ResponseBody
//	public BudgetBillEntry getBillAmountByContract(@RequestBody String encodedMessageString) throws Exception{
//		
//		log.info("Request Accepted. Request : {}", encodedMessageString);
//
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		BudgetBillEntry budgetBillEntry = new BudgetBillEntry();
//		// ItemPerBill itemPerBill = new ItemPerBill();
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
//		}.getType());
//
//		List<BudgetBillEntry> budgetBillEntryList = new ArrayList<BudgetBillEntry>();
//
//		budgetBillEntryList = (List<BudgetBillEntry>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
//				new TypeToken<List<BudgetBillEntry>>() {
//				}.getType());
//		DataHeader messageHeader = message.getDataHeader();
//				
//		budgetBillEntry.setBudget_bill_amnt_by_cont(budgetMasterService.getTotalBillPerContract(budgetBillEntryList.get(0).getCont_id()));
//		
//		//In case CONT_BILL_ENTRY TABLE IS CONSIDERED FOR TOTAL BILL CALCULATION
//		//budgetBillEntry = budgetbillentryService.getPerContractBill(budgetBillEntryList.get(0).getCont_id());
//		
//		
//		return budgetBillEntry;
//		
//	}
	
//	@GetMapping("/getBudgetBillEnList")
//	public List<BudgetBillEntry> getBudgetBillEntry() throws Exception {
//		return budgetbillentryService.getBudgetBillEntryInfo();
//	}
	
//	@GetMapping("/getBudgetBillEntryList/{contractId}")
//	public List<BudgetBillEntry> getBudgetBillEntryByCont(@PathVariable String contractId) throws Exception {
//		List<BudgetBillEntry> employeeObj = null;
//		try {
//			employeeObj = budgetMasterService.getBudgetBillEntryByContract(contractId);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if(employeeObj == null) {
//			throw new RuntimeException("Budget Bill list with id: "+contractId+" is not found.");
//		}
//		return employeeObj;
//	}
//	
//	@GetMapping("/getBudgetBillEntry/{refId}")
//	public List<Object> getBudgetBillEntry(@PathVariable String refId) throws Exception {
//		List<Object> employeeObj = null;
//		try {
//			employeeObj = budgetMasterService.getBudgetBillEntryByRef(refId);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if(employeeObj == null) {
//			throw new RuntimeException("Budget Bill with id: "+refId+" is not found.");
//		}
//		return employeeObj;
//	}
}

