package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.BudgetBillPayment;
import com.sbl.spms.service.BudgetBillEntryService;
import com.sbl.spms.service.BudgetBillPaymentService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class BudgetBillPaymentController {

	@Autowired
	private DataProcessor<?> messageProcessor;

	@Autowired
	private BudgetBillPaymentService budgetbillpaymentService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/budgetBillPayment", method = RequestMethod.POST)
	@ResponseBody
	public String BudgetBillPayment(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		BudgetBillPayment budgetBillPayment = new BudgetBillPayment();
		// ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<BudgetBillPayment> budgetBillPaymentList = new ArrayList<BudgetBillPayment>();

		budgetBillPaymentList = (List<BudgetBillPayment>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<BudgetBillPayment>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {
			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {

				budgetBillPayment = budgetbillpaymentService.saveBudgetBillPayment(budgetBillPaymentList.get(0));

				log.info("Payment Info Saved.");
				budgetBillPaymentList.set(0, budgetBillPayment);
			}

						
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(budgetBillPaymentList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}

	@GetMapping("/getBudgetBillPaymentList")
	public List<Object> getBudgetBillPayment() throws Exception {
		return budgetbillpaymentService.getBudgetBillPaymentJoin();
	}
	

	
	 @GetMapping("/getBillPaymentAmount/{in_bill_amount}/{in_vat}/{in_inclusive}/{in_with_vat}/{in_tds}/{in_with_tds}/{in_penalty_amt}/{in_invoice}/{in_fiscal_year}")
	 	
	public String getBillPaymentAmount(@PathVariable Double in_bill_amount,@PathVariable int in_vat,@PathVariable int in_inclusive, @PathVariable int in_with_vat,@PathVariable int in_tds,@PathVariable int in_with_tds, @PathVariable Double in_penalty_amt,@PathVariable String in_invoice,@PathVariable String in_fiscal_year) throws Exception {
		return budgetbillpaymentService.getPaymentAmount(in_bill_amount,in_vat,in_inclusive,in_with_vat,in_tds,in_with_tds,in_penalty_amt,in_invoice,in_fiscal_year);
	}
	

}
