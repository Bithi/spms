package com.sbl.spms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.service.UserInfoService;
import com.sbl.spms.service.VendorInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class VendorInfoController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private VendorInfoService vendorInfoService;
	@Value("${file.store.location}")
	private String fileStoreLocation;
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vendorInfo", method = RequestMethod.POST)
	@ResponseBody
	public String insertVendorInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		VendorInfo vendorInfo = new VendorInfo();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<VendorInfo> vendorInfoList = new ArrayList<VendorInfo>();
		vendorInfoList = (List<VendorInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<VendorInfo>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				vendorInfo = vendorInfoService.saveOrUpdateUserInfo(vendorInfoList.get(0));
				
				log.info("Vendor Info Saved.");
				vendorInfoList.set(0, vendorInfo);
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				
				vendorInfo = vendorInfoService.saveOrUpdateUserInfox(vendorInfoList.get(0));
					log.info("Vendor Info Updated.");
					vendorInfoList.set(0, vendorInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
				vendorInfo = vendorInfoService.deleteAllVendorInfo(vendorInfoList.get(0));
				 log.info("Vendor Info Deleted.");
				 vendorInfoList.set(0, vendorInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				vendorInfoList = vendorInfoService.getVendorInfoList();
					
				}
			
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(vendorInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}

	
	
	

	
	@GetMapping("/vendors")
	public List<VendorInfo> get() throws Exception{
		return vendorInfoService.getLabels();
	}
	
	
	@GetMapping("/contract/{vendorId}")
	public List<ContractInfo> getAll(@PathVariable String vendorId) {
		System.err.print("vendorId"+vendorId+"vendorId");
		List<ContractInfo> employeeObj =  vendorInfoService.getContractInfos(vendorId);
		if(employeeObj == null) {
			throw new RuntimeException("Employee with id: "+vendorId+" is not found.");
		}
		return employeeObj;
	}
	

	
	@GetMapping("/branch/{contractId}")
	public List<ContractBranch> getAllBrach(@PathVariable String contractId) {
		System.err.print("vendorId"+contractId+"vendorId");
		List<ContractBranch> employeeObj =  vendorInfoService.getContractBranhchInfos(contractId);
		if(employeeObj == null) {
			throw new RuntimeException("Employee with id: "+contractId+" is not found.");
		}
		return employeeObj;
	}
	
	@RequestMapping("/uploadVendorInfoFile")
	public List<String> uploadFile(@RequestParam("file") MultipartFile file,@RequestParam(name="data") String jSONContractBranch, HttpServletRequest request) {
    	
    	VendorInfo vendor = (VendorInfo) messageProcessor.getObjectFromJsonString(jSONContractBranch, new TypeToken<VendorInfo>() {}.getType());

		 Part filePart = null;
		 File fileStore = null;
		 String message = null;
		 String messageHeader = null;
		 List<String> messageList = new ArrayList<>();
		try {
			
			
			fileStore = new File(fileStoreLocation+"/VendorInfoFile", file.getOriginalFilename());
			FileUtils.writeByteArrayToFile(fileStore, file.getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Retrieves <input type="file" name="file">
			FileInputStream fis = null;
			boolean status=false;
		    try {
		    	fis = new FileInputStream(fileStore);
		    	message=vendorInfoService.readWrite(fis,vendor);
		    	if (message.indexOf("Error") > -1) {
					messageHeader="error";
					
				}
		    	else {
		    		messageHeader="success";
		    	}
			} catch (IOException e) {
				messageHeader="error";
				message=e.getMessage();
				//e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				messageHeader="error";
				message=e.getMessage();
				//e.printStackTrace();
			}
		   
		    messageList.add(messageHeader);
		    messageList.add(message);
			return messageList;

    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/vendorList", method = RequestMethod.POST)
	@ResponseBody
	public String getVendorList(@RequestBody String  encodedMessageString) {
		log.info("Penalty Process Request Accepted. Request : {}" , encodedMessageString);
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		String status = null;
		
		/*
		 * TO_DO:H:Need to do Message Validation
		 */
		List<VendorInfo> vendorList = new ArrayList<VendorInfo>();
		DataHeader messageHeader = message.getDataHeader();
		
		try {
			if(messageHeader.getActionType().equals(ActionTypes.VENDOR_LIST.toString())) {
				vendorList = (List<VendorInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<VendorInfo>>(){}.getType());
				vendorList= vendorInfoService.getVendorInfoList();
			}
			
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), status);
			
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage());
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			vendorList=null;
			
		}
		if(messageHeader.getActionType().equals(ActionTypes.VENDOR_LIST.toString())) {
			message.setPayLoad(vendorList);
		}
		
		byte[] encoded = Base64.getEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
		
		return new String(encoded);
		
	}
	
	 @GetMapping("/vendorID")
		public List<VendorInfo> getVendorId() throws Exception{
			return vendorInfoService.getLabels();
		}
		

}
