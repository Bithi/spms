package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Role;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.service.RoleInfoService;
import com.sbl.spms.service.VendorInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")

public class RoleInfoController {
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private RoleInfoService roleInfoService;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertRoleInfo", method = RequestMethod.POST)
	@ResponseBody
	public String insertRoleInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Role roleInfo = new Role();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Role> roleInfoList = new ArrayList<Role>();
		roleInfoList = (List<Role>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Role>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				roleInfo = roleInfoService.saveOrUpdateRoleInfo(roleInfoList.get(0));
				
				log.info("Role Info Saved.");
				roleInfoList.set(0, roleInfo);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				roleInfoList = roleInfoService.getRoleList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					roleInfo = roleInfoService.updateRoleInfo(roleInfoList.get(0));
						log.info("Role Updated.");
						roleInfoList.set(0, roleInfo);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						roleInfo = roleInfoService.DeleteRoleInfo(roleInfoList.get(0));
						 log.info("Role Info Deleted.");
						 roleInfoList.set(0, roleInfo);
							
						}

			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(roleInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	@GetMapping("/roleList")
	public List<Role> getRoleId() throws Exception{
		return roleInfoService.getLabels();
	}

}
