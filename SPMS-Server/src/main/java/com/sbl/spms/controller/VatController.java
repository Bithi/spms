package com.sbl.spms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbl.spms.model.BRL;
import com.sbl.spms.model.Vat;
import com.sbl.spms.model.VatAitProduct;
import com.sbl.spms.service.BRLService;
import com.sbl.spms.service.VatAITService;
import com.sbl.spms.service.VatService;

@RestController
@RequestMapping("/v1")
@CrossOrigin(origins="*")

public class VatController {

	@Autowired
	private VatAITService vatAITService;
	
	
	@GetMapping("/getVatList")
	public List<VatAitProduct> getVatList() throws Exception{
		return vatAITService.getVATLabels();

	}
	
	@GetMapping("/getAitList")
	public List<VatAitProduct> getAitList() throws Exception{
		return vatAITService.getAITLabels();

	}
}
