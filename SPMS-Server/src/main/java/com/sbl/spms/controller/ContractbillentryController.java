package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.ContSubItem;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contractbilltax;
import com.sbl.spms.model.ItemPerBill;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.service.ContbillchgheadService;
import com.sbl.spms.service.ContractbillentryService;
import com.sbl.spms.service.ContractbilltaxService;
import com.sbl.spms.service.SubItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class ContractbillentryController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	
	@Autowired
	private ContractbillentryService contractbillentryService;
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contractbillentry", method = RequestMethod.POST)
	@ResponseBody
	public String ContractBillEntry(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Contractbillentry contEntry = new Contractbillentry();
		ItemPerBill itemPerBill = new ItemPerBill();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Contractbillentry> contEntryList = new ArrayList<Contractbillentry>();
		List<ItemPerBill> itemList = new ArrayList<ItemPerBill>();
		List<ItemPerBill> itemListPre = new ArrayList<ItemPerBill>();
		
		itemList = (List<ItemPerBill>) messageProcessor.getObjectListFromPayload(message.getOthers(), new TypeToken<List<ItemPerBill>>(){}.getType());
		
		contEntryList = (List<Contractbillentry>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Contractbillentry>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				
				contEntry = contractbillentryService.saveOrUpdateContBillEntry(contEntryList.get(0));
				for (int i = 0; i < itemList.size(); i++) {
		            itemList.get(i).setBillno(contEntry.getEntry_id());
		            itemPerBill = contractbillentryService.saveItemBillEntry(itemList.get(i));
		        }
				
				log.info("Info Saved.");
				contEntryList.set(0, contEntry);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				contEntryList = contractbillentryService.getContBillEntry();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					contEntry = contractbillentryService.updateContBillEntry(contEntryList.get(0));
					itemListPre = contractbillentryService.ItemListByEntry(contEntry.getEntry_id());
					
					//////  delete previous items
					for (int i = 0; i < itemListPre.size(); i++) {
			            itemPerBill = contractbillentryService.deleteItemPerBill(itemListPre.get(i));
			        }
					///////   delete previous items
					
					//////  insert latest items
					for (int i = 0; i < itemList.size(); i++) {
			            itemList.get(i).setBillno(contEntry.getEntry_id());
			            itemPerBill = contractbillentryService.saveItemBillEntry(itemList.get(i));
			        }
					///////   insert previous items
					
						log.info("Info Updated.");
						contEntryList.set(0, contEntry);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						itemListPre = contractbillentryService.ItemListByEntry(contEntryList.get(0).getEntry_id());
						
						//////  delete previous items
						for (int i = 0; i < itemListPre.size(); i++) {
				            itemPerBill = contractbillentryService.deleteItemPerBill(itemListPre.get(i));
				        }
						///////   delete previous items
						contEntry = contractbillentryService.deleteContBillEntry(contEntryList.get(0));
						 log.info("Info Deleted.");
						 contEntryList.set(0, contEntry);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(contEntryList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	@GetMapping("/billEntryContId")
	public List<ContractInfo> get() throws Exception{
		return contractbillentryService.getAllId();
	}
	@GetMapping("/billEntryByRet")
	public List<Contractbillentry> getAllBillId() throws Exception{
		return contractbillentryService.getAllBillId();
	}
	@GetMapping("/billEntryByRetandcon/{contractId}")
	public List<Contractbillentry> getAllbillEntry(@PathVariable String contractId) throws Exception{
		return contractbillentryService.getbillEntryByRetandcon(contractId);
	}
	
	
	
	@GetMapping("/ItemByEntry/{entry}")
	public List<Object[]> getAllItem(@PathVariable String entry) {
		List<Object[]> employeeObj =  contractbillentryService.getItemNameByEntry(entry);
		if(employeeObj == null) {
			throw new RuntimeException("Item with id: "+entry+" is not found.");
		}
		return employeeObj;
	}
	
	@GetMapping("/AllByEntry/{entry}")
	public Object AllByEntry(@PathVariable String entry) {
		Object employeeObj = null;
		try {
			employeeObj = contractbillentryService.getAllByEntry(entry);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(employeeObj == null) {
			throw new RuntimeException("Item with id: "+entry+" is not found.");
		}
		return employeeObj;
	}
	
	
	@GetMapping("/getContractBillEntryList")
	public List<Object> getcon() throws Exception{
		return contractbillentryService.getContractBillEntryJoin();
	}
}
