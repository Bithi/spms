package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.AMCNotification;
import com.sbl.spms.service.AMCNotificationService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class AMCNotificationController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private AMCNotificationService service;

	@GetMapping("/getNotifications")
	public List<AMCNotification> get() throws Exception {
		return service.getVendorInfoList();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	@ResponseBody
	public String insertVendorInfo(@RequestBody String encodedMessageString) throws Exception {

		log.info("Notification Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<AMCNotification> notificationList = new ArrayList<AMCNotification>();
		notificationList = (List<AMCNotification>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<AMCNotification>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {

			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				notificationList.set(0, service.saveAmcNotification(notificationList.get(0)));
				log.info("AMC Notification Saved.");

			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				notificationList.set(0, service.updateAmcNotification(notificationList.get(0)));
				log.info("AMC Notification Updated.");

			} else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				notificationList.set(0, service.deleteAmcNotification(notificationList.get(0)));
				log.info("AMC Notification Deleted.");

			} else if (messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {

				notificationList = service.getVendorInfoList();

			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			log.error("", e);
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(notificationList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	@GetMapping("/getcontAmcJoin")
	public List<Object> getcon() throws Exception{
		return service.getcontAmcJoin();
	}

}
