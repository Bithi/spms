package com.sbl.spms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbl.spms.model.VendorServiceType;
import com.sbl.spms.model.VendorType;
import com.sbl.spms.service.VendorServiceTypeService;
import com.sbl.spms.service.VendorTypeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class VendorServiceTypeController {

	
	
	@Autowired
	private VendorServiceTypeService vendorServiceTypeService;

	
	
	@GetMapping("/vendorServiceType")
	public List<VendorServiceType> get() throws Exception{
		return vendorServiceTypeService.getLabels();
	}
}
