package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.MenuAssignedToRole;
import com.sbl.spms.model.Role;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.service.BrandService;
import com.sbl.spms.service.ItemTypeService;
import com.sbl.spms.service.SubItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class BrandController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	
	@Autowired
	private BrandService brandService;
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/brand", method = RequestMethod.POST)
	@ResponseBody
	public String itemType(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Brand brand = new Brand();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Brand> brandList = new ArrayList<Brand>();
		brandList = (List<Brand>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Brand>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				brand = brandService.saveOrUpdateBrand(brandList.get(0));
				
				log.info("Brand Info Saved.");
				brandList.set(0, brand);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				brandList = brandService.getBrandList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					brand = brandService.updateBrand(brandList.get(0));
						log.info("Brand Updated.");
						brandList.set(0, brand);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						brand = brandService.deleteBrand(brandList.get(0));
						 log.info("Brand Deleted.");
						 brandList.set(0, brand);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(brandList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	@GetMapping("/BrandList")
	public List<Brand> getRoleId() throws Exception{
		return brandService.getLabels();
	}
	
	
	 @GetMapping("/getSubItemAndBrandJoin")
		public List<Object> getcon() throws Exception{
			return brandService.getSubItemAndBrandJoin();
		}

}
