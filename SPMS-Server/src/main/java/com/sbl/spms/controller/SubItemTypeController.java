package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.service.SubItemTypeService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class SubItemTypeController {
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	
	@Autowired
	private SubItemTypeService subItemTypeService;
	
	

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/subItemType", method = RequestMethod.POST)
	@ResponseBody
	public String itemType(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		SubItemType subItemType = new SubItemType();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<SubItemType> subItemTypeList = new ArrayList<SubItemType>();
		subItemTypeList = (List<SubItemType>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<SubItemType>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				subItemType = subItemTypeService.saveOrUpdateSubItemType(subItemTypeList.get(0));
				
				log.info("ItemType Info Saved.");
				subItemTypeList.set(0, subItemType);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				subItemTypeList = subItemTypeService.getsubItemTypeList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					subItemType = subItemTypeService.updateSubItemType(subItemTypeList.get(0));
						log.info("ItemType Updated.");
						subItemTypeList.set(0, subItemType);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						subItemType = subItemTypeService.deleteSubItemType(subItemTypeList.get(0));
						 log.info("ItemType Deleted.");
						 subItemTypeList.set(0, subItemType);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(subItemTypeList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	@GetMapping("/SubitemTypeList")
	public List<SubItemType> getRoleId() throws Exception{
		return subItemTypeService.getLabels();
	}

}
