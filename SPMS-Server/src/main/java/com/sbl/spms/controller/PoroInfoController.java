package com.sbl.spms.controller;


import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.PoroInfo;
import com.sbl.spms.service.PoroInfoService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;



@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins="*")
public class PoroInfoController {
	
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private PoroInfoService poroInfoService;
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/poroInfo", method = RequestMethod.POST)
	@ResponseBody
	public String insertPoroInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		PoroInfo poroInfo = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<PoroInfo> proInfoList = new ArrayList<PoroInfo>();
		proInfoList = (List<PoroInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PoroInfo>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				poroInfo = poroInfoService.saveOrUpdatePoroInfo(proInfoList.get(0));
				
				log.info("Poro Info Saved.");
				proInfoList.set(0, poroInfo);
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				
				poroInfo = poroInfoService.updatePoroInfo(proInfoList.get(0));
					log.info("Poro Info Updated.");
					proInfoList.set(0, poroInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
				poroInfo = poroInfoService.DeletePoroInfo(proInfoList.get(0));
				 log.info("Poro Info Deleted.");
				 proInfoList.set(0, poroInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				proInfoList = poroInfoService.getPoroInfoList();
					
				}
			
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(proInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	

//	@RequestMapping(value = "/getPoroInfo", method = RequestMethod.GET)
//	@ResponseBody
//	public List<PoroInfo> get() throws Exception  {
//	
//		List<PoroInfo> poroInfoList = new ArrayList<PoroInfo>();	
//		poroInfoList = poroInfoService.getPoroInfoList();
//		return poroInfoList;
//	}
//
//	
//	
//	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value = "/insertPoroInfo", method = RequestMethod.POST)
//	@ResponseBody
//	public String insertPoro(@RequestBody String  encodedMessageString) throws Exception {
//		log.info("insert poro info Request Accepted. Request : {}" , encodedMessageString);
//		
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		
//		List<PoroInfo> poroInfoList = new ArrayList<PoroInfo>();
//		poroInfoList = (List<PoroInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PoroInfo>>(){}.getType());
//		DataHeader messageHeader = message.getDataHeader();
//		String status = "Error occured";
//		List<String> messageList = new ArrayList<>();
//		String statusHeader = null;
//		
//		
//		try {
//			
//			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
//				
//				//poroInfoService.saveOrUpdatePoroInfo(poroInfoList.get(0));
//				poroInfoList.set(0, poroInfoService.saveOrUpdatePoroInfo(poroInfoList.get(0)));
//				log.info("Poro Saved.");
//				if (status.indexOf("Error") > -1) {
//		    		statusHeader="error";
//					
//				}
//		    	else {
//		    		statusHeader="success";
//		    	}
//			}
//			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//		}catch (Exception e) {
//				// TODO Auto-generated catch block
//				statusHeader="error";
//				status=e.getMessage();
//				e.printStackTrace();
//				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
//			}
//		   
//		    messageList.add(statusHeader);
//		    messageList.add(status);
//		    message.setPayLoad(poroInfoList);
//			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//			
//			return new String(encoded);
//	}
//	
//	
//    @SuppressWarnings("unchecked")
//	@RequestMapping(value = "/updatePoroInfo", method = RequestMethod.POST)
//	@ResponseBody
//	public String updatePoro(@RequestBody String  encodedMessageString) throws Exception {
//		log.info("Update Poro Accepted");
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		
//		/*
//		 * TO_DO:H:Need to do Message Validation
//		 */
//		
//		List<PoroInfo> poroInfoList = new ArrayList<PoroInfo>();
//		poroInfoList = (List<PoroInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PoroInfo>>(){}.getType());
//		
//		DataHeader messageHeader = message.getDataHeader();
//		
//		
//	
//		try {
//		 if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
//				
//			 PoroInfo poroInfo = poroInfoService.updatePoroInfo(poroInfoList.get(0));
//	        	//chargeConfigList.set(0, chargeConfig) ;
//				
//			}
//		 messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//		
//		}catch (Exception e) {
//			
//		
//				log.error(e.getLocalizedMessage());
//				//e.printStackTrace();
//				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
//				
//	
//		}
//			message.setPayLoad(poroInfoList);
//			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//			
//			return new String(encoded);
//		
//	}
//    
//
//    @SuppressWarnings("unchecked")
//	@RequestMapping(value = "/deletePoroInfo", method = RequestMethod.POST)
//	@ResponseBody
//	public String deletePoro(@RequestBody String  encodedMessageString) throws Exception {
//		log.info("Delete poro info");
//		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
//		String messageString = new String(decoded);
//		
//		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
//		
//		/*
//		 * TO_DO:H:Need to do Message Validation
//		 */
//		
//		List<PoroInfo> poroInfoList = new ArrayList<PoroInfo>();
//		poroInfoList = (List<PoroInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<PoroInfo>>(){}.getType());
//		
//		DataHeader messageHeader = message.getDataHeader();
//		
//		
//	
//		try {
//		 if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
//				
//			 PoroInfo poroInfo = poroInfoService.DeletePoroInfo(poroInfoList.get(0));
//	        	//chargeConfigList.set(0, chargeConfig) ;
//				
//			}
//		 messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
//		
//		}catch (Exception e) {
//			
//		
//				log.error(e.getLocalizedMessage());
//				//e.printStackTrace();
//				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
//				
//	
//		}
//			message.setPayLoad(poroInfoList);
//			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
//			
//			return new String(encoded);
//		
//	}
	
	
	}

