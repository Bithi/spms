package com.sbl.spms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;
import com.sbl.spms.constants.ActionTypes;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.constants.StatusType;
import com.sbl.spms.model.ContPaymentMethod;
import com.sbl.spms.service.ContPaymentMethodService;
import com.sbl.spms.util.Data;
import com.sbl.spms.util.DataHeader;
import com.sbl.spms.util.DataProcessor;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1")
@Slf4j
@CrossOrigin(origins = "*")
public class ContPaymentMethodController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private ContPaymentMethodService service;

	@GetMapping("/getContPaymentMethods")
	public List<ContPaymentMethod> get() throws Exception {
		return service.getPaymentMethodList();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/contPaymentMethod", method = RequestMethod.POST)
	@ResponseBody
	public String insertVendorInfo(@RequestBody String encodedMessageString) throws Exception {

		log.info("Contract PaymentMethod Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<ContPaymentMethod> payloadList = new ArrayList<ContPaymentMethod>();
		payloadList = (List<ContPaymentMethod>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<ContPaymentMethod>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {

			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				payloadList.set(0, service.saveData(payloadList.get(0)));
				log.info("AMC Notification Saved.");

			}

			else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				payloadList.set(0, service.updateData(payloadList.get(0)));
				log.info("AMC Notification Updated.");

			} else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				payloadList.set(0, service.deleteData(payloadList.get(0)));
				log.info("AMC Notification Deleted.");

			} else if (messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {

				payloadList = service.getPaymentMethodList();

			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			log.error("", e);
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(payloadList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	
	@GetMapping("/getPaymentMethodJoin")
	public List<Object> getcon() throws Exception{
		return service.getPaymentMethodJoin();
	}

}
