package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "VAT_AIT_MASTER")
public class VatAitProduct {
	  
		@Id
		@Column(name = "VAT_AIT_PRODUCT_ID")
		private Integer vatAitProductId;
		
		@Column(name = "PRODUCT_TYPE")
		private String productType;
		
		@Column(name = "VAT_AIT_TYPE")
		private String vatAitType;
		
		@Column(name = "REMARKS")
		private String remarks;

		

}
