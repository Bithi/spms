package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "VENDOR_INFO")
public class Vendor {
	@Id
	@Column(name = "VEN_ID", nullable = false)
	private String VEN_ID;
	
	@Column(name = "VEN_NAME")
	private String VEN_NAME;
	
	@Column(name = "VEN_TYPE_ID")
	private String VEN_TYPE_ID;
	
	@Column(name = "SERVICE_TYPE_ID")
	private String SERVICE_TYPE_ID;
	
	@Column(name = "VEN_ADD")
	private String VEN_ADD;
	
	@Column(name = "VEN_PHONE_NO")
	private Integer VEN_PHONE_NO;
	
	@Column(name = "VEN_CONT_NAME")
	private String VEN_CONT_NAME;
	
	@Column(name = "VEN_CONT_NO")
	private Integer VEN_CONT_NO;
	
	@Column(name = "VEN_TRADE_LICENSE_NO")
	private String VEN_TRADE_LICENSE_NO;
	
	@Column(name = "VEN_TIN_NO")
	private  Integer VEN_TIN_NO;
	
	@Column(name = "USER_ID")
	private  String USER_ID;
	
	@Column(name = "ENTRYDT")
	private  Timestamp ENTRYDT;
	
	@Column(name = "MACHINE_IP")
	private  String MACHINE_IP;
    
	public String getVEN_ID() {
		return VEN_ID;
	}
	public void setVEN_ID(String vEN_ID) {
		VEN_ID = vEN_ID;
	}
	public String getVEN_NAME() {
		return VEN_NAME;
	}
	public void setVEN_NAME(String vEN_NAME) {
		VEN_NAME = vEN_NAME;
	}
	public String getVEN_TYPE_ID() {
		return VEN_TYPE_ID;
	}
	public void setVEN_TYPE_ID(String vEN_TYPE_ID) {
		VEN_TYPE_ID = vEN_TYPE_ID;
	}
	public String getSERVICE_TYPE_ID() {
		return SERVICE_TYPE_ID;
	}
	public void setSERVICE_TYPE_ID(String sERVICE_TYPE_ID) {
		SERVICE_TYPE_ID = sERVICE_TYPE_ID;
	}
	public String getVEN_ADD() {
		return VEN_ADD;
	}
	public void setVEN_ADD(String vEN_ADD) {
		VEN_ADD = vEN_ADD;
	}
	public Integer getVEN_PHONE_NO() {
		return VEN_PHONE_NO;
	}
	public void setVEN_PHONE_NO(Integer vEN_PHONE_NO) {
		VEN_PHONE_NO = vEN_PHONE_NO;
	}
	public String getVEN_CONT_NAME() {
		return VEN_CONT_NAME;
	}
	public void setVEN_CONT_NAME(String vEN_CONT_NAME) {
		VEN_CONT_NAME = vEN_CONT_NAME;
	}
	public Integer getVEN_CONT_NO() {
		return VEN_CONT_NO;
	}
	public void setVEN_CONT_NO(Integer vEN_CONT_NO) {
		VEN_CONT_NO = vEN_CONT_NO;
	}
	public String getVEN_TRADE_LICENSE_NO() {
		return VEN_TRADE_LICENSE_NO;
	}
	public void setVEN_TRADE_LICENSE_NO(String vEN_TRADE_LICENSE_NO) {
		VEN_TRADE_LICENSE_NO = vEN_TRADE_LICENSE_NO;
	}
	public Integer getVEN_TIN_NO() {
		return VEN_TIN_NO;
	}
	public void setVEN_TIN_NO(Integer vEN_TIN_NO) {
		VEN_TIN_NO = vEN_TIN_NO;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public Timestamp getENTRYDT() {
		return ENTRYDT;
	}
	public void setENTRYDT(Timestamp eNTRYDT) {
		ENTRYDT = eNTRYDT;
	}
	public String getMACHINE_IP() {
		return MACHINE_IP;
	}
	public void setMACHINE_IP(String mACHINE_IP) {
		MACHINE_IP = mACHINE_IP;
	}
    
    
    



}
