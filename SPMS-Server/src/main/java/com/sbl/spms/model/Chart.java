package com.sbl.spms.model;

import  java.util.Date;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Chart {
	String contractNo;
	String yearRange;
	String count;
	Date  startValidity;
	Date  endValidity;
	String validity;
}
