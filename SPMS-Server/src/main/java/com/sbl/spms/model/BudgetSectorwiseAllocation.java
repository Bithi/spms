package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "BUDGET_SECTORWISE_ALLOCATION")

public class BudgetSectorwiseAllocation {
	
	@Id
	@Column(name = "ID", nullable = false)
	private int id;
	
	@Column(name = "SECTOR_ID", nullable = false)
	private int sectorId;
	
	@Column(name = "AMOUNT")
	private Double amount;
	
	@Column(name = "YEAR")
	private String year;
	
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "CREATED_ON")
	private  Timestamp createdOn;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "UPDATED_ON")
	private  Timestamp updatedOn;
	
	

}
