package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONTRACT_BILL_TAX")
public class Contractbilltax {

	@Id
	@Column(name = "CONT_TAX_ID", nullable = false)
	private String cont_tax_id;
	
	@Column(name = "MIN_AMT")
	private long min_amt;
	
	@Column(name = "MAX_AMT")
	private long max_amt;
	
	@Column(name = "CONT_TAX_RATE")
	private Integer cont_tax_rate;
	
	@Column(name = "FISCAL_YEAR")
	private String fiscal_year;
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;

	
}
