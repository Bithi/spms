package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "VENDOR_TYPE")
public class VendorType {

	@Id
	@Column(name = "VEN_TYPE_ID", nullable = false)
	private String venTypeId;
	
	@Column(name = "VEN_TYPE_NAME")
	private String venTypeName;	
	

	public VendorType() {
	
	}

	public String getVenTypeId() {
		return venTypeId;
	}

	public void setVenTypeId(String venTypeId) {
		this.venTypeId = venTypeId;
	}

	public String getVenTypeName() {
		return venTypeName;
	}

	public void setVenTypeName(String venTypeName) {
		this.venTypeName = venTypeName;
	}
}
