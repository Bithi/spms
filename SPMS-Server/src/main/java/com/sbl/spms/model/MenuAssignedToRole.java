package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "MENUASSIGNEDTOROLE")
public class MenuAssignedToRole {

	@Id
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "M_ID")
	private Integer m_id;
	
	@Column(name = "R_ID")
	private Integer r_id;

	public MenuAssignedToRole() {
		
	}

	public MenuAssignedToRole(Integer m_id, Integer r_id) {
		this.m_id = m_id;
		this.r_id = r_id;
	}

	public MenuAssignedToRole(Integer id2, Integer mId, Integer rId) {
		// TODO Auto-generated constructor stub
		this.id = id2;
		this.m_id = mId;
		this.r_id = rId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getM_id() {
		return m_id;
	}

	public void setM_id(Integer m_id) {
		this.m_id = m_id;
	}

	public Integer getR_id() {
		return r_id;
	}

	public void setR_id(Integer r_id) {
		this.r_id = r_id;
	}
	
}
