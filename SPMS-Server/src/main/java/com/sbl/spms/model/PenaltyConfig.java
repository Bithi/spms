package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "PENALTY_CONF")
public class PenaltyConfig {



	@Id
	@Column(name = "CONF_ID")
	private Integer configId;
	
	@Column(name = "CONT_ID")
	private String contractId;
	
	@Column(name = "VEND_ID")
	private String vendorId;
	
	@Column(name = "COND_TYPE")
	private String condType;
	

	@Column(name = "MIN_DOWNTIME")
	private Integer minDowntime;
	
	@Column(name = "MAX_DOWNTIME")
	private Integer maxDowntime;
	
	@Column(name = "RATE")
	private Integer penaltyRate;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDate;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;
	
	public PenaltyConfig() {}

	public PenaltyConfig(Integer configId, String contractId, String vendorId, String condType, Integer minDowntime,
			Integer maxDowntime, Integer penaltyRate, String entryUser, Timestamp entryDate, String updateUser,
			Timestamp updateDt) {
		super();
		this.configId = configId;
		this.contractId = contractId;
		this.vendorId = vendorId;
		this.condType = condType;
		this.minDowntime = minDowntime;
		this.maxDowntime = maxDowntime;
		this.penaltyRate = penaltyRate;
		this.entryUser = entryUser;
		this.entryDate = entryDate;
		this.updateUser = updateUser;
		this.updateDt = updateDt;
	}

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getCondType() {
		return condType;
	}

	public void setCondType(String condType) {
		this.condType = condType;
	}

	public Integer getMinDowntime() {
		return minDowntime;
	}

	public void setMinDowntime(Integer minDowntime) {
		this.minDowntime = minDowntime;
	}

	public Integer getMaxDowntime() {
		return maxDowntime;
	}

	public void setMaxDowntime(Integer maxDowntime) {
		this.maxDowntime = maxDowntime;
	}

	public Integer getPenaltyRate() {
		return penaltyRate;
	}

	public void setPenaltyRate(Integer penaltyRate) {
		this.penaltyRate = penaltyRate;
	}

	public String getEntryUser() {
		return entryUser;
	}

	public void setEntryUser(String entryUser) {
		this.entryUser = entryUser;
	}

	public Timestamp getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Timestamp entryDate) {
		this.entryDate = entryDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
	

//	public PenaltyConfig(Integer configId,
//			String contractId, String vendorId, Integer distInMonth, Integer atTimeInMonth, Integer penaltyRate,Integer downtime,String entryUser, Timestamp entryDate, String updateUser, Timestamp updateDt) {
//		
//		this.entryUser = entryUser;
//		this.entryDate = entryDate;
//		this.updateUser = updateUser;
//		this.updateDt = updateDt;
//		this.configId = configId;
//		this.contractId = contractId;
//		this.vendorId = vendorId;
//		this.distInMonth = distInMonth;
//		this.atTimeInMonth = atTimeInMonth;
//		this.penaltyRate = penaltyRate;
//		this.downTime = downtime;
//	}




}
