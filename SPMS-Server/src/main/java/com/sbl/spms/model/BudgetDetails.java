package com.sbl.spms.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "BUDGET_DETAILS")

public class BudgetDetails {
	
	@Id
	@Column(name = "BUDGET_DETAILS_ID", nullable = false)
	private int budgetDetailsId;
	
	@Column(name = "BUDGET_SECTOR_ID", nullable = false)
	private int budgetSectorId;
	
	@Column(name = "AMOUNT")
	private String amount;
	
	@Column(name = "PAYMENT_ID")
	private String paymentId;
	
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "CREATED_ON")
	private  Date createdOn;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "UPDATED_ON")
	private  Date updatedOn;
	
	
	
	

}
