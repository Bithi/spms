package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_BILL_CHG_HEAD")
public class Contbillchghead {

	@Id
	@Column(name = "HEAD_ID", nullable = false)
	private String head_id;
	
	@Column(name = "CHG_HEAD")
	private String chg_head;
	
	@Column(name = "CHG_HEAD_NAME")
	private String chg_head_name;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;

	

}
