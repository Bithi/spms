package com.sbl.spms.model;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

public class DowntimeCalcRep {

	private Integer id;
	private String vendor;
	private Timestamp fromDate;
	private Timestamp toDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public Timestamp getFromDate() {
		return fromDate;
	}
	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}
	public Timestamp getToDate() {
		return toDate;
	}
	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}
}
