package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_YEARWISE_RATE")
public class Contyearwiserate {

	
	@Id
	@Column(name = "ID", nullable = false)
	private String id;
	
	
	@Column(name = "CONT_ID")
	private String cont_id;
	
	
	@Column(name = "RATE")
	private Integer rate;
	
	
	@Column(name = "START_DATE")
	private Timestamp start_date;
	
	
	@Column(name = "END_DATE")
	private Timestamp end_date;
	
	
	@Column(name = "CONTRACT_TYPE")
	private String contract_type;
		
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	
	@Column(name = "UPDATEUSR")
	private String updateusr;
	
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
}
