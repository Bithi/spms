package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_BILL_TRAN")
public class Contractbilltran {

	@Id
	@Column(name = "BILL_TRAN_ID", nullable = false)
	private String bill_tran_id;
	
//	@Column(name = "BILL_NO")
	@Transient
	private String bill_no;
//	
//	@Column(name = "CONT_ID")
	@Transient
	private String cont_id;
//	
//	@Column(name = "BILL_PART_FULL")
	@Transient
	private String bill_part_full;
//	
//	@Column(name = "BILL_DATE")
	@Transient
	private Timestamp bill_date;
	
	@Column(name = "CHG_HEAD")
	private String chg_head;
	
//	@Column(name = "ITEMID")
//	private String item_id;
	
	@Column(name = "BILL_PERIOD_START")
	private Timestamp bill_period_start;
	
	@Column(name = "BILL_PERIOD_END")
	private Timestamp bill_period_end; 
	
	@Column(name = "CURRENT_BILL")
	private long current_bill;
	
	@Column(name = "VAT")
	private long vat;
	
	@Column(name = "BILL_AFTER_VAT")
	private long bill_after_vat;
	
	@Column(name = "ACCUM_BILL_AFTER_VAT")
	private long accum_bill_after_vat;
	
	@Column(name = "CURRENT_INCOME_TAX")
	private long current_income_tax;

	@Column(name = "ACCUM_INCOME_TAX")
	private long accum_income_tax;

	@Column(name = "PENALTY")
	private long penalty;

	@Column(name = "FINAL_BILL_VENDOR")
	private long final_bill_vendor;
	
	@Column(name = "CONTACT_NO")
	private String contract_no;
	
	@Column(name = "REFERENCE_NO")
	private String reference_no;
	
	@Column(name = "REFERENCE_DATE")
	private Timestamp reference_date;
	
	@Column(name = "NARRATION")
	private String narration;
	
	@Column(name = "NEXT_BILL_DATE")
	private Timestamp next_bill_date;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "BILL_INITIATE_ID")
	private String bill_initiate_id;
	
	@Column(name = "BILL_AUTHORIZE_ID")
	private String bill_authorize_id;
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
	@Column(name = "ENTRY_ID")
	private String entry_id;

	

}
