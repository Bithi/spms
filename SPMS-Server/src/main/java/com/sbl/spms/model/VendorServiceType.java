package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "VENDOR_SERVICE_TYPE")
public class VendorServiceType {
	
	@Id
	@Column(name = "SERVICE_TYPE_ID", nullable = false)
	private String serviceTypeId;
	
	@Column(name = "SERVICE_TYPE_NAME")
	private String serviceTypeName;
	
	@Column(name = "SERVICE_TYPE_CODE")
	private String serviceTypeCode;

	public VendorServiceType() {
		
	}

	public String getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(String serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	public String getServiceTypeCode() {
		return serviceTypeCode;
	}

	public void setServiceTypeCode(String serviceTypeCode) {
		this.serviceTypeCode = serviceTypeCode;
	}
}
