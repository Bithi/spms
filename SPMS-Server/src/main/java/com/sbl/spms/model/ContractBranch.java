package com.sbl.spms.model;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(
	    name="CONTRACT_BRANCH", 
	    uniqueConstraints=
	        @UniqueConstraint(columnNames={"ID", "BRC_ATM","BRC", "BRNAME","CONT_ID"})
	)

public class ContractBranch {

	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "CONT_ID")
	private String contractId;
	
	@Column(name = "BRC")
	private String  brac;
	
	@Column(name = "BRC_ATM")
	private String brcAtm;
	
	@Column(name = "BRNAME")
	private String brName;
	
	
	@Column(name = "BW ")
	private Integer bw;
	
	@Column(name = "MONTHLY_CHARGE")
	private long monthlyCharge;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDate;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;
	
	
	
	
//	public ContractBranch() {
//		
//	}
//	public ContractBranch(Integer id, String contractId, String brac, String brcAtm, Integer bw, Integer monthlyChage,
//			 String updateUser, Timestamp updateDt, String brName,
//			String status) {
//		
//		this.id = id;
//		this.contractId = contractId;
//		this.brac = brac;
//		this.brcAtm = brcAtm;
//		this.bw = bw;
//		this.monthlyChage = monthlyChage;
//		this.updateUser = updateUser;
//		this.updateDt = updateDt;
//		this.brName = brName;
//		this.status = status;
//	}
	
	

}
