package com.sbl.spms.model;

import java.sql.Timestamp;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "MODEL")
public class Model {

	@Id
	@Column(name = "MODELID")
	private String modelId;
	
	@Column(name = "BRANDID")
	private String brandId;
	
	@Column(name = "MODELNAME")
	private String modelName;
	
	@Column(name = "WARRMON")
	private Double warrantyInMonth;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;

	
}
