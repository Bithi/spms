package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_PAYMENT_METHOD")
public class ContPaymentMethod {
	
	@Id
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "CONT_ID")
	private String contId;
	
	@Column(name = "PART_FULL")
	private String partFull;
	
	@Column(name = "PMT_SCHEDULE")
	private String pmtSchedule;
	
	@Column(name = "TOTAL_VALUE_PRODUCT")
	private long ttValueProduct;
	
	@Column(name = "PAYMENT_DATE")
	private Date paymentDate;
	
	@Column(name = "PAYMENT_AMOUNT")
	private long paymentAmount;
	
	@Column(name = "DUE_AMOUNT")
	private long dueAmount;
	
	@Column(name = "DUE_DATE")
	private Date dueDate;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	

}
