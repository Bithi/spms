package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "ROLEASSIGNEDTOUSER")
public class RoleWiseUser {

	@Id
	@Column(name = "ID", nullable = false)
	private Integer id;
	
	@Column(name = "U_ID")
	private Integer u_id;
	
	@Column(name = "R_ID")
	private Integer r_id;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getU_id() {
		return u_id;
	}

	public void setU_id(Integer u_id) {
		this.u_id = u_id;
	}

	public Integer getR_id() {
		return r_id;
	}

	public void setR_id(Integer r_id) {
		this.r_id = r_id;
	}

	
	public RoleWiseUser(Integer id, Integer u_id, Integer r_id) {
		super();
		this.id = id;
		this.u_id = u_id;
		this.r_id = r_id;
	}

	public RoleWiseUser() {
		super();
	}



	
	

}
