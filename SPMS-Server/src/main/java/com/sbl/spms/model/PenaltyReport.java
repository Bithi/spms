package com.sbl.spms.model;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class PenaltyReport {

	private String contId;
	
	private String venId;
	
	private Timestamp startDate;
	
	private Timestamp endDate;
	
	private String clause;
	 
	private String item;
	
	private String site;
	
	private String status;
	
	private String reportType;
	
	private String user;
	
	
}
