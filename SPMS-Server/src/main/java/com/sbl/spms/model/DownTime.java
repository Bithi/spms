package com.sbl.spms.model;



import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "DOWNTIME_CALC")
public class DownTime {
    
	@Id
	@Column(name = "DOWNID")
	private String id;
	
	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "DOWNDATE")
	private Timestamp date;
	
	@Column(name = "BRC")
	private String branchCode;
	
	@Column(name = "VEN_ID")
	private String vendor;
	
	@Column(name = "PROBLEM_DETAIL")
	private String problem;
	
	@Column(name = "DOWN_TIME")
	private Timestamp downTime;
	
	@Column(name = "UP_TIME")
	private Timestamp upTime;
	
	@Column(name = "TOTAL_TIME")
	private Integer totalTime;
	
	@Column(name = "FOLLOWED_BY")
	private String followedBy;
	
	@Column(name = "CONCERN_OFFICER")
	private String concernOfficer;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDate;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;
	
	

}
