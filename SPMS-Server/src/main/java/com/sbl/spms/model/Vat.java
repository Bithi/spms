package com.sbl.spms.model;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "VAT")
public class Vat {

	@Id
	@Column(name = "VAT_ID", nullable = false)
	private String vat_id;
	
	@Column(name = "MICRO_GL_HEAD")
	private String micro_gl_head;
	
	@Column(name = "VAT_RATE")
	private Double vat_rate;
	
	@Column(name = "FISCAL_YEAR")
	private String fiscal_year;
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;

	

}
