package com.sbl.spms.model;

import java.beans.Transient;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "BUDGET_BILL_ENTRY")
public class BudgetBillEntry {
	
	@Id
	@Column(name = "BUDGET_BILL_REF", nullable = false)
	private int budget_bill_ref;
	
	@Column(name = "INVOICE_NO", nullable = false)
	private String invoice_no;
	
	@Column(name = "INVOICE_DT")
	private  Timestamp invoice_dt;
	
	@Column(name = "PRODUCT_DESCRIPTION")
	private String product_description;
	
	@Column(name = "BILL_AMOUNT")
	private String bill_amount;
	
	@Column(name = "CONT_ID")
	private String cont_id;
	
//	@Column(name = "VEN_ID")
//	private String ven_id;
	
	@Column(name = "WORK_ORDER_NO")
	private String work_order_no;
	
	@Column(name = "DELIVERY_DT")
	private  Timestamp delivery_dt;
	
	@Column(name = "BILL_RECEIVE_DT")
	private  Timestamp bill_recieve_dt;
	
	
	@Column(name = "BILL_TO_AUDIT_DT")
	private  Timestamp bill_to_audit_dt;
	
	@Column(name = "AUDIT_REPORT_RECEIVE_DT")
	private  Timestamp audit_report_receive_dt;
	
	@Column(name = "SEND_TO_BDC_DT")
	private  Timestamp send_to_bdc_dt;
	
	@Column(name = "PAYMENT_NOTE_DT")
	private  Timestamp payment_note_dt;
	
	@Column(name = "FROM_BDC_DT")
	private  Timestamp from_bdc_dt;
	
	@Column(name = "NOTE_APPROVE_DT")
	private  Timestamp note_approve_dt;
	
	@Column(name = "ENTRY_ID")
	private String entry_id;
	
	@Column(name = "ENTRY_DT")
	private  Timestamp entry_dt;
	
	@Column(name = "UPDATE_ID")
	private String update_id;
	
	@Column(name = "UPDATE_DT")
	private  Timestamp update_dt;
	
	@Column(name = "VAT_PRODUCT", nullable = false)
	private int vat_product;
	
	@Column(name = "AIT_PRODUCT", nullable = false)
	private int ait_product;
	
    private transient String budget_bill_amnt_by_cont;
    
    private transient String cont_bill_amnt_by_cont;
    
    private transient int vat;
    private transient int ait;

}
