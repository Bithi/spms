package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "CONT_BILL_ITEM_ENTRY")
public class ItemPerBill {
	@Id
	@Column(name = "ID")
	private String id;
	
	@Column(name = "ITEMID")
	private String itemId;
	
	@Column(name = "BILL")
	private String billno;
}
