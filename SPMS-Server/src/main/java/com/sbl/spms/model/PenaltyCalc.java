package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "PENALTY_CALC")
public class PenaltyCalc {
	@Id
	@Column(name = "CALC_ID")
	private Integer calcId;
	
	@Column(name = "CONT_ID")
	private String contId;
	
	@Column(name = "VEN_ID")
	private String venId;
	
	@Column(name = "BRCODE")
	private String brCode;
	
	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "MONTHLY_CHARGE")
	private long monthlyCharge;
	
	@Column(name = "NET_PENALTY")
	private long netPenalty;
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
	@Column(name = "BRNAME")
	private String brName;
	
	@Column(name = "DOWNID")
	private String downid;
	
	@Column(name = "DOWNDATE")
	private String downdate;
	
	@Transient
	private Timestamp bill_period_start;
	@Transient
	private Timestamp bill_period_end;
	
}
