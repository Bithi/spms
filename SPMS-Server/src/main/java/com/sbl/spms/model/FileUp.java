package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_FILE_LOCATION")
public class FileUp {
	@Id
	@Column(name = "FILE_ID", nullable = false)
	private Integer fileId;
	
	@Column(name = "CONT_ID")
	private String contId;
	
	@Column(name = "FILE_TYPE")
	private String fileType;
	
	@Column(name = "FILE_LOC")
	private String fileLoc;
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;
	
//	    fileName: string;
}
