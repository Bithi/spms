package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_BILL_ENTRY")
public class Contractbillentry {

	
	@Id
	@Column(name = "ENTRY_ID", nullable = false)
	private String entry_id;
	
	
	@Column(name = "CONT_ID")
	private String cont_id;
	
	
	@Column(name = "BILL_NO")
	private String bill_no;
	
	
	@Column(name = "BILL_DATE")
	private Timestamp bill_date;
	
	
	@Column(name = "BILL_RECIEVE_DATE")
	private Timestamp bill_recieve_date;
	
	
	@Column(name = "PART_FULL")
	private String part_full;
	
	@Column(name = "SEND_AUDIT_DATE")
	private Timestamp send_audit_date;
	
	
	@Column(name = "RETURN_FROM_AUDIT_DATE")
	private Timestamp return_from_audit_date;
	
	
	@Column(name = "ENTRYUSR")
	private String entryusr;
	
	
	@Column(name = "ENTRYDT")
	private Timestamp entrydt;
	
	
	@Column(name = "UPDATEUSR")
	private String updateusr;
	
	
	@Column(name = "UPDATEDT")
	private Timestamp updatedt;

	
	
	@Column(name = "BILL_AMOUNT")
	private long billamount;
	
	
	
}
