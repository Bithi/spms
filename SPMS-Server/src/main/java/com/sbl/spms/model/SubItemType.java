package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "SUBITEMTYPE")
public class SubItemType {

	@Id
	@Column(name = "ITEMID")
	private String itemId;
	
	@Column(name = "ITEMNAME")
	private String itemName;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;
	
	@Column(name = "TYPEID")
	private String typeId;


	public SubItemType(String itemId, String itemName,Timestamp entryDt,
			String entryUser, String updateUser, Timestamp updateDt, String typeId) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.entryDt = entryDt;
		this.entryUser = entryUser;
		this.updateUser = updateUser;
		this.updateDt = updateDt;
		this.typeId = typeId;
	}


	public SubItemType() {
		super();
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public Timestamp getEntryDt() {
		return entryDt;
	}


	public void setEntryDt(Timestamp entryDt) {
		this.entryDt = entryDt;
	}


	public String getEntryUser() {
		return entryUser;
	}


	public void setEntryUser(String entryUser) {
		this.entryUser = entryUser;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public Timestamp getUpdateDt() {
		return updateDt;
	}


	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}


	public String getTypeId() {
		return typeId;
	}


	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
}
