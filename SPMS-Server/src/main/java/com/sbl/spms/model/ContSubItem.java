package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_SUBITEM")
public class ContSubItem {
	
	@Id
	@Column(name = "CONT_SUBITEM_ID")
	private String contSubItemId;
	
	@Column(name = "CONT_ID")
	private String contId;
	
	@Column(name = "ITEMID")
	private String itemId;
	
	@Column(name = "ITEM_QUANTITY")
	private Integer itemQuantity;
	
	@Column(name = "TOTAL_VALUE")
	private long totalValue;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;
	
	@Column(name = "MODELID")
	private String modelId;

}
