package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@DynamicUpdate
@Entity
@Table(name = "BRL")
public class BRL {
	  
	  @Column(name = "GMOID")
	  private String gmoId;
	  
	  @Column(name = "GMOCODE")
	  private String gmoCodeE;
	  
	  @Column(name = "POROPREFIX")
	  private String poroPrefix;
	  
	  @Column(name = "PORONAME")
	  private String poroName;
	  
	  @Column(name = "CORP")
	  private String coRp;
	  
	  @Id
	  @Column(name = "BRC", nullable = false)
	  private String branCode;
	  
	  @Column(name = "BRNAME")
	  private String branName;
	  
	  @Column(name = "DISTRICT_CODE")
	  private String distCode;
	  
	  @Column(name = "DISTRICT_NAME")
	  private String distName;
	  
	  @Column(name = "ROUTINGNO")
	  private String routingNo;
	  
	  @Column(name = "GMONAME")
	  private String gmoName;
	  
	  @Column(name = "POROCODE")
	  private String poroCode;  
	  
	  @Column(name = "STATUS")
	  private String status; 
	  
	  @Column(name = "BRLOCATION")
	  private String brLocation;
	  
	  
	public String getGmoId() {
		return gmoId;
	}
	public void setGmoId(String gmoId) {
		this.gmoId = gmoId;
	}
	public String getGmoCodeE() {
		return gmoCodeE;
	}
	public void setGmoCodeE(String gmoCodeE) {
		this.gmoCodeE = gmoCodeE;
	}
	public String getPoroPrefix() {
		return poroPrefix;
	}
	public void setPoroPrefix(String poroPrefix) {
		this.poroPrefix = poroPrefix;
	}
	public String getPoroName() {
		return poroName;
	}
	public void setPoroName(String poroName) {
		this.poroName = poroName;
	}
	public String getCoRp() {
		return coRp;
	}
	public void setCoRp(String coRp) {
		this.coRp = coRp;
	}
	public String getBranCode() {
		return branCode;
	}
	public void setBranCode(String branCode) {
		this.branCode = branCode;
	}
	public String getBranName() {
		return branName;
	}
	public void setBranName(String branName) {
		this.branName = branName;
	}
	public String getDistCode() {
		return distCode;
	}
	public void setDistCode(String distCode) {
		this.distCode = distCode;
	}
	public String getDistName() {
		return distName;
	}
	public void setDistName(String distName) {
		this.distName = distName;
	}
	public String getRoutingNo() {
		return routingNo;
	}
	public void setRoutingNo(String routingNo) {
		this.routingNo = routingNo;
	}
	public String getGmoName() {
		return gmoName;
	}
	public void setGmoName(String gmoName) {
		this.gmoName = gmoName;
	}
	public String getPoroCode() {
		return poroCode;
	}
	public void setPoroCode(String poroCode) {
		this.poroCode = poroCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBrLocation() {
		return brLocation;
	}
	public void setBrLocation(String brLocation) {
		this.brLocation = brLocation;
	}
	
	public BRL() {}
	public BRL(String gmoId, String gmoCodeE, String poroPrefix, String poroName, String coRp, String branCode,
			String branName, String distCode, String distName, String routingNo, String gmoName, String poroCode,
			String status, String brLocation) {
	
		this.gmoId = gmoId;
		this.gmoCodeE = gmoCodeE;
		this.poroPrefix = poroPrefix;
		this.poroName = poroName;
		this.coRp = coRp;
		this.branCode = branCode;
		this.branName = branName;
		this.distCode = distCode;
		this.distName = distName;
		this.routingNo = routingNo;
		this.gmoName = gmoName;
		this.poroCode = poroCode;
		this.status = status;
		this.brLocation = brLocation;
	}
	  
	  

}
