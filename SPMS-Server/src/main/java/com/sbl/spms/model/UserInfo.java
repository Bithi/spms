package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "USER_INFO")
public class UserInfo {

	// USER_ID
	@Id
	@Column(name = "ID", nullable = false)
	private Integer Id;

	@Column(name = "USER_ID", nullable = false)
	private String userId;

	@Column(name = "USER_NAME")
	private String userName;

	// PASSWORD
	@Column(name = "PASSWORD")

	private String password;
	// OFFICE_CODE
	@Column(name = "OFFICE_CODE")

	private String officeCode;

	// USER_STATUS
	@Column(name = "USER_STATUS")

	private String userStatus;

	// PWD_CHG_DT
	@Column(name = "PWD_CHG_DT")

	private Timestamp passwordChangeDate;
	
	@Column(name = "LAST_LOGIN_DT")

	private Timestamp lastLoginDate;
	
	@Column(name = "LOGIN_ATTEMPT")

	private Integer loginAttempt;
	
	@Transient
	private String message;
}

//	public UserInfo(String userId, String userName, String password, String officeCode, String userStatus,
//			Integer failCount, Timestamp passwordChangeDate) {
//		super();
//		this.userId = userId;
//		this.userName = userName;
//		this.password = password;
//		this.officeCode = officeCode;
//		this.userStatus = userStatus;
//		this.failCount = failCount;
//		this.passwordChangeDate = passwordChangeDate;
//	}
//
//	public UserInfo() {
//		super();
//	}
//
//	public UserInfo(Integer id, String userId, String userName, String password, String officeCode, String userStatus,
//			Integer failCount, Timestamp passwordChangeDate) {
//		super();
//		Id = id;
//		this.userId = userId;
//		this.userName = userName;
//		this.password = password;
//		this.officeCode = officeCode;
//		this.userStatus = userStatus;
//		this.failCount = failCount;
//		this.passwordChangeDate = passwordChangeDate;
//	}
//
//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}
//
//	public Integer getId() {
//		return Id;
//	}
//
//	public void setId(Integer id) {
//		Id = id;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public String getOfficeCode() {
//		return officeCode;
//	}
//
//	public void setOfficeCode(String officeCode) {
//		this.officeCode = officeCode;
//	}
//
//	public String getUserStatus() {
//		return userStatus;
//	}

//	public void setUserStatus(String userStatus) {
//		this.userStatus = userStatus;
//	}
//
//	public Integer getFailCount() {
//		return failCount;
//	}
//
//	public void setFailCount(Integer failCount) {
//		this.failCount = failCount;
//	}
//
//	public Timestamp getPasswordChangeDate() {
//		return passwordChangeDate;
//	}
//
//	public void setPasswordChangeDate(Timestamp passwordChangeDate) {
//		this.passwordChangeDate = passwordChangeDate;
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	@Override
//	public String toString() {
//		return "UserInfo [userId=" + userId + ",userName=" + userName + ", password=" + password + ", officeCode="
//				+ officeCode + ", userStatus=" + userStatus + ", failCount=" + failCount + ", passwordChangeDate="
//				+ passwordChangeDate + "]";
//	}
//
//}
