package com.sbl.spms.model;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONTRACT_INFO")

public class ContractInfo {
	
	  @Id
	  @Column(name = "CONT_ID")
	  private String contId;
	 
	  @Column(name = "CONT_NO")
	  private String contNo;
	  
	  @Column(name = "CONT_NAME")
	  private String contName;
	  
	  @Column(name = "SERVICE_TYPE_ID")
	  private String serviceTypeId;
	  
	  @Column(name = "CONT_PARTY")
	  private String contParty;
	  
	  @Column(name = "CONT_VALID_START")
	  private Timestamp contValidStart;
	  
	  @Column(name = "CONT_VALID_END")
	  private Timestamp contValidEnd;
	  
	  @Column(name = "TENDER_DT")
	  private Timestamp tenderDt;
	  
	  @Column(name = "TENDER_NO")
	  private String tenderNo;
	  
	  @Column(name = "TOTAL_MONTH")
	  private Integer totalMonth;
	  
	  @Column(name = "MONTHLY_CHARGE")
	  private long monthlyCharge;
	  
	  @Column(name = "TOTAL_CHARGE")
	  private long totalCharge;
	  
	  @Column(name = "TOTAL_BR")
	  private Integer totalBr;
	  
	  @Column(name = "TOTAL_ATM")
	  private Integer totalAtm;
	  
	  @Column(name = "TOTAL_DC_DR_CONNECTIVITY")
	  private Integer totalDcDrConnectivity;
	  
	  @Column(name = "VEN_ID")
	  private String venId;
	  
	  @Column(name = "STATUS")
	  private String status;
	  
	  @Column(name = "MODULE_ID")
	  private Integer moduleId;
	  
	  @Column(name = "ENTRYUSR")
	  private String entryUser;
	  
	  @Column(name = "ENTRYDT")
	  private Timestamp entryDate;
	  
	  @Column(name = "UPDATEUSR")
	  private String updateUser;
	  
	  @Column(name = "UPDATEDT")
	  private Timestamp updateDt;
	  
	  @Column(name = "CONT_RENEW_DT")
	  private Timestamp renewDate;
	  
	  @Column(name = "VAT")
	  private String vat;
	  
	  @Column(name = "TAX")
	  private String tax;
	 
	  @Column(name = "TYPE")
	  private String type;
	  
	  @Column(name = "WARRANT_PERIOD")
	  private String warrantyPeriod;
	  
	  @Column(name = "REF_CONTACT_NO")
	  private String refConNo;
	  
	  @Column(name = "REMARKS")
	  private String remarks;
	  
	  @Column(name = "COMM_DT")
	  private Timestamp commDate;
	  
	  @Column(name = "CONT_DT")
	  private Timestamp contDate;
	  
	  @Column(name = "PERFORM_REPORT_DT")
	  private Timestamp performReportDt;
	  
	  @Column(name = "INSPECT_REPORT_DT")
	  private Timestamp inspectReportDt;

}
