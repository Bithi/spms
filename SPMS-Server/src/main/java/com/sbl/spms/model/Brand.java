package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "BRAND")
public class Brand {

	@Id
	@Column(name = "BRANDID")
	private String brandId;
	
	@Column(name = "ITEMID")
	private String itemId;
	
	@Column(name = "BRANDNAME")
	private String brandName;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;

	public Brand(String brandId, String itemId, String brandName, Timestamp entryDt, String entryUser,
			String updateUser, Timestamp updateDt) {
		super();
		this.brandId = brandId;
		this.itemId = itemId;
		this.brandName = brandName;
		this.entryDt = entryDt;
		this.entryUser = entryUser;
		this.updateUser = updateUser;
		this.updateDt = updateDt;
	}

	public Brand() {
		super();
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Timestamp getEntryDt() {
		return entryDt;
	}

	public void setEntryDt(Timestamp entryDt) {
		this.entryDt = entryDt;
	}

	public String getEntryUser() {
		return entryUser;
	}

	public void setEntryUser(String entryUser) {
		this.entryUser = entryUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
}
