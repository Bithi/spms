package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "CONT_SECURITY_MONEY")
public class ContSecurityMoney {
	
	@Id
	@Column(name = "CONT_SECURITY_ID")
	private String contSecurityId;
	
	@Column(name = "CONT_ID")
	private String contId;
	
	@Column(name = "SECURITY_TYPE")
	private String securityType;
	
	@Column(name = "ISS_BANK")
	private String issueBank;
	
	@Column(name = "ISS_BRANCH")
	private String issueBranch;
	
	@Column(name = "ISS_AMOUNT")
	private long issueAmount;
	
	@Column(name = "ISS_DATE")
	private Date issueDate;
	
	@Column(name = "PERIOD_SECURITY")
	private Integer periodSecurity;
	
	@Column(name = "RETURN_DATE")
	private Date returnDate;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "UPDATEUSR")
	private String updateUser;
	
	@Column(name = "UPDATEDT")
	private Timestamp updateDt;

}
