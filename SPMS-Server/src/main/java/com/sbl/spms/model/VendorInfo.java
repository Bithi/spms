package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "VENDOR_INFO")
public class VendorInfo {

	@Id
	@Column(name = "VEN_ID", nullable = false)
	private String venId;
	
	@Column(name = "VEN_NAME")
	private String venName;
	
	@Column(name = "VEN_TYPE_ID")
	private String venTypeId;
	
	@Column(name = "SERVICE_TYPE_ID")
	private String serviceTypeId;
	
	@Column(name = "VEN_ADD")
	private String venAdd;
	
	@Column(name = "VEN_PHONE_NO")
	private Integer venPhoneNumber;
	
	@Column(name = "VEN_TRADE_LICENSE_NO")
	private String venTradeLicenseNo;
	
	@Column(name = "VEN_TIN_NO")
	private Integer venTinNo;
	
	@Column(name = "USER_ID")
	private String userId;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDt;
	
	@Column(name = "MACHINE_IP")
	private String machineIp;
	
	@Column(name = "UPDATEUSR")
	private String updateusr;
	
	@Column(name="UPDATEDT")
	private Timestamp updatedt;
	
	@Column(name = "BIN_NO")
	private String bin_no;
	
	

	public VendorInfo() {
		
	}
	
	
	

	public VendorInfo(String venId, String venName, String venTypeId, String serviceTypeId, String venAdd,
			Integer venPhoneNumber,  String venTradeLicenseNo, Integer venTinNo,
			String userId, Timestamp entryDt, String machineIp) {
		this.venId = venId;
		this.venName = venName;
		this.venTypeId = venTypeId;
		this.serviceTypeId = serviceTypeId;
		this.venAdd = venAdd;
		this.venPhoneNumber = venPhoneNumber;
		this.venTradeLicenseNo = venTradeLicenseNo;
		this.venTinNo = venTinNo;
		this.userId = userId;
		this.entryDt = entryDt;
		this.machineIp = machineIp;
	}




	public String getBin_no() {
		return bin_no;
	}




	public void setBin_no(String bin_no) {
		this.bin_no = bin_no;
	}




	public String getVenId() {
		return venId;
	}

	public void setVenId(String venId) {
		this.venId = venId;
	}

	public String getVenName() {
		return venName;
	}

	public void setVenName(String venName) {
		this.venName = venName;
	}

	public String getVenTypeId() {
		return venTypeId;
	}

	public void setVenTypeId(String venTypeId) {
		this.venTypeId = venTypeId;
	}

	public String getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(String serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public String getVenAdd() {
		return venAdd;
	}

	public void setVenAdd(String venAdd) {
		this.venAdd = venAdd;
	}

	public Integer getVenPhoneNumber() {
		return venPhoneNumber;
	}

	public void setVenPhoneNumber(Integer venPhoneNumber) {
		this.venPhoneNumber = venPhoneNumber;
	}


	

	public String getVenTradeLicenseNo() {
		return venTradeLicenseNo;
	}

	public void setVenTradeLicenseNo(String venTradeLicenseNo) {
		this.venTradeLicenseNo = venTradeLicenseNo;
	}

	public Integer getVenTinNo() {
		return venTinNo;
	}

	public void setVenTinNo(Integer venTinNo) {
		this.venTinNo = venTinNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getEntryDt() {
		return entryDt;
	}

	public void setEntryDt(Timestamp entryDt) {
		this.entryDt = entryDt;
	}

	public String getMachineIp() {
		return machineIp;
	}

	public void setMachineIp(String machineIp) {
		this.machineIp = machineIp;
	}




	public String getUpdateusr() {
		return updateusr;
	}




	public void setUpdateusr(String updateusr) {
		this.updateusr = updateusr;
	}




	public Timestamp getUpdatedt() {
		return updatedt;
	}




	public void setUpdatedt(Timestamp updatedt) {
		this.updatedt = updatedt;
	}
	
}
