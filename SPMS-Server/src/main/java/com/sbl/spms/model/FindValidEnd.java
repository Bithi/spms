package com.sbl.spms.model;
import java.sql.Timestamp;

public class FindValidEnd {

	private Timestamp fromDate;
	
	private Timestamp toDate;

	public FindValidEnd() {
		super();
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	public Timestamp getToDate() {
		return toDate;
	}

	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}
}
