package com.sbl.spms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "PORO")
public class PoroInfo {
	

	@Column(name = "POROPREFIX")
	private String poroPrefix;
	
	@Column(name = "PORONAME")
	private String poroName;
	
	@Column(name = "POROTYPE")
	private char poroType;
	
	@Column(name = "GMOID")
	private String gmoId;
	
	@Id
	@Column(name = "POROCODE", nullable = false)
	private String poroCode;
	
	@Column(name = "GMOCODE")
	private String gmoCode;
	
	public PoroInfo() {}
	public PoroInfo(String poroPrefix2, String poroName2, char poroType2, String gmoId2, String poroCode2,
			String gmoCode2) {
		this.poroPrefix = poroPrefix2;
		this.poroName = poroName2;
		this.poroType = poroType2;
		this.gmoId = gmoId2;
		this.poroCode = poroCode2;
		this.gmoCode = gmoCode2;
	}
	public String getPoroPrefix() {
		return poroPrefix;
	}
	public void setPoroPrefix(String poroPrefix) {
		this.poroPrefix = poroPrefix;
	}
	public String getPoroName() {
		return poroName;
	}
	public void setPoroName(String poroName) {
		this.poroName = poroName;
	}
	public char getPoroType() {
		return poroType;
	}
	public void setPoroType(char poroType) {
		this.poroType = poroType;
	}
	public String getGmoId() {
		return gmoId;
	}
	public void setGmoId(String gmoId) {
		this.gmoId = gmoId;
	}
	public String getPoroCode() {
		return poroCode;
	}
	public void setPoroCode(String poroCode) {
		this.poroCode = poroCode;
	}
	public String getGmoCode() {
		return gmoCode;
	}
	public void setGmoCode(String gmoCode) {
		this.gmoCode = gmoCode;
	}
	
	

}
