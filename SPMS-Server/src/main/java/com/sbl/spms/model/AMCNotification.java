package com.sbl.spms.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate

@Entity
@Table(name = "AMC_NOTIFICATION")
public class AMCNotification  {
	

	@Id
	@Column(name = "AMC_NOTIF_ID")
	private Integer amcNotifId;
	
	public Integer getAmcNotifId() {
		return amcNotifId;
	}

	public void setAmcNotifId(Integer amcNotifId) {
		this.amcNotifId = amcNotifId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getVendorMobile() {
		return vendorMobile;
	}

	public void setVendorMobile(String vendorMobile) {
		this.vendorMobile = vendorMobile;
	}

	public String getVendorEmail() {
		return vendorEmail;
	}

	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}

	public String getGmItMobile() {
		return gmItMobile;
	}

	public void setGmItMobile(String gmItMobile) {
		this.gmItMobile = gmItMobile;
	}

	public String getGmItEmail() {
		return gmItEmail;
	}

	public void setGmItEmail(String gmItEmail) {
		this.gmItEmail = gmItEmail;
	}

	public String getDgmItProcMobile() {
		return dgmItProcMobile;
	}

	public void setDgmItProcMobile(String dgmItProcMobile) {
		this.dgmItProcMobile = dgmItProcMobile;
	}

	public String getDgmItProcEmail() {
		return dgmItProcEmail;
	}

	public void setDgmItProcEmail(String dgmItProcEmail) {
		this.dgmItProcEmail = dgmItProcEmail;
	}

	public String getDealingOffMobile() {
		return dealingOffMobile;
	}

	public void setDealingOffMobile(String dealingOffMobile) {
		this.dealingOffMobile = dealingOffMobile;
	}

	public String getDealingOffEmail() {
		return dealingOffEmail;
	}

	public void setDealingOffEmail(String dealingOffEmail) {
		this.dealingOffEmail = dealingOffEmail;
	}

	public String getAgm1ItprocMob() {
		return agm1ItprocMob;
	}

	public void setAgm1ItprocMob(String agm1ItprocMob) {
		this.agm1ItprocMob = agm1ItprocMob;
	}

	public String getAgm1ItprocEmail() {
		return agm1ItprocEmail;
	}

	public void setAgm1ItprocEmail(String agm1ItprocEmail) {
		this.agm1ItprocEmail = agm1ItprocEmail;
	}

	public String getAgm2ItprocMob() {
		return agm2ItprocMob;
	}

	public void setAgm2ItprocMob(String agm2ItprocMob) {
		this.agm2ItprocMob = agm2ItprocMob;
	}

	public String getAgm2ItprocEmail() {
		return agm2ItprocEmail;
	}

	public void setAgm2ItprocEmail(String agm2ItprocEmail) {
		this.agm2ItprocEmail = agm2ItprocEmail;
	}

	public Integer getNotifBefore() {
		return notifBefore;
	}

	public void setNotifBefore(Integer notifBefore) {
		this.notifBefore = notifBefore;
	}

	public Integer getNotifIntervalDay() {
		return notifIntervalDay;
	}

	public void setNotifIntervalDay(Integer notifIntervalDay) {
		this.notifIntervalDay = notifIntervalDay;
	}

	public Date getLastNotifDate() {
		return lastNotifDate;
	}

	public void setLastNotifDate(Date lastNotifDate) {
		this.lastNotifDate = lastNotifDate;
	}

	public String getEntryUser() {
		return entryUser;
	}

	public void setEntryUser(String entryUser) {
		this.entryUser = entryUser;
	}

	public Timestamp getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Timestamp entryDate) {
		this.entryDate = entryDate;
	}

	@Column(name = "CONT_ID")
	private String contractId;
	
	@Column(name = "VENDOR_MOB")
	private String vendorMobile;
	
	@Column(name = "VENDOR_EMAIL")
	private String vendorEmail;
	
	@Column(name = "GM_IT_MOB")
	private String gmItMobile;
	
	@Column(name = "GM_IT_EMAIL")
	private String gmItEmail;
	
	@Column(name = "DGM_ITPROC_MOB")
	private String dgmItProcMobile;
	
	@Column(name = "DGM_ITPROC_EMAIL")
	private String dgmItProcEmail;
	
	@Column(name = "DEALING_OFF_MOB")
	private String dealingOffMobile;
	
	@Column(name = "DEALING_OFF_EMAIL")
	private String dealingOffEmail;
	
	@Column(name = "AGM1_itproc_MOB")
	private String agm1ItprocMob;
	
	@Column(name = "AGM1_itproc_EMAIL")
	private String agm1ItprocEmail;
	
	@Column(name = "AGM2_itproc_MOB")
	private String agm2ItprocMob;
	
	@Column(name = "AGM2_itproc_EMAIL")
	private String agm2ItprocEmail;
	
	@Column(name = "NOTIF_BEFORE")
	private Integer notifBefore;
	
	@Column(name = "NOTIF_INTERVAL_DAY")
	private Integer notifIntervalDay;
	
	@Column(name = "LAST_NOTIF_DATE")
	private Date lastNotifDate;
	
	@Column(name = "ENTRYUSR")
	private String entryUser;
	
	@Column(name = "ENTRYDT")
	private Timestamp entryDate;

	
	

}
