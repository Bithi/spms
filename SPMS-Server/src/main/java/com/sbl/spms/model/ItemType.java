package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
@DynamicUpdate
@Entity
@Table(name = "ITEMTYPE")
public class ItemType {

	  @Id
	  @Column(name = "TYPEID")
	  private String typeId;
	  
	  @Column(name = "TYPE_SHORTNAME")
	  private String shortName;
	  
	  @Column(name = "TYPEDESC")
	  private String description;
	  
	  @Column(name = "CATEGORY")
	  private String category;
	  
	  @Column(name = "ENTRYUSR")
	  private String entryUser;
	  
	  @Column(name = "ENTRYDT")
	  private Timestamp entryDate;
	  
	  @Column(name = "UPDATEUSR")
	  private String updateUser;
	  
	  @Column(name = "UPDATEDT")
	  private Timestamp updateDt;

	public ItemType(String typeId, String shortName, String description, String category, String entryUser,
			Timestamp entryDate, String updateUser, Timestamp updateDt) {
		super();
		this.typeId = typeId;
		this.shortName = shortName;
		this.description = description;
		this.category = category;
		this.entryUser = entryUser;
		this.entryDate = entryDate;
		this.updateUser = updateUser;
		this.updateDt = updateDt;
	}

	public ItemType() {
		super();
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEntryUser() {
		return entryUser;
	}

	public void setEntryUser(String entryUser) {
		this.entryUser = entryUser;
	}

	public Timestamp getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Timestamp entryDate) {
		this.entryDate = entryDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Timestamp getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Timestamp updateDt) {
		this.updateDt = updateDt;
	}
	  
	  
	  
	  
	  
}
