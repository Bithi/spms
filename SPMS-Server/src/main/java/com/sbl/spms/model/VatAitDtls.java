package com.sbl.spms.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "VAT_AIT_DTLS")
public class VatAitDtls {
	  
		@Id
		@Column(name = "VAT_AIT_ID")
		private Integer vatAitId;
		
		@Column(name = "PRODUCT_TYPE")
		private String productType;
		
		@Column(name = "MIN_BASE_AMT")
		private Double min_base_amt;
		
		@Column(name = "MAX_BASE_AMT")
		private Double max_base_amt;

		@Column(name = "APPLICABLE_DT")
		private Timestamp applicable_dt;
		
		@Column(name = "STATUS")
		private String status;
		
		@Column(name = "RATE")
		private Integer rate;

		@Column(name = "REMARKS")
		private Integer remarks;
}
