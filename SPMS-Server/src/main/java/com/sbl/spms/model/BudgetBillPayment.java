package com.sbl.spms.model;

import java.beans.Transient;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Entity
@Getter
@Setter
@Table(name = "BUDGET_BILL_PAYMENT")
public class BudgetBillPayment {
	
	@Id
	@Column(name = "PAYMENT_ID", nullable = false)
	private int payment_id;
	
	@Column(name = "INVOICE_NO", nullable = false)
	private String invoice_no;
	
	@Column(name = "PAYMENT_DT")
	private  Timestamp payment_dt;
	
	@Column(name = "PENALTY_AMT")
	private  Double penalty_amt;
	
	@Column(name = "PAY_ORDER_AMOUNT")
	private Double pay_order_amt;
	
	@Column(name = "PAY_ORDER_NUMBER")
	private String pay_order_no;
	
	@Column(name = "VAT_CHALAN_NUMBER")
	private String vat_chalan_no;
	
	@Column(name = "VAT_CHALAN_DT")
	private Timestamp vat_chalan_dt;
	
	@Column(name = "AIT_CHALAN_NUMBER")
	private  String ait_chalan_no;
	
	@Column(name = "AIT_CHALAN_DT")
	private  Timestamp ait_chalan_dt;
	
	
	@Column(name = "FORM_NO")
	private  String form_no;
	
	@Column(name = "FORM_DT")
	private  Timestamp form_dt;
	
	@Column(name = "ENTRY_ID")
	private String entry_id;
	
	@Column(name = "ENTRY_DT")
	private  Timestamp entry_dt;
	
	@Column(name = "UPDATE_ID")
	private String update_id;
	
	@Column(name = "UPDATE_DT")
	private  Timestamp update_dt;
	
	@Column(name = "FISCAL_YEAR")
	private  String fiscal_year;
	
	@Column(name = "BILL_AFTER_VAT")
	private  Double bill_after_vat;
	
	
	@Column(name = "ACCUM_BILL_AFTER_VAT")
	private  Double accu_bill_after_vat;
	
	@Column(name = "CURRENT_INCOME_TAX")
	private  Double curr_tds;
	
	@Column(name = "ACCUM_INCOME_TAX")
	private  Double accu_tds;
	
	@Column(name = "VAT_RATE")
	private  Integer vat_rate;
	
	@Column(name = "AIT_RATE")
	private  Integer ait_rate;
	
	@javax.persistence.Transient 
	private Integer sector_id;
	
	
	
	
	
	

}
