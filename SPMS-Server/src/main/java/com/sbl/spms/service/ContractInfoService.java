package com.sbl.spms.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.AMCNotification;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contyearwiserate;
import com.sbl.spms.model.Menu;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
@Service
@Slf4j
public class ContractInfoService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	@Autowired
	private VendorServiceTypeService vendorServiceType;
	@Autowired
	private ContyearwiserateService contyearwiserateService;
	
	public List<ContractInfo> getContractInfoList() {
		return commonService.getDataList(new ContractInfo(), null);
	}

	public ContractInfo saveOrUpdateContractInfo(ContractInfo contractInfo) throws Exception {

		String status = null;
		ContractInfo closedContractInfo = null;
		final Session session = sessionFactory.openSession();
		try {

			log.debug("Calling Contract Info procedure");

			// ==null)?"0":contractInfo.getContId()
			session.getTransaction().begin();
			if (contractInfo.getRefConNo() != null) {
				closedContractInfo = (ContractInfo) session.get(ContractInfo.class, contractInfo.getRefConNo());
			}
			if(closedContractInfo!=null) {
				contractInfo.setTenderDt(contractInfo.getTenderDt() == null ? (closedContractInfo.getTenderDt()==null?new Timestamp(System.currentTimeMillis()):closedContractInfo.getTenderDt()):contractInfo.getTenderDt());
				contractInfo.setTenderNo(contractInfo.getTenderNo() == null ? (closedContractInfo.getTenderNo()==null?"":closedContractInfo.getTenderNo()) : contractInfo.getTenderNo());
				contractInfo.setTotalDcDrConnectivity(contractInfo.getTotalDcDrConnectivity() == null ? (closedContractInfo.getTotalDcDrConnectivity()==null?0:closedContractInfo.getTotalDcDrConnectivity()) : contractInfo.getTotalDcDrConnectivity());
				contractInfo.setCommDate(contractInfo.getCommDate() == null ? (closedContractInfo.getCommDate()==null?new Timestamp(System.currentTimeMillis()):closedContractInfo.getCommDate()):closedContractInfo.getCommDate());
//				contractInfo.setRenewDate(new Timestamp(System.currentTimeMillis()));
			}
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTRACT_INFO.toString(),
					ContractInfo.class);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContId() == null ? "0" : contractInfo.getContId());
			call.registerParameter("pCONT_NO", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContNo() == null ? "" : contractInfo.getContNo());
			call.registerParameter("pCONT_NAME", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContName() == null ? "" : contractInfo.getContName());
			call.registerParameter("pSERVICE_TYPE_ID", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getServiceTypeId() == null ? "" : contractInfo.getServiceTypeId());
			call.registerParameter("pCONT_PARTY", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContParty() == null ? "" : contractInfo.getContParty());
			call.registerParameter("pCONT_VALID_START", Timestamp.class, ParameterMode.IN).bindValue(
					contractInfo.getContValidStart() == null ? new Timestamp(0) : contractInfo.getContValidStart());
			call.registerParameter("pCONT_VALID_END", Timestamp.class, ParameterMode.IN).bindValue(
					contractInfo.getContValidEnd() == null ? new Timestamp(0) : contractInfo.getContValidEnd());
			call.registerParameter("pTENDER_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(contractInfo.getTenderDt()==null?new Timestamp(System.currentTimeMillis()):contractInfo.getTenderDt());
			call.registerParameter("pTENDER_NO", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getTenderNo()==null?"":contractInfo.getTenderNo());
			call.registerParameter("pTOTAL_MONTH", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalMonth() == null ? 0 : contractInfo.getTotalMonth());
			call.registerParameter("pMONTHLY_CHARGE", long.class, ParameterMode.IN)
					.bindValue(contractInfo.getMonthlyCharge());
			call.registerParameter("pTOTAL_CHARGE", long.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalCharge());
			call.registerParameter("pTOTAL_BR", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalBr() == null ? 0 : contractInfo.getTotalBr());
			call.registerParameter("pTOTAL_ATM", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalAtm() == null ? 0 : contractInfo.getTotalAtm());
			call.registerParameter("pTOTAL_DC_DR_CONNECTIVITY", Integer.class, ParameterMode.IN).bindValue(contractInfo.getTotalDcDrConnectivity()==null?0:contractInfo.getTotalDcDrConnectivity());
			call.registerParameter("pVEN_ID", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getVenId() == null ? "" : contractInfo.getVenId());
			call.registerParameter("pSTATUS", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getStatus() == null ? "" : contractInfo.getStatus());
			call.registerParameter("pMODULE_ID", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getModuleId() == null ? 0 : contractInfo.getModuleId());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(contractInfo.getEntryUser());
			call.registerParameter("pVAT", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getVat() == null ? "" : contractInfo.getVat());
			call.registerParameter("pTAX", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getTax() == null ? "" : contractInfo.getTax());
			call.registerParameter("pType", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getType() == null ? "" : contractInfo.getType());

			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getUpdateUser() == null ? "" : contractInfo.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));

			call.registerParameter("pCONT_RENEW_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(contractInfo.getRenewDate() == null ? new Timestamp(0) : contractInfo.getRenewDate());
			call.registerParameter("pREF_CONTACT_NO", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getRefConNo() == null ? "" : contractInfo.getRefConNo());
			call.registerParameter("pWARRANT_PERIOD", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getWarrantyPeriod() == null ? "" : contractInfo.getWarrantyPeriod());
			call.registerParameter("pREMARKS", String.class, ParameterMode.IN)
			.bindValue(contractInfo.getRemarks() == null ? "" : contractInfo.getRemarks());
			
			call.registerParameter("pCOMM_DT", Timestamp.class, ParameterMode.IN)
			.bindValue(contractInfo.getCommDate()==null?new Timestamp(System.currentTimeMillis()):contractInfo.getCommDate());
			
			call.registerParameter("pCONT_DT", Timestamp.class, ParameterMode.IN)
			.bindValue(contractInfo.getContDate() == null ? new Timestamp(System.currentTimeMillis()) : contractInfo.getContDate());
			
//			call.registerParameter("pPERFORM_REPORT_DT", Timestamp.class, ParameterMode.IN)
//			.bindValue(contractInfo.getPerformReportDt()==null?new Timestamp(System.currentTimeMillis()):contractInfo.getPerformReportDt());
//			
//			call.registerParameter("pINSPECT_REPORT_DT", Timestamp.class, ParameterMode.IN)
//			.bindValue(contractInfo.getInspectReportDt() == null ? new Timestamp(System.currentTimeMillis()) : contractInfo.getInspectReportDt());
			
			//IN ORDER TO SAVE DATE VALUE BLANK IN DB AS THESE FIELD AREN'T MANDATORY 
			
			call.registerParameter("pINSPECT_REPORT_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pINSPECT_REPORT_DT", contractInfo.getInspectReportDt());
			
			call.registerParameter("pPERFORM_REPORT_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pPERFORM_REPORT_DT", contractInfo.getPerformReportDt());
			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			// String opStatusCode = (String) call.getOutputParameterValue("OUT_STATUS");

			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			} 
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			if (session != null) {
				session.close();
			}

			throw e;
		} 
		if (contractInfo.getRefConNo() != null) {					
			
		
			closedContractInfo.setStatus("CAN");
			closedContractInfo.setRenewDate(new Timestamp(System.currentTimeMillis()));
			closedContractInfo.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			closedContractInfo = UpdateContractInfoService(closedContractInfo);
		}
		
		
		

		return contractInfo;
	}

	public ContractInfo UpdateContractInfoService(ContractInfo contractInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		ContractInfo closedContractInfo = null;
		try {

				log.debug("update calling");
				session.beginTransaction();
				contractInfo.setUpdateDt(new Timestamp(System.currentTimeMillis()));
				session.update(contractInfo);
			

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contractInfo;
	}

	
	public ContractInfo DeleteContractInfo(ContractInfo contractInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(contractInfo);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contractInfo;
	}

	public List<ContractInfo> getLabels() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();

		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		List<ContractInfo> contractId = null;
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			contractId = session.createQuery("from ContractInfo where status='ACT'", ContractInfo.class).list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return contractId;
	}

	public String readWrite(FileInputStream excelFile, ContractInfo conInfo) throws Exception {

		System.out.println("Start Reading File.");
		log.debug("session calling");
		String status = null;
		try {

			// FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			Workbook workbook = WorkbookFactory.create(excelFile);
			// Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			iterator.next();
			ContractInfo contractInfo = new ContractInfo();
			Cell mobileCell = null;
			int i = 1;
			while (iterator.hasNext()) {

				Row currentRow = iterator.next();
				i++;
				// if (datatypeSheet.getPhysicalNumberOfRows() == i)
				// break;
				//
				// else {
				if (currentRow.getRowNum() > 0) {

					if (currentRow.getRowNum() > 0) {

						///////// contract id
						contractInfo.setContId("");

						///////// contract id

						///////// contract number
						mobileCell = currentRow.getCell(0);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setContNo(mobileCell.getStringCellValue());
						} else
							contractInfo.setContNo("");
						///////// contract number

						///////// contract name
						mobileCell = currentRow.getCell(1);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setContName(mobileCell.getStringCellValue());
						} else
							contractInfo.setContName("");
						///////// contract name

						///////// service type
						mobileCell = currentRow.getCell(2);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							String serviceId = vendorServiceType.serviceIdByName(mobileCell.getStringCellValue());
							contractInfo.setServiceTypeId(serviceId);
						} else
							contractInfo.setServiceTypeId("");

						///////// service type

						///////// contract party
						mobileCell = currentRow.getCell(3);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setContParty(mobileCell.getStringCellValue());
						} else
							contractInfo.setContParty("");

						///////// contract party

						///////// contract valid start
						mobileCell = currentRow.getCell(4);
						String strStartDate = "";
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							strStartDate = mobileCell.getStringCellValue();
						}
						java.util.Date startDate;
						if (strStartDate == null || strStartDate.equals("")) {
							startDate = new Date();
						} else {

							//
							startDate = DateUtil.getJavaDate(Double.parseDouble(strStartDate));

						}
						contractInfo.setContValidStart(new Timestamp(startDate.getTime()));

						///////// contract valid start

						///////// contract valid end
						mobileCell = currentRow.getCell(5);
						String strEndDate = "";
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							strEndDate = mobileCell.getStringCellValue();
						}
						java.util.Date endDate;
						if (strEndDate == null || strEndDate.equals("")) {
							endDate = new Date();
						} else {

							//
							endDate = DateUtil.getJavaDate(Double.parseDouble(strEndDate));

						}
						contractInfo.setContValidEnd(new Timestamp(endDate.getTime()));

						///////// contract valid end

						///////// contract tender date
						mobileCell = currentRow.getCell(6);
						String strTenderDate = "";
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							strTenderDate = mobileCell.getStringCellValue();
						}
						java.util.Date tenderDate;
						if (strTenderDate == null || strTenderDate.equals("")) {
							tenderDate = new Date();
						} else {

							//
							tenderDate = DateUtil.getJavaDate(Double.parseDouble(strTenderDate));

						}
						contractInfo.setTenderDt(new Timestamp(tenderDate.getTime()));

						///////// contract tender date

						///////// contract tender number
						mobileCell = currentRow.getCell(7);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setTenderNo(mobileCell.getStringCellValue());
						} else
							contractInfo.setTenderNo("");

						///////// contract tender number

						///////// contract total month

						mobileCell = currentRow.getCell(8);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setTotalMonth(StringUtils.isNotBlank(mobileCell.getStringCellValue())
									? Integer.parseInt(mobileCell.getStringCellValue())
									: 0);
						} else
							contractInfo.setTotalMonth(0);

						///////// contract total month
						///////// contract monthly charge

						mobileCell = currentRow.getCell(9);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setMonthlyCharge(StringUtils.isNotBlank(mobileCell.getStringCellValue())
									? Integer.parseInt(mobileCell.getStringCellValue())
									: 0);
						} else
							contractInfo.setMonthlyCharge(0);

						///////// contract monthly charge

						///////// contract total charge
						mobileCell = currentRow.getCell(10);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setTotalCharge(StringUtils.isNotBlank(mobileCell.getStringCellValue())
									? Integer.parseInt(mobileCell.getStringCellValue())
									: 0);
						} else
							contractInfo.setTotalCharge(0);
						///////// contract total charge

						///////// contract total branch
						mobileCell = currentRow.getCell(11);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setTotalBr(StringUtils.isNotBlank(mobileCell.getStringCellValue())
									? Integer.parseInt(mobileCell.getStringCellValue())
									: 0);
						} else
							contractInfo.setTotalBr(0);
						///////// contract total branch

						///////// contract total atm
						mobileCell = currentRow.getCell(12);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setTotalAtm(StringUtils.isNotBlank(mobileCell.getStringCellValue())
									? Integer.parseInt(mobileCell.getStringCellValue())
									: 0);
						} else
							contractInfo.setTotalAtm(0);
						///////// contract total atm

						///////// total dc dr
						mobileCell = currentRow.getCell(13);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo
									.setTotalDcDrConnectivity(StringUtils.isNotBlank(mobileCell.getStringCellValue())
											? Integer.parseInt(mobileCell.getStringCellValue())
											: 0);
						} else
							contractInfo.setTotalDcDrConnectivity(0);

						///////// total dc dr

						///////// vendor
						mobileCell = currentRow.getCell(14);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							String vendor = mobileCell.getStringCellValue();
							if (vendor.indexOf("+") > -1)
								vendor = vendor.substring(0, vendor.indexOf("+"));
							String venId = callVendorInfo(vendor);
							contractInfo.setVenId(venId);
						} else
							contractInfo.setVenId("");
						///////// vendor

						///////// status
						mobileCell = currentRow.getCell(15);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setStatus(mobileCell.getStringCellValue());
						} else
							contractInfo.setStatus("");

						///////// status

						contractInfo.setModuleId(conInfo.getModuleId());
						contractInfo.setMonthlyCharge(0);
						contractInfo.setEntryUser(conInfo.getEntryUser());
						contractInfo.setEntryDate(conInfo.getEntryDate());
						contractInfo.setUpdateDt(conInfo.getUpdateDt());
						contractInfo.setUpdateUser(conInfo.getUpdateUser());
						///////// renew date
						mobileCell = currentRow.getCell(21);
						String strrenewDate = "";
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							strrenewDate = mobileCell.getStringCellValue();
						}
						java.util.Date renewDate;
						if (strrenewDate == null || strrenewDate.equals("")) {
							renewDate = new Timestamp(0);
						} else {

							//
							renewDate = DateUtil.getJavaDate(Double.parseDouble(strrenewDate));

						}
						contractInfo.setRenewDate(new Timestamp(renewDate.getTime()));

						///////// renew date

						///////// vat
						mobileCell = currentRow.getCell(22);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setVat(mobileCell.getStringCellValue());
						} else
							contractInfo.setVat("");

						///////// vat

						///////// tax
						mobileCell = currentRow.getCell(23);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setTax(mobileCell.getStringCellValue());
						} else
							contractInfo.setTax("");

						///////// tax

						///////// type
						mobileCell = currentRow.getCell(24);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setType(mobileCell.getStringCellValue());
						} else
							contractInfo.setType("");

						///////// type

						///////// warranty period
						mobileCell = currentRow.getCell(25);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setWarrantyPeriod(mobileCell.getStringCellValue());
						} else
							contractInfo.setWarrantyPeriod("");

						///////// warranty period

						///////// ref contract number
						String conNum = "";
						mobileCell = currentRow.getCell(26);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							conNum = contractNumberIdByNumber(mobileCell.getStringCellValue());
							contractInfo.setRefConNo(conNum);
						} else
							contractInfo.setRefConNo("");

						///////// ref contract number
						
						
				///////// Remarks
						mobileCell = currentRow.getCell(27);
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							contractInfo.setRemarks(mobileCell.getStringCellValue());
						} else
							contractInfo.setRemarks("");

				///////// Remarks
										
				///////// comm date
						mobileCell = currentRow.getCell(28);
						String strCommDate = "";
						if (mobileCell != null) {
							mobileCell.setCellType(CellType.STRING);
							strCommDate = mobileCell.getStringCellValue();
						}
						java.util.Date commDate;
						if (strCommDate == null || strCommDate.equals("")) {
							commDate = new Date();
						} else {

							//
							commDate = DateUtil.getJavaDate(Double.parseDouble(strCommDate));

						}
						contractInfo.setCommDate(new Timestamp(commDate.getTime()));

				///////// comm date

						status = InsertRowInDB(contractInfo);
						if (status.indexOf("Error") > -1) {
							break;

						}

					}
				}
			}

		} catch (FileNotFoundException e) {
			log.error("", e);
			e.printStackTrace();
			status = e.getMessage();
		} catch (IOException e) {
			log.error("", e);
			e.printStackTrace();
			status = e.getMessage();
		}

		System.out.println("Finish File Reading.");
		return status;

	}

	private String callVendorInfo(String vendor) {
		Transaction transaction = null;
		Session session = null;
		String vendorId = null;

		VendorInfo vendorInfo = new VendorInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			//vendor = vendor.toUpperCase();
			// vendor =vendor.replaceAll("\\s", "");
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<VendorInfo> query = builder.createQuery(VendorInfo.class);
			Root<VendorInfo> rootDetails = query.from(VendorInfo.class);
			query.select(rootDetails).where(
					builder.equal(builder.trim(builder.upper(rootDetails.get("venName"))), vendor.toUpperCase().trim()));
//			query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("venName")), vendor));

			Query<VendorInfo> qResult = session.createQuery(query);
			try {
				vendorInfo = qResult.getSingleResult();
				vendorId = vendorInfo.getVenId();

			} catch (NoResultException nre) {
				vendorInfo = null;
//				vendorId = vendor;
			}

			transaction.commit();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return vendorId;
	}

	public String InsertRowInDB(ContractInfo contractInfo) {

		final Session session = sessionFactory.openSession();
		String status = null;
		try {

			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTRACT_INFO.toString(),
					ContractInfo.class);
			

			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContId() == null ? "0" : contractInfo.getContId());
			call.registerParameter("pCONT_NO", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContNo() == null ? "" : contractInfo.getContNo());
			call.registerParameter("pCONT_NAME", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContName() == null ? "" : contractInfo.getContName());
			call.registerParameter("pSERVICE_TYPE_ID", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getServiceTypeId() == null ? "" : contractInfo.getServiceTypeId());
			call.registerParameter("pCONT_PARTY", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getContParty() == null ? "" : contractInfo.getContParty());
			call.registerParameter("pCONT_VALID_START", Timestamp.class, ParameterMode.IN).bindValue(
					contractInfo.getContValidStart() == null ? new Timestamp(0) : contractInfo.getContValidStart());
			call.registerParameter("pCONT_VALID_END", Timestamp.class, ParameterMode.IN).bindValue(
					contractInfo.getContValidEnd() == null ? new Timestamp(0) : contractInfo.getContValidEnd());
			call.registerParameter("pTENDER_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(contractInfo.getTenderDt() == null ? new Timestamp(System.currentTimeMillis())
							: contractInfo.getTenderDt());
			call.registerParameter("pTENDER_NO", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getTenderNo() == null ? "" : contractInfo.getTenderNo());
			call.registerParameter("pTOTAL_MONTH", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalMonth() == null ? 0 : contractInfo.getTotalMonth());
			call.registerParameter("pMONTHLY_CHARGE", long.class, ParameterMode.IN)
					.bindValue(contractInfo.getMonthlyCharge());
			call.registerParameter("pTOTAL_CHARGE", long.class, ParameterMode.IN)
					.bindValue( contractInfo.getTotalCharge());
			call.registerParameter("pTOTAL_BR", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalBr() == null ? 0 : contractInfo.getTotalBr());
			call.registerParameter("pTOTAL_ATM", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getTotalAtm() == null ? 0 : contractInfo.getTotalAtm());
			call.registerParameter("pTOTAL_DC_DR_CONNECTIVITY", Integer.class, ParameterMode.IN).bindValue(
					contractInfo.getTotalDcDrConnectivity() == null ? 0 : contractInfo.getTotalDcDrConnectivity());
			call.registerParameter("pVEN_ID", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getVenId() == null ? "" : contractInfo.getVenId());
			call.registerParameter("pSTATUS", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getStatus() == null ? "" : contractInfo.getStatus());
			call.registerParameter("pMODULE_ID", Integer.class, ParameterMode.IN)
					.bindValue(contractInfo.getModuleId() == null ? 0 : contractInfo.getModuleId());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(contractInfo.getEntryUser());
			call.registerParameter("pVAT", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getVat() == null ? "" : contractInfo.getVat());
			call.registerParameter("pTAX", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getTax() == null ? "" : contractInfo.getTax());
			call.registerParameter("pType", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getType() == null ? "" : contractInfo.getType());

			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getUpdateUser() == null ? "" : contractInfo.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));

			call.registerParameter("pCONT_RENEW_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(contractInfo.getRenewDate() == null ? new Timestamp(0) : contractInfo.getRenewDate());
			call.registerParameter("pREF_CONTACT_NO", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getRefConNo() == null ? "" : contractInfo.getRefConNo());
			call.registerParameter("pWARRANT_PERIOD", String.class, ParameterMode.IN)
					.bindValue(contractInfo.getWarrantyPeriod() == null ? "" : contractInfo.getWarrantyPeriod());
			
			call.registerParameter("pREMARKS", String.class, ParameterMode.IN)
			.bindValue(contractInfo.getRemarks() == null ? "" : contractInfo.getRemarks());
			
			call.registerParameter("pCOMM_DT", Timestamp.class, ParameterMode.IN)
			.bindValue(contractInfo.getCommDate() == null ? new Timestamp(0) : contractInfo.getCommDate());

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();

			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			// e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return status;
	}

	public String contractNumberById(String conId) {
		Transaction transaction = null;
		Session session = null;
		String contractNumber = null;

		ContractInfo conInfo = new ContractInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			conId = conId.toUpperCase();
			conId = conId.replaceAll("\\s", "");
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractInfo> query = builder.createQuery(ContractInfo.class);
			Root<ContractInfo> rootDetails = query.from(ContractInfo.class);
			query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("contId")), conId));

			Query<ContractInfo> qResult = session.createQuery(query);
			try {
				conInfo = qResult.getSingleResult();
				contractNumber = conInfo.getContNo();

			} catch (NoResultException nre) {
				conInfo = null;
				contractNumber = conId;
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return contractNumber;
	}
	public Date startagreementById(String conId) {
		Transaction transaction = null;
		Session session = null;
		Timestamp startagreement = new Timestamp(System.currentTimeMillis());

		ContractInfo conInfo = new ContractInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			conId = conId.toUpperCase();
			conId = conId.replaceAll("\\s", "");
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractInfo> query = builder.createQuery(ContractInfo.class);
			Root<ContractInfo> rootDetails = query.from(ContractInfo.class);
			query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("contId")), conId));

			Query<ContractInfo> qResult = session.createQuery(query);
			try {
				conInfo = qResult.getSingleResult();
				startagreement = conInfo.getContValidStart();

			} catch (NoResultException nre) {
				conInfo = null;
				startagreement = new Timestamp(System.currentTimeMillis());
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return new Date(startagreement.getTime());

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return new Date(startagreement.getTime());
	}
	
	public Date endagreementById(String conId) {
		Transaction transaction = null;
		Session session = null;
		Timestamp endagreement = new Timestamp(System.currentTimeMillis());

		ContractInfo conInfo = new ContractInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			conId = conId.toUpperCase();
			conId = conId.replaceAll("\\s", "");
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractInfo> query = builder.createQuery(ContractInfo.class);
			Root<ContractInfo> rootDetails = query.from(ContractInfo.class);
			query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("contId")), conId));

			Query<ContractInfo> qResult = session.createQuery(query);
			try {
				conInfo = qResult.getSingleResult();
				endagreement = conInfo.getContValidEnd();

			} catch (NoResultException nre) {
				conInfo = null;
				endagreement = new Timestamp(System.currentTimeMillis());
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return new Date(endagreement.getTime());

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return  new Date(endagreement.getTime());
	}

	public String contractNumberIdByNumber(String conNo) {
		// Transaction transaction = null;
		Session session = null;
		String contractId = null;

		ContractInfo conInfo = new ContractInfo();
		try {
			session = sessionFactory.openSession();
			// transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractInfo> query = builder.createQuery(ContractInfo.class);
			Root<ContractInfo> rootDetails = query.from(ContractInfo.class);
			// query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("contNo")),
			// conNo.toUpperCase().trim()));
			query.select(rootDetails).where(
					builder.equal(builder.trim(builder.upper(rootDetails.get("contNo"))), conNo.toUpperCase().trim()));

			Query<ContractInfo> qResult = session.createQuery(query);
			try {
				conInfo = qResult.getSingleResult();
				contractId = conInfo.getContId();

			} catch (NoResultException nre) {
				conInfo = null;
				contractId = conNo;
			}

			// transaction.commit();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			// if (transaction != null) {
			// transaction.rollback();
			// }
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return contractId;
	}
	public ContractInfo getContract(String contract) {
		Session session = sessionFactory.openSession();
		ContractInfo contractInfo = null;
		try {
//			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			 contractInfo = (ContractInfo) session.get(ContractInfo.class, contract);
	             
	            session.getTransaction().commit();
//			contractInfo =  (ContractInfo) session.get(ContractInfo.class, contract);
			
//			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contractInfo;
	}
	public List<Object> getcontractInfoJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("select c.*,V.VEN_NAME from Contract_info c join vendor_info v on(c.VEN_ID=v.VEN_ID)")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
	
	public List<Object> getChildByCont() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        String[] strArray;
        MultiMap multiMap = new MultiValueMap();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("select cont_id, listagg(child, ';' ) within group (order by cont_id) child from\r\n" + 
					" (SELECT\r\n" + 
					"   e1.cont_id,e2.cont_id child   \r\n" + 
					"FROM\r\n" + 
					"    contract_info e1\r\n" + 
					"INNER JOIN contract_info e2 ON\r\n" + 
					"    e1.cont_id = e2.ref_contact_no\r\n" + 
					" ORDER BY  \r\n" + 
					"   e1.cont_id,e2.ref_contact_no)\r\n" + 
					"   \r\n" + 
					"     group by cont_id order by cont_id")
		            .list();
			
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
