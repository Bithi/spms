package com.sbl.spms.service;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.model.VendorType;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VendorTypeService {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public List<VendorType> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<VendorType> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from VendorType", VendorType.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
        return labels;
    }

	public String typeIdByName(String serName) {
		Transaction transaction = null;
		Session session = null;
		String serviceId ="";

		VendorType serviceType = new VendorType();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			serName = serName.toUpperCase();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<VendorType> query = builder.createQuery(VendorType.class);
			Root<VendorType> rootDetails = query.from(VendorType.class);
			query.select(rootDetails)
				.where(builder.equal(builder.upper(rootDetails.get("venTypeName")), serName));

			Query<VendorType> qResult = session.createQuery(query);
			try {
				serviceType = qResult.getSingleResult();
				serviceId = serviceType.getVenTypeId();
				
			} catch (NoResultException nre) {
				log.error("", nre);
				serviceType = null;
				serviceId = serName;
			}

			transaction.commit();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
		        session.close();
		    }
			return "";
			
		}
		finally {
			if (session != null) {
		        session.close();
		      }
		}
		
		return serviceId;
	}
}
