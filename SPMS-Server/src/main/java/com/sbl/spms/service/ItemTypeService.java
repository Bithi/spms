package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ItemTypeService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	public ItemType saveOrUpdateItemType(ItemType itemType) throws Exception {
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_ITEMTYPE.toString(), ItemType.class);
			call.registerParameter("pTYPE_SHORTNAME", String.class, ParameterMode.IN).bindValue((itemType.getShortName()==null)?"":itemType.getShortName());
			call.registerParameter("pTYPEDESC", String.class, ParameterMode.IN).bindValue((itemType.getDescription()==null)?"":itemType.getDescription());			
			call.registerParameter("pCATEGORY", String.class, ParameterMode.IN).bindValue((itemType.getCategory()==null)?"":itemType.getCategory());
			
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(itemType.getEntryUser());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue(itemType.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return itemType;
	}
	
	public List<ItemType> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<ItemType> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from ItemType", ItemType.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return labels;
    }
	
	public List<ItemType> getitemTypeList() {
		return commonService.getDataList(new ItemType(),null);
	}

	
	
	public ItemType updateItemType(ItemType mATR) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			mATR.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(mATR);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}

	
	public ItemType DeleteItemType(ItemType itemType) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(itemType);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return itemType;
	}

}
