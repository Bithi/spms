package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.PenaltyConfig;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PenaltyConfigService {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	@Autowired
	private DowntimeService downtimeService;
	

	
	public List<PenaltyConfig> getPenaltyConfigList() {

		List<PenaltyConfig>penaltyConfigList = new ArrayList<PenaltyConfig>();
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<PenaltyConfig> criteriaQuery = builder.createQuery(PenaltyConfig.class);
			Root<PenaltyConfig> root = criteriaQuery.from(PenaltyConfig.class);
			criteriaQuery.select(root);
			Query<PenaltyConfig> query = session.createQuery(criteriaQuery);

			penaltyConfigList = query.getResultList();
			transaction.commit();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return penaltyConfigList;
//		return commonService.getDataList(new ContractBranch(), null);
	}
	
	private String callVendorInfo(String vendor) {
		Transaction transaction = null;
		Session session = null;
		String vendorId =null;

		VendorInfo vendorInfo = new VendorInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<VendorInfo> query = builder.createQuery(VendorInfo.class);
			Root<VendorInfo> rootDetails = query.from(VendorInfo.class);
			query.select(rootDetails)
				.where(builder.equal(rootDetails.get("venName"), vendor));

			Query<VendorInfo> qResult = session.createQuery(query);
			try {
				vendorInfo = qResult.getSingleResult();
				vendorId = vendorInfo.getVenId();
				
			} catch (NoResultException nre) {
				vendorInfo = null;
				vendorId = vendor;
			}

			transaction.commit();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
		        session.close();
		    }
			return null;
			
		}
		finally {
			if (session != null) {
		        session.close();
		      }
		}
		
		return vendorId;
	}
	

	
	
	
	public PenaltyConfig saveOrUpdatePenaltyConfig(PenaltyConfig penaltyConfig) throws Exception {
//		System.err.print("----"+penaltyConfig.getContractId()+" "+penaltyConfig.getVendorId()+" "+penaltyConfig.getEntryUser()+" "
//				+penaltyConfig.getUpdateUser()+" "+penaltyConfig.getEntryDate()+" "+penaltyConfig.getUpdateDt()
//				+penaltyConfig.getAtTimeInMonth()+" "+penaltyConfig.getDistInMonth()+" "+penaltyConfig.getPenaltyRate()+"-----");
		final Session session = sessionFactory.openSession();
		String status = null;
		try {
			
			log.debug("Calling SP");
			String idx = penaltyConfig.getVendorId();
			String  id = callVendorInfo(idx);
			
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_PENALTY_CONFIG.toString(), PenaltyConfig.class);
			call.registerParameter("pCONFIG_ID", Integer.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN).bindValue( penaltyConfig.getContractId());
			call.registerParameter("pVEN_ID", String.class, ParameterMode.IN).bindValue(penaltyConfig.getVendorId());
			call.registerParameter("pCOND_TYPE", String.class, ParameterMode.IN).bindValue(penaltyConfig.getCondType());
			call.registerParameter("pMIN_DOWNTIME", Integer.class, ParameterMode.IN).bindValue(penaltyConfig.getMinDowntime());
			call.registerParameter("pMAX_DOWNTIME", Integer.class, ParameterMode.IN).bindValue(penaltyConfig.getMaxDowntime());
			call.registerParameter("pPENALTY_RATE", Integer.class, ParameterMode.IN).bindValue(penaltyConfig.getPenaltyRate());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(penaltyConfig.getEntryUser());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue(penaltyConfig.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			//String opStatusCode = (String) call.getOutputParameterValue("OUT_STATUS");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		
		return penaltyConfig;
	}
	
	public PenaltyConfig UpdatePenaltyConfigService(PenaltyConfig penaltyConfig) throws Exception {
		final Session session = sessionFactory.openSession();
		System.err.print("--------- "+penaltyConfig.getConfigId()+"----");
		
		try {
			session.beginTransaction();
			penaltyConfig.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(penaltyConfig);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
		return penaltyConfig;
	}

/*public PenaltyConfig updatePenaltyConfigInfo(PenaltyConfig penaltyConfig) throws Exception {

	
	Integer configId = 0;
	if(penaltyConfig.getConfigId()!=null) {
		configId = penaltyConfig.getConfigId();
	}
		
		String  contractId = penaltyConfig.getContractId();
		String    vendorId = penaltyConfig.getVendorId();
		Integer  distInMonth = penaltyConfig.getDistInMonth();
		Integer  atTimeInMonth = penaltyConfig.getAtTimeInMonth();
		Integer penaltyRate = penaltyConfig.getPenaltyRate();
		String entryUser = penaltyConfig.getEntryUser();
		Timestamp entryDate = penaltyConfig.getEntryDate();
		String updateUser = penaltyConfig.getUpdateUser();
		Timestamp updateDt = penaltyConfig.getUpdateDt();

		
		PenaltyConfig penaltyConfigOne = new PenaltyConfig(configId,contractId,vendorId,distInMonth,atTimeInMonth,penaltyRate,entryUser,entryDate,updateUser,updateDt);
		return UpdatePenaltyConfigService(penaltyConfigOne);
}


*/
public PenaltyConfig DeletePenaltyConfigInfo(PenaltyConfig penaltyConfig) throws Exception {
	final Session session = sessionFactory.openSession();
	System.err.print("--------- "+penaltyConfig.getConfigId()+"----");
	
	try {
		session.beginTransaction();
		session.delete(penaltyConfig);
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
		session.close();
		
		}

}
	return penaltyConfig;
}

public List<Object> getPenaltyConfigJoin() throws Exception{
	final Session session = sessionFactory.openSession();
    session.beginTransaction();
    List<Object> object = new ArrayList<Object>();
    // We read labels record from database using a simple Hibernate
    // query, the Hibernate Query Language (HQL).
    session.getTransaction().commit();
    
    try {
		session.beginTransaction();
		object = session.createNativeQuery("SELECT p.*, \r\n" + 
				"       C.cont_no, \r\n" + 
				"       V.ven_name \r\n" + 
				"FROM   penalty_conf p \r\n" + 
				"       join vendor_info v \r\n" + 
				"         ON( p.vend_id = v.ven_id ) \r\n" + 
				"       join contract_info c \r\n" + 
				"         ON ( c.cont_id = p.cont_id ) ")
	            .list();
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
		session.close();
		
		}

}

    return object;
}

}
