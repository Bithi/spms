package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SubItemTypeService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
public SubItemType saveOrUpdateSubItemType(SubItemType subItemType) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			System.err.print(subItemType.getItemId()+" "+subItemType.getItemName()+" "+subItemType.getEntryUser()+" "
			+subItemType.getUpdateUser()+" "+subItemType.getTypeId()+" "+subItemType.getEntryDt()+" "+subItemType.getUpdateDt());
			
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_SUBITEMTYPE.toString(), SubItemType.class);
			call.registerParameter("pITEMID", String.class, ParameterMode.IN).bindValue((subItemType.getItemId()==null)?"":subItemType.getItemId());
			call.registerParameter("pITEMNAME", String.class, ParameterMode.IN).bindValue((subItemType.getItemName()==null)?"":subItemType.getItemName());	
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue((subItemType.getEntryUser()==null)?"":subItemType.getEntryUser());
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue((subItemType.getUpdateUser()==null)?"":subItemType.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("pTYPEID", String.class, ParameterMode.IN).bindValue(subItemType.getTypeId());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return subItemType;
	}


	public List<SubItemType> getsubItemTypeList() {
		return commonService.getDataList(new SubItemType(),null);
	}
	
	
	public SubItemType updateSubItemType(SubItemType mATR) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			mATR.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(mATR);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}
	
	
	public SubItemType deleteSubItemType(SubItemType subItemType) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(subItemType);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return subItemType;
	}


	public List<SubItemType> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        //session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<SubItemType> labels = null;
        //session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from SubItemType", SubItemType.class)
		            .list();
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			//session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				
			session.close();
			
			}

	}

        return labels;
    }

}
