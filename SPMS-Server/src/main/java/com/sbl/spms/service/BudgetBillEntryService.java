package com.sbl.spms.service;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.BudgetBillEntry;
import com.sbl.spms.model.Contractbilltran;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VatAitProduct;
import com.sbl.spms.model.VendorServiceType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class BudgetBillEntryService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;

	public BudgetBillEntry saveBudgetBillEntry(BudgetBillEntry budgetBillEntry) throws Exception {

		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();

			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_BUDGETBILLENTRY.toString(),
					BudgetBillEntry.class);

			call.registerParameter("pENTRY_ID", String.class, ParameterMode.IN)
					.bindValue((budgetBillEntry.getEntry_id() == null) ? "" : budgetBillEntry.getEntry_id());

			call.registerParameter("pENTRY_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));

			call.registerParameter("pUPDATE_ID", String.class, ParameterMode.IN)
					.bindValue((budgetBillEntry.getUpdate_id() == null) ? "" : budgetBillEntry.getUpdate_id());

			call.registerParameter("pUPDATE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pUPDATE_DT", null);

			call.registerParameter("pINVOICE_NO", String.class, ParameterMode.IN)
					.bindValue((budgetBillEntry.getInvoice_no() == null) ? "" : budgetBillEntry.getInvoice_no());

			call.registerParameter("pINVOICE_DT", Timestamp.class, ParameterMode.IN).bindValue(
					budgetBillEntry.getInvoice_dt() == null ? new Timestamp(0) : budgetBillEntry.getInvoice_dt());

			call.registerParameter("pPRODUCT_DESCRIPTION", String.class, ParameterMode.IN).bindValue(
					(budgetBillEntry.getProduct_description() == null) ? "" : budgetBillEntry.getProduct_description());

			call.registerParameter("pBILL_AMOUNT", String.class, ParameterMode.IN)
					.bindValue((budgetBillEntry.getBill_amount() == null) ? "" : budgetBillEntry.getBill_amount());

			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN)
					.bindValue((budgetBillEntry.getCont_id() == null) ? "" : budgetBillEntry.getCont_id());

			call.registerParameter("pWORK_ORDER_NO", String.class, ParameterMode.IN)
					.bindValue((budgetBillEntry.getWork_order_no() == null) ? "" : budgetBillEntry.getWork_order_no());

			call.registerParameter("pDELIVERY_DT", Timestamp.class, ParameterMode.IN).bindValue(
					budgetBillEntry.getDelivery_dt() == null ? new Timestamp(0) : budgetBillEntry.getDelivery_dt());

//			call.registerParameter("pBILL_RECEIVE_DT", Timestamp.class, ParameterMode.IN)
//					.bindValue(budgetBillEntry.getBill_recieve_dt() == null ? null
//							: budgetBillEntry.getBill_recieve_dt());
			
			call.registerParameter("pBILL_RECEIVE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pBILL_RECEIVE_DT", budgetBillEntry.getBill_recieve_dt());
			
			call.registerParameter("pBILL_TO_AUDIT_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pBILL_TO_AUDIT_DT", budgetBillEntry.getBill_to_audit_dt());
			
			call.registerParameter("pAUDIT_REPORT_RECEIVE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pAUDIT_REPORT_RECEIVE_DT", budgetBillEntry.getAudit_report_receive_dt());
			
			call.registerParameter("pSEND_TO_BDC_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pSEND_TO_BDC_DT", budgetBillEntry.getSend_to_bdc_dt());
			
			call.registerParameter("pPAYMENT_NOTE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pPAYMENT_NOTE_DT", budgetBillEntry.getPayment_note_dt());	
			
			call.registerParameter("pFROM_BDC_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pFROM_BDC_DT", budgetBillEntry.getFrom_bdc_dt());	
			
			call.registerParameter("pNOTE_APPROVE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pNOTE_APPROVE_DT", budgetBillEntry.getNote_approve_dt());
			
			call.registerParameter("pVAT_PRODUCT", Integer.class, ParameterMode.IN)
			.bindValue(budgetBillEntry.getVat_product());
			
			call.registerParameter("pAIT_PRODUCT", Integer.class, ParameterMode.IN)
			.bindValue(budgetBillEntry.getAit_product());

			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);

			call.execute();

			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);

			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return budgetBillEntry;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getBudgetBillEntryJoin() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session
					.createNativeQuery("SELECT B.*" + ",C.CONT_DT" + ",C.VEN_ID" + ",C.WARRANT_PERIOD"
							+ ",C.PERFORM_REPORT_DT" + ",C.INSPECT_REPORT_DT "
							+ "FROM PROCMNT.BUDGET_BILL_ENTRY B JOIN PROCMNT.CONTRACT_INFO C ON B.CONT_ID = C.CONT_ID")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}

	public BudgetBillEntry UpdateBudgetBillInfoService(BudgetBillEntry budgetBillInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		try {

				log.debug("update calling");
				session.beginTransaction();
				budgetBillInfo.setUpdate_dt(new Timestamp(System.currentTimeMillis()));
				session.update(budgetBillInfo);	

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return budgetBillInfo;
	}

	
	public BudgetBillEntry DeleteBudgetBillInfo(BudgetBillEntry budgetBillInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		try {

				log.info("delete calling");
				session.beginTransaction();
				//budgetBillInfo.setUpdate_dt(new Timestamp(System.currentTimeMillis()));
//				session.merge(budgetBillInfo);
//				session.delete(budgetBillInfo);	
				BudgetBillEntry persistentBudgetBillEntry = session.load(BudgetBillEntry.class, budgetBillInfo.getBudget_bill_ref());
				session.delete(persistentBudgetBillEntry);


		} catch (Exception e) {
			
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return budgetBillInfo;
		
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String getTotalBillPerContract(String contId) throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		String totalBillPerContract;
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			totalBillPerContract = String.valueOf(session
					.createNativeQuery("SELECT SUM(B.BILL_AMOUNT) AS TOTAL_BILL FROM PROCMNT.BUDGET_BILL_ENTRY B WHERE B.CONT_ID= :contId")
					.setParameter("contId", contId)
		            .getSingleResult());

			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return totalBillPerContract;
	}
	
	//FOR BACKUP OPTION IN CASE REQUIREMENT CHANGE
	//(IF CONT_BILL_ENTRY TABLE IS CONSIDERED FOR TOTAL CONTRACT AMOUNT CALCULATION)
	
	@SuppressWarnings("unchecked")
	public BudgetBillEntry getPerContractBill(String contId) throws Exception {
		
		BudgetBillEntry budgetBill = new BudgetBillEntry();
		
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		//String totalBillPerContract;
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			List<Object[]> result = session
					.createNativeQuery("SELECT SUM(B.BILL_AMOUNT) AS BUDGET, SUM(C.BILL_AMOUNT) AS CONT FROM PROCMNT.BUDGET_BILL_ENTRY B JOIN PROCMNT.CONT_BILL_ENTRY C ON B.CONT_ID = C.CONT_ID WHERE B.CONT_ID = :contId")
					.setParameter("contId", contId)
		            .getResultList();

			session.flush();
			
			for (Object[] row : result) {
				budgetBill.setBudget_bill_amnt_by_cont(String.valueOf(row[0]));
				budgetBill.setCont_bill_amnt_by_cont(String.valueOf(row[1]));
			}
			
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return budgetBill;
	}
	
	
	
	
	public List<BudgetBillEntry> getBudgetBillEntryByContract(String cont) throws Exception{
		BudgetBillEntry budgetBillEntry = new BudgetBillEntry();
			List<BudgetBillEntry> billEntryList = null;
			final Session session = sessionFactory.openSession();

			try {
				session.beginTransaction();
				// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
				Criteria crit = session.createCriteria(BudgetBillEntry.class);
				crit.add(Restrictions.eq("cont_id", cont));
				billEntryList = crit.list();
				if (billEntryList.size() > 0) {
					budgetBillEntry = billEntryList.get(0);
				}

				session.flush();

			} catch (Exception e) {
				e.printStackTrace();
				session.getTransaction().rollback();
				session.close();
				throw e;
			} finally {
				if (session != null) {
					if (session.getTransaction().isActive()) {
						session.getTransaction().commit();
					}
					session.close();

				}

			}
			return billEntryList;
		}

	
	public List<Object> getBudgetBillEntryByRef(String ref) throws Exception{
		BudgetBillEntry budgetBillEntry = new BudgetBillEntry();
			List<Object> objectList = null;
			final Session session = sessionFactory.openSession();

			try {
				session.beginTransaction();
				objectList = session
						.createNativeQuery("select  e.INVOICE_NO,e.BILL_AMOUNT,e.CONT_ID,v.RATE as AIT , (select rate from VAT_AIT_DTLS where e.VAT_PRODUCT=PRODUCT_TYPE ) as vat from BUDGET_BILL_ENTRY e,VAT_AIT_DTLS v where e.BILL_AMOUNT >= v.MIN_BASE_AMT and e.BILL_AMOUNT <= v.MAX_BASE_AMT and e.AIT_PRODUCT=v.PRODUCT_TYPE and invoice_no='"+ref+"'")
						.list();
				session.flush();
				
			
				session.flush();

			} catch (Exception e) {
				e.printStackTrace();
				session.getTransaction().rollback();
				session.close();
				throw e;
			} finally {
				if (session != null) {
					if (session.getTransaction().isActive()) {
						session.getTransaction().commit();
					}
					session.close();

				}

			}
			return objectList;
		}

	
}
