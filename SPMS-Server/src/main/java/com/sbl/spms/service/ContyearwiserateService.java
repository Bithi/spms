package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contractbilltax;
import com.sbl.spms.model.Contyearwiserate;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContyearwiserateService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
public Contyearwiserate saveOrUpdateContYearwiseRate(Contyearwiserate contRate) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTYEARWISERATE.toString(), Contyearwiserate.class);
			call.registerParameter("pID", String.class, ParameterMode.IN).bindValue((contRate.getId()==null)?"":contRate.getId());
			call.registerParameter("pCONTID", String.class, ParameterMode.IN).bindValue((contRate.getCont_id()==null)?"":contRate.getCont_id());
			call.registerParameter("pRATE", Integer.class, ParameterMode.IN).bindValue((contRate.getRate()==null)?0:contRate.getRate());
			call.registerParameter("pSTARTDATE", Timestamp.class, ParameterMode.IN).bindValue(contRate.getStart_date()==null?new Timestamp(0):contRate.getStart_date());
			call.registerParameter("pENDDATE", Timestamp.class, ParameterMode.IN).bindValue(contRate.getEnd_date()==null?new Timestamp(0):contRate.getEnd_date());
			call.registerParameter("pCONTRACTTYPE", String.class, ParameterMode.IN).bindValue(contRate.getContract_type());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue((contRate.getEntryusr()==null)?"":contRate.getEntryusr());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue((contRate.getUpdateusr()==null)?"":contRate.getUpdateusr());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contRate;
	}



public List<Contyearwiserate> getContYearwiseRate() {
	return commonService.getDataList(new Contyearwiserate(),null);
}


public Contyearwiserate updateContyearwiserate(Contyearwiserate mATR) throws  Exception {
	final Session session = sessionFactory.openSession();

	try {
		log.debug("session calling");
		session.getTransaction().begin();
		mATR.setUpdatedt(new Timestamp(System.currentTimeMillis()));
		session.saveOrUpdate(mATR);
		session.flush();

	} 
	catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		
		System.out.println(e.getCause());
		session.getTransaction().rollback();
		session.close();
		if(e.getCause().toString().contains("ConstraintViolationException")) {
			throw new Exception("Constraint Violation occured");
		}
		else
			throw e;
	}
	finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return mATR;
}


public Contyearwiserate deleteContYearwiseRate(Contyearwiserate contRate) throws Exception {
	final Session session = sessionFactory.openSession();
	// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

	try {
		session.beginTransaction();
		session.delete(contRate);
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return contRate;
}


public List<ContractInfo> getAllId() throws Exception{
	final Session session = sessionFactory.openSession();
    session.beginTransaction();

    // We read labels record from database using a simple Hibernate
    // query, the Hibernate Query Language (HQL).
    List<ContractInfo> labels = null;
    session.getTransaction().commit();
    
    try {
		session.beginTransaction();
		labels = session.createQuery("from ContractInfo", ContractInfo.class)
	            .list();
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
		session.close();
		
		}

}
    return labels;
}


public List<Object> getContYearWiseRate() throws Exception{
	final Session session = sessionFactory.openSession();
    session.beginTransaction();
    List<Object> object = new ArrayList<Object>();
    // We read labels record from database using a simple Hibernate
    // query, the Hibernate Query Language (HQL).
    session.getTransaction().commit();
    
    try {
		session.beginTransaction();
		object = session.createNativeQuery("SELECT b.*, \r\n" + 
				"       i.cont_no \r\n" + 
				"FROM   cont_yearwise_rate b \r\n" + 
				"       join contract_info i \r\n" + 
				"         ON( b.cont_id = i.cont_id ) ")
	            .list();
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
		session.close();
		
		}

}

    return object;
}




}
