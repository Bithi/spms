package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.Contractbilltax;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContractbilltaxService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
public Contractbilltax saveOrUpdateContBillTax(Contractbilltax contTax) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		 
		     
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTRACTBILLTAX.toString(), Contractbilltax.class);
			call.registerParameter("pCONTTAXID", String.class, ParameterMode.IN).bindValue((contTax.getCont_tax_id()==null)?"":contTax.getCont_tax_id());
			call.registerParameter("pMINAMT", long.class, ParameterMode.IN).bindValue(contTax.getMin_amt());
			call.registerParameter("pMAXAMT", long.class, ParameterMode.IN).bindValue(contTax.getMax_amt());
			call.registerParameter("pCONTTAXRATE", Integer.class, ParameterMode.IN).bindValue((contTax.getCont_tax_rate()==null)?0:contTax.getCont_tax_rate());
			call.registerParameter("pFISCALYEAR", String.class, ParameterMode.IN).bindValue((contTax.getFiscal_year()==null)?"":contTax.getFiscal_year());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue((contTax.getEntryusr()==null)?"":contTax.getEntryusr());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue((contTax.getUpdateusr()==null)?"":contTax.getUpdateusr());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contTax;
	}



public List<Contractbilltax> getContBillTax() {
	return commonService.getDataList(new Contractbilltax(),null);
}


public Contractbilltax updateContBillTax(Contractbilltax mATR) throws  Exception {
	final Session session = sessionFactory.openSession();

	try {
		log.debug("session calling");
		session.beginTransaction();
		mATR.setUpdatedt(new Timestamp(System.currentTimeMillis()));
		session.saveOrUpdate(mATR);
		session.flush();

	} 
	catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		
		System.out.println(e.getCause());
		session.getTransaction().rollback();
		if(e.getCause().toString().contains("ConstraintViolationException")) {
			throw new Exception("Constraint Violation occured");
		}
		else
			throw e;
	}
	finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return mATR;
}


public Contractbilltax deleteContBillTax(Contractbilltax contTax) throws Exception {
	final Session session = sessionFactory.openSession();
	// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

	try {
		session.beginTransaction();
		session.delete(contTax);
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		throw e;
	} finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return contTax;
}
}
