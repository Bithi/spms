package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Model;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ModelService {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CommonService commonService;

	public Model saveOrUpdateModel(Model model) throws Exception {

		
		final Session session = sessionFactory.openSession();

		String status = null;
		Model model2= new Model();
		model2=getDuplicateEntryByModelNameWithBrand(model);
		if(model2==null) {
			
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();

			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_MODEL.toString(), Model.class);
			call.registerParameter("pMODELID", String.class, ParameterMode.IN)
					.bindValue((model.getModelId() == null) ? "" : model.getModelId());
			call.registerParameter("pBRANDID", String.class, ParameterMode.IN)
					.bindValue((model.getBrandId() == null) ? "" : model.getBrandId());
			call.registerParameter("pMODELNAME", String.class, ParameterMode.IN)
					.bindValue((model.getModelName() == null) ? "" : model.getModelName());
			call.registerParameter("pWARRMON", Double.class, ParameterMode.IN)
					.bindValue((model.getWarrantyInMonth() == null) ? 0.0 : model.getWarrantyInMonth());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(model.getEntryUser());
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue(model.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}
		

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
//			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return model;
		}
		else {
			return null;
		}
		
	}

	public List<Model> getModelList() {
		return commonService.getDataList(new Model(), null);
	}

	public Model updateModel(Model mATR) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			mATR.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			Model model2= new Model();
			model2=getDuplicateEntryByModelNameWithBrand(mATR);
			if(model2==null) {
			session.saveOrUpdate(mATR);
			}
			else {
				mATR = null;
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();

			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if (e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			} else
				throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}

	public Model deleteModel(Model model) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(model);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return model;
	}

	
	public List<Object> getmodelBrandJJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("select m.*,b.brandname from model m join brand b on(m.brandid=b.brandid)")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
	
	public Model getDuplicateEntryByModelNameWithBrand(Model model) {
		Transaction transaction = null;
		Session session = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Model> query = builder.createQuery(Model.class);
			Root<Model> rootDetails = query.from(Model.class);
			if(model.getModelId()==null) {
			query.select(rootDetails).where(
					builder.and(
							builder.equal(rootDetails.get("modelName"), model.getModelName()),
									builder.equal(rootDetails.get("brandId"), model.getBrandId())
//									,builder.notEqual(rootDetails.get("modelId"), model.getModelId())
									)
					
					);
			}
			else {
				query.select(rootDetails).where(
						builder.and(
								builder.equal(rootDetails.get("modelName"), model.getModelName()),
										builder.equal(rootDetails.get("brandId"), model.getBrandId())
										,builder.notEqual(rootDetails.get("modelId"), model.getModelId())
										)
						
						);
			}

			Query<Model> qResult = session.createQuery(query);
			try {
				model = qResult.getSingleResult();

			} catch (NoResultException nre) {
				model=null;
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return model;
	}
}
