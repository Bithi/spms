package com.sbl.spms.service;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.apache.commons.io.FileUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.FileUp;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FileUploadService {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CommonService commonService;

	@Value("${file.store.location}")
	private String fileStoreLocation;

	public FileUp fileInsertToDB(FileUp fileUp, MultipartFile file) throws Exception {
		final Session session = sessionFactory.openSession();

		String status = null;
		int fileId = 0;
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_FILE.toString(), FileUp.class);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN)
					.bindValue((fileUp.getContId() == null) ? "" : fileUp.getContId());
			call.registerParameter("pFILE_TYPE", String.class, ParameterMode.IN)
					.bindValue((fileUp.getFileType() == null) ? "" : fileUp.getFileType());
			call.registerParameter("pFILE_LOC", String.class, ParameterMode.IN)
					.bindValue((fileUp.getFileLoc() == null) ? "" : fileUp.getFileLoc() + file.getOriginalFilename());

			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(fileUp.getEntryusr());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue(fileUp.getUpdateusr());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			File fileStore = null;
			log.info(opStatus);
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			} else {
				String[] parts = opStatus.split("-");
				fileId = Integer.parseInt(parts[0]);
				status = parts[1];
				fileUp.setFileId(fileId);
				fileStore = new File(fileStoreLocation+"/",fileUp.getFileLoc()+fileId+"@"+ file.getOriginalFilename());

				FileUtils.writeByteArrayToFile(fileStore, file.getBytes());
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return fileUp;
	}

	public List<FileUp> getFileList() {
		return commonService.getDataList(new FileUp(), null);
	}

	public FileUp delete(FileUp fileUp) throws Exception {
		final Session session = sessionFactory.openSession();
		String removeFileCheck = "false";

		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {

			session.beginTransaction();
			File file = new File(fileUp.getFileLoc());

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
				removeFileCheck = "true";
				session.delete(fileUp);
				session.flush();
			} else {
				session.getTransaction().rollback();
				throw new Exception("Delete operation is failed.");
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return fileUp;
	}
	
	
	
	public List<Object> getFileUploadWithContractInfoJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.cont_no \r\n" + 
					"FROM   cont_file_location b \r\n" + 
					"       join contract_info i \r\n" + 
					"         ON( b.cont_id = i.cont_id ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
