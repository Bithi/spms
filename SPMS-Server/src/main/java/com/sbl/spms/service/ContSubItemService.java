package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContSubItem;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContSubItemService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;

	public List<ContSubItem> getPaymentMethodList() {
		return commonService.getDataList(new ContSubItem(), null);
	}

	public ContSubItem saveData(ContSubItem data) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_CONT_SUBITEM.toString(),
					ContSubItem.class);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN).bindValue(data.getContId());
			call.registerParameter("pITEMID", String.class, ParameterMode.IN)
					.bindValue(data.getItemId());
			call.registerParameter("pITEM_QUANTITY", Integer.class, ParameterMode.IN)
					.bindValue(data.getItemQuantity());
			call.registerParameter("pTOTAL_VALUE", long.class, ParameterMode.IN)
					.bindValue(data.getTotalValue());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN)
					.bindValue(data.getEntryUser());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pUPDATEUSR", null);
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pUPDATEDT", new Timestamp(0));
			
			call.registerParameter("pCONT_SUBITEM_ID", String.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pCONT_SUBITEM_ID", null);
			
			call.registerParameter("pMODELID", String.class, ParameterMode.IN)
			.bindValue(data.getModelId());
			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return data;
	}

	public ContSubItem updateData(ContSubItem data) throws Exception {
		final Session session = sessionFactory.openSession();
		/*
		 * TO_DO:H: If needed sp will be added
		 */
		try {
			log.debug("session calling");
			session.getTransaction().begin();
			data.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(data);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return data;
	}

	public ContSubItem deleteData(ContSubItem data) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(data);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return data;
	}
	public List<ContSubItem> getItemByContractt(String contId) {
		List<ContSubItem> list = null;
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(ContSubItem.class);
			crit.add(Restrictions.eq("contId", contId));
			list = crit.list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return list;
	}
	
	public List<Object[]> getItemByContract(String contId) {
		List<ContSubItem> list = null;
		final Session session = sessionFactory.openSession();
		List<Object[]> africanContinents = null;
		try {
			session.beginTransaction();
			africanContinents = session.createQuery(
			        "select cont.contId,conit.contSubItemId,mod.modelName,subit.itemName,conit.itemId"
			        + " from ContractInfo cont join ContSubItem conit on (cont.contId=conit.contId)"
			        + "join SubItemType subit on(conit.itemId=subit.itemId)"
			        + "join Model mod on(conit.modelId=mod.modelId)"
			        + "  where cont.contId=:con")
					.setParameter("con", contId)
			        .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return africanContinents;
	}
	
	
	public List<Object> getcontsubitemModelJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("select cs.*,c.cont_no,s.itemname,m.modelname from CONT_SUBITEM cs join contract_info c on(c.cont_id=cs.cont_id)\r\n" + 
					"join subitemtype s on(s.itemid=cs.itemid) join model m on(m.modelid=cs.modelid)")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
