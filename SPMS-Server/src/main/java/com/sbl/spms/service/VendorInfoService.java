package com.sbl.spms.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VendorInfoService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	@Autowired
	private VendorServiceTypeService vendorServiceTypeService;
	@Autowired
	private VendorTypeService vendorTypeService;

	public List<VendorInfo> getVendorInfoList() {
		return commonService.getDataList(new VendorInfo(), null);
	}

	
	public VendorInfo saveOrUpdateUserInfo(VendorInfo vendorInfo) throws Exception {
		final Session session = sessionFactory.openSession();

		String status = null;

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_VENDOR.toString(),
					VendorInfo.class);
			call.registerParameter("pVEN_ID", String.class, ParameterMode.IN)
					.bindValue((vendorInfo.getVenId() == null) ? "0" : vendorInfo.getVenId());
			call.registerParameter("pVEN_NAME", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenName() == null ? "" : vendorInfo.getVenName());
			call.registerParameter("pVEN_TYPE_ID", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenTypeId() == null ? "" : vendorInfo.getVenTypeId());
			call.registerParameter("pSERVICE_TYPE_ID", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getServiceTypeId() == null ? "" : vendorInfo.getServiceTypeId());
			call.registerParameter("pVEN_ADD", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenAdd() == null ? "" : vendorInfo.getVenAdd());
			call.registerParameter("pVEN_PHONE_NO", Integer.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenPhoneNumber() == null ? 0 : vendorInfo.getVenPhoneNumber());
			call.registerParameter("pVEN_TRADE_LICENSE_NO", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenTradeLicenseNo() == null ? "" : vendorInfo.getVenTradeLicenseNo());
			call.registerParameter("pVEN_TIN_NO", Integer.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenTinNo() == null ? 0 : vendorInfo.getVenTinNo());
			call.registerParameter("pUSER_ID", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getUserId() == null ? "" : vendorInfo.getUserId());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pMACHINE_IP", String.class, ParameterMode.IN)
					.bindValue((vendorInfo.getMachineIp() == null) ? "" : vendorInfo.getMachineIp());
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getUpdateusr() == null ? "" : vendorInfo.getUpdateusr());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.registerParameter("pBIN_NO", String.class, ParameterMode.IN).bindValue(vendorInfo.getBin_no()== null ? "" : vendorInfo.getBin_no());;
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return vendorInfo;
	}

	public List<VendorInfo> getLabels() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();

		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		List<VendorInfo> labels = null;
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			labels = session.createQuery("from VendorInfo", VendorInfo.class).list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return labels;
	}
	
	

	public VendorInfo saveOrUpdateUserInfox(VendorInfo vendorInfo) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			vendorInfo.setUpdatedt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(vendorInfo);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return vendorInfo;
	}

	public VendorInfo deleteAllVendorInfo(VendorInfo vendorInfo) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(vendorInfo);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return vendorInfo;
	}

	public List<ContractInfo> getContractInfos(String venId) {
		List<ContractInfo> list = null;
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(ContractInfo.class);
			crit.add(Restrictions.eq("venId", venId));
			list = crit.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return list;
	}

	public List<ContractBranch> getContractBranhchInfos(String contId) {
		List<ContractBranch> list = null;
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(ContractBranch.class);
			crit.add(Restrictions.eq("contractId", contId));
			list = crit.list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return list;
	}

	public boolean matchUserId(String userId) {
		List<UserInfo> list = null;
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(UserInfo.class);
			crit.add(Restrictions.eq("userId", userId));
			list = crit.list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		if (!list.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public String readWrite(FileInputStream excelFile, VendorInfo venInfo) throws Exception {

		System.out.println("Start Reading File.");
		String status = null;
		try {

			// FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			Workbook workbook = WorkbookFactory.create(excelFile);
			// Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			iterator.next();
			VendorInfo vendorInfo = new VendorInfo();
			Cell mobileCell = null;

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getRowNum() > 0) {
					///////// vendor id
					vendorInfo.setVenId("");

					///////// vendor id

					///////// vendor Name
					mobileCell = currentRow.getCell(0);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						vendorInfo.setVenName(mobileCell.getStringCellValue());
					} else
						vendorInfo.setVenName("");
					///////// vendor Name

					///////// vendor Type
					mobileCell = currentRow.getCell(1);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						String typeId = vendorTypeService.typeIdByName(mobileCell.getStringCellValue());
						vendorInfo.setVenTypeId(typeId);
					} else
						vendorInfo.setVenTypeId("");
					///////// vendor Type

					///////// vendor service Type
					mobileCell = currentRow.getCell(2);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						String serviceId = vendorServiceTypeService.serviceIdByName(mobileCell.getStringCellValue());
						vendorInfo.setServiceTypeId(serviceId);
					} else
						vendorInfo.setServiceTypeId("");
					///////// vendor service Type

					///////// vendor Address
					mobileCell = currentRow.getCell(3);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						vendorInfo.setVenAdd(mobileCell.getStringCellValue());

					} else
						vendorInfo.setVenAdd("");
					///////// vendor Address

					///////// vendor PhoneNumber
					mobileCell = currentRow.getCell(4);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						vendorInfo.setVenPhoneNumber(Integer.parseInt(mobileCell.getStringCellValue()));

					} else
						vendorInfo.setVenPhoneNumber(0);
					///////// vendor PhoneNumber

					///////// vendor TRADE_LICENSE_NO
					mobileCell = currentRow.getCell(5);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						vendorInfo.setVenTradeLicenseNo(mobileCell.getStringCellValue());

					} else
						vendorInfo.setVenTradeLicenseNo("");
					///////// vendor TRADE_LICENSE_NO

					///////// vendor TinNo
					mobileCell = currentRow.getCell(6);
					if (mobileCell != null) {
						mobileCell.setCellType(CellType.STRING);
						vendorInfo.setVenTinNo(Integer.parseInt(mobileCell.getStringCellValue()));

					} else
						vendorInfo.setVenTinNo(0);
					///////// vendor TinNo

					vendorInfo.setUserId(venInfo.getUserId());
					vendorInfo.setEntryDt(venInfo.getEntryDt());
					vendorInfo.setUpdatedt(venInfo.getUpdatedt());
					vendorInfo.setUpdateusr(venInfo.getUpdateusr());

					status = InsertRowInDB(vendorInfo);
					if (status.indexOf("Error") > -1) {
						break;

					}
				}
			}

		} catch (FileNotFoundException e) {
			log.error("", e);
			e.printStackTrace();
			status = e.getMessage();
		} catch (IOException e) {
			log.error("", e);
			e.printStackTrace();
			status = e.getMessage();
		}

		System.out.println("Finish File Reading.");
		return status;

	}

	public String InsertRowInDB(VendorInfo vendorInfo) {

		final Session session = sessionFactory.openSession();
		String status = null;
		try {

			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_VENDOR.toString(),
					VendorInfo.class);
			call.registerParameter("pVEN_ID", String.class, ParameterMode.IN)
					.bindValue((vendorInfo.getVenId() == null) ? "0" : vendorInfo.getVenId());
			call.registerParameter("pVEN_NAME", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenName() == null ? "" : vendorInfo.getVenName());
			call.registerParameter("pVEN_TYPE_ID", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenTypeId() == null ? "" : vendorInfo.getVenTypeId());
			call.registerParameter("pSERVICE_TYPE_ID", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getServiceTypeId() == null ? "" : vendorInfo.getServiceTypeId());
			call.registerParameter("pVEN_ADD", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenAdd() == null ? "" : vendorInfo.getVenAdd());
			call.registerParameter("pVEN_PHONE_NO", Integer.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenPhoneNumber() == null ? 0 : vendorInfo.getVenPhoneNumber());
			call.registerParameter("pVEN_TRADE_LICENSE_NO", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenTradeLicenseNo() == null ? "" : vendorInfo.getVenTradeLicenseNo());
			call.registerParameter("pVEN_TIN_NO", Integer.class, ParameterMode.IN)
					.bindValue(vendorInfo.getVenTinNo() == null ? 0 : vendorInfo.getVenTinNo());
			call.registerParameter("pUSER_ID", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getUserId() == null ? "" : vendorInfo.getUserId());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pMACHINE_IP", String.class, ParameterMode.IN)
					.bindValue((vendorInfo.getMachineIp() == null) ? "" : vendorInfo.getMachineIp());
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN)
					.bindValue(vendorInfo.getUpdateusr() == null ? "" : vendorInfo.getUpdateusr());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			// e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return status;
	}

	public String vendorNameById(String vendor) {
		Transaction transaction = null;
		Session session = null;
		String vendorName = null;

		VendorInfo vendorInfo = new VendorInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			vendor = vendor.toUpperCase();
			vendor = vendor.replaceAll("\\s", "");
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<VendorInfo> query = builder.createQuery(VendorInfo.class);
			Root<VendorInfo> rootDetails = query.from(VendorInfo.class);
			query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("venId")), vendor));

			Query<VendorInfo> qResult = session.createQuery(query);
			try {
				vendorInfo = qResult.getSingleResult();
				vendorName = vendorInfo.getVenName();

			} catch (NoResultException nre) {
				vendorInfo = null;
				vendorName = vendor;
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return vendorName;
	}
}
