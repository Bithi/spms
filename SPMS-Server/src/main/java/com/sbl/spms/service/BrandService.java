package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Brand;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BrandService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
public Brand saveOrUpdateBrand(Brand brand) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_BRAND.toString(), Brand.class);
			call.registerParameter("pBRANDID", String.class, ParameterMode.IN).bindValue((brand.getBrandId()==null)?"":brand.getBrandId());
			call.registerParameter("pITEMID", String.class, ParameterMode.IN).bindValue((brand.getItemId()==null)?"":brand.getItemId());	
			call.registerParameter("pBRANDNAME", String.class, ParameterMode.IN).bindValue((brand.getBrandName()==null)?"":brand.getBrandName());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(brand.getEntryUser());
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue(brand.getUpdateUser());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return brand;
	}


	public List<Brand> getBrandList() {
		return commonService.getDataList(new Brand(),null);
	}
	
	
	public Brand updateBrand(Brand mATR) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			log.debug("session calling");
			session.beginTransaction();
			mATR.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(mATR);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}
	
	
	public Brand deleteBrand(Brand brand) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(brand);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return brand;
	}
	
	
	public List<Brand> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        //session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<Brand> labels = null;
        //session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from Brand", Brand.class)
		            .list();
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			//session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				
			session.close();
			
			}

	}

        return labels;
    }
	
	
	public List<Object> getSubItemAndBrandJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.itemname \r\n" + 
					"FROM   brand b \r\n" + 
					"       join subitemtype i \r\n" + 
					"         ON( b.itemid = i.itemid ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
