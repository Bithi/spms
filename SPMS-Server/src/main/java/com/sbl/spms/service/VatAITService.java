package com.sbl.spms.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.model.Vat;
import com.sbl.spms.model.VatAitProduct;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class VatAITService {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<VatAitProduct> getVATLabels() throws Exception{
		
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<VatAitProduct> vatList = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			vatList = session.createQuery("from VatAitProduct where vatAitType='VAT'", VatAitProduct.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return vatList;
    }

public List<VatAitProduct> getAITLabels() throws Exception{
		
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<VatAitProduct> aitList = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			aitList = session.createQuery("from VatAitProduct where vatAitType='AIT'", VatAitProduct.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return aitList;
    }

}

