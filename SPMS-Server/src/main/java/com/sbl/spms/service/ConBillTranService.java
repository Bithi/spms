package com.sbl.spms.service;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.poi.ss.usermodel.DateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.hibernate.result.ResultSetOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.AMCNotification;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contractbilltran;
import com.sbl.spms.model.ItemPerBill;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.util.CommonService;
import com.sbl.spms.util.MailSender;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConBillTranService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	@Autowired
	private MailSender mailsender;
	@Autowired
	private ContractInfoService contractService;
	@Value("${globalIncomeYr}")
	private String globalIncomeYr;
	
	public Contractbilltran saveBillTran(Contractbilltran contBillTran) throws Exception {
		final Session session = sessionFactory.openSession();
		ContractInfo contractInfo = new ContractInfo();
		Contractbilltran contractbilltranPrev = new Contractbilltran();
		contractbilltranPrev = getBillTranByCont(contBillTran.getCont_id());
		if(contractbilltranPrev==null) {
			contractbilltranPrev = new Contractbilltran();
			contractbilltranPrev.setBill_after_vat(0);
		}
		contractInfo = contractService.getContract(contBillTran.getCont_id());
		contBillTran.setVat((Long.parseLong(contractInfo.getVat()) / (100 + Long.parseLong(contractInfo.getVat()))));
		
		// contBillTran.setCurrent_income_tax(Integer.parseInt(contractInfo.getTax()));
		// contBillTran.setCurrent_income_tax(getIncomeTax(contractInfo.getTotalCharge()));
		contBillTran.setBill_after_vat(
				contBillTran.getCurrent_bill() - (contBillTran.getCurrent_bill() * contBillTran.getVat()));
		contBillTran.setAccum_bill_after_vat(contractbilltranPrev.getBill_after_vat()+contBillTran.getBill_after_vat());
		contBillTran.setCurrent_income_tax(getIncomeTax(contractInfo.getTotalCharge()));
		Double incomeTaxRate = contBillTran.getCurrent_income_tax()*0.01;
		contBillTran.setFinal_bill_vendor(contBillTran.getVat() + contBillTran.getCurrent_income_tax()
				+ contBillTran.getCurrent_bill() - (contBillTran.getPenalty()));
		contBillTran.setAccum_income_tax(Math.round(contBillTran.getFinal_bill_vendor()*incomeTaxRate));
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_BILL_TRAN.toString(),
					Contractbilltran.class);
			call.registerParameter("pbill_tran_id", String.class, ParameterMode.IN)
					.bindValue((contBillTran.getBill_tran_id() == null) ? "" : contBillTran.getBill_tran_id());
			// call.registerParameter("pbill_no", String.class,
			// ParameterMode.IN).bindValue(contBillTran.getBill_no()==null?"":contBillTran.getBill_no());
			// call.registerParameter("pcont_id", String.class,
			// ParameterMode.IN).bindValue(contBillTran.getCont_id()==null?"":contBillTran.getCont_id());
			// call.registerParameter("pbill_part_full", String.class,
			// ParameterMode.IN).bindValue(contBillTran.getBill_part_full()==null?"":contBillTran.getBill_part_full());
			// call.registerParameter("pbill_date", Timestamp.class,
			// ParameterMode.IN).bindValue(contBillTran.getBill_date()==null?new
			// Timestamp(0):contBillTran.getBill_date());
			call.registerParameter("pchg_head", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getChg_head() == null ? "" : contBillTran.getChg_head());
			// call.registerParameter("pitemid", String.class,
			// ParameterMode.IN).bindValue(contBillTran.getItem_id()==null?"":contBillTran.getItem_id());
			call.registerParameter("pbill_period_start", Timestamp.class, ParameterMode.IN)
					.bindValue(contBillTran.getBill_period_start() == null ? new Timestamp(0) :  contBillTran.getBill_period_start());
			call.registerParameter("pbill_period_end", Timestamp.class, ParameterMode.IN)
			.bindValue(contBillTran.getBill_period_end() == null ? new Timestamp(0) :  contBillTran.getBill_period_end());
			call.registerParameter("pcurrent_bill", long.class, ParameterMode.IN)
					.bindValue(contBillTran.getCurrent_bill());
			call.registerParameter("pvat", long.class, ParameterMode.IN).bindValue(contBillTran.getVat());
			call.registerParameter("pbill_after_vat", long.class, ParameterMode.IN)
					.bindValue(contBillTran.getBill_after_vat());
			call.registerParameter("paccum_bill_after_vat", long.class, ParameterMode.IN)
					.bindValue(contBillTran.getAccum_bill_after_vat());
			call.registerParameter("pcurrent_income_tax", long.class, ParameterMode.IN)
					.bindValue(contBillTran.getCurrent_income_tax());
			call.registerParameter("pACCUM_INCOME_TAX", long.class, ParameterMode.IN)
					.bindValue(contBillTran.getAccum_income_tax());
			call.registerParameter("ppenalty", long.class, ParameterMode.IN).bindValue(contBillTran.getPenalty());
			call.registerParameter("pfinal_bill_vendor", long.class, ParameterMode.IN)
					.bindValue(contBillTran.getFinal_bill_vendor());
			call.registerParameter("pcontact_no", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getContract_no() == null ? "" : contBillTran.getContract_no());
			call.registerParameter("preference_no", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getReference_no() == null ? "" : contBillTran.getReference_no());
			call.registerParameter("preference_date", Timestamp.class, ParameterMode.IN).bindValue(
					contBillTran.getReference_date() == null ? new Timestamp(0) : contBillTran.getReference_date());
			call.registerParameter("pnarration", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getNarration() == null ? "" : contBillTran.getNarration());
			call.registerParameter("pnext_bill_date", Timestamp.class, ParameterMode.IN).bindValue(
					contBillTran.getNext_bill_date() == null ? new Timestamp(0) : contBillTran.getNext_bill_date());
			call.registerParameter("pstatus", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getStatus() == null ? "" : contBillTran.getStatus());
			call.registerParameter("pbill_initiate_id", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getBill_initiate_id() == null ? "" : contBillTran.getBill_initiate_id());
			call.registerParameter("pbill_authorize_id", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getBill_authorize_id() == null ? "" : contBillTran.getBill_authorize_id());
			call.registerParameter("pentryusr", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getEntryusr() == null ? "" : contBillTran.getEntryusr());
			call.registerParameter("pentrydt", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pupdateusr", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getUpdateusr() == null ? "" : contBillTran.getUpdateusr());
			call.registerParameter("pupdatedt", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("pentry_id", String.class, ParameterMode.IN)
					.bindValue(contBillTran.getEntry_id() == null ? "" : contBillTran.getEntry_id());

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contBillTran;
	}

	public Contractbilltran updateBillTran(Contractbilltran contractbilltran) throws Exception {
		final Session session = sessionFactory.openSession();
		/*
		 * TO_DO:H: If needed sp will be added
		 */
		try {
			log.debug("session calling");
			session.getTransaction().begin();
			contractbilltran.setFinal_bill_vendor(contractbilltran.getVat() + contractbilltran.getCurrent_income_tax()
					+ contractbilltran.getCurrent_bill() - (contractbilltran.getPenalty()));
			contractbilltran.setUpdatedt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(contractbilltran);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contractbilltran;
	}

	public Contractbilltran authBillTran(Contractbilltran contractbilltran) throws Exception {
		final Session session = sessionFactory.openSession();
		/*
		 * TO_DO:H: If needed sp will be added
		 */
		try {
			log.debug("session calling");
			session.getTransaction().begin();
			session.saveOrUpdate(contractbilltran);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contractbilltran;
	}

	public Contractbilltran deleteBillTran(Contractbilltran contractbilltran) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(contractbilltran);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contractbilltran;
	}

	public List<Contractbilltran> getContractbilltran() {
		return commonService.getDataList(new Contractbilltran(), null);
	}

	public Contractbilltran getBillTranByEntry(String entry) {
		Contractbilltran conBillTran = new Contractbilltran();
		List<Contractbilltran> contractbilltranList = null;
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(Contractbilltran.class);
			crit.add(Restrictions.eq("entry_id", entry));
			contractbilltranList = crit.list();
			if (contractbilltranList.size() > 0) {
				conBillTran = contractbilltranList.get(0);
			}

			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return conBillTran;
	}

	public Contractbilltran getBillTranByCont(String cont) throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Contractbilltran> object = new ArrayList<Contractbilltran>();
		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		session.getTransaction().commit();
		Contractbilltran contractbilltran ;
		List<Contractbilltran> contractbilltranList = new ArrayList<Contractbilltran>();

		try {
			session.beginTransaction();

			List<Object[]> results = session.createNativeQuery(
					"select t.* from CONT_BILL_TRAN t join CONT_BILL_ENTRY e on(t.ENTRY_ID=e.ENTRY_ID) where T.STATUS='auth' and E.CONT_ID="+cont)
					.list();

			for (Object[] result : results) {
				contractbilltran = new Contractbilltran();
				contractbilltran.setBill_tran_id(result[0]==null?"":result[0].toString());
				contractbilltran.setChg_head(result[1]==null?"":result[1].toString());
				
				contractbilltran.setCurrent_bill(result[3]==null?0:Long.parseLong(result[3].toString()) );
				contractbilltran.setVat(result[4]==null?0:Long.parseLong(result[4].toString()));
				contractbilltran.setBill_after_vat(result[5]==null?0:Long.parseLong(result[5].toString()));
				contractbilltran.setAccum_bill_after_vat(result[6]==null?0:Long.parseLong(result[6].toString()));
				contractbilltran.setCurrent_income_tax(result[7]==null?0:Long.parseLong(result[7].toString()));
				contractbilltran.setAccum_income_tax(result[8]==null?0:Long.parseLong(result[8].toString()));
				contractbilltran.setPenalty(result[9]==null?0:Long.parseLong(result[9].toString()));
				contractbilltran.setFinal_bill_vendor(result[10]==null?0:Long.parseLong(result[10].toString()));
				contractbilltran.setContract_no(result[11]==null?"":result[11].toString());
				contractbilltran.setReference_no(result[12]==null?"":result[12].toString());
				java.util.Date RefDate;
				
				SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				Date lFromDate1;
				if(result[13]==null) {
					lFromDate1=new Date();
				}
				else lFromDate1 = datetimeFormatter1.parse(result[13].toString());
				Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());
				contractbilltran.setReference_date(fromTS1);
				
				contractbilltran.setNarration(result[14]==null?"":result[14].toString());
				Date next_bill_date;
				if(result[15]==null) {
					next_bill_date=new Date();
				}
				else next_bill_date = datetimeFormatter1.parse(result[15].toString());
				Timestamp next_bill_dateT = new Timestamp(next_bill_date.getTime());
				contractbilltran.setNext_bill_date(next_bill_dateT);
				
				contractbilltran.setStatus(result[16]==null?"":result[16].toString());
				contractbilltran.setBill_initiate_id(result[17]==null?"":result[17].toString());
				contractbilltran.setBill_authorize_id(result[18]==null?"":result[18].toString());
				Date entrydt;
				if(result[20]==null) {
					entrydt=new Date();
				}
				else entrydt = datetimeFormatter1.parse(result[20].toString());
				
				Timestamp entrydtT = new Timestamp(entrydt.getTime());
				contractbilltran.setEntrydt(entrydtT);
				contractbilltran.setEntryusr(result[19]==null?"":result[19].toString());
				
				
				
				contractbilltran.setUpdateusr(result[21]==null?"":result[21].toString());
				
				Date updatedt;
				if(result[22]==null) {
					updatedt=new Date();
				}
				else updatedt = datetimeFormatter1.parse(result[22].toString());
				
				Timestamp updatedtT = new Timestamp(updatedt.getTime());
				contractbilltran.setUpdatedt(updatedtT);
				
				
				Date bill_period_start;
				Date bill_period_end;
				if(result[2]==null) {
					bill_period_start=new Date();
				}
				else bill_period_start = datetimeFormatter1.parse(result[2].toString());
				if(result[24]==null) {
					bill_period_end=new Date();
				}
				else bill_period_end = datetimeFormatter1.parse(result[24].toString());
				
				
				Timestamp bill_period_startdt = new Timestamp(bill_period_start.getTime());
				Timestamp bill_period_enddt = new Timestamp(bill_period_end.getTime());
				contractbilltran.setBill_period_start(bill_period_startdt);
				contractbilltran.setBill_period_start(bill_period_enddt);
				contractbilltran.setEntry_id(result[23]==null?"":result[23].toString());
				contractbilltranList.add(contractbilltran);
			}

			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			return new Contractbilltran();
			
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return contractbilltranList.size()==0? null:contractbilltranList.get(0);
	}

	public boolean getDuplicateEntryExists(String entry) {
		List<Contractbilltran> contractbilltranList = null;
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(Contractbilltran.class);
			crit.add(Restrictions.eq("entry_id", entry));
			contractbilltranList = crit.list();
			if (contractbilltranList.size() > 0) {
				session.close();
				return true;
			}
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.close();
			throw e;
		} finally {
			if (session != null) {
//				if (session.getTransaction().isActive()) {
//					session.getTransaction().commit();
//				}
				session.close();

			}

		}
		return false;
	}

	public List<Contractbilltran> getInitialBillTran() throws Exception {

		Transaction transaction = null;
		Session session = null;
		List<Contractbilltran> contractbilltranList = null;
		try {

			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			/*
			 * create criteria which is used for and/or etc.
			 */

			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<Contractbilltran> query = builder.createQuery(Contractbilltran.class);
			Root<Contractbilltran> root = query.from(Contractbilltran.class);
			query.select(root).where(// builder.or(builder.equal(root.get("status"), null),
					(root.get("status").isNull()));// );

			Query<Contractbilltran> q = session.createQuery(query);

			try {
				contractbilltranList = q.getResultList();

			} catch (NoResultException nre) {
				contractbilltranList = null;
			}

			transaction.commit();
			return contractbilltranList;

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	public int getIncomeTax(long balance) throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		Integer incomeTax = 0 ;
		String in="";
		String fiscalYr = globalIncomeYr;
		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		session.getTransaction().commit();

		try {
			session.beginTransaction();

			in = session.createNativeQuery(
					"select cont_tax_rate from CONTRACT_BILL_TAX where FISCAL_YEAR='"+fiscalYr+"' AND\r\n" + 
					"  \r\n" + 
					" "+balance+">=min_amt AND ("+balance+"<=max_amt OR max_amt=-1)")
					.getSingleResult().toString();
//			for (Object[] result : results) {
			incomeTax=Integer.parseInt(in);
//			}

			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return incomeTax;
	}
	// public long getIncomeTax(long charge,String incomrYr ) throws Exception {
	// final Session session = sessionFactory.openSession();
	// try {
	// log.debug("Calling SP");
	// session.getTransaction().begin();
	// ProcedureCall call =
	// session.createStoredProcedureCall(Constant.FN_INCOME_TAX.toString());
	// call.registerParameter("amount", long.class,
	// ParameterMode.IN).bindValue(charge);
	// call.registerParameter("incomeyr", String.class,
	// ParameterMode.IN).bindValue(incomrYr);
	// call.execute();
	// long tax = (long) call.getOutputParameterValue("InStatus");
	// log.info(opStatus);
	// if (opStatus.indexOf("Error") > -1) {
	// throw new Exception(opStatus);
	//
	// }
	//
	// } catch (Exception e) {
	// log.error("", e);
	// if (session != null && session.getTransaction().isActive()) {
	// session.getTransaction().rollback();
	// }
	//
	// return null;
	// } finally {
	// if (session != null && session.isOpen()) {
	// if (session.getTransaction().isActive()) {
	// session.getTransaction().commit();
	// }
	//
	// session.close();
	// }
	//
	// }
	// return contBillTran;
	// }
	
	
	
	public List<Object> getContractBillTransactionJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.chg_head_name \r\n" + 
					"FROM   cont_bill_tran b \r\n" + 
					"       join cont_bill_chg_head i \r\n" + 
					"         ON( b.chg_head = i.head_id ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }


}
