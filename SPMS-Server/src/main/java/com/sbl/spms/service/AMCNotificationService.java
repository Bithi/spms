package com.sbl.spms.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.result.ResultSetOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.AMCNotification;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.util.CommonService;
import com.sbl.spms.util.MailSender;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AMCNotificationService {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private MailSender mailsender;

	@Autowired
	public AMCNotificationService(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
		
	}
	public List<AMCNotification> getVendorInfoList() {
		return commonService.getDataList(new AMCNotification(), null);
	}

	public AMCNotification saveAmcNotification(AMCNotification notif) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_AMC_NOTIFICATION.toString(),
					AMCNotification.class);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN).bindValue(notif.getContractId());
			call.registerParameter("pVENDOR_MOB", String.class, ParameterMode.IN).bindValue(notif.getVendorMobile());
			call.registerParameter("pVENDOR_EMAIL", String.class, ParameterMode.IN).bindValue(notif.getVendorEmail());
			call.registerParameter("pGM_IT_MOB", String.class, ParameterMode.IN).bindValue(notif.getGmItMobile());
			call.registerParameter("pGM_IT_EMAIL", String.class, ParameterMode.IN).bindValue(notif.getGmItEmail());
			call.registerParameter("pDGM_ITPROC_MOB", String.class, ParameterMode.IN)
					.bindValue(notif.getDgmItProcMobile());
			call.registerParameter("pDGM_ITPROC_EMAIL", String.class, ParameterMode.IN)
					.bindValue(notif.getDgmItProcEmail());
			call.registerParameter("pDEALING_OFF_MOB", String.class, ParameterMode.IN)
					.bindValue(notif.getDealingOffMobile());
			call.registerParameter("pDEALING_OFF_EMAIL", String.class, ParameterMode.IN)
					.bindValue(notif.getDealingOffEmail());
			call.registerParameter("pAGM1_itproc_MOB", String.class, ParameterMode.IN)
			.bindValue(notif.getAgm1ItprocMob());
			call.registerParameter("pAGM1_itproc_EMAIL", String.class, ParameterMode.IN)
			.bindValue(notif.getAgm1ItprocEmail());
			call.registerParameter("pAGM2_itproc_MOB", String.class, ParameterMode.IN)
			.bindValue(notif.getAgm2ItprocMob());
			call.registerParameter("pAGM2_itproc_EMAIL", String.class, ParameterMode.IN)
			.bindValue(notif.getAgm2ItprocEmail());
			call.registerParameter("pNOTIF_BEFORE", Integer.class, ParameterMode.IN).bindValue((notif.getNotifBefore()==null)?0:notif.getNotifBefore());
			call.registerParameter("pNOTIF_INTERVAL_DAY", Integer.class, ParameterMode.IN)
					.bindValue((notif.getNotifIntervalDay()==null)?0:notif.getNotifIntervalDay());
			call.registerParameter("pLAST_NOTIF_DATE", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(0));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue(notif.getEntryUser());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			return null;
			
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return notif;
	}

	public AMCNotification updateAmcNotification(AMCNotification amcNotification) throws Exception {
		final Session session = sessionFactory.openSession();
		/*
		 * TO_DO:H: If needed sp will be added
		 */
		try {
			log.debug("session calling");
			session.getTransaction().begin();
			session.saveOrUpdate(amcNotification);
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return amcNotification;
	}
	public AMCNotification deleteAmcNotification(AMCNotification amcNotification) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(amcNotification);
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			return null;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return amcNotification;
	}
	@Scheduled(cron="${corn.time}")
	public void sendMailNotification() throws Exception {
		log.info("start mail sending schedule.");
		//get list of object for sending mail
		Session session = null;
		List<Object> objList= null;
		AMCNotification amcNotification = new AMCNotification();
		try{
			session = sessionFactory.openSession();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_GET_NOTIFICATION.toString());
			call.registerStoredProcedureParameter("OUT_RESULT",Object.class, ParameterMode.REF_CURSOR);
			call.execute();
			objList= ((ResultSetOutput) call.getOutputs().getCurrent()).getResultList();
			Object[] stringValues = null;
            Timestamp ts=new Timestamp(System.currentTimeMillis());  
            Date date=new Date(ts.getTime());
			for (Object o : objList) {
				stringValues= (Object[]) o;
				mailsender.sendMail(String.valueOf(stringValues[0]),"AMC NOTIFICATION", "Contract ("+String.valueOf(stringValues[3] + ") is going to be expired on "+String.valueOf(stringValues[5])));
				amcNotification =  (AMCNotification) session.get(AMCNotification.class, Integer.parseInt(String.valueOf(stringValues[2])));
				
				amcNotification.setLastNotifDate(date);
				amcNotification = updateAmcNotification(amcNotification);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending schedule.");
	}
	
	public List<Object> getcontAmcJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("select a.*, C.CONT_NO from amc_notification a join contract_info c on(c.cont_id=a.cont_id) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			return null;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
