package com.sbl.spms.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContractBranch;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContractBranchService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;

	public List<ContractBranch> getContactBranchList() {

		List<ContractBranch> contractBranchList = new ArrayList<ContractBranch>();
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractBranch> criteriaQuery = builder.createQuery(ContractBranch.class);
			Root<ContractBranch> root = criteriaQuery.from(ContractBranch.class);
			criteriaQuery.select(root);
			Query<ContractBranch> query = session.createQuery(criteriaQuery);

			contractBranchList = query.getResultList();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return contractBranchList;
		// return commonService.getDataList(new ContractBranch(), null);
	}

	public ContractBranch saveOrUpdateContractBranch(ContractBranch contractBranch) throws Exception {

		System.err.print("----" + contractBranch.getBrName() + "---");

		final Session session = sessionFactory.openSession();
		try {
			log.debug("Calling SP");

			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTRACT_BRANCH.toString(),
					ContractBranch.class);
			call.registerParameter("pID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getContractId() == null ? "" :contractBranch.getContractId());
			call.registerParameter("pBRC", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getBrac() == null ? "" : contractBranch.getBrac());
			call.registerParameter("pBRC_ATM", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getBrcAtm() == null ? "" : contractBranch.getBrcAtm());
			call.registerParameter("pBRC_NAME", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getBrName() == null ? "" : contractBranch.getBrName());

			call.registerParameter("pBW", Integer.class, ParameterMode.IN).bindValue(contractBranch.getBw()== null ? 0 : contractBranch.getBw());
			call.registerParameter("pMONTHLY_CHARGE", long.class, ParameterMode.IN)
					.bindValue(contractBranch.getMonthlyCharge());//contractBranch.getMonthlyChage() == null ? 0 : contractBranch.getMonthlyChage());
			call.registerParameter("pStatus", String.class, ParameterMode.IN).bindValue(contractBranch.getStatus()== null ?"":contractBranch.getStatus());
			call.registerParameter("pUserId", String.class, ParameterMode.IN).bindValue(contractBranch.getEntryUser()== null ?"":contractBranch.getEntryUser());
			call.registerParameter("pInsertDate", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getUpdateUser()== null ?"":contractBranch.getUpdateUser());
			call.registerParameter("pUpdateDate", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			// String opStatusCode = (String) call.getOutputParameterValue("OUT_STATUS");

			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contractBranch;
	}

	public ContractBranch UpdateContractBranch(ContractBranch contractBranch) throws Exception {
		final Session session = sessionFactory.openSession();
		System.err.print("--------- " + contractBranch.getId() + "----");

		try {
			log.debug("session calling");
			session.beginTransaction();
			contractBranch.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(contractBranch);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();

			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if (e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			} else
				throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contractBranch;
	}

//	public ContractBranch updateContactBranchInfo(ContractBranch contractBranch) throws Exception {
//
//		Integer Id = 0;
//		if (contractBranch.getId() != null) {
//			Id = contractBranch.getId();
//		}
//		String contractId = contractBranch.getContractId();
//		String brac = contractBranch.getBrac();
//		String brcAtm = contractBranch.getBrcAtm();
//		Integer bw = contractBranch.getBw();
//		Integer monthlyChage = contractBranch.getMonthlyChage();
//		String updateUser = contractBranch.getUpdateUser();
//		Timestamp updateDt = contractBranch.getUpdateDt();
//		String brName = contractBranch.getBrName();
//		String status = contractBranch.getStatus();
//
//		ContractBranch contractBranchone = new ContractBranch(Id, contractId, brac, brcAtm, bw, monthlyChage,
//				updateUser, updateDt, brName, status);
//		return UpdateContractBranch(contractBranchone);
//	}

	public ContractBranch DeleteContractBranchInfo(ContractBranch contractBranch) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(contractBranch);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contractBranch;
	}

	public List<ContractBranch> getBranchList() {
		List<ContractBranch> branchList = new ArrayList<ContractBranch>();
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractBranch> criteriaQuery = builder.createQuery(ContractBranch.class);
			Root<ContractBranch> root = criteriaQuery.from(ContractBranch.class);
			criteriaQuery.select(root);
			Query<ContractBranch> query = session.createQuery(criteriaQuery);

			branchList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
			if (transaction != null) {
				transaction.rollback();
			}
			return null;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();
			}
		}

		return branchList;
	}

	public String readWrite(FileInputStream excelFile, ContractBranch conBranch) throws Exception {

		System.out.println("Start Reading File.");
		String status = null;
		try {

			// FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			// Workbook workbook = new XSSFWorkbook(excelFile);
			Workbook workbook = WorkbookFactory.create(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			iterator.next();
			ContractBranch contractBranch = new ContractBranch();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getRowNum() > 0) {

					if (currentRow.getCell(0).getCellType() == CellType.NUMERIC) {
						currentRow.getCell(0).setCellType(CellType.STRING);
					}
					contractBranch.setId(0);
					if (currentRow.getCell(1).getCellType() == CellType.NUMERIC) {
						currentRow.getCell(1).setCellType(CellType.STRING);
					}

					if (currentRow.getCell(2).getCellType() == CellType.NUMERIC) {
						currentRow.getCell(2).setCellType(CellType.STRING);
					}
					contractBranch.setBrName(currentRow.getCell(2).getStringCellValue());
					if (contractBranch.getBrName().contains("ATM")) {
						contractBranch.setBrcAtm(currentRow.getCell(1).getStringCellValue());
						contractBranch.setBrac("");

					} else {
						contractBranch.setBrac(currentRow.getCell(1).getStringCellValue());
						contractBranch.setBrcAtm("");
					}
					if (currentRow.getCell(1).getStringCellValue().toUpperCase().contains("CAN")) {
						contractBranch.setStatus("CAN");
					} else {
						contractBranch.setStatus("ACT");
					}

					if (currentRow.getCell(4).getCellType() == CellType.NUMERIC) {
						currentRow.getCell(4).setCellType(CellType.STRING);
					}
					contractBranch.setContractId(getContractIdByNumber(currentRow.getCell(4).getStringCellValue()));
					if (currentRow.getCell(5).getCellType() == CellType.NUMERIC) {
						currentRow.getCell(5).setCellType(CellType.STRING);
					}
					contractBranch.setMonthlyCharge(Long.valueOf(currentRow.getCell(5).getStringCellValue()));

					contractBranch.setBw(0);
					contractBranch.setEntryUser(conBranch.getEntryUser());
					contractBranch.setEntryDate(conBranch.getEntryDate());
					contractBranch.setUpdateDt(conBranch.getUpdateDt());
					contractBranch.setUpdateUser(conBranch.getUpdateUser());

					status = InsertRowInDB(contractBranch);
					if (status.indexOf("Error") > -1) {
						break;

					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			status = e.getMessage();
		} catch (IOException e) {
			e.printStackTrace();
			status = e.getMessage();
		}

		System.out.println("Finish File Reading.");
		return status;

	}

	public String InsertRowInDB(ContractBranch contractBranch) {

		final Session session = sessionFactory.openSession();
		String status = null;
		try {

			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTRACT_BRANCH.toString(),
					ContractBranch.class);
			call.registerParameter("pID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getContractId() == null ? "" :contractBranch.getContractId());
			call.registerParameter("pBRC", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getBrac() == null ? "" : ("00000" + contractBranch.getBrac()).substring(contractBranch.getBrac().length()) );
			call.registerParameter("pBRC_ATM", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getBrcAtm() == null ? "" : contractBranch.getBrcAtm());
			call.registerParameter("pBRC_NAME", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getBrName() == null ? "" : contractBranch.getBrName());

			call.registerParameter("pBW", Integer.class, ParameterMode.IN).bindValue(contractBranch.getBw()== null ? 0 : contractBranch.getBw());
			call.registerParameter("pMONTHLY_CHARGE", long.class, ParameterMode.IN)
					.bindValue(contractBranch.getMonthlyCharge());//contractBranch.getMonthlyChage() == null ? 0 : contractBranch.getMonthlyChage());
			call.registerParameter("pStatus", String.class, ParameterMode.IN).bindValue(contractBranch.getStatus()== null ?"":contractBranch.getStatus());
			call.registerParameter("pUserId", String.class, ParameterMode.IN).bindValue(contractBranch.getEntryUser()== null ?"":contractBranch.getEntryUser());
			call.registerParameter("pInsertDate", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN)
					.bindValue(contractBranch.getUpdateUser()== null ?"":contractBranch.getUpdateUser());
			call.registerParameter("pUpdateDate", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			// e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return status;
	}

	public String getContractIdByNumber(String conNumber) {

		Transaction transaction = null;
		Session session = null;
		String contractId = null;

		ContractInfo contractInfo = new ContractInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			conNumber = conNumber.toUpperCase();
			;
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<ContractInfo> query = builder.createQuery(ContractInfo.class);
			Root<ContractInfo> rootDetails = query.from(ContractInfo.class);
			query.select(rootDetails)
					.where(builder.equal(builder.upper(rootDetails.get("contNo")), conNumber));

			Query<ContractInfo> qResult = session.createQuery(query);
			try {
				contractInfo = qResult.getSingleResult();
				contractId = contractInfo.getContId();

			} catch (NoResultException nre) {
				contractInfo = null;
				contractId = conNumber;
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return contractId;
	}
	
	public List<Object> getcontractBranchJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.cont_no \r\n" + 
					"FROM   contract_branch b \r\n" + 
					"       join contract_info i \r\n" + 
					"         ON( b.cont_id = i.cont_id ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
