package com.sbl.spms.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.BudgetBillEntry;
import com.sbl.spms.model.BudgetMaster;
import com.sbl.spms.model.BudgetSectorwiseAllocation;
import com.sbl.spms.model.Contractbilltran;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VatAitProduct;
import com.sbl.spms.model.VendorServiceType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BudgetSectorwiseAllocationService {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CommonService commonService;

	public List<Object> getSectorwiseBudgetAllocationList() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();

		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session.createNativeQuery(
					"SELECT A.*, M.SECTOR_NAME FROM PROCMNT.BUDGET_ALLOCATION_MAP A, PROCMNT.BUDGET_MASTER_SECTOR M WHERE A.SECTOR_ID = M.SECTOR_ID")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}

	public BudgetSectorwiseAllocation saveBudgetAllocationEntry(BudgetSectorwiseAllocation budgetAllocation)
			throws Exception {

		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();

			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_BUDGETALLOCATIONENTRY.toString(),
					BudgetBillEntry.class);

			call.registerParameter("pENTRY_ID", String.class, ParameterMode.IN)
					.bindValue((budgetAllocation.getCreatedBy() == null) ? "" : budgetAllocation.getCreatedBy());

			call.registerParameter("pENTRY_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));

			call.registerParameter("pUPDATE_ID", String.class, ParameterMode.IN)
					.bindValue((budgetAllocation.getUpdatedBy() == null) ? "" : budgetAllocation.getUpdatedBy());

			call.registerParameter("pUPDATE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pUPDATE_DT", null);

			call.registerParameter("pSECTOR_ID", Integer.class, ParameterMode.IN)
					.bindValue(budgetAllocation.getSectorId());

			call.registerParameter("pYEAR", String.class, ParameterMode.IN)
					.bindValue((budgetAllocation.getYear() == null) ? "" : budgetAllocation.getYear());

			call.registerParameter("pAMOUNT", Double.class, ParameterMode.IN)
					.bindValue((budgetAllocation.getAmount() == null) ? 0 : budgetAllocation.getAmount());

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);

			call.execute();

			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);

			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return budgetAllocation;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getBudgetAllocationInfo() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();

		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session.createNativeQuery(
					"SELECT A.*, S.SECTOR_NAME FROM PROCMNT.BUDGET_SECTORWISE_ALLOCATION A, PROCMNT.BUDGET_MASTER_SECTOR S WHERE A.SECTOR_ID = S.SECTOR_ID")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}

	public BudgetSectorwiseAllocation UpdateBudgetAllocationInfoService(BudgetSectorwiseAllocation budgetAllocation)
			throws Exception {
		final Session session = sessionFactory.openSession();
		try {

			log.debug("update calling");
			session.beginTransaction();
			budgetAllocation.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			session.update(budgetAllocation);

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return budgetAllocation;
	}

	public BudgetMaster DeleteBudgetMasterInfo(BudgetMaster budgetMasterInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		try {

			log.info("delete calling");
			session.beginTransaction();
			// budgetBillInfo.setUpdate_dt(new Timestamp(System.currentTimeMillis()));
//				session.merge(budgetBillInfo);
//				session.delete(budgetBillInfo);	
			BudgetBillEntry persistentBudgetBillEntry = session.load(BudgetBillEntry.class, budgetMasterInfo.getId());
			session.delete(persistentBudgetBillEntry);

		} catch (Exception e) {

			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return budgetMasterInfo;

	}

	public List<Object> getBudgetSectorInfo() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();

		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT * FROM PROCMNT.BUDGET_MASTER_SECTOR").list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}

	@SuppressWarnings("unchecked")
	public Double getCADAllocationByYear(String year) throws Exception {

		Double amount = null;

		final Session session = sessionFactory.openSession();

		session.beginTransaction();

		session.getTransaction().commit();

		try {
			session.beginTransaction();

			amount = ((BigDecimal) session
					.createNativeQuery("SELECT SUM(M.AMOUNT) FROM PROCMNT.BUDGET_MASTER M WHERE M.YEAR = :year")
					.setParameter("year", year).getSingleResult()).doubleValue();

			session.getTransaction().commit();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return amount;
	}

	public Double getBudgetAllocationByYear(String year) throws Exception {

		Double amount = null;

		final Session session = sessionFactory.openSession();

		session.beginTransaction();

		session.getTransaction().commit();

		try {
			session.beginTransaction();

			amount = ((BigDecimal) session
					.createNativeQuery("SELECT SUM(M.AMOUNT) FROM PROCMNT.BUDGET_SECTORWISE_ALLOCATION M WHERE M.YEAR = :year")
					.setParameter("year", year).getSingleResult()).doubleValue();

			session.getTransaction().commit();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return amount;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object> getYearWiseCADFund() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session
					.createNativeQuery("SELECT SUM(M.AMOUNT), M.YEAR FROM PROCMNT.BUDGET_MASTER M GROUP BY M.YEAR")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getYearWiseAllocation() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session
					.createNativeQuery("SELECT SUM(M.AMOUNT), M.YEAR FROM PROCMNT.BUDGET_SECTORWISE_ALLOCATION M GROUP BY M.YEAR")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getSectorAndYearWiseAllocation() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session
					.createNativeQuery("SELECT * FROM PROCMNT.BUDGET_ALLOCATION_MAP")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}

}
