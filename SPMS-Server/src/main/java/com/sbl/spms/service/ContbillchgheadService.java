package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContbillchgheadService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
public Contbillchghead saveOrUpdateContBillChgHead(Contbillchghead contBill) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTBILLCHARGEHEAD.toString(), Contbillchghead.class);
			call.registerParameter("pHEADID", String.class, ParameterMode.IN).bindValue((contBill.getHead_id()==null)?"":contBill.getHead_id());
			call.registerParameter("pCHGHEAD", String.class, ParameterMode.IN).bindValue((contBill.getChg_head()==null)?"":contBill.getChg_head());
			call.registerParameter("pCHGHEADNAME", String.class, ParameterMode.IN).bindValue((contBill.getChg_head_name()==null)?"":contBill.getChg_head_name());
			call.registerParameter("pSTATUS", String.class, ParameterMode.IN).bindValue((contBill.getStatus()==null)?"":contBill.getStatus());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue((contBill.getEntryusr()==null)?"":contBill.getEntryusr());
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue((contBill.getUpdateusr()==null)?"":contBill.getUpdateusr());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contBill;
	}


	public List<Contbillchghead> getAllId() throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Contbillchghead> labels = null;
	    session.getTransaction().commit();
	    
	    try {
			session.beginTransaction();
			labels = session.createQuery("from Contbillchghead", Contbillchghead.class)
		            .list();
			session.flush();
	
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}
	
	}
	    return labels;
	}
	
	
	public List<Contbillchghead> getContBillChgHead() {
		return commonService.getDataList(new Contbillchghead(),null);
	}
	
	
	public Contbillchghead updateContBillChgHead(Contbillchghead mATR) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			log.debug("session calling");
			session.beginTransaction();
			mATR.setUpdatedt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(mATR);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}
	
	
	public Contbillchghead deleteContBillChgHead(Contbillchghead contBill) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(contBill);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contBill;
	}


}
