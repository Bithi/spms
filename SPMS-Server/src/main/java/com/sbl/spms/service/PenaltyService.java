package com.sbl.spms.service;

import java.security.MessageDigest;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Chart;
import com.sbl.spms.model.DownTime;
import com.sbl.spms.model.PenaltyCalc;
import com.sbl.spms.model.PenaltyProcess;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PenaltyService {
	@Autowired
	private SessionFactory sessionFactory;
	
	public String process(PenaltyProcess penaltyCalc) {
		final Session session = sessionFactory.openSession();
		String status= null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] digest = md.digest();
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_PENALTYPROCESS.toString());

			System.out.println(penaltyCalc.getDate());
			call.registerParameter("IN_Month_Yr", String.class, ParameterMode.IN).bindValue( penaltyCalc.getDate());
			call.registerParameter("in_usr", String.class, ParameterMode.IN).bindValue(penaltyCalc.getUserId());
			call.registerParameter("in_vendor", String.class, ParameterMode.IN).bindValue((penaltyCalc.getVendor()==null)?"no":penaltyCalc.getVendor());
			call.registerParameter("in_contract", String.class, ParameterMode.IN).bindValue((penaltyCalc.getContract()==null)?"no":penaltyCalc.getContract());
//			call.registerParameter("IN_BRC", String.class, ParameterMode.IN).bindValue(penaltyCalc.getBrCode());
			
			call.registerParameter("out_net_Penalty", String.class, ParameterMode.OUT);
			call.registerParameter("out_msg", String.class, ParameterMode.OUT);
			

			call.execute();
			String opStatus = (String) call.getOutputParameterValue("out_msg");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("FAIL") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			//e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
		return status;
		
	}
	public List<PenaltyCalc> getPenaltyCalcList() {
		List<PenaltyCalc>penaltyCalcList = new ArrayList<PenaltyCalc>();
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<PenaltyCalc> criteriaQuery = builder.createQuery(PenaltyCalc.class);
			Root<PenaltyCalc> root = criteriaQuery.from(PenaltyCalc.class);
			criteriaQuery.select(root);
			Query<PenaltyCalc> query = session.createQuery(criteriaQuery);

			penaltyCalcList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
			if (transaction != null) {
				transaction.rollback();
			}
			return null;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();
			}
		}

		return penaltyCalcList;
	}
	
	public PenaltyCalc deletePenalty(PenaltyCalc penaltyProcess) throws Exception {
		final Session session = sessionFactory.openSession();
		
		try {
			session.beginTransaction();
			session.delete(penaltyProcess);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
		return penaltyProcess;
	}
	public Long getNetPenaltyForBillPay(PenaltyCalc penaltyCalc) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Long netPenalty= new Long(0);
	    
	    session.getTransaction().commit();
	    try {
	    	session.beginTransaction();
	    	Date  bill_period_start=new Date(penaltyCalc.getBill_period_start().getTime());
	    	Date  bill_period_end=new Date(penaltyCalc.getBill_period_end().getTime());
//			List<Object[]> results = session.createNativeQuery("select  PROCMNT.FindNetPenaltyForBillPay \r\n" + 
//					"   ( "+penaltyCalc.getContId()+", "+penaltyCalc.getMonth()+") from dual")
//					.list();
			String results = session.createNativeQuery("select  FindNetPenaltyForBillPay \r\n" + 
					"   ( '" +penaltyCalc.getContId() +"',TO_DATE ('"+bill_period_start+"', 'rrrr-mm-dd'),TO_DATE ('"+bill_period_end+"', 'rrrr-mm-dd')) from dual")
					.getSingleResult().toString();
			
				netPenalty=Long.parseLong(results.toString());
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return netPenalty;
	}
	
}
