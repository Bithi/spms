package com.sbl.spms.service;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.BudgetBillEntry;
import com.sbl.spms.model.BudgetBillPayment;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class BudgetBillPaymentService {

	@Autowired
	private SessionFactory sessionFactory;

	public BudgetBillPayment saveBudgetBillPayment(BudgetBillPayment budgetBillPayment) throws Exception {

		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();

			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_BUDGETBILLPAYMENT.toString(),
					BudgetBillPayment.class);
			call.registerParameter("pENTRY_ID", String.class, ParameterMode.IN)
					.bindValue((budgetBillPayment.getEntry_id() == null) ? "" : budgetBillPayment.getEntry_id());

			call.registerParameter("pENTRY_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));

//			call.registerParameter("pUPDATE_ID", String.class, ParameterMode.IN)
//					.bindValue((budgetBillPayment.getUpdate_id() == null) ? "" : budgetBillPayment.getUpdate_id());
//
//			call.registerParameter("pUPDATE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
//			call.setParameter("pUPDATE_DT", null);

			call.registerParameter("pINVOICE_NO", String.class, ParameterMode.IN)
					.bindValue((budgetBillPayment.getInvoice_no() == null) ? "" : budgetBillPayment.getInvoice_no());

			call.registerParameter("pPENALTY_AMT", Double.class, ParameterMode.IN).bindValue(
					budgetBillPayment.getPenalty_amt() == null ? 0 : budgetBillPayment.getPenalty_amt());

			call.registerParameter("pPAY_ORDER_AMOUNT", Double.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getPay_order_amt() == null) ? 0 : budgetBillPayment.getPay_order_amt());

			call.registerParameter("pPAY_ORDER_NUMBER", String.class, ParameterMode.IN)
					.bindValue((budgetBillPayment.getPay_order_no() == null) ? "" : budgetBillPayment.getPay_order_no());

			call.registerParameter("pVAT_CHALAN_NUMBER", String.class, ParameterMode.IN)
			.bindValue((budgetBillPayment.getVat_chalan_no() == null) ? "" : budgetBillPayment.getVat_chalan_no());

	call.registerParameter("pVAT_CHALAN_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
	call.setParameter("pVAT_CHALAN_DT", budgetBillPayment.getVat_chalan_dt());
	
			call.registerParameter("pAIT_CHALAN_NUMBER", String.class, ParameterMode.IN)
			.bindValue((budgetBillPayment.getAit_chalan_no() == null) ? "" : budgetBillPayment.getAit_chalan_no());

			call.registerParameter("pAIT_CHALAN_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pAIT_CHALAN_DT", budgetBillPayment.getAit_chalan_dt());
	
			call.registerParameter("pFORM_NO", String.class, ParameterMode.IN)
			.bindValue((budgetBillPayment.getForm_no() == null) ? "" : budgetBillPayment.getForm_no());

			call.registerParameter("pFORM_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pFORM_DT", budgetBillPayment.getForm_dt());
//			call.registerParameter("pPAYMENT_ID", Integer.class, ParameterMode.IN)
//			.bindValue(0);
			call.registerParameter("pBILL_AFTER_VAT", Double.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getBill_after_vat() == null) ? 0 : budgetBillPayment.getBill_after_vat());
			call.registerParameter("pACCUM_BILL_AFTER_VAT", Double.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getAccu_bill_after_vat() == null) ? 0 : budgetBillPayment.getAccu_bill_after_vat());
			call.registerParameter("pCURRENT_INCOME_TAX", Double.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getCurr_tds() == null) ? 0 : budgetBillPayment.getCurr_tds());
			call.registerParameter("pACCUM_INCOME_TAX", Double.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getAccu_tds() == null) ? 0 : budgetBillPayment.getAccu_tds());
			
			
			call.registerParameter("pPAYMENT_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pPAYMENT_DT", budgetBillPayment.getPayment_dt());
			call.registerParameter("pVAT_RATE", Integer.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getVat_rate() == null) ? 0 : budgetBillPayment.getVat_rate());
			call.registerParameter("pAIT_RATE", Integer.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getAit_rate() == null) ? 0 : budgetBillPayment.getAit_rate());
			call.registerParameter("pFISCAL_YEAR", String.class, ParameterMode.IN)
			.bindValue((budgetBillPayment.getFiscal_year() == null) ? "" : budgetBillPayment.getFiscal_year());
			
			call.registerParameter("pSECTOR_ID", Integer.class, ParameterMode.IN).bindValue(
					(budgetBillPayment.getSector_id() == null) ? 0 : budgetBillPayment.getSector_id());
			

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);

			call.execute();

			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);

			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return budgetBillPayment;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getBudgetBillPaymentJoin() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session
					.createNativeQuery("SELECT * FROM PROCMNT.BUDGET_BILL_PAYMENT ")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}
	
	@SuppressWarnings("unchecked")
	public String getPaymentAmount(Double in_bill_amount,
		    int in_vat,
		    int in_inclusive,
		    int in_with_vat,
		    int in_tds,
		    int in_with_tds,
		    Double in_penalty_amt,
		    String in_invoice,
		    String in_fiscal_year) throws Exception {
		final Session session = sessionFactory.openSession();
		String payment = "";

		try {
			session.beginTransaction();
			NativeQuery<String> query = session.createSQLQuery(
	                "SELECT PROCMNT.FindBudgetPayment(:param1, :param2, :param3, :param4, :param5, :param6, :param7, :param8, :param9) as paymentAmount  FROM DUAL")
	                .setParameter("param1", in_bill_amount)
	                .setParameter("param2", in_vat)
	                .setParameter("param3", in_inclusive)
	                .setParameter("param4", in_with_vat)
	                .setParameter("param5", in_tds)
	                .setParameter("param6", in_with_tds)
	                .setParameter("param7", in_penalty_amt)
	                .setParameter("param8", in_invoice)
	                .setParameter("param9", in_fiscal_year)
	                .addScalar("paymentAmount", StringType.INSTANCE); // Assuming "findBudgetPaymentResult" is the column name or alias

			payment = (String) query.uniqueResult();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return payment;
	}

	}
