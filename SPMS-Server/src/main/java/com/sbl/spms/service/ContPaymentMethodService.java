package com.sbl.spms.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContPaymentMethod;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContPaymentMethodService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;

	public List<ContPaymentMethod> getPaymentMethodList() {
		return commonService.getDataList(new ContPaymentMethod(), null);
	}

	public ContPaymentMethod saveData(ContPaymentMethod contPaymentMethod) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_CONT_PAYMENT_METHOD.toString(),
					ContPaymentMethod.class);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN).bindValue(contPaymentMethod.getContId());
			call.registerParameter("pPART_FULL", String.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getPartFull());
			call.registerParameter("pPMT_SCHEDULE", String.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getPmtSchedule());
			call.registerParameter("pTOTAL_VALUE_PRODUCT", long.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getTtValueProduct());
			call.registerParameter("pPAYMENT_DATE", Date.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getPaymentDate());
			call.registerParameter("pPAYMENT_AMOUNT", long.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getPaymentAmount());
			call.registerParameter("pDUE_AMOUNT", long.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getDueAmount());
			call.registerParameter("pDUE_DATE", Date.class, ParameterMode.IN).bindValue(contPaymentMethod.getDueDate());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN)
					.bindValue(contPaymentMethod.getEntryUser());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));

			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();

			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contPaymentMethod;
	}

	public ContPaymentMethod updateData(ContPaymentMethod contPaymentMethod) throws Exception {
		final Session session = sessionFactory.openSession();
		/*
		 * TO_DO:H: If needed sp will be added
		 */
		try {
			log.debug("session calling");
			session.getTransaction().begin();
			session.saveOrUpdate(contPaymentMethod);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contPaymentMethod;
	}

	public ContPaymentMethod deleteData(ContPaymentMethod contPaymentMethod) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(contPaymentMethod);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return contPaymentMethod;
	}
	
	
	public List<Object> getPaymentMethodJoin() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.cont_no \r\n" + 
					"FROM   cont_payment_method b \r\n" + 
					"       join contract_info i \r\n" + 
					"         ON( b.cont_id = i.cont_id ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }

}
