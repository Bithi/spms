package com.sbl.spms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Brand;
import com.sbl.spms.model.ContSubItem;
import com.sbl.spms.model.Contbillchghead;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.Contractbillentry;
import com.sbl.spms.model.Contractbilltax;
import com.sbl.spms.model.ItemPerBill;
import com.sbl.spms.model.ItemType;
import com.sbl.spms.model.SubItemType;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContractbillentryService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
public ItemPerBill saveItemBillEntry(ItemPerBill itemPerBill) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_ITEMPERBILL.toString(), ItemPerBill.class);
			call.registerParameter("pid", String.class, ParameterMode.IN).bindValue((itemPerBill.getId()==null)?"":itemPerBill.getId());
			call.registerParameter("pitemid", String.class, ParameterMode.IN).bindValue((itemPerBill.getItemId()==null)?"":itemPerBill.getItemId());
			call.registerParameter("pbill", String.class, ParameterMode.IN).bindValue((itemPerBill.getBillno()==null)?"":itemPerBill.getBillno());
			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return itemPerBill;
	}


public Contractbillentry saveOrUpdateContBillEntry(Contractbillentry contEntry) throws Exception {
		
		
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CONTRACTBILLENTRY.toString(), Contractbillentry.class);
			call.registerParameter("pENTRYID", String.class, ParameterMode.IN).bindValue((contEntry.getEntry_id()==null)?"":contEntry.getEntry_id());
			call.registerParameter("pCONTID", String.class, ParameterMode.IN).bindValue((contEntry.getCont_id()==null)?"":contEntry.getCont_id());
			call.registerParameter("pBILLNO", String.class, ParameterMode.IN).bindValue((contEntry.getBill_no()==null)?"":contEntry.getBill_no());
			call.registerParameter("pBILLDATE", Timestamp.class, ParameterMode.IN).bindValue(contEntry.getBill_date()==null?new Timestamp(0):contEntry.getBill_date());
			call.registerParameter("pBILLRECIEVEDATE", Timestamp.class, ParameterMode.IN).bindValue(contEntry.getBill_recieve_date()==null?new Timestamp(0):contEntry.getBill_recieve_date());
			call.registerParameter("pPARTFULL", String.class, ParameterMode.IN).bindValue(contEntry.getPart_full());
			call.registerParameter("pSENDAUDITDATE", Timestamp.class, ParameterMode.IN).bindValue(contEntry.getSend_audit_date()==null?new Timestamp(0):contEntry.getSend_audit_date());
			call.registerParameter("pRETURNFROMAUDITDATE", Timestamp.class, ParameterMode.IN).bindValue(contEntry.getReturn_from_audit_date()==null?new Timestamp(0):contEntry.getReturn_from_audit_date());
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN).bindValue((contEntry.getEntryusr()==null)?"":contEntry.getEntryusr());
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue((contEntry.getUpdateusr()==null)?"":contEntry.getUpdateusr());
			call.registerParameter("pBILLAMOUNT", long.class, ParameterMode.IN).bindValue(contEntry.getBillamount());
			
			call.registerParameter("pUPDATEDT", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			} else {
				String[] parts = opStatus.split("-");
				contEntry.setEntry_id(parts[0]);
				status = parts[1];
			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return contEntry;
	}



public List<Contractbillentry> getContBillEntry() {
	return commonService.getDataList(new Contractbillentry(),null);
}


public Contractbillentry updateContBillEntry(Contractbillentry mATR) throws  Exception {
	final Session session = sessionFactory.openSession();

	try {
		log.debug("session calling");
		session.getTransaction().begin();
		mATR.setUpdatedt(new Timestamp(System.currentTimeMillis()));
		session.saveOrUpdate(mATR);
		session.flush();

	} 
	catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		
		System.out.println(e.getCause());
		session.getTransaction().rollback();
		session.close();
		if(e.getCause().toString().contains("ConstraintViolationException")) {
			throw new Exception("Constraint Violation occured");
		}
		else
			throw e;
	}
	finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return mATR;
}

public ItemPerBill updateItemPerBill(ItemPerBill mATR) throws  Exception {
	final Session session = sessionFactory.openSession();

	try {
		log.debug("session calling");
		session.getTransaction().begin();
		session.saveOrUpdate(mATR);
		session.flush();

	} 
	catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		
		System.out.println(e.getCause());
		session.getTransaction().rollback();
		session.close();
		if(e.getCause().toString().contains("ConstraintViolationException")) {
			throw new Exception("Constraint Violation occured");
		}
		else
			throw e;
	}
	finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return mATR;
}

public Contractbillentry deleteContBillEntry(Contractbillentry contEntry) throws Exception {
	final Session session = sessionFactory.openSession();
	// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

	try {
		session.beginTransaction();
		session.delete(contEntry);
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return contEntry;
}

public ItemPerBill deleteItemPerBill(ItemPerBill itmPerBill) throws Exception {
	final Session session = sessionFactory.openSession();
	// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

	try {
		session.beginTransaction();
		session.delete(itmPerBill);
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return itmPerBill;
}

public List<ContractInfo> getAllId() throws Exception{
	final Session session = sessionFactory.openSession();
    session.beginTransaction();

    // We read labels record from database using a simple Hibernate
    // query, the Hibernate Query Language (HQL).
    List<ContractInfo> labels = null;
    session.getTransaction().commit();
    
    try {
		session.beginTransaction();
		labels = session.createQuery("from ContractInfo", ContractInfo.class)
	            .list();
		session.flush();

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
		session.close();
		
		}

}
    return labels;
}

public List<Contractbillentry> getAllBillId() throws Exception{
	final Session session = sessionFactory.openSession();
    session.beginTransaction();

    // We read labels record from database using a simple Hibernate
    // query, the Hibernate Query Language (HQL).
    List<Contractbillentry> labels = null;
    session.getTransaction().commit();
    
    try {
		session.beginTransaction();
		Criteria crit = session.createCriteria(Contractbillentry.class);
		crit.add(Restrictions.ne("return_from_audit_date", new Timestamp(0)));
		labels = crit.list();
		session.flush();
		

	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if(session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
		session.close();
		
		}

}
    return labels;
}

public List<Object[]> getItemNameByEntry(String entry) {
	final Session session = sessionFactory.openSession();
	List<Object[]> africanContinents = null;
	try {
		session.beginTransaction();
		
		africanContinents = session.createQuery("select \r\n" + 
				"subit.itemName\r\n" + 
				",mod.modelName,\r\n" + 
				"billent.entry_id,\r\n" + 
				"billent.bill_no,\r\n" + 
				"billent.bill_date,\r\n" + 
				"billent.part_full,\r\n" + 
				"contsub.itemQuantity,\r\n" + 
				"billent.billamount,\r\n" + 
				"contsub.contSubItemId\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"from ItemPerBill ent\r\n" + 
				"join Contractbillentry billent on(ent.billno=billent.entry_id) \r\n" + 
				"\r\n" + 
				"join ContSubItem contsub on(ent.itemId=contsub.contSubItemId)\r\n" + 
				" \r\n" + 
				" \r\n" + 
				"left join SubItemType subit on(subit.itemId=contsub.itemId) \r\n" + 
				"\r\n" + 
				"left join Model mod on(contsub.modelId=mod.modelId) "
				+ "where billent.entry_id=:entry")
				.setParameter("entry", entry)
				.list();
		
		session.flush();

	} catch (Exception e) {
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return africanContinents;
}
public List<ItemPerBill> getItemByEntry(String entry) {
	List<ItemPerBill> list = null;
	final Session session = sessionFactory.openSession();

	try {
		session.beginTransaction();
		// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
		Criteria crit = session.createCriteria(ItemPerBill.class);
		crit.add(Restrictions.eq("billno", entry));
		list = crit.list();
		session.flush();

	} catch (Exception e) {
		e.printStackTrace();
		session.getTransaction().rollback();
		session.close();
		throw e;
	} finally {
		if (session != null) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().commit();
			}
			session.close();

		}

	}
	return list;
}

public List<ItemPerBill> ItemListByEntry(String entry) {
	Transaction transaction = null;
	Session session = null;
	String vendorId =null;

	List<ItemPerBill> itemList = new ArrayList<ItemPerBill>();
	try {
		session = sessionFactory.openSession();
		transaction = session.beginTransaction();

		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<ItemPerBill> query = builder.createQuery(ItemPerBill.class);
		Root<ItemPerBill> rootDetails = query.from(ItemPerBill.class);
		query.select(rootDetails)
			.where(builder.equal(rootDetails.get("billno"), entry));

		Query<ItemPerBill> qResult = session.createQuery(query);
		try {
			itemList = qResult.getResultList();
			
		} catch (NoResultException nre) {
			itemList = null;
		}

		transaction.commit();
	} catch (Exception e) {
		log.error("", e);
		e.printStackTrace();
		if (transaction != null) {
			transaction.rollback();
		}
		if (session != null) {
	        session.close();
	    }
		return null;
		
	}
	finally {
		if (session != null) {
	        session.close();
	      }
	}
	
	return itemList;
}

	public List<Contractbillentry> getbillEntryByRetandcon(String entry) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();

	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Contractbillentry> labels = null;
	    session.getTransaction().commit();
	    
	    try {
			session.beginTransaction();
			Criteria crit = session.createCriteria(Contractbillentry.class);
			crit.add(Restrictions.ne("return_from_audit_date", new Timestamp(0)));
			crit.add(Restrictions.and(Restrictions.eq("cont_id", entry)));
			labels = crit.list();
			session.flush();
			

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return labels;
	}
	public List<Object> getContractBillEntryJoin() throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<Object> object = new ArrayList<Object>();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    
	    try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.cont_no \r\n" + 
					"FROM   cont_bill_entry b \r\n" + 
					"       join contract_info i \r\n" + 
					"         ON( b.cont_id = i.cont_id ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return object;
	}
	
	public Object getAllByEntry(String entry) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<Object> object = new ArrayList<Object>();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    
	    try {
			session.beginTransaction();
			object = session.createNativeQuery("select en.*,CON.VEN_ID from CONT_BILL_ENTRY en join contract_info con on(en.cont_id=con.cont_id) where EN.ENTRY_ID='"+entry+"'")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return object.get(0);
	}
}
