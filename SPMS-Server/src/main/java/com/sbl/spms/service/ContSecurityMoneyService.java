package com.sbl.spms.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContSecurityMoney;
import com.sbl.spms.model.ContSubItem;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContSecurityMoneyService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;

	public List<ContSecurityMoney> getList() {
		return commonService.getDataList(new ContSecurityMoney(), null);
	}

	public ContSecurityMoney saveData(ContSecurityMoney data) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_CONT_SECURITY_MONEY.toString(),
					ContSubItem.class);
			call.registerParameter("pCONT_ID", String.class, ParameterMode.IN).bindValue(data.getContId());
			call.registerParameter("pSECURITY_TYPE", String.class, ParameterMode.IN)
					.bindValue(data.getSecurityType());
			call.registerParameter("pISS_BANK", String.class, ParameterMode.IN)
					.bindValue(data.getIssueBank());
			call.registerParameter("pISS_BRANCH", String.class, ParameterMode.IN)
					.bindValue(data.getIssueBranch());
			call.registerParameter("pRETURN_DATE", Date.class, ParameterMode.IN)
					.bindValue(data.getReturnDate());
			
			call.registerParameter("pISS_DATE", Date.class, ParameterMode.IN)
			.bindValue(data.getIssueDate());
			
			call.registerParameter("pISS_AMOUNT", long.class, ParameterMode.IN)
			.bindValue(data.getIssueAmount());
			
			call.registerParameter("pPERIOD_SECURITY", Integer.class, ParameterMode.IN)
			.bindValue(data.getPeriodSecurity());
			
			call.registerParameter("pENTRYDT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pENTRYUSR", String.class, ParameterMode.IN)
			.bindValue(data.getEntryUser());
			
			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return data;
	}

	public ContSecurityMoney updateData(ContSecurityMoney data) throws Exception {
		final Session session = sessionFactory.openSession();
		/*
		 * TO_DO:H: If needed sp will be added
		 */
		try {
			log.debug("session calling");
			session.getTransaction().begin();
			data.setUpdateDt(new Timestamp(System.currentTimeMillis()));
			session.saveOrUpdate(data);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return data;
	}

	public ContSecurityMoney deleteData(ContSecurityMoney data) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(data);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return data;
	}
	
	
	public List<Object> getSecurityMoneyList() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT b.*, \r\n" + 
					"       i.cont_no \r\n" + 
					"FROM   cont_security_money b \r\n" + 
					"       join contract_info i \r\n" + 
					"         ON( b.cont_id = i.cont_id ) ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
}
