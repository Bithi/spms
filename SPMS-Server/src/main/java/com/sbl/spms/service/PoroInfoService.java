package com.sbl.spms.service;

import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.PoroInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class PoroInfoService {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	
	public List<PoroInfo> getPoroInfoList() {
		return commonService.getDataList(new PoroInfo(),null);
	}
	public PoroInfo saveOrUpdatePoroInfo(PoroInfo poroInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		try {
			
			log.debug("Calling SP");
		
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_PORO.toString(), PoroInfo.class);
			call.registerParameter("pPOROPREFIX", String.class, ParameterMode.IN).bindValue(poroInfo.getPoroPrefix());
			call.registerParameter("pPORONAME", String.class, ParameterMode.IN).bindValue(poroInfo.getPoroName());
			call.registerParameter("pPOROTYPE", char.class, ParameterMode.IN).bindValue(poroInfo.getPoroType());
			call.registerParameter("pGMOID", String.class, ParameterMode.IN).bindValue(poroInfo.getGmoId());
			call.registerParameter("pPOROCODE", String.class, ParameterMode.IN).bindValue(poroInfo.getPoroCode());
			call.registerParameter("pGMOCODE", String.class, ParameterMode.IN).bindValue(poroInfo.getGmoCode());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			//String opStatusCode = (String) call.getOutputParameterValue("OUT_STATUS");

			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		
		return poroInfo;
	}

	public PoroInfo UpdatePoroInfoService(PoroInfo poroInfo) throws Exception {
			final Session session = sessionFactory.openSession();
			System.err.print("--------- "+poroInfo+"----");
			
			try {
				session.beginTransaction();
				session.saveOrUpdate(poroInfo);
				session.flush();

			} catch (Exception e) {
				e.printStackTrace();
				session.getTransaction().rollback();
				session.close();
				throw e;
			} finally {
				if (session != null) {
					if(session.getTransaction().isActive()) {
						session.getTransaction().commit();
					}
				session.close();
				
				}

		}
			return poroInfo;
		}

	public PoroInfo updatePoroInfo(PoroInfo poroInfo) throws Exception {
		
		
			String  poroPrefix = poroInfo.getPoroPrefix();
			String  poroName = poroInfo.getPoroName();
			char    poroType = poroInfo.getPoroType();
			String  gmoId = poroInfo.getGmoId();
			String  poroCode = poroInfo.getPoroCode();
			String  gmoCode = poroInfo.getGmoCode();

			 
			
			PoroInfo poroInfoOne = new PoroInfo(poroPrefix,poroName,poroType,gmoId,poroCode,gmoCode);
			  
			
			
			return UpdatePoroInfoService(poroInfoOne);
	}
	
	public PoroInfo DeletePoroInfo(PoroInfo poroInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		System.err.print("--------- "+poroInfo.getPoroCode()+"----");
		
		try {
			session.beginTransaction();
			session.delete(poroInfo);
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
		return poroInfo;
	}



}


