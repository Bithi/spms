package com.sbl.spms.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.ContractInfo;
import com.sbl.spms.model.DownTime;
import com.sbl.spms.model.PenaltyCalc;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VendorInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DowntimeService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;

	public String readWrite(FileInputStream excelFile, DownTime downtimeVar) throws Exception {

		System.out.println("Start Reading File.");
		String status = null;
		try {

			// FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
			// Workbook workbook = new XSSFWorkbook(excelFile);
			Workbook workbook = WorkbookFactory.create(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			iterator.next();
			DownTime downtime = new DownTime();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getRowNum() > 0) {

					try {
						if (currentRow.getCell(0).getCellType() == CellType.NUMERIC) {
							currentRow.getCell(0).setCellType(CellType.STRING);
						}

						if (!currentRow.getCell(0).getStringCellValue().equals("")) {
							try {
								downtime.setId(currentRow.getCell(0).getStringCellValue());
							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][0]";
							}

							String month = "";
							try {
								if (currentRow.getCell(1).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(1).setCellType(CellType.STRING);
								}

								month = currentRow.getCell(1).getStringCellValue();
								downtime.setMonth(month.trim());

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][1]";
							}
							String strReportDate = null;
							try {
								if (currentRow.getCell(2).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(2).setCellType(CellType.STRING);
								}
								strReportDate = currentRow.getCell(2).getStringCellValue();
								java.util.Date reportDate;
								if (strReportDate == null || strReportDate.equals("")) {
									reportDate = new Date();
									// downtime.setDate(reportDate);
								} else {

									//
									reportDate = DateUtil.getJavaDate(Double.parseDouble(strReportDate));

								}
								downtime.setDate(new Timestamp(reportDate.getTime()));

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][2]";
							}

							try {
								if (currentRow.getCell(3).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(3).setCellType(CellType.STRING);
								}
								downtime.setBranchCode(currentRow.getCell(3).getStringCellValue().trim());

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][3]";
							}
							String venId = "";
							String vendor = "";
							try {
								if (currentRow.getCell(5).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(5).setCellType(CellType.STRING);
								}
								vendor = currentRow.getCell(5).getStringCellValue();
								if (vendor.indexOf("+") > -1)
									vendor = vendor.substring(0, vendor.indexOf("+"));
								venId = callVendorInfo(vendor);
								downtime.setVendor(venId);

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][5]";
							}

							String problem = "";
							try {
								if (currentRow.getCell(6).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(6).setCellType(CellType.STRING);
								}
								problem = currentRow.getCell(6).getStringCellValue();
								downtime.setProblem(problem.trim());

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][6]";
							}
							String strDownTime = null;
							java.util.Date downTime = null;
							try {
								if (currentRow.getCell(7).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(7).setCellType(CellType.STRING);
								}
								strDownTime = currentRow.getCell(7).getStringCellValue();

								if (strDownTime == null || strDownTime.equals("")
										|| strDownTime.equals("0000-00-00 00:00:00")) {
									downTime = new Date();
									// downtime.setDownTime(downTime);
								} else {
									downTime = DateUtil.getJavaDate(Double.parseDouble(strDownTime));

								}
								downtime.setDownTime(new Timestamp(downTime.getTime()));

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][7]";
							}
							String strUpTime = null;
							java.util.Date upTime = null;

							try {
								if (currentRow.getCell(8).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(8).setCellType(CellType.STRING);
								}

								strUpTime = currentRow.getCell(8).getStringCellValue();
								System.out.println(strUpTime);

								if (strUpTime == null || strUpTime.equals("")
										|| strUpTime.equals("0000-00-00 00:00:00")) {
									upTime = DateUtil.getJavaDate(Double.parseDouble(strDownTime));
									// downtime.setUpTime(upTime);
								}

								else {
									upTime = DateUtil.getJavaDate(Double.parseDouble(strUpTime));

								}
								downtime.setUpTime(new Timestamp(upTime.getTime()));

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][8]";
							}

							String strTotalTime = "";
							Integer diffInMillies = 0;
							try {
								if (currentRow.getCell(9).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(9).setCellType(CellType.STRING);
								}
								strTotalTime = currentRow.getCell(9).getStringCellValue();
								diffInMillies = (int) (Math.abs(upTime.getTime() - downTime.getTime()));
								downtime.setTotalTime(diffInMillies);

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][9]";
							}
							String followedBy = "";
							try {
								if (currentRow.getCell(10).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(10).setCellType(CellType.STRING);
								}
								followedBy = currentRow.getCell(10).getStringCellValue();
								downtime.setFollowedBy(followedBy.trim());

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][10]";
							}
							String concernOfficer = "";
							try {
								if (currentRow.getCell(11).getCellType() == CellType.NUMERIC) {
									currentRow.getCell(11).setCellType(CellType.STRING);
								}
								concernOfficer = currentRow.getCell(11).getStringCellValue();
								downtime.setConcernOfficer(concernOfficer.trim());

							} catch (Exception ex) {
								log.error("", ex);
								ex.printStackTrace();
								status = ex.getMessage() + " at [" + currentRow.getRowNum() + "][11]";
							}

							downtime.setEntryUser(downtimeVar.getEntryUser());
							downtime.setEntryDate(downtimeVar.getEntryDate());
							downtime.setUpdateDt(downtimeVar.getUpdateDt());
							downtime.setUpdateUser(downtimeVar.getUpdateUser());

							status = InsertRowInDB(downtime);
							log.info(status);
							if (status.indexOf("Error") > -1) {
								break;

							}
						}

					} catch (Exception ex) {
						log.error("", ex);
						ex.printStackTrace();
						status = ex.getMessage();
					}

				}
			}

		} catch (FileNotFoundException e) {
			log.error("", e);
			e.printStackTrace();
			status = e.getMessage();
		} catch (IOException e) {
			log.error("", e);
			e.printStackTrace();
			status = e.getMessage();
		}

		System.out.println("Finish File Reading.");
		return status;

	}

	private String callVendorInfo(String vendor) {
		Transaction transaction = null;
		Session session = null;
		String vendorId = null;

		VendorInfo vendorInfo = new VendorInfo();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			vendor = vendor.toUpperCase();
			// vendor =vendor.replaceAll("\\s", "");
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<VendorInfo> query = builder.createQuery(VendorInfo.class);
			Root<VendorInfo> rootDetails = query.from(VendorInfo.class);
			query.select(rootDetails).where(builder.equal(builder.upper(rootDetails.get("venName")), vendor));

			Query<VendorInfo> qResult = session.createQuery(query);
			try {
				vendorInfo = qResult.getResultList().get(0);
				vendorId = vendorInfo.getVenId();

			} catch (NoResultException nre) {
				vendorInfo = null;
				vendorId = vendor;
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
				session.close();
			}
			return null;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return vendorId;
	}

	public List<DownTime> getDowntimeList() {
		List<DownTime> downTimeList = new ArrayList<DownTime>();
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<DownTime> criteriaQuery = builder.createQuery(DownTime.class);
			Root<DownTime> root = criteriaQuery.from(DownTime.class);
			criteriaQuery.select(root);
			Query<DownTime> query = session.createQuery(criteriaQuery);

			downTimeList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
			if (transaction != null) {
				transaction.rollback();
			}
			return null;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();
			}
		}

		return downTimeList;
	}

	public List<PenaltyCalc> getPenaltyCalcList() {
		List<PenaltyCalc> penaltyCalcList = new ArrayList<PenaltyCalc>();
		Transaction transaction = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<PenaltyCalc> criteriaQuery = builder.createQuery(PenaltyCalc.class);
			Root<PenaltyCalc> root = criteriaQuery.from(PenaltyCalc.class);
			criteriaQuery.select(root);
			Query<PenaltyCalc> query = session.createQuery(criteriaQuery);

			penaltyCalcList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
			if (transaction != null) {
				transaction.rollback();
			}
			return null;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();
			}
		}

		return penaltyCalcList;
	}

	public String InsertRowInDB(DownTime downtime) {

		final Session session = sessionFactory.openSession();
		String status = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] digest = md.digest();

			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_DOWNTIME.toString(), DownTime.class);

			downtime.setId(downtime.getId() + "-" + downtime.getDownTime() + "-" + downtime.getUpTime());

			call.registerParameter("pDownid", String.class, ParameterMode.IN).bindValue(downtime.getId());
			call.registerParameter("pMONTH", String.class, ParameterMode.IN).bindValue(downtime.getMonth());
			call.registerParameter("pDOWNDATE", Timestamp.class, ParameterMode.IN).bindValue(downtime.getDate());
			call.registerParameter("pBRC", String.class, ParameterMode.IN).bindValue(downtime.getBranchCode());
			call.registerParameter("pVEN_ID", String.class, ParameterMode.IN).bindValue(downtime.getVendor());
			call.registerParameter("pPROBLEM_DETAIL", String.class, ParameterMode.IN).bindValue(downtime.getProblem());
			call.registerParameter("pDOWN_TIME", Timestamp.class, ParameterMode.IN).bindValue(downtime.getDownTime());
			call.registerParameter("pUP_TIME", Timestamp.class, ParameterMode.IN).bindValue(downtime.getUpTime());
			call.registerParameter("pTOTAL_TIME", Integer.class, ParameterMode.IN).bindValue(downtime.getTotalTime());
			call.registerParameter("pFOLLOWED_BY", String.class, ParameterMode.IN).bindValue((downtime.getFollowedBy()==null)?"":downtime.getFollowedBy());
			call.registerParameter("pCONCERN_OFFICER", String.class, ParameterMode.IN)
					.bindValue((downtime.getConcernOfficer()==null)?"":downtime.getConcernOfficer() );
			call.registerParameter("pUserId", String.class, ParameterMode.IN).bindValue(downtime.getEntryUser());
			call.registerParameter("pInsertDate", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));
			call.registerParameter("pUPDATEUSR", String.class, ParameterMode.IN).bindValue(downtime.getUpdateUser());
			call.registerParameter("pUpdateDate", Timestamp.class, ParameterMode.IN).bindValue(new Timestamp(0));
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);

			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);

			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return status;
	}

	public List<ContractInfo> getLabels() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();

		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		List<ContractInfo> labels = null;
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			labels = session.createQuery("from ContractInfo", ContractInfo.class).list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return labels;
	}

	public DownTime deleteDowntime(DownTime downtime) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(downtime);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return downtime;
	}

	public List<DownTime> lastDowntimeRow(String user) throws Exception {
		final Session session = sessionFactory.openSession();
		List<DownTime> lastRowList = new ArrayList<DownTime>();
		try {
			session.beginTransaction();
			lastRowList = (List<DownTime>) session.createNativeQuery("select f.*\r\n" + 
					"from\r\n" + 
					"(select t2.*, row_number() over (order by entrydt, entryusr desc) as seqnum,\r\n" + 
					"             count(*) over () as cnt\r\n" + 
					"      from\r\n" + 
					"      (\r\n" + 
					"Select t.*\r\n" + 
					"FROM DOWNTIME_CALC t\r\n" + 
					"where entryusr='"+user+"' \r\n" + 
					"AND entrydt = (select max(entrydt) \r\n" + 
					"   from DOWNTIME_CALC  t\r\n" + 
					"   where entryusr='"+user+"') order by DOWNDATE desc\r\n" + 
					"   ) t2 ) f\r\n" + 
					"where seqnum = 1 or seqnum = cnt")
		            .getResultList();
			session.flush();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return lastRowList;
	}

	
}
