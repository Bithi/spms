package com.sbl.spms.service;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserInfoService {	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	@Value("${globalPasswrdExpirationTimeInDays}")
	private String PasswrdExpirationTimeInDays;
	@Value("${globalLastLoginDurationInDays}")
	private String LastLoginDurationInDays;
	@Value("${globalLoginAttempt}")
	private String LoginAttempt;
	@Value("${globalResetPassword}")
	private String globalResetPassword;
	
	
	/**
	 * login in the user and generate token
	 * 
	 * @param userid
	 * @param password
	 * @param type
	 * @return
	 */
	
	public UserInfo login(String userId, String password) throws Exception {

		Transaction transaction = null;
		Session session = null;

		try {
			Date date= new Date();
			 
			 long time = date.getTime();
			Timestamp today = new Timestamp(time);
			Timestamp firstDate = new Timestamp(0);
			/*
			 * As here sha-256 encoder is user for saving password so we have to encoded it
			 * before checking on database
			 */
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
			byte[] digest = md.digest();
			String encodedPassword = new String(Hex.encode(digest));

			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			/*
			 * create criteria which is used for and/or etc.
			 */

			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<UserInfo> query = builder.createQuery(UserInfo.class);
			Root<UserInfo> root = query.from(UserInfo.class);
			query.select(root).where(builder.and(builder.equal(root.get("userId"), userId)
					
//				,	builder.equal(root.get("password"), encodedPassword),
//					builder.equal(root.get("userStatus"), "A")
							));

			Query<UserInfo> q = session.createQuery(query);

			UserInfo user=new UserInfo();
			try {
				user = q.getSingleResult();
				
				if(user.getUserStatus().equals("L")) {
					user.setMessage("locked");
				}
				
				else if( user.getUserStatus().equals("I")) {
					user.setMessage("inactive");
				}
				else if(user.getPassword().equals(encodedPassword) && user.getUserStatus().equals("A")) {
					long milliseconds = today.getTime() - user.getPasswordChangeDate().getTime();
					int days = (int) (milliseconds / (1000*60*60*24));
					
					long millisecondsForLastLogin = today.getTime() - user.getLastLoginDate().getTime();
					int daysForLastLogin = (int) (millisecondsForLastLogin / (1000*60*60*24));
					
					if(days>Integer.parseInt(PasswrdExpirationTimeInDays))
					{
						user.setMessage("expired");
					}
					else if((firstDate.getTime()!=user.getLastLoginDate().getTime()) && daysForLastLogin>Integer.parseInt(LastLoginDurationInDays)) {
						user.setMessage("expired");
					}
					else {
						user.setMessage("not expired");
						
						/////// For last successful login /////
						user.setLastLoginDate(today);
						user.setLoginAttempt(0);
						saveOrUpdateUserInfo(user);
					/////// For last successful login /////
					}
				}
				
				else if(!(user.getPassword().equals(encodedPassword )) && user.getUserStatus().equals("A")) {
					user.setLoginAttempt(user.getLoginAttempt()+1);
					if(user.getLoginAttempt()>=Integer.parseInt(LoginAttempt)) {
						user.setUserStatus("L"); ////////// status Locked for unsuccessful login
					}
					saveOrUpdateUserInfo(user);
					user=null;
				}
				
				
				
			} catch (NoResultException nre) {
				user = null;
			}
			
			transaction.commit();
			return user;

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public List<UserInfo> getUserInfoList() {
		return commonService.getDataList(new UserInfo(),null);
	}

	public UserInfo saveUserInfo(UserInfo userInfo) throws Exception {
			Timestamp passwordGenerateDate = new Timestamp(0);
			Timestamp lastLoginDate = new Timestamp(0);
			try {
	            // Create MessageDigest instance for MD5
	            MessageDigest md = MessageDigest.getInstance("SHA-256");
	            //Add password bytes to digest
	            md.update(globalResetPassword.getBytes());
	            //Get the hash's bytes 
	            byte[] bytes = md.digest();
	            //This bytes[] has bytes in decimal format;
	            //Convert it to hexadecimal format
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            //Get complete hashed password in hex format
	            userInfo.setPassword(sb.toString());
	            userInfo.setPasswordChangeDate(passwordGenerateDate);
	            userInfo.setLastLoginDate(lastLoginDate);
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            e.printStackTrace();
	        }
			
			return saveOrUpdateUserInfoX(userInfo);
	}
	public UserInfo resetPasswordOfUserInfo(UserInfo userInfo) throws Exception {
		
				Date date= new Date();
				 
				 long time = date.getTime();
				Timestamp passwordGenerateDate = new Timestamp(time);
				try {
		            // Create MessageDigest instance for MD5
		            MessageDigest md = MessageDigest.getInstance("SHA-256");
		            //Add password bytes to digest
		            md.update(globalResetPassword.getBytes());
		            //Get the hash's bytes 
		            byte[] bytes = md.digest();
		            //This bytes[] has bytes in decimal format;
		            //Convert it to hexadecimal format
		            StringBuilder sb = new StringBuilder();
		            for(int i=0; i< bytes.length ;i++)
		            {
		                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		            }
		            //Get complete hashed password in hex format
		            userInfo.setPassword(sb.toString());
		            
		        } 
		        catch (NoSuchAlgorithmException e) 
		        {
		        	log.error("", e);
		            e.printStackTrace();
		        }
				
				return saveOrUpdateUserInfo(userInfo);
		
}
	
	
	public UserInfo updatePasswordOfUserInfo(UserInfo userInfo) throws Exception {
		 if(userInfo.getPassword()==null) {
			 return saveOrUpdateUserInfo(userInfo);
		 }
		 else {
			 String password = userInfo.getPassword();
				Date date= new Date();
				 
				 long time = date.getTime();
				Timestamp passwordGenerateDate = new Timestamp(time);
				try {
		            // Create MessageDigest instance for MD5
		            MessageDigest md = MessageDigest.getInstance("SHA-256");
		            //Add password bytes to digest
		            md.update(password.getBytes());
		            //Get the hash's bytes 
		            byte[] bytes = md.digest();
		            //This bytes[] has bytes in decimal format;
		            //Convert it to hexadecimal format
		            StringBuilder sb = new StringBuilder();
		            for(int i=0; i< bytes.length ;i++)
		            {
		                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		            }
		            //Get complete hashed password in hex format
		            userInfo.setPassword(sb.toString());
		            userInfo.setPasswordChangeDate(passwordGenerateDate);
		            userInfo.setLastLoginDate(passwordGenerateDate);
		            
		        } 
		        catch (NoSuchAlgorithmException e) 
		        {
		        	log.error("", e);
		            e.printStackTrace();
		        }
				
				return saveOrUpdateUserInfo(userInfo);
		 }
		
}
	
	
	
	
	public UserInfo saveOrUpdateUserInfoX(UserInfo userInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		String status = null;
		System.err.print("--------- "+userInfo.getId()+"----");
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_USER.toString(), UserInfo.class);
			call.registerParameter("pID", Integer.class, ParameterMode.IN).bindValue((userInfo.getId()==null)?0:userInfo.getId());
			call.registerParameter("pUSER_ID", String.class, ParameterMode.IN).bindValue(userInfo.getUserId()==null?"":userInfo.getUserId());
			call.registerParameter("pUSER_NAME", String.class, ParameterMode.IN).bindValue(userInfo.getUserName()==null?"":userInfo.getUserName());
			call.registerParameter("pPASSWORD", String.class, ParameterMode.IN).bindValue(userInfo.getPassword()==null?"":userInfo.getPassword());
			call.registerParameter("pOFFICE_CODE", String.class, ParameterMode.IN).bindValue((userInfo.getOfficeCode()==null)?"":userInfo.getOfficeCode());
			call.registerParameter("pUSER_STATUS", String.class, ParameterMode.IN).bindValue(userInfo.getUserStatus()==null?"":userInfo.getUserStatus());
			call.registerParameter("pPWD_CHG_DT", Timestamp.class, ParameterMode.IN).bindValue(userInfo.getPasswordChangeDate()==null?new Timestamp(0):userInfo.getPasswordChangeDate());
			call.registerParameter("pLAST_LOGIN_DT", Timestamp.class, ParameterMode.IN).bindValue(userInfo.getLastLoginDate()==null?new Timestamp(0):userInfo.getLastLoginDate());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return userInfo;
	}
	
	public UserInfo saveOrUpdateUserInfo(UserInfo userInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		System.err.print("--------- "+userInfo.getId()+"----");
		
		try {
			session.beginTransaction();
			session.saveOrUpdate(userInfo);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				
			session.close();
			
			}

	}
		return userInfo;
	}
	
	
	
	public UserInfo deleteAllUserInfo(UserInfo userInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		System.err.print("--------- "+userInfo.getId()+"----");
		
		try {
			session.beginTransaction();
			session.delete(userInfo);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
		return userInfo;
	}
	
	public List<UserInfo> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<UserInfo> users = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			users = session.createQuery("from UserInfo where userStatus='A' ", UserInfo.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return users;
    }
}
