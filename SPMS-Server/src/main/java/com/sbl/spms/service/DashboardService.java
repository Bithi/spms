package com.sbl.spms.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.model.Chart;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DashboardService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	private static final long ONE_DAY_MILLI_SECONDS = 24 * 60 * 60 * 1000;
	public List<Chart> getConnectivityChart() throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Chart connectivityChart = new Chart();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Chart> connectivityChartList = new ArrayList<Chart>();
	    
	    session.getTransaction().commit();
	    
	    try {
	    	session.beginTransaction();

			List<Object[]> results = session.createNativeQuery(
					"select cont.Cont_no, extract(year from cont.CONT_VALID_START)||'-'||extract(year from cont.CONT_VALID_END) year,cont.TOTAL_DC_DR_CONNECTIVITY conn\r\n" + 
					" from contract_info cont\r\n" + 
					" join VENDOR_INFO vendor on vendor.VEN_ID = cont.VEN_ID\r\n" + 
					" join \r\n" + 
					" ( select count(cont_no) as ct_cont, vendor.ven_id\r\n" + 
					" from contract_info vendor where TYPE='SLA' OR TYPE='conn' \r\n" + 
					" group by vendor.ven_id\r\n" + 
					") cnt on CNT.VEN_ID = vendor.VEN_ID\r\n" + 
					"where (TYPE='SLA' OR TYPE='conn') and CONT.status='ACT' ")
					.list();

			for (Object[] result : results) {
				connectivityChart = new Chart();
				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
				connectivityChart.setYearRange(result[1]==null?"":result[1].toString());
				connectivityChart.setCount (result[2]==null?"":result[2].toString());
				connectivityChartList.add(connectivityChart);
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return connectivityChartList;
	}
	
//	public List<Chart> getSLAChartValidity(int duration) throws Exception{
//		final Session session = sessionFactory.openSession();
//	    session.beginTransaction();
//	    Chart connectivityChart = new Chart();
//	    // We read labels record from database using a simple Hibernate
//	    // query, the Hibernate Query Language (HQL).
//	    List<Chart> connectivityChartList = new ArrayList<Chart>();
//	    
//	    session.getTransaction().commit();
//	    
//	    try {
//	    	session.beginTransaction();
//
//			List<Object[]> results = session.createNativeQuery("SELECT     cont_no,CONT_VALID_END\r\n" + 
//					"     ,     CASE \r\n" + 
//					"            WHEN duration < 90 THEN '3 month'\r\n" + 
//					"            WHEN duration BETWEEN 90 AND 180 THEN '6 month'\r\n" + 
//					"            WHEN duration BETWEEN 180 AND 270 THEN '9 month'\r\n" + 
//					"        END AS duration\r\n" + 
//					"     ,     COUNT(cont_no) AS ContCount \r\n" + 
//					"  FROM   \r\n" + 
//					"  \r\n" + 
//					"   (select cont_no,CONT_VALID_END, (cont_valid_end - Trunc(SYSDATE)) duration from (\r\n" + 
//					"SELECT CONT.cont_no, SYSDATE, CASE WHEN ( YR.end_date > CONT.cont_valid_end ) THEN YR.end_date ELSE CONT.cont_valid_end    END CONT_VALID_END\r\n" + 
//					"  FROM   procmnt.contract_info CONT \r\n" + 
//					"     join procmnt.cont_yearwise_rate YR   ON( YR.cont_id = CONT.cont_id )   where CONT.type='SLA'))\r\n" + 
//					"     \r\n" + 
//					"     \r\n" + 
//					"     \r\n" + 
//					"  GROUP BY cont_no,CONT_VALID_END\r\n" + 
//					"     ,     CASE \r\n" + 
//					"            WHEN duration < 90 THEN '3 month'\r\n" + 
//					"            WHEN duration BETWEEN 90 AND 180 THEN '6 month'\r\n" + 
//					"            WHEN duration BETWEEN 180 AND 270 THEN '9 month'\r\n" + 
//					"        END ")
//					.list();
//			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");// 2020-10-31 00:00:00.0
//			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
//			
//			for (Object[] result : results) {
//				connectivityChart = new Chart();
//				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
//				Date agreementEnd= FORMAT.parse(result[1].toString());  
//				connectivityChart.setEndValidity(agreementEnd);
//				connectivityChart.setValidity(result[2].toString());
//				connectivityChartList.add(connectivityChart);
//			}
//			session.flush();
//
//		} catch (Exception e) {
//			log.error("", e);
//			e.printStackTrace();
//			session.getTransaction().rollback();
//			session.close();
//			throw e;
//		} finally {
//			if (session != null) {
//				if(session.getTransaction().isActive()) {
//					session.getTransaction().commit();
//				}
//			session.close();
//			
//			}
//
//	}
//	    return connectivityChartList;
//	}
	
	
	public List<Chart> getCountSLAChartValidity() throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Chart connectivityChart = new Chart();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Chart> connectivityChartList = new ArrayList<Chart>();
	    
	    session.getTransaction().commit();
	    
	    try {
	    	session.beginTransaction();

			List<Object[]> results = session.createNativeQuery("select  cont_no,CONT_VALID_END,duration, COUNT(duration) OVER (PARTITION BY duration) AS count ,cont_valid_start \r\n" + 
					"from \r\n" + 
					"    (select distinct cont_no,CONT_VALID_END,cont_valid_start,  CASE       WHEN (cont_valid_end < Trunc(SYSDATE)) THEN 'f: already expired'\r\n" + 
					"                                      WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 0 AND 90 THEN 'a: 0 to 3 months'\r\n" + 
					"                   WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 90 AND 180 THEN 'b: 3 to 6 months'\r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 180 AND 270 THEN 'c: 6 to 9 months' \r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) >360 THEN 'e: 12 months to above' \r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 270 AND 360 THEN 'd: 9 to 12 months' END duration\r\n" + 
					"from\r\n" + 
					"  contract_info CONT \r\n" +  
					"              where (CONT.type='SLA' OR CONT.type='conn') AND (STATUS!='CAN' AND STATUS!='CLS')   \r\n" + 
					"\r\n" + 
					")")
					.list();
			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");// 2020-10-31 00:00:00.0
			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Object[] result : results) {
				connectivityChart = new Chart();
				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
				Date agreementEnd= FORMAT.parse(result[1].toString());  
				Date agreementStart= FORMAT.parse(result[4].toString());  
				connectivityChart.setEndValidity(agreementEnd);
				connectivityChart.setStartValidity(agreementStart);
				connectivityChart.setValidity(result[2]==null?"":result[2].toString());
				
				connectivityChart.setCount(result[3].toString());
				connectivityChartList.add(connectivityChart);
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return connectivityChartList;
	}
	
	
	public List<Chart> getConnectivityChartValidity() throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Chart connectivityChart = new Chart();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Chart> connectivityChartList = new ArrayList<Chart>();
	    
	    session.getTransaction().commit();
	    
	    try {
	    	session.beginTransaction();

			List<Object[]> results = session.createNativeQuery("select  cont_no,CONT_VALID_END,duration, COUNT(duration) OVER (PARTITION BY duration) AS count ,cont_valid_start \r\n" + 
					"from \r\n" + 
					"    (select distinct cont_no,CONT_VALID_END,cont_valid_start,  CASE       WHEN (cont_valid_end < Trunc(SYSDATE)) THEN 'already expired'\r\n" + 
					"                                      WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 0 AND 90 THEN '0 to 3 months'\r\n" + 
					"                   WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 90 AND 180 THEN '3 to 6 months'\r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 180 AND 270 THEN '6 to 9 months' \r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) >360 THEN '12 months to above' \r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 270 AND 360 THEN '9 to 12 months' END duration\r\n" + 
					"from\r\n" + 
					"  contract_info CONT \r\n" +  
					"              where (CONT.type='conn')  AND (STATUS!='CAN' AND STATUS!='CLS')    \r\n" + 
					"\r\n" + 
					")")
					.list();
			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");// 2020-10-31 00:00:00.0
			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Object[] result : results) {
				connectivityChart = new Chart();
				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
				Date agreementEnd= FORMAT.parse(result[1].toString());  
				Date agreementStart= FORMAT.parse(result[4].toString());  
				connectivityChart.setEndValidity(agreementEnd);
				connectivityChart.setStartValidity(agreementStart);
				connectivityChart.setValidity(result[2]==null?"":result[2].toString());
				
				connectivityChart.setCount(result[3].toString());
				connectivityChartList.add(connectivityChart);
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return connectivityChartList;
	}
	
	
	public List<Chart> getCountAMCChartValidity() throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Chart connectivityChart = new Chart();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Chart> connectivityChartList = new ArrayList<Chart>();
	    
	    session.getTransaction().commit();
	    
	    try {
	    	session.beginTransaction();

			List<Object[]> results = session.createNativeQuery("select  cont_no,CONT_VALID_END,duration, COUNT(duration) OVER (PARTITION BY duration) AS count,cont_valid_start \r\n" + 
					"from \r\n" + 
					"    (select distinct cont_no,CONT_VALID_END,cont_valid_start,  CASE       WHEN (cont_valid_end < Trunc(SYSDATE)) THEN 'f: already expired'\r\n" + 
					"                                      WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 0 AND 90 THEN 'a: 0 to 3 months'\r\n" +
					"                   WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 90 AND 180 THEN 'b: 3 to 6 months'\r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 180 AND 270 THEN 'c: 6 to 9 months' \r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) >360 THEN 'e: 12 months to above' \r\n" + 
					"                       WHEN (cont_valid_end - Trunc(SYSDATE)) BETWEEN 270 AND 360 THEN 'd: 9 to 12 months' END duration\r\n" + 
					"from\r\n" + 
					"  contract_info CONT \r\n" + 
					"              where (CONT.type='AMC' OR CONT.type='pur') AND (STATUS!='CAN' AND STATUS!='CLS')    \r\n" + 
					"\r\n" + 
					")")
					.list();
			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");// 2020-10-31 00:00:00.0
			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Object[] result : results) {
				connectivityChart = new Chart();
				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
				Date agreementEnd= FORMAT.parse(result[1].toString());  
				Date agreementStart= FORMAT.parse(result[4].toString());  
				connectivityChart.setEndValidity(agreementEnd);
				connectivityChart.setValidity(result[2]==null?"":result[2].toString());
				connectivityChart.setStartValidity(agreementStart);
				connectivityChart.setCount(result[3].toString());
				connectivityChartList.add(connectivityChart);
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return connectivityChartList;
	}
	
	public List<Chart> getCountAMCChartValidityPrevData(String fdate, String tdate) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Chart connectivityChart = new Chart();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Chart> connectivityChartList = new ArrayList<Chart>();
	    
	    session.getTransaction().commit();
	    
	    try {
	    	session.beginTransaction();
	    	SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
	    	Date nextDate= myFormat.parse(fdate.toString());  
	    	long nextDayMilliSeconds = nextDate.getTime() + ONE_DAY_MILLI_SECONDS;
			Date nexttDate = new Date(nextDayMilliSeconds);
			String nextDateStr = myFormat.format(nexttDate);
			List<Object[]> results = session.createNativeQuery(
					"SELECT cont_no,\r\n" + 
					"       CONT_VALID_END,\r\n" + 
					"       duration,\r\n" + 
					"       COUNT (duration) OVER (PARTITION BY duration) AS COUNT,\r\n" + 
					"       cont_valid_start\r\n" + 
					"  FROM (SELECT DISTINCT\r\n" + 
					"               cont_no,\r\n" + 
					"               cont_id,\r\n" + 
					"               CONT_VALID_END,\r\n" + 
					"               cont_valid_start,\r\n" + 
					"               REF_CONTACT_NO,\r\n" + 
					"               CONT_RENEW_DT,\r\n" + 
					"               CASE\r\n" + 
					"                  WHEN (cont_valid_end < TO_DATE ('"+fdate+"', 'RRRR-MM-DD'))\r\n" + 
					"                  THEN\r\n" + 
					"                     'f: already expired'\r\n" + 
					"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 0\r\n" + 
					"                                                                                   AND 90\r\n" + 
					"                  THEN\r\n" + 
					"                     'a: 0 to 3 months'\r\n" + 
					"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 90\r\n" + 
					"                                                                                   AND 180\r\n" + 
					"                  THEN\r\n" + 
					"                     'b: 3 to 6 months'\r\n" + 
					"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 180\r\n" + 
					"                                                                                   AND 270\r\n" + 
					"                  THEN\r\n" + 
					"                     'c: 6 to 9 months'\r\n" + 
					"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) >\r\n" + 
					"                          360\r\n" + 
					"                  THEN\r\n" + 
					"                     'e: 12 months to above'\r\n" + 
					"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 270\r\n" + 
					"                                                                                   AND 360\r\n" + 
					"                  THEN\r\n" + 
					"                     'd: 9 to 12 months'\r\n" + 
					"               END\r\n" + 
					"                  duration\r\n" + 
					"          FROM (SELECT DISTINCT cont_id,\r\n" + 
					"                                REF_CONTACT_NO,\r\n" + 
					"                                cont_no,\r\n" + 
					"                                VEN_ID,\r\n" + 
					"                                remarks,\r\n" + 
					"                                comm_dt,\r\n" + 
					"                                CONT_VALID_START,\r\n" + 
					"                                CONT_VALID_END,\r\n" + 
					"                                TYPE,\r\n" + 
					"                                status,\r\n" + 
					"                                ENTRYDT,\r\n" + 
					"                                CONT_RENEW_DT\r\n" + 
					"                  FROM CONTRACT_INFO CONT\r\n" + 
					"                 WHERE     NOT EXISTS\r\n" + 
					"                              (SELECT 1\r\n" + 
					"                                 FROM CONTRACT_INFO\r\n" + 
					"                                WHERE ref_contact_no = cont.cont_id AND ENTRYDT <= TO_DATE ('"+fdate+"', 'RRRR-MM-DD') )\r\n" + 
					"                       AND TRUNC (CONT.ENTRYDT) <=\r\n" + 
					"                              TO_DATE ('"+fdate+"', 'RRRR-MM-DD')\r\n" + 
					"                       AND (CONT.TYPE = 'AMC' OR CONT.TYPE = 'pur') )) CONT")
					.list();
			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");// 2020-10-31 00:00:00.0
			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Object[] result : results) {
				connectivityChart = new Chart();
				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
				Date agreementEnd= FORMAT.parse(result[1].toString());  
				Date agreementStart= FORMAT.parse(result[4].toString());  
				connectivityChart.setEndValidity(agreementEnd);
				connectivityChart.setValidity(result[2]==null?"":result[2].toString());
				connectivityChart.setStartValidity(agreementStart);
				connectivityChart.setCount(result[3].toString());
				connectivityChartList.add(connectivityChart);
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return connectivityChartList;
	}
	
	public List<Chart> getCountSLAChartValidityPrevData(String fdate,String tdate) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Chart connectivityChart = new Chart();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    List<Chart> connectivityChartList = new ArrayList<Chart>();
	    
	    session.getTransaction().commit();
	    
	    try {
	    	session.beginTransaction();
	    	SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
	    	
	    	Date nextDate= myFormat.parse(fdate.toString());  
	    	long nextDayMilliSeconds = nextDate.getTime() + ONE_DAY_MILLI_SECONDS;
			Date nexttDate = new Date(nextDayMilliSeconds);
			String nextDateStr = myFormat.format(nexttDate);
	    	
	    	
	    	String nextStrDate=myFormat.format(nextDate.getTime() + 2);
			List<Object[]> results = session.createNativeQuery(
					"SELECT cont_no,\r\n" + 
							"       CONT_VALID_END,\r\n" + 
							"       duration,\r\n" + 
							"       COUNT (duration) OVER (PARTITION BY duration) AS COUNT,\r\n" + 
							"       cont_valid_start\r\n" + 
							"  FROM (SELECT DISTINCT\r\n" + 
							"               cont_no,\r\n" + 
							"               cont_id,\r\n" + 
							"               CONT_VALID_END,\r\n" + 
							"               cont_valid_start,\r\n" + 
							"               REF_CONTACT_NO,\r\n" + 
							"               CONT_RENEW_DT,\r\n" + 
							"               CASE\r\n" + 
							"                  WHEN (cont_valid_end < TO_DATE ('"+fdate+"', 'RRRR-MM-DD'))\r\n" + 
							"                  THEN\r\n" + 
							"                     'f: already expired'\r\n" + 
							"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 0\r\n" + 
							"                                                                                   AND 90\r\n" + 
							"                  THEN\r\n" + 
							"                     'a: 0 to 3 months'\r\n" + 
							"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 90\r\n" + 
							"                                                                                   AND 180\r\n" + 
							"                  THEN\r\n" + 
							"                     'b: 3 to 6 months'\r\n" + 
							"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 180\r\n" + 
							"                                                                                   AND 270\r\n" + 
							"                  THEN\r\n" + 
							"                     'c: 6 to 9 months'\r\n" + 
							"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) >\r\n" + 
							"                          360\r\n" + 
							"                  THEN\r\n" + 
							"                     'e: 12 months to above'\r\n" + 
							"                  WHEN (cont_valid_end - TO_DATE ('"+fdate+"', 'RRRR-MM-DD')) BETWEEN 270\r\n" + 
							"                                                                                   AND 360\r\n" + 
							"                  THEN\r\n" + 
							"                     'd: 9 to 12 months'\r\n" + 
							"               END\r\n" + 
							"                  duration\r\n" + 
							"          FROM (SELECT DISTINCT cont_id,\r\n" + 
							"                                REF_CONTACT_NO,\r\n" + 
							"                                cont_no,\r\n" + 
							"                                VEN_ID,\r\n" + 
							"                                remarks,\r\n" + 
							"                                comm_dt,\r\n" + 
							"                                CONT_VALID_START,\r\n" + 
							"                                CONT_VALID_END,\r\n" + 
							"                                TYPE,\r\n" + 
							"                                status,\r\n" + 
							"                                ENTRYDT,\r\n" + 
							"                                CONT_RENEW_DT\r\n" + 
							"                  FROM CONTRACT_INFO CONT\r\n" + 
							"                 WHERE     NOT EXISTS\r\n" + 
							"                              (SELECT 1\r\n" + 
							"                                 FROM CONTRACT_INFO\r\n" + 
							"                                WHERE ref_contact_no = cont.cont_id  AND ENTRYDT <= TO_DATE ('"+fdate+"', 'RRRR-MM-DD'))\r\n" + 
							"                       AND TRUNC (CONT.ENTRYDT) <=\r\n" + 
							"                              TO_DATE ('"+fdate+"', 'RRRR-MM-DD')\r\n" + 
							"                       AND (CONT.TYPE = 'SLA' OR CONT.TYPE = 'conn')  )) CONT")
					.list();
			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");// 2020-10-31 00:00:00.0
			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Object[] result : results) {
				connectivityChart = new Chart();
				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
				Date agreementEnd= FORMAT.parse(result[1].toString());  
				Date agreementStart= FORMAT.parse(result[4].toString());  
				connectivityChart.setEndValidity(agreementEnd);
				connectivityChart.setValidity(result[2]==null?"":result[2].toString());
				connectivityChart.setStartValidity(agreementStart);
				connectivityChart.setCount(result[3].toString());
				connectivityChartList.add(connectivityChart);
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
	    return connectivityChartList;
	}
	
	
//	public List<Chart> getAMCChartValidity(int duration) throws Exception{
//		final Session session = sessionFactory.openSession();
//	    session.beginTransaction();
//	    Chart connectivityChart = new Chart();
//	    // We read labels record from database using a simple Hibernate
//	    // query, the Hibernate Query Language (HQL).
//	    List<Chart> connectivityChartList = new ArrayList<Chart>();
//	    
//	    session.getTransaction().commit();
//	    
//	    try {
//	    	session.beginTransaction();
//
//			List<Object[]> results = session.createNativeQuery("select cont_no,CONT_VALID_END, (cont_valid_end - Trunc(SYSDATE)) duration from (\r\n" + 
//					"SELECT CONT.cont_no, SYSDATE, CASE WHEN ( YR.end_date > CONT.cont_valid_end ) THEN YR.end_date ELSE CONT.cont_valid_end    END CONT_VALID_END\r\n" + 
//					"  FROM   procmnt.contract_info CONT \r\n" + 
//					"     join procmnt.cont_yearwise_rate YR   ON( YR.cont_id = CONT.cont_id )   where (CONT.type='AMC' OR CONT.type='pur' ) where (cont_valid_end - Trunc(SYSDATE))<="+duration)
//					.list();
//			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S"); 
//			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
//			
//			for (Object[] result : results) {
//				connectivityChart = new Chart();
//				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
//				Date agreementEnd= FORMAT.parse(result[1].toString());  
//				connectivityChart.setEndValidity( agreementEnd);
//				connectivityChart.setValidity(result[2].toString());
//				connectivityChartList.add(connectivityChart);
//			}
//			session.flush();
//
//		} catch (Exception e) {
//			log.error("", e);
//			e.printStackTrace();
//			session.getTransaction().rollback();
//			session.close();
//			throw e;
//		} finally {
//			if (session != null) {
//				if(session.getTransaction().isActive()) {
//					session.getTransaction().commit();
//				}
//			session.close();
//			
//			}
//
//	}
//	    return connectivityChartList;
//	}
	
//	public List<Chart> getAMCChartValidityPrev(int duration) throws Exception{
//		final Session session = sessionFactory.openSession();
//	    session.beginTransaction();
//	    Chart connectivityChart = new Chart();
//	    // We read labels record from database using a simple Hibernate
//	    // query, the Hibernate Query Language (HQL).
//	    List<Chart> connectivityChartList = new ArrayList<Chart>();
//	    
//	    session.getTransaction().commit();
//	    
//	    try {
//	    	session.beginTransaction();
//
//			List<Object[]> results = session.createNativeQuery("select cont_no,CONT_VALID_END, (cont_valid_end - Trunc(SYSDATE)) duration from (\r\n" + 
//					"SELECT CONT.cont_no, SYSDATE, CASE WHEN ( YR.end_date > CONT.cont_valid_end ) THEN YR.end_date ELSE CONT.cont_valid_end    END CONT_VALID_END\r\n" + 
//					"  FROM   procmnt.contract_info CONT \r\n" + 
//					"     join procmnt.cont_yearwise_rate YR   ON( YR.cont_id = CONT.cont_id )   where (CONT.type='AMC' OR CONT.type='pur' ) where (cont_valid_end - Trunc(SYSDATE))<="+duration)
//					.list();
//			DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S"); 
//			SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
//			
//			for (Object[] result : results) {
//				connectivityChart = new Chart();
//				connectivityChart.setContractNo(result[0]==null?"":result[0].toString());
//				Date agreementEnd= FORMAT.parse(result[1].toString());  
//				connectivityChart.setEndValidity( agreementEnd);
//				connectivityChart.setValidity(result[2].toString());
//				connectivityChartList.add(connectivityChart);
//			}
//			session.flush();
//
//		} catch (Exception e) {
//			log.error("", e);
//			e.printStackTrace();
//			session.getTransaction().rollback();
//			session.close();
//			throw e;
//		} finally {
//			if (session != null) {
//				if(session.getTransaction().isActive()) {
//					session.getTransaction().commit();
//				}
//			session.close();
//			
//			}
//
//	}
//	    return connectivityChartList;
//	}
	
	

}
