package com.sbl.spms.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Menu;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MenuInfoService {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	
	public List<Menu> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<Menu> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from Menu", Menu.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return labels;
    }
	public List<Object> getParentMenu() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<Menu> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT  m_id, menuname \"menu\",\r\n" + 
					"    SYS_CONNECT_BY_PATH(menuname, '/') \"Path\"\r\n" + 
					"   FROM menu m\r\n" + 
					"   WHERE LEVEL > 0 \r\n" + 
					"   CONNECT BY PRIOR m_id = parentmenuid\r\n" + 
					"    START WITH\r\n" + 
					"   parentmenuid IS NULL\r\n" + 
					"    order by m_id")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }
	
	public Menu saveOrUpdateMenuInfo(Menu menuInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_MENU.toString(), Menu.class);
			call.registerParameter("pM_ID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pMENUNAME", String.class, ParameterMode.IN).bindValue((menuInfo.getMenuName()==null)?"":menuInfo.getMenuName());
			call.registerParameter("pPARENTMENUID", int.class, ParameterMode.IN).bindValue((menuInfo.getParentMenuId()==null)?0:menuInfo.getParentMenuId());	
			call.registerParameter("pURL", String.class, ParameterMode.IN).bindValue((menuInfo.getUrl()==null)?"":menuInfo.getUrl());
			call.registerParameter("pICON", String.class, ParameterMode.IN).bindValue((menuInfo.getIcon()==null)?"":menuInfo.getIcon());
			call.registerParameter("pCOMPONENT", String.class, ParameterMode.IN).bindValue((menuInfo.getComponent()==null)?"":menuInfo.getComponent());
			call.registerParameter("pMODULE", String.class, ParameterMode.IN).bindValue((menuInfo.getModule()==null)?"":menuInfo.getModule());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return menuInfo;
	}
	
	
	public List<Menu> getMenuList() {
		return commonService.getDataList(new Menu(),null);
	}

	public List<Menu> showMenu(UserInfo user) throws Exception {
		Session session = null;

		try {

			session = sessionFactory.openSession();
			//transaction = session.beginTransaction();
			

			/*
			 * create criteria which is used for and/or etc.
			 */
			
			
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_SHOW_MENU.toString(), Menu.class);

			call.registerParameter("pUSER_ID", Integer.class, ParameterMode.IN).
			bindValue(user.getId());
			
			call.registerParameter("OUT_RESULT", Class.class, ParameterMode.REF_CURSOR);
			
			
			
			call.execute();

			List<Menu> menuList;
			int rootMenuIndex = 0;
			try {
				menuList =  (List<Menu>) call.getResultList();
				for (int j = 0; j < menuList.size(); j++) {
		            if(menuList.get(j).getMenuName().equals("root")) {
		            	rootMenuIndex = menuList.get(j).getM_id();
		            	menuList.remove(j);
		            }
		            
		        }
				for (int i = 0; i < menuList.size(); i++) {
		            if(menuList.get(i).getParentMenuId().equals(rootMenuIndex)) {
		            	menuList.get(i).setParentMenuId(null);
		            }
		        }
				
			} catch (NoResultException nre) {
				menuList = null;
			}

//			transaction.commit();
			if (session != null) {
				session.close();
			}
			
			return menuList;

		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
			session.close();
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}

}
    public Menu updateMenuInfo(Menu menuInfo) throws Exception {

        Integer Id = 0;
        if(menuInfo.getM_id()!=null) {
            Id = menuInfo.getM_id();
        }
        
        Menu menuOne = new Menu(Id,menuInfo.getMenuName(),menuInfo.getParentMenuId(),menuInfo.getUrl(),menuInfo.getIcon(),menuInfo.getComponent(),menuInfo.getModule());
        return updateMenu(menuOne);
    }
    
    public Menu updateMenu(Menu menuOne) throws  Exception {
        final Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();
            session.saveOrUpdate(menuOne);
            session.flush();

        } 
        catch (Exception e) {
            e.printStackTrace();
            
            System.out.println(e.getCause());
            session.getTransaction().rollback();
            session.close();
            if(e.getCause().toString().contains("ConstraintViolationException")) {
                throw new Exception("Constraint Violation occured");
            }
            else
                throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();

            }

        }
        return menuOne;
    }
    
    
    public Menu DeleteMenuInfo(Menu menu) throws Exception {
        final Session session = sessionFactory.openSession();
        // System.err.print("--------- "+poroInfo.getPoroCode()+"----");

        try {
            session.beginTransaction();
            session.delete(menu);
            session.flush();

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            session.close();
            throw e;
        } finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();

            }

        }
        return menu;
    }

}