package com.sbl.spms.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.Menu;
import com.sbl.spms.model.MenuAssignedToRole;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MenuAssigendToRoleService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
	public MenuAssignedToRole saveOrUpdateMenuAssignedToRoleInfo(MenuAssignedToRole menuAssignedToRole) throws Exception {
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_MENUASSIGNEDTOROLE.toString(), MenuAssignedToRole.class);
			call.registerParameter("pID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pM_ID", int.class, ParameterMode.IN).bindValue((menuAssignedToRole.getM_id()==null)?0:menuAssignedToRole.getM_id());
			call.registerParameter("pR_ID", int.class, ParameterMode.IN).bindValue((menuAssignedToRole.getR_id()==null)?0:menuAssignedToRole.getR_id());			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				session.close();
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return menuAssignedToRole;
	}
	
	public List<MenuAssignedToRole> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<MenuAssignedToRole> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from MenuAssignedToRole", MenuAssignedToRole.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return labels;
    }
	
	public List<MenuAssignedToRole> getMenuAssignedToRoleList() {
		return commonService.getDataList(new MenuAssignedToRole(),null);
	}
	
	
	public MenuAssignedToRole updateMenuAssignedToRoleInfo(MenuAssignedToRole menuAssignedToRole) throws Exception {

		Integer Id = 0;
		if(menuAssignedToRole.getId()!=null) {
			Id = menuAssignedToRole.getId();
		}
	
		Integer mId = menuAssignedToRole.getM_id();
		Integer rId = menuAssignedToRole.getR_id();
		
		MenuAssignedToRole mATR = new MenuAssignedToRole(Id,mId,rId);
		return updateMenuAssignedToRole(mATR);
	}
	
	public MenuAssignedToRole updateMenuAssignedToRole(MenuAssignedToRole mATR) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.saveOrUpdate(mATR);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			session.close();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}

	
	public MenuAssignedToRole DeleteMenuAssignedToRoleInfo(MenuAssignedToRole menuAssignedRole) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(menuAssignedRole);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return menuAssignedRole;
	}
	
	public List<Object>  getMenuAssignedToRole() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Object> object = new ArrayList<Object>();
        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<Menu> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			object = session.createNativeQuery("SELECT mr.id, \r\n" + 
					"       mr.m_id, \r\n" + 
					"       p.path, \r\n" + 
					"       mr.r_id, \r\n" + 
					"       R.name rolename \r\n" + 
					"FROM   menuassignedtorole mr \r\n" + 
					"       inner join ROLE r \r\n" + 
					"               ON( mr.r_id = r.r_id ) \r\n" + 
					"       join (SELECT m_id, \r\n" + 
					"                    Sys_connect_by_path(menuname, '/') Path \r\n" + 
					"             FROM   menu m \r\n" + 
					"             WHERE  LEVEL > 1 \r\n" + 
					"             CONNECT BY PRIOR m_id = parentmenuid \r\n" + 
					"             START WITH parentmenuid IS NULL) p \r\n" + 
					"         ON ( p.m_id = mr.m_id ) \r\n" + 
					"ORDER  BY mr.id ")
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return object;
    }


}
