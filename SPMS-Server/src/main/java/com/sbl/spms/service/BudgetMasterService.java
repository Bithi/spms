package com.sbl.spms.service;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.BudgetBillEntry;
import com.sbl.spms.model.BudgetMaster;
import com.sbl.spms.model.Contractbilltran;
import com.sbl.spms.model.UserInfo;
import com.sbl.spms.model.VatAitProduct;
import com.sbl.spms.model.VendorServiceType;
import com.sbl.spms.util.CommonService;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class BudgetMasterService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;

	public BudgetMaster saveBudgetMasterEntry(BudgetMaster budgetMasterEntry) throws Exception {

		final Session session = sessionFactory.openSession();

		try {
			log.debug("Calling SP");
			session.getTransaction().begin();

			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_BUDGETMASTERENTRY.toString(),
					BudgetBillEntry.class);

			call.registerParameter("pENTRY_ID", String.class, ParameterMode.IN)
					.bindValue((budgetMasterEntry.getCreatedBy() == null) ? "" : budgetMasterEntry.getCreatedBy());

			call.registerParameter("pENTRY_DT", Timestamp.class, ParameterMode.IN)
					.bindValue(new Timestamp(System.currentTimeMillis()));

			call.registerParameter("pUPDATE_ID", String.class, ParameterMode.IN)
					.bindValue((budgetMasterEntry.getUpdatedBy() == null) ? "" : budgetMasterEntry.getUpdatedBy());

			call.registerParameter("pUPDATE_DT", Timestamp.class, ParameterMode.IN).enablePassingNulls(true);
			call.setParameter("pUPDATE_DT", null);


			call.registerParameter("pAMOUNT_ALLOCATION_DATE", Timestamp.class, ParameterMode.IN).bindValue(
					budgetMasterEntry.getAmountAllocationDate() == null ? new Timestamp(0) : budgetMasterEntry.getAmountAllocationDate());

			call.registerParameter("pYEAR", String.class, ParameterMode.IN).bindValue(
					(budgetMasterEntry.getYear() == null) ? "" : budgetMasterEntry.getYear());

			call.registerParameter("pAMOUNT", Double.class, ParameterMode.IN)
					.bindValue((budgetMasterEntry.getAmount() == null) ? 0 : budgetMasterEntry.getAmount());

			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);

			call.execute();

			String opStatus = (String) call.getOutputParameterValue("InStatus");

			log.info(opStatus);

			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);

			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			session.close();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return budgetMasterEntry;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getBudgetMasterInfo() throws Exception {
		final Session session = sessionFactory.openSession();
		session.beginTransaction();
		List<Object> object = new ArrayList<Object>();
		
		session.getTransaction().commit();

		try {
			session.beginTransaction();
			object = session
					.createNativeQuery("SELECT * FROM PROCMNT.BUDGET_MASTER")
					.list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}

		return object;
	}

	public BudgetMaster UpdateBudgetMasterInfoService(BudgetMaster budgetMasterInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		try {

				log.debug("update calling");
				session.beginTransaction();
				budgetMasterInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				session.update(budgetMasterInfo);	

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return budgetMasterInfo;
	}

	
	public BudgetMaster DeleteBudgetMasterInfo(BudgetMaster budgetMasterInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		try {

				log.info("delete calling");
				session.beginTransaction();
				//budgetBillInfo.setUpdate_dt(new Timestamp(System.currentTimeMillis()));
//				session.merge(budgetBillInfo);
//				session.delete(budgetBillInfo);	
				BudgetBillEntry persistentBudgetBillEntry = session.load(BudgetBillEntry.class, budgetMasterInfo.getId());
				session.delete(persistentBudgetBillEntry);


		} catch (Exception e) {
			
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return budgetMasterInfo;
		
		
	}
	
	
	
	

	
}

