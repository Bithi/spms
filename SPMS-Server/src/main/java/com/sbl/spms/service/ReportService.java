package com.sbl.spms.service;

import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.sbl.spms.constants.Constant;
import com.sbl.spms.model.DowntimeCalcRep;
import com.sbl.spms.model.FindValidEnd;
import com.sbl.spms.model.PenaltyCalc;
import com.sbl.spms.model.PenaltyReport;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import oracle.jdbc.OracleTypes;

@Service
@Slf4j
public class ReportService {
	@Autowired
	private ResourceLoader resourceLoader;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private VendorInfoService vendorInfoService;
	@Autowired
	private ContractInfoService contractInfoService;

	@Value("classpath:downtime_report.jrxml")
	private String downtimeTemplate;

	@Value("classpath:Penalty_Report.jrxml")
	private String penaltyTemplate;

	@Value("classpath:DowntimeWithPenaltyFour.jrxml")
	private String joinTwoTables;

	@Value("classpath:SPMS_Report3.jrxml")
	private String amcReport;

	@Value("classpath:SPMS_Report4.jrxml")
	private String amcExWithItemReport;
	
	@Value("classpath:SPMS_AMC_CURRENT_WITHOUT_ITEM.jrxml")
	private String amcExWithoutItemReport;

	@Value("classpath:SPMS_Report.jrxml")
	private String connectivityReport;

	@Value("classpath:Contract_validity_end_report.jrxml")
	private String contractValidityReport;

	@Value("classpath:SPMS_Connectivity_Expiration.jrxml")
	private String slaExpReport;
	
	@Value("classpath:ServiceProviderManagement.jrxml")
	private String slaServiceReport;

	

	public void getDowntimeReport(DowntimeCalcRep downTimeInfo, OutputStream ou) {

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			Date date = new Date();
			if (downTimeInfo.getFromDate() == null) {
				downTimeInfo.setFromDate(new Timestamp(date.getTime()));

			}
			if (downTimeInfo.getToDate() == null) {
				downTimeInfo.setToDate(new Timestamp(date.getTime()));

			}
			session = sessionFactory.openSession();
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_DOWNTIME_REPORT.toString() + " (?,?,?,?,?,?) }")) {
					function.setInt(1, (downTimeInfo.getId()));
					function.setTimestamp(2, (downTimeInfo.getFromDate()));
					function.setTimestamp(3, (downTimeInfo.getToDate()));
					function.setString(4, (downTimeInfo.getVendor()));
					function.setString(5, ("DOWNTIME_DETAILS"));
					function.registerOutParameter(6, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(6);
					/*
					 * while (rst.next()) {
					 * 
					 * System.out.println(rst.getObject(1)); }
					 */
					Resource rs = resourceLoader.getResource(downtimeTemplate);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getPenaltyCalcReport(PenaltyCalc penaltyCalc, OutputStream ou) {

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */

			session = sessionFactory.openSession();
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_PENALTYCALC_REPORT.toString() + " (?,?,?,?,?,?) }")) {
					function.setString(1, (penaltyCalc.getContId()));
					function.setString(2, (penaltyCalc.getVenId()));
					function.setString(3, (penaltyCalc.getBrCode()));
					function.setString(4, (penaltyCalc.getMonth()));
					function.setString(5, ("PENALTY_DETAILS"));
					function.registerOutParameter(6, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(6);
					/*
					 * while (rst.next()) {
					 * 
					 * System.out.println(rst.getObject(1)); }
					 */
					Resource rs = resourceLoader.getResource(penaltyTemplate);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	

	public void getPenaltyReport(PenaltyReport penaltyReport, OutputStream ou) {

		Date date = new Date();
		if (penaltyReport.getStartDate() == null) {
			penaltyReport.setStartDate((new Timestamp(date.getTime())));

		}
		if (penaltyReport.getEndDate() == null) {
			penaltyReport.setEndDate((new Timestamp(date.getTime())));

		}
		System.err.print("---" + penaltyReport.getContId() + " " + penaltyReport.getVenId() + " "
				+ penaltyReport.getEndDate() + " " + penaltyReport.getStartDate() + "---");
		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_PENALTY_REPORTT.toString() + " (?,?,?,?,?,?) }")) {
					function.setString(1, (penaltyReport.getContId()));
					function.setString(2, (penaltyReport.getVenId()));
					function.setTimestamp(3, (penaltyReport.getStartDate()));
					function.setTimestamp(4, (penaltyReport.getEndDate()));
					function.setString(5, ("PENALTY_REPORT"));
					function.registerOutParameter(6, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(6);

					// while (rst.next()) {
					//
					// System.out.println(rst.getString(1));
					// }
					DateFormat OLD_FORMAT = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
					SimpleDateFormat NEW_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
					Date agreementStart = OLD_FORMAT.parse(
							OLD_FORMAT.format(contractInfoService.startagreementById(penaltyReport.getContId())));
					Date agreementEnd = OLD_FORMAT
							.parse(OLD_FORMAT.format(contractInfoService.endagreementById(penaltyReport.getContId())));
					String agreementStartconvert = NEW_FORMAT.format(agreementStart);
					String agreementEndconvert = NEW_FORMAT.format(agreementEnd);

					Resource rs = resourceLoader.getResource(joinTwoTables);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("contId", penaltyReport.getContId());
					parameters.put("contNo", contractInfoService.contractNumberById(penaltyReport.getContId()));
					parameters.put("venId", penaltyReport.getVenId());
					parameters.put("venName", vendorInfoService.vendorNameById(penaltyReport.getVenId()));
					parameters.put("fromDate", penaltyReport.getStartDate());
					parameters.put("toDate", penaltyReport.getEndDate());
					parameters.put("agreementStart", agreementStartconvert);
					parameters.put("agreementEnd", agreementEndconvert);

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getAmcReport(PenaltyReport penaltyReport, OutputStream ou) {
		
		
		Date date = new Date();
		
			if (penaltyReport.getStartDate() == null) {
				penaltyReport.setStartDate((new Timestamp(date.getTime())));

			}
			if (penaltyReport.getEndDate() == null) {
				penaltyReport.setEndDate((new Timestamp(date.getTime())));

			}
	

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_AMC_REPORT.toString() + " (?,?,?,?,?,?,?,?) }")) {
					function.setTimestamp(1, (penaltyReport.getStartDate()));
					function.setTimestamp(2, (penaltyReport.getEndDate()));
					function.setString(3, penaltyReport.getReportType()==null?"":penaltyReport.getReportType());
					function.setString(4, penaltyReport.getVenId()==null?"":penaltyReport.getVenId());
					function.setString(5, penaltyReport.getItem()==null?"":penaltyReport.getItem());
					function.setString(6, penaltyReport.getSite()==null?"":penaltyReport.getSite());
					function.setString(7, penaltyReport.getStatus()==null?"":penaltyReport.getStatus());
					function.registerOutParameter(8, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(8);

//					 while (rst.next()) {
//
//					 System.out.println(rst.getObject(1));
//					 }
					Resource rs = resourceLoader.getResource(amcReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					
						parameters.put("fromDate", penaltyReport.getStartDate());
						parameters.put("toDate", penaltyReport.getEndDate());
						parameters.put("clause", penaltyReport.getReportType());
						parameters.put("user", penaltyReport.getUser());
		

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	

	public void getAmcExpirationReport(PenaltyReport penaltyReport, OutputStream ou) {

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			String clause = penaltyReport.getClause();
			if (penaltyReport.getClause().substring(3).equals("already expired")) {
				penaltyReport.setClause("-999,0");
			} else if (penaltyReport.getClause().substring(3).equals("0 to 3 months")) {
				penaltyReport.setClause("0,90");
			}
			else if (penaltyReport.getClause().substring(3).equals("3 to 6 months")) {
				penaltyReport.setClause("90,180");
			}
			else if (penaltyReport.getClause().substring(3).equals("6 to 9 months")) {
				penaltyReport.setClause("180,270");
			}
			else if (penaltyReport.getClause().substring(3).equals("9 to 12 months")) {
				penaltyReport.setClause("270,360");
			}
			else if (penaltyReport.getClause().substring(3).equals("12 months to above")) {
				penaltyReport.setClause("360,9999");
			}
			
			if (penaltyReport.getReportType().equals("AMC_CURR_WITH_ITEM")) {
				session.doWork(connection -> {
					try (CallableStatement function = connection
							.prepareCall("{ call " + Constant.SP_AMC_EXPIRATION_REPORT.toString() + " (?,?,?) }")) {
						function.setString(1, (penaltyReport.getClause()));
						function.setString(2, (penaltyReport.getReportType()));
						function.registerOutParameter(3, OracleTypes.CURSOR);
						function.execute();
						ResultSet rst = (ResultSet) function.getObject(3);

						// while (rst.next()) {
						//
						// System.out.println(rst.getObject(1));
						// }
						Resource rs = resourceLoader.getResource(amcExWithItemReport);
						JasperDesign design = JRXmlLoader.load(rs.getInputStream());
						JasperReport jasperReport = JasperCompileManager.compileReport(design);
						parameters.put("clause", clause.substring(3));
						parameters.put("user", penaltyReport.getUser());
						JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
								new JRResultSetDataSource(rst));
						JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

					} catch (Exception ex) {
						log.error("", ex);
						ex.printStackTrace();

					}
				});
			}
			else if(penaltyReport.getReportType().equals("AMC_CURR_WITHOUT_ITEM")) {
				session.doWork(connection -> {
					try (CallableStatement function = connection
							.prepareCall("{ call " + Constant.SP_AMC_EXPIRATION_REPORT.toString() + " (?,?,?) }")) {
						function.setString(1, (penaltyReport.getClause()));
						function.setString(2, (penaltyReport.getReportType()));
						function.registerOutParameter(3, OracleTypes.CURSOR);
						function.execute();
						ResultSet rst = (ResultSet) function.getObject(3);

						// while (rst.next()) {
						//
						// System.out.println(rst.getObject(1));
						// }
						Resource rs = resourceLoader.getResource(amcExWithoutItemReport);
						JasperDesign design = JRXmlLoader.load(rs.getInputStream());
						JasperReport jasperReport = JasperCompileManager.compileReport(design);
						parameters.put("clause", clause.substring(3));
						parameters.put("user", penaltyReport.getUser());
						JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
								new JRResultSetDataSource(rst));
						JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

					} catch (Exception ex) {
						log.error("", ex);
						ex.printStackTrace();

					}
				});
			}
			

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	public void getAmcExpirationPrevReport(PenaltyReport penaltyReport, OutputStream ou) {

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			String clause = penaltyReport.getClause();
			if (penaltyReport.getClause().substring(3).equals("already expired")) {
				penaltyReport.setClause("-999,0");
			} else if (penaltyReport.getClause().substring(3).equals("0 to 3 months")) {
				penaltyReport.setClause("0,90");
			}
			else if (penaltyReport.getClause().substring(3).equals("3 to 6 months")) {
				penaltyReport.setClause("90,180");
			}
			else if (penaltyReport.getClause().substring(3).equals("6 to 9 months")) {
				penaltyReport.setClause("180,270");
			}
			else if (penaltyReport.getClause().substring(3).equals("9 to 12 months")) {
				penaltyReport.setClause("270,360");
			}
			else if (penaltyReport.getClause().substring(3).equals("12 months to above")) {
				penaltyReport.setClause("360,9999");
			}
			if (penaltyReport.getReportType().equals("AMC_CURR_WITH_ITEM")) {
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_AMC_EXPIRATION_PREV_REPORT.toString() + " (?,?,?,?,?) }")) {
					function.setString(1, (penaltyReport.getClause()));
					function.setTimestamp(2, (penaltyReport.getStartDate()));
					function.setTimestamp(3, (penaltyReport.getEndDate()));
					function.setString(4, (penaltyReport.getReportType()));
					function.registerOutParameter(5, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(5);

//					 while (rst.next()) {
//					
//					 System.out.println(rst.getObject(1));
//					 }
					Resource rs = resourceLoader.getResource(amcExWithItemReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("clause", clause.substring(3));
					parameters.put("user", penaltyReport.getUser());
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});
			}
			else if (penaltyReport.getReportType().equals("AMC_CURR_WITHOUT_ITEM")) {
				session.doWork(connection -> {
					try (CallableStatement function = connection
							.prepareCall("{ call " + Constant.SP_AMC_EXPIRATION_PREV_REPORT.toString() + " (?,?,?,?,?) }")) {
						function.setString(1, (penaltyReport.getClause()));
						function.setTimestamp(2, (penaltyReport.getStartDate()));
						function.setTimestamp(3, (penaltyReport.getEndDate()));
						function.setString(4, (penaltyReport.getReportType()));
						function.registerOutParameter(5, OracleTypes.CURSOR);
						function.execute();
						ResultSet rst = (ResultSet) function.getObject(5);

//						 while (rst.next()) {
//						
//						 System.out.println(rst.getObject(1));
//						 }
						Resource rs = resourceLoader.getResource(amcExWithoutItemReport);
						JasperDesign design = JRXmlLoader.load(rs.getInputStream());
						JasperReport jasperReport = JasperCompileManager.compileReport(design);
						parameters.put("clause", clause.substring(3));
						parameters.put("user", penaltyReport.getUser());
						JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
								new JRResultSetDataSource(rst));
						JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

					} catch (Exception ex) {
						log.error("", ex);
						ex.printStackTrace();

					}
				});
				}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void getSlaExpirationPrevReport(PenaltyReport penaltyReport, OutputStream ou) {

//		Date date = new Date();
//		if (penaltyReport.getStartDate() == null) {
//			penaltyReport.setStartDate((new Timestamp(date.getTime())));
//
//		}
//		if (penaltyReport.getEndDate() == null) {
//			penaltyReport.setEndDate((new Timestamp(date.getTime())));
//
//		}

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			String clause = penaltyReport.getClause();
			if (penaltyReport.getClause().substring(3).equals("already expired")) {
				penaltyReport.setClause("-999,0");
			} else if (penaltyReport.getClause().substring(3).equals("0 to 3 months")) {
				penaltyReport.setClause("0,90");
			}
			else if (penaltyReport.getClause().substring(3).equals("3 to 6 months")) {
				penaltyReport.setClause("90,180");
			}
			else if (penaltyReport.getClause().substring(3).equals("6 to 9 months")) {
				penaltyReport.setClause("180,270");
			}
			else if (penaltyReport.getClause().substring(3).equals("9 to 12 months")) {
				penaltyReport.setClause("270,360");
			}
			else if (penaltyReport.getClause().substring(3).equals("12 months to above")) {
				penaltyReport.setClause("360,9999");
			}
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_SLA_PREV_EXP_REPORT.toString() + " (?,?,?,?) }")) {
					function.setString(1, (penaltyReport.getClause()));
					function.setTimestamp(2, (penaltyReport.getStartDate()));
					function.setTimestamp(3, (penaltyReport.getEndDate()));
					function.registerOutParameter(4, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(4);
					
					/* while (rst.next()) {
					
						 System.out.println(rst.getObject(1));
					 }*/
					
					Resource rs = resourceLoader.getResource(slaExpReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("clause", clause.substring(3));
					parameters.put("user", penaltyReport.getUser());

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	
	public void getSlaExpirationReport(PenaltyReport penaltyReport, OutputStream ou) {

		Date date = new Date();
		if (penaltyReport.getStartDate() == null) {
			penaltyReport.setStartDate((new Timestamp(date.getTime())));

		}
		if (penaltyReport.getEndDate() == null) {
			penaltyReport.setEndDate((new Timestamp(date.getTime())));

		}

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			String clause = penaltyReport.getClause() ;
			if (penaltyReport.getClause().substring(3).equals("already expired")) {
				penaltyReport.setClause("-999,0");
			} else if (penaltyReport.getClause().substring(3).equals("0 to 3 months")) {
				penaltyReport.setClause("0,90");
			}
			else if (penaltyReport.getClause().substring(3).equals("3 to 6 months")) {
				penaltyReport.setClause("90,180");
			}
			else if (penaltyReport.getClause().substring(3).equals("6 to 9 months")) {
				penaltyReport.setClause("180,270");
			}
			else if (penaltyReport.getClause().substring(3).equals("9 to 12 months")) {
				penaltyReport.setClause("270,360");
			}
			else if (penaltyReport.getClause().substring(3).equals("12 months to above")) {
				penaltyReport.setClause("360,9999");
			}
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_SLA_EXP_REPORT.toString() + " (?,?) }")) {
					function.setString(1, penaltyReport.getClause());
					function.registerOutParameter(2, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(2);
					
					/* while (rst.next()) {
					
						 System.out.println(rst.getObject(1));
					 }*/
					
					Resource rs = resourceLoader.getResource(slaExpReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("clause", clause.substring(3));
					parameters.put("user", penaltyReport.getUser());
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void getConnExpirationReport(PenaltyReport penaltyReport, OutputStream ou) {

		Date date = new Date();
		if (penaltyReport.getStartDate() == null) {
			penaltyReport.setStartDate((new Timestamp(date.getTime())));

		}
		if (penaltyReport.getEndDate() == null) {
			penaltyReport.setEndDate((new Timestamp(date.getTime())));

		}

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			String clause = penaltyReport.getClause() ;
			if (penaltyReport.getClause().equals("already expired")) {
				penaltyReport.setClause("-999,0");
			} else if (penaltyReport.getClause().equals("0 to 3 months")) {
				penaltyReport.setClause("0,90");
			}
			else if (penaltyReport.getClause().equals("3 to 6 months")) {
				penaltyReport.setClause("90,180");
			}
			else if (penaltyReport.getClause().equals("6 to 9 months")) {
				penaltyReport.setClause("180,270");
			}
			else if (penaltyReport.getClause().equals("9 to 12 months")) {
				penaltyReport.setClause("270,360");
			}
			else if (penaltyReport.getClause().equals("12 months to above")) {
				penaltyReport.setClause("360,9999");
			}
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_CONN_EXP_REPORT.toString() + " (?,?) }")) {
					function.setString(1, penaltyReport.getClause());
					function.registerOutParameter(2, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(2);
					
					/* while (rst.next()) {
					
						 System.out.println(rst.getObject(1));
					 }*/
					
					Resource rs = resourceLoader.getResource(slaExpReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("clause", clause);

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getConnectivityReport(PenaltyReport penaltyReport, OutputStream ou) {

		Date date = new Date();
		if (penaltyReport.getStartDate() == null) {
			penaltyReport.setStartDate((new Timestamp(date.getTime())));

		}
		if (penaltyReport.getEndDate() == null) {
			penaltyReport.setEndDate((new Timestamp(date.getTime())));

		}

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();

			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_CONNECTIVITY_REPORT.toString() + " (?,?,?) }")) {
					function.setTimestamp(1, (penaltyReport.getStartDate()));
					function.setTimestamp(2, (penaltyReport.getEndDate()));
					function.registerOutParameter(3, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(3);

					// while (rst.next()) {

					// System.out.println(rst.getObject(1));
					// }
					Resource rs = resourceLoader.getResource(connectivityReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("fromDate", penaltyReport.getStartDate());
					parameters.put("toDate", penaltyReport.getEndDate());

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getContractValidityEndReport(FindValidEnd findValidEnd, OutputStream ou) {

		Date date = new Date();
		if (findValidEnd.getFromDate() == null) {
			findValidEnd.setFromDate((new Timestamp(date.getTime())));

		}
		if (findValidEnd.getToDate() == null) {
			findValidEnd.setToDate((new Timestamp(date.getTime())));

		}

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_CONTRACT_VALIDITY_END_REPORT.toString() + " (?,?,?) }")) {
					function.setTimestamp(1, (findValidEnd.getFromDate()));
					function.setTimestamp(2, (findValidEnd.getToDate()));
					function.registerOutParameter(3, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(3);

					// while (rst.next()) {

					// System.out.println(rst.getObject(1));
					// }
					Resource rs = resourceLoader.getResource(contractValidityReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					parameters.put("fromDate", findValidEnd.getFromDate());
					parameters.put("toDate", findValidEnd.getToDate());

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
public void getServiceReport(PenaltyReport penaltyReport, OutputStream ou) {
		
		
		Date date = new Date();
		
			if (penaltyReport.getStartDate() == null) {
				penaltyReport.setStartDate((new Timestamp(date.getTime())));

			}
			if (penaltyReport.getEndDate() == null) {
				penaltyReport.setEndDate((new Timestamp(date.getTime())));

			}
	

		Session session = null;
		try {

			/// JasperReport jasperReport =
			/// JasperCompileManager.compileReport(rs.getInputStream());
			Map<String, Object> parameters = new HashMap<String, Object>();

			/*
			 * get result
			 */
			session = sessionFactory.openSession();
			session.doWork(connection -> {
				try (CallableStatement function = connection
						.prepareCall("{ call " + Constant.SP_SERVICE_REPORT.toString() + " (?,?,?,?) }")) {
					function.setString(1, penaltyReport.getReportType()==null?"":penaltyReport.getReportType());
					function.setTimestamp(2, (penaltyReport.getStartDate()));
					function.setTimestamp(3, (penaltyReport.getEndDate()));
					function.registerOutParameter(4, OracleTypes.CURSOR);
					function.execute();
					ResultSet rst = (ResultSet) function.getObject(4);

//					 while (rst.next()) {
//
//					 System.out.println(rst.getObject(1));
//					 }
					Resource rs = resourceLoader.getResource(slaServiceReport);
					JasperDesign design = JRXmlLoader.load(rs.getInputStream());
					JasperReport jasperReport = JasperCompileManager.compileReport(design);
					
						parameters.put("fromDate", penaltyReport.getStartDate());
						parameters.put("toDate", penaltyReport.getEndDate());
						parameters.put("user", penaltyReport.getUser());
		

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
							new JRResultSetDataSource(rst));
					JasperExportManager.exportReportToPdfStream(jasperPrint, ou);

				} catch (Exception ex) {
					log.error("", ex);
					ex.printStackTrace();

				}
			});

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

}
