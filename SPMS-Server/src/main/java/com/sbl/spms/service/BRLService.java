package com.sbl.spms.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.model.BRL;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BRLService {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<BRL> getLabels() throws Exception{
		
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<BRL> brl = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			brl = session.createQuery("from BRL", BRL.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return brl;
    }

}
