package com.sbl.spms.service;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbl.spms.model.VendorServiceType;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VendorServiceTypeService {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	public String serviceIdByName(String serName) {
		Transaction transaction = null;
		Session session = null;
		String serviceId ="";

		VendorServiceType serviceType = new VendorServiceType();
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			serName = serName.toUpperCase();
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<VendorServiceType> query = builder.createQuery(VendorServiceType.class);
			Root<VendorServiceType> rootDetails = query.from(VendorServiceType.class);
			query.select(rootDetails)
				.where(builder.equal(builder.upper(rootDetails.get("serviceTypeName")), serName));

			Query<VendorServiceType> qResult = session.createQuery(query);
			try {
				serviceType = qResult.getSingleResult();
				serviceId = serviceType.getServiceTypeId();
				
			} catch (NoResultException nre) {
				serviceType = null;
				serviceId = serName;
			}

			transaction.commit();
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			if (session != null) {
		        session.close();
		    }
			return "";
			
		}
		finally {
			if (session != null) {
		        session.close();
		      }
		}
		
		return serviceId;
	}
	
	public List<VendorServiceType> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<VendorServiceType> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from VendorServiceType", VendorServiceType.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}
        return labels;
    }
}
