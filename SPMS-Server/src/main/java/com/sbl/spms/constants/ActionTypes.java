package com.sbl.spms.constants;

public enum ActionTypes {
	
	SELECT("SELECT"),
	SELECT_ALL("SELECT_ALL"),
	SAVE("SAVE"),
	SELECT_ALL_DATA("S"),
	INSERT("I"),
	UPDATE("UPDATE"),
	PASSUPDATE("PASSUPDATE"),
	RESET("RESET"),
	USER_LOGIN("USER_LOGIN"),
	DATA_UPLOAD("Data_Upload"),
	AUTH("AUTH"),
	

	QUICK_REGISTER("QUICK_REGISTER"),
	PENALTY_PROCESS("PENALTY_PROCESS"),
	SHOW_MENU("SHOW_MENU"),
	VENDOR_LIST("VENDOR_LIST"),
	BRANCH_LIST("BRANCH_LIST"),

	DELETE("DELETE");

	
	private final String value;

	ActionTypes(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

}
