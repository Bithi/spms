package com.sbl.spms.constants;

public enum StatusType {
	
	STATUS_OK("OK"),
	STATUS_ACTIVE("A"),
	STATUS_ERROR("error"),
	STATUS_SUCCESS("success"),
	STATUS_DUPLICATE("duplicate"),
	STATUS_CODE_200("200"),
	STATUS_CODE_404("404"),
	STATUS_CODE_500("500");
	
	private final String value;

	StatusType(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

}
