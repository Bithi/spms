package com.sbl.spms.util;

import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Data {

	private DataHeader dataHeader;
	private List<?> payLoad;
	private List<?> others;
	
	
}
