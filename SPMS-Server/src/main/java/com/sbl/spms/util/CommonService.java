package com.sbl.spms.util;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommonService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public <T> List<T> getDataList(T classObject, Map<String,Object> rectrictions) {
		
		log.info("Data Fetch Request for: {}",classObject.getClass());
		
		List<T>outputList = new ArrayList<T>();
		
		Transaction transaction = null;
		
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
			
			transaction = session.beginTransaction();
			
			CriteriaBuilder builder = session.getCriteriaBuilder();
			
			CriteriaQuery<T> criteriaQuery = (CriteriaQuery<T>) builder.createQuery(classObject.getClass());
			
			Root<T> root = (Root<T>)criteriaQuery.from(classObject.getClass());
			List<Predicate> expressionList = new ArrayList<>();
			
			if(rectrictions!=null) {
				for (Map.Entry<String, Object> entry : rectrictions.entrySet())
				{
					expressionList.add(builder.equal(root.get(entry.getKey()), entry.getValue()));
				}
			}
			
			
			criteriaQuery.select(root).where(expressionList.stream().toArray(Predicate[]::new));
			
			Query<T> query = session.createQuery(criteriaQuery);
			
			outputList = query.getResultList();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			session.close();
			
			if (transaction != null) {
				
				transaction.rollback();
			}
			
			return null;
			
		} finally {
			
			if (session != null) {
				
				if(session.getTransaction().isActive()) {
					
					session.getTransaction().commit();
				}
				session.close();
			}
		}

		return outputList;
	}
	public <T> List<T> mapResultSetToObjectList(Class<T> classz, Map<String,String> mapping, ResultSet rs) throws Exception{
		List<T>outputList = new ArrayList<T>();
		
		while(rs.next()) {
			T object = classz.getDeclaredConstructor().newInstance();
			for (Map.Entry<String,String> entry : mapping.entrySet()) {
				Field f =object.getClass().getDeclaredField(entry.getKey());
				f.setAccessible(true);
				
				try {
					if (f.getType().getName().equals("java.lang.Integer")) { 
						f.set(object,rs.getInt(entry.getValue()));
	                } 
					else if (f.getType().getName().equals("java.lang.Long")) {
	                    	f.set(object,rs.getLong(entry.getValue()));
	                }
					else if (f.getType().getName().equals("java.lang.Double")) {
                    	f.set(object,rs.getDouble(entry.getValue()));
                }
	                else  if (f.getType().getName().equals("java.lang.String")) { 
	                	f.set(object,rs.getString(entry.getValue()));
	                }
	                else  if (f.getType().getName().equals("java.sql.Date")) { 
	                	f.set(object,rs.getDate(entry.getValue()));
	                }else {
	                	f.set(object,rs.getString(entry.getValue()));
	                }
				}
				catch(Exception ex) {
					log.info(ex.getMessage());
				}
				
				
			}
			outputList.add(object);
		}
		
		return outputList;
	}
}
